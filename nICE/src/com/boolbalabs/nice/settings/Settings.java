/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.settings;

import android.content.SharedPreferences;

import java.util.ArrayList;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import com.boolbalabs.lib.utils.DebugLog;
import com.boolbalabs.lib.utils.Encryptor;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.gamecomponents.IcicleStatic;
import com.boolbalabs.nice.menu.BonusMenu;
import com.boolbalabs.nice.modes.GameMode;

public class Settings {

	public static int ROWS = 8;
    public static int COLUMNS = 8;
    private final String ENCRYPTION_SEED = "MH8734()9Akg&*3d";
    public static ArrayList<Integer> allowedKeyCodes;
    public static int currentLaunch = 1;
    public final static int LAUNCHES_BEFORE_SHOW_RATE_ME = 8;
    public static boolean rateMeDialogHasBeenShown = false;
    public static boolean isHelpBasicShouldShown = true;
    public static boolean isHelpPinguinShouldShown = true;
    public static boolean isHelpStoneShouldShown = true;
    public static boolean isHelpShakesShouldShown = true;
    public static boolean isHelpRepeatShouldShown = true;
    public static boolean isBonusShouldBeUpdated = true;
    public static boolean isHelpWelcomeBonusShouldBeShown = true;
    public static boolean isHelpClassicShouldShown = true;
    public static boolean isHelpQuickShouldShown = true;
    public static boolean isHelpPuzzleShouldShown = true;
    public static boolean isHelpTimeShouldShown = true;
    
    public static boolean isAdsEnabled = true;
    
    private Resources gameResources;
    //** device info **//
    /**
     * MDPI or android 2.1 are considered as slow
     */
    public static boolean isSlowDevice = false;

    /* other */
    private SharedPreferences sharedPreferences;
    public static Typeface MAIN_TYPEFACE;
    public static final String MARKET_URL = "market://details?id=com.boolbalabs.nice";
    public static int LAST_SCENE_ID = 1;
    /**
     * Singleton
     */
    private static Settings settings;
    private static boolean isInitialised = false;
    private static final Object isInitialisedLock = new Object();
    //** gameplay bundle **//
    public Bundle gameplayBundle;
    //** shakes **//
    public static final int SHAKES_INITIAL = 3;
    public static final int SHAKES_MAX = 3;
    public static int SHAKES_LEFT = 3;
    public static int SHAKE_SENSITIVITY = 50;
    
    public static Settings getInstance() {
        return settings;
    }

    public static void initialise(SharedPreferences sharedPreferences, Resources gameResources) {
        synchronized (isInitialisedLock) {
            if (isInitialised) {
                return;
            }
            settings = new Settings();
            settings.sharedPreferences = sharedPreferences;
            settings.gameResources = gameResources;
            MAIN_TYPEFACE = Typeface.createFromAsset(gameResources.getAssets(), "fonts/nicefont.ttf");
            settings.createAllowedKeys();
            isInitialised = true;
        }
    }

    public static void release() {
        synchronized (isInitialisedLock) {
            if (!isInitialised) {
                return;
            }
            if (settings != null) {
                settings = null;
            }
            isInitialised = false;
        }

    }

    private Settings() {
        gameplayBundle = new Bundle();
    }

    private void createAllowedKeys() {
        allowedKeyCodes = new ArrayList<Integer>();
        allowedKeyCodes.add(KeyEvent.KEYCODE_BACK);
        allowedKeyCodes.add(KeyEvent.KEYCODE_MENU);
        allowedKeyCodes.add(KeyEvent.KEYCODE_VOLUME_DOWN);
        allowedKeyCodes.add(KeyEvent.KEYCODE_VOLUME_UP);
    }

    public boolean isKeyAllowed(int key) {
        if (allowedKeyCodes != null) {
            return allowedKeyCodes.contains(key);
        }
        return false;
    }

    public void getDeviceInfo() {
        if (ScreenMetrics.resolution == ScreenMetrics.RESOLUTION_LOW || ScreenMetrics.resolution == ScreenMetrics.RESOLUTION_MEDIUM) {
            Settings.isSlowDevice = true;
            return;
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO) {
            Settings.isSlowDevice = true;
            return;
        }
        Settings.isSlowDevice = false;
    }

    public void saveSharedPreferences() {
        SharedPreferences.Editor ed = sharedPreferences.edit();
        // settings
        ed.putBoolean("SOUND_ENABLED", ZageCommonSettings.soundEnabled);
        ed.putBoolean("MUSIC_ENABLED", ZageCommonSettings.musicEnabled);
        ed.putInt("SHAKE_SENSITIVITY", Settings.SHAKE_SENSITIVITY);
        ed.putInt("LAST_SCENE_ID", LAST_SCENE_ID);
        ed.commit();
    }

    public void loadSharedPreferences() {
        // settings
        ZageCommonSettings.soundEnabled = sharedPreferences.getBoolean("SOUND_ENABLED", true);
        ZageCommonSettings.musicEnabled = sharedPreferences.getBoolean("MUSIC_ENABLED", true);
        Settings.SHAKE_SENSITIVITY= sharedPreferences.getInt("SHAKE_SENSITIVITY", 50);
    }

    public void loadOnResumePreferences() {
        LAST_SCENE_ID = sharedPreferences.getInt("LAST_SCENE_ID", 1);
    }
    
    public void saveBalanceData() {
    	PlayerProfile playerProfile = PlayerProfile.getInstance();
    	String mPrefix = "BALANCE" + "_";
        SharedPreferences.Editor ed = sharedPreferences.edit();
                
        saveIntEncrypted(ed, mPrefix + "MONEY", playerProfile.getCurrentBalance());
        saveIntEncrypted(ed, mPrefix + "BOMB", playerProfile.getBombUpgradeLevel());
        saveIntEncrypted(ed, mPrefix + "POWER_GEM", playerProfile.getPowerGemUpgradeLevel());
        saveIntEncrypted(ed, mPrefix + "COLOR_BOMB", playerProfile.getColorBombUpgradeLevel());
        saveIntEncrypted(ed, mPrefix + "YELLOW", playerProfile.getYellowUpgradeLevel());
        saveIntEncrypted(ed, mPrefix + "BLUE", playerProfile.getBlueUpgradeLevel());
        saveIntEncrypted(ed, mPrefix + "GREEN", playerProfile.getGreenUpgradeLevel());
        saveIntEncrypted(ed, mPrefix + "RED", playerProfile.getRedUpgradeLevel());
        saveIntEncrypted(ed, mPrefix + "WHITE", playerProfile.getWhiteUpgradeLevel());
        saveIntEncrypted(ed, mPrefix + "ORANGE", playerProfile.getOrangeUpgradeLevel());
        saveIntEncrypted(ed, mPrefix + "PURPLE", playerProfile.getPurpleUpgradeLevel());
        saveIntEncrypted(ed, mPrefix + "LEVEL_PUZZLE", playerProfile.currentPuzzleLevel);
        
        playerProfile.hasBeenPreviouslySaved = true;
        ed.putBoolean(mPrefix + "SAVED", playerProfile.hasBeenPreviouslySaved);
        ed.putBoolean(mPrefix + "HELP_BASIC", isHelpBasicShouldShown);
        ed.putBoolean(mPrefix + "HELP_PINGUIN", isHelpPinguinShouldShown);
        ed.putBoolean(mPrefix + "HELP_STONE", isHelpStoneShouldShown);
        ed.putBoolean(mPrefix + "HELP_SHAKES", isHelpShakesShouldShown);
        ed.putBoolean(mPrefix + "HELP_REPEAT", isHelpRepeatShouldShown);
        ed.putBoolean(mPrefix + "HELP_WELCOME_BONUS", isHelpWelcomeBonusShouldBeShown);
        ed.putBoolean(mPrefix + "HELP_CLASSIC", isHelpClassicShouldShown);
        ed.putBoolean(mPrefix + "HELP_TIME", isHelpTimeShouldShown);
        ed.putBoolean(mPrefix + "HELP_QUICK", isHelpQuickShouldShown);
        ed.putBoolean(mPrefix + "HELP_PUZZLE", isHelpPuzzleShouldShown);
        for(int i = 0; i < BonusMenu.isHelpShouldBeShown.length; i++) {
        	ed.putBoolean(mPrefix + "HELP_" + i, BonusMenu.isHelpShouldBeShown[i]);
        }
        
        ed.commit();
    }
    
    public void loadBalanceData() {
    	PlayerProfile playerProfile = PlayerProfile.getInstance();
    	String mPrefix = "BALANCE" + "_";
        playerProfile.hasBeenPreviouslySaved = sharedPreferences.getBoolean(mPrefix + "SAVED", false);

        if (playerProfile.hasBeenPreviouslySaved) {
        	playerProfile.setCurrentBalance(loadIntEncrypted(sharedPreferences, mPrefix + "MONEY"));
            playerProfile.setBombUpgradeLevel(loadIntEncrypted(sharedPreferences, mPrefix + "BOMB"));
            playerProfile.setPowerGemUpgradeLevel(loadIntEncrypted(sharedPreferences, mPrefix + "POWER_GEM"));
            playerProfile.setColorBombUpgradeLevel(loadIntEncrypted(sharedPreferences, mPrefix + "COLOR_BOMB"));
            playerProfile.setYellowUpgradeLevel(loadIntEncrypted(sharedPreferences, mPrefix + "YELLOW"));
            playerProfile.setBlueUpgradeLevel(loadIntEncrypted(sharedPreferences, mPrefix + "BLUE"));
            playerProfile.setGreenUpgradeLevel(loadIntEncrypted(sharedPreferences, mPrefix + "GREEN"));
            playerProfile.setRedUpgradeLevel(loadIntEncrypted(sharedPreferences, mPrefix + "RED"));
            playerProfile.setWhiteUpgradeLevel(loadIntEncrypted(sharedPreferences, mPrefix + "WHITE"));
            playerProfile.setOrangeUpgradeLevel(loadIntEncrypted(sharedPreferences, mPrefix + "ORANGE"));
            playerProfile.setPurpleUpgradeLevel(loadIntEncrypted(sharedPreferences, mPrefix + "PURPLE"));
            playerProfile.currentPuzzleLevel = loadIntEncrypted(sharedPreferences,  mPrefix + "LEVEL_PUZZLE");
            
            isHelpBasicShouldShown = sharedPreferences.getBoolean(mPrefix + "HELP_BASIC", true);
            isHelpPinguinShouldShown = sharedPreferences.getBoolean(mPrefix + "HELP_PINGUIN", true);
            isHelpStoneShouldShown = sharedPreferences.getBoolean(mPrefix + "HELP_STONE", true);
            isHelpShakesShouldShown = sharedPreferences.getBoolean(mPrefix + "HELP_SHAKES", true);
            isHelpRepeatShouldShown = sharedPreferences.getBoolean(mPrefix + "HELP_REPEAT", true);
            isHelpWelcomeBonusShouldBeShown = sharedPreferences.getBoolean(mPrefix + "HELP_WELCOME_BONUS", true);
            isHelpClassicShouldShown = sharedPreferences.getBoolean(mPrefix + "HELP_CLASSIC", true);
            isHelpQuickShouldShown = sharedPreferences.getBoolean(mPrefix + "HELP_QUICK", true);
            isHelpTimeShouldShown = sharedPreferences.getBoolean(mPrefix + "HELP_TIME", true);
            isHelpPuzzleShouldShown = sharedPreferences.getBoolean(mPrefix + "HELP_PUZZLE", true);
            for(int i = 0; i < BonusMenu.isHelpShouldBeShown.length; i++) {
            	BonusMenu.isHelpShouldBeShown[i] = sharedPreferences.getBoolean(mPrefix + "HELP_" + i, true);
            }
        }
    }
    
    public void saveAdsState() {
    	SharedPreferences.Editor ed = sharedPreferences.edit();
    	ed.putBoolean("ADS_STATE", isAdsEnabled);
    	ed.commit();
    }
    
    public void loadAdsState() {
    	isAdsEnabled = sharedPreferences.getBoolean("ADS_STATE", true);
    }

    public void saveGameplayData() {
        if (gameplayBundle == null) {
            return;
        }
        int modeId = gameplayBundle.getInt("MODE_ID", PlayerProfile.MODE_CLASSIC);
        SharedPreferences.Editor ed = sharedPreferences.edit();
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                String key = modeId + "POS" + String.valueOf(i) + String.valueOf(j);
                int value = gameplayBundle.getInt(key, IcicleStatic.highEntropyIndices[i][j]);
                saveIntEncrypted(ed, key, value);
            }
        }
        ed.commit();
    }

    public void loadGameplayData() {
        if (gameplayBundle == null) {
            gameplayBundle = new Bundle();
        }
        int modeId = PlayerProfile.getInstance().getCurrentModeId();
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                String key = modeId + "POS" + String.valueOf(i) + String.valueOf(j);
                int value = loadIntEncrypted(sharedPreferences, key);
                gameplayBundle.putInt(key, value);
            }
        }
    }

    public void saveMode(GameMode m) {
        String mPrefix = "MODE" + m.getId() + "_";
        SharedPreferences.Editor ed = sharedPreferences.edit();
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                String key = mPrefix + "POS" + String.valueOf(i) + String.valueOf(j);
                int value = m.icicleTypes[i][j];
                saveIntEncrypted(ed, key, value);
            }
        }
        
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                String key = mPrefix + "POW" + String.valueOf(i) + String.valueOf(j);
                int value = m.iciclePowers[i][j];
                saveIntEncrypted(ed, key, value);
            }
        }
        
        saveIntEncrypted(ed, mPrefix + "SCORE", m.currentScore);
        saveIntEncrypted(ed, mPrefix + "BONUS", m.currentBonus);
        saveIntEncrypted(ed, mPrefix + "ICICLES_REMOVED", m.iciclesRemoved);
        saveIntEncrypted(ed, mPrefix + "PROGRESS_UNITS", m.progressUnits);
        saveIntEncrypted(ed, mPrefix + "MAXCHAIN", m.maxChain);
        saveIntEncrypted(ed, mPrefix + "SECONDS_LEFT", m.getSecondsLeft());
        saveIntEncrypted(ed, mPrefix + "SHAKES_LEFT", Settings.SHAKES_LEFT);
        saveIntEncrypted(ed, mPrefix + "MAX_SCORE", m.maxScore);
        saveIntEncrypted(ed, mPrefix + "STONES_ON_FIELD", m.stonesOnField);
        saveIntEncrypted(ed, mPrefix + "STONES_MAX", m.stonesMax);
        saveIntEncrypted(ed, mPrefix + "REPEAT_BONUS", m.repeatBonus.lastUsedIcicle);
        m.hasBeenPreviouslySaved = true;
        ed.putBoolean(mPrefix + "SAVED", m.hasBeenPreviouslySaved);
       
        ed.commit();
    }

    public void loadMode(GameMode m) {
        String mPrefix = "MODE" + m.getId() + "_";
        m.hasBeenPreviouslySaved = sharedPreferences.getBoolean(mPrefix + "SAVED", false);
        m.setIsGameOver(false);
        
        int[][] indices = new int[ROWS][COLUMNS];
        
        if(m.getId() == PlayerProfile.MODE_PUZZLE) {
        	Settings.SHAKES_LEFT = 0;
        } else {
        
        	if (m.hasBeenPreviouslySaved) {
        		for (int i = 0; i < ROWS; i++) {
        			for (int j = 0; j < COLUMNS; j++) {
        				String key = mPrefix + "POS" + String.valueOf(i) + String.valueOf(j);
        				indices[i][j] = loadIntEncrypted(sharedPreferences, key);
        			}
        		}
        		
        		m.setIcicleTypes(indices);
        		
        		for (int i = 0; i < ROWS; i++) {
        			for (int j = 0; j < COLUMNS; j++) {
        				String key = mPrefix + "POW" + String.valueOf(i) + String.valueOf(j);
        				indices[i][j] = loadIntEncrypted(sharedPreferences, key);
        			}
        		}
        		
        		m.setIciclePowers(indices);
        		m.currentScore = loadIntEncrypted(sharedPreferences, mPrefix + "SCORE");
            	m.currentBonus = loadIntEncrypted(sharedPreferences, mPrefix + "BONUS");
            	m.iciclesRemoved = loadIntEncrypted(sharedPreferences, mPrefix + "ICICLES_REMOVED");
            	m.progressUnits = loadIntEncrypted(sharedPreferences, mPrefix + "PROGRESS_UNITS");
            	m.maxChain = loadIntEncrypted(sharedPreferences, mPrefix + "MAXCHAIN");
            	m.maxScore = loadIntEncrypted(sharedPreferences, mPrefix + "MAX_SCORE");
            	m.stonesOnField = loadIntEncrypted(sharedPreferences, mPrefix + "STONES_ON_FIELD");
            	m.stonesMax = loadIntEncrypted(sharedPreferences, mPrefix + "STONES_MAX");
            	m.repeatBonus.update(loadIntEncrypted(sharedPreferences, mPrefix + "REPEAT_BONUS"));
            	Settings.SHAKES_LEFT = loadIntEncrypted(sharedPreferences, mPrefix + "SHAKES_LEFT");
        	} else {
        		m.resetMode();
        	}
        }

    }

    public void setGameOver(GameMode m, boolean gameOver) {
        String mPrefix = "MODE" + m.getId() + "_";
        SharedPreferences.Editor ed = sharedPreferences.edit();
        ed.putBoolean(mPrefix + "GAME_OVER", gameOver);
        ed.commit();
    }

    public boolean wasGameOver(GameMode m) {
        String mPrefix = "MODE" + m.getId() + "_";
        return sharedPreferences.getBoolean(mPrefix + "GAME_OVER", true);
    }

    public Resources getResources() {
        return gameResources;
    }

    // ***************** SAVE/LOAD******************//
    private void saveIntEncrypted(SharedPreferences.Editor ed, String key, int value) {
        try {
            String valueEncrypted = Encryptor.encrypt(ENCRYPTION_SEED + key, Integer.toString(value));
            ed.putString(key, valueEncrypted);
        } catch (Exception e) {
            DebugLog.d(key, e.getLocalizedMessage());
        }
    }

    private void saveFloatEncrypted(SharedPreferences.Editor ed, String key, float value) {
        try {
            String valueEncrypted = Encryptor.encrypt(ENCRYPTION_SEED + key, Float.toString(value));
            ed.putString(key, valueEncrypted);
        } catch (Exception e) {
            DebugLog.d(key, e.getLocalizedMessage());
        }
    }

    private int loadIntEncrypted(SharedPreferences prefs, String key) {
        try {
            String valueEncrypted = prefs.getString(key, "NULL");
            String valueDecrypted = Encryptor.decrypt(ENCRYPTION_SEED + key, valueEncrypted);
            return Integer.valueOf(valueDecrypted);
        } catch (Exception e) {
            return 0;
        }
    }

    private float loadFloatEncrypted(SharedPreferences prefs, String key) {
        try {
            String valueEncrypted = prefs.getString(key, "NULL");
            String valueDecrypted = Encryptor.decrypt(ENCRYPTION_SEED + key, valueEncrypted);
            return Float.valueOf(valueDecrypted);
        } catch (Exception e) {
            return 0;
        }
    }
    
    public int loadIntEncrypted(String valueEncrypted, String key) {
        try {
            String valueDecrypted = Encryptor.decrypt(ENCRYPTION_SEED + key, valueEncrypted);
            return Integer.valueOf(valueDecrypted);
        } catch (Exception e) {
            return 0;
        }
    }
    
    private static final String PREF_PURCHASES_INITIALIZED = "PURCHASES_INITIALIZED";
    
    public boolean purchasesInitialized() {
		return sharedPreferences.getBoolean(PREF_PURCHASES_INITIALIZED, false);
	}
    
	public void savePurchasesInitialized() {
		SharedPreferences.Editor ed = sharedPreferences.edit();
		ed.putBoolean(PREF_PURCHASES_INITIALIZED, true);
		ed.commit();
	}
}
