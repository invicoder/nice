/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.settings;

import java.util.Random;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class StaticConstants {

    public static final String TAG = "nICE";
    //**** ACTIONS **///
    public static final int ACTION_STAR_GLITTER = 10123;
    public static final int ACTION_STAR_GLITTER_STOP = 10124;
    public static final int ACTION_RESTART_SCENE_GAMEPLAY = 11071;
    public static final int ACTION_SHOW_HINT = 11145;
    public static final int ACTION_HINT_NOT_AVAILABLE = 11146;
    public static final int ACTION_SHOW_WATER = 33781;
    public static final int ACTION_HIDE_WATER = 33789;
    // messages to main activity handler, should be random integers
    public static final int MESSAGE_SHOW_MENU = 97866;
    public static final int MESSAGE_SHOW_SETTINGS = 89331;
    public static final int MESSAGE_GAME_OVER = 13896;
    public static final int MESSAGE_EXIT_APPLICATION = 87996;
    //other
    public static final int MESSAGE_SHOW_DIALOG_CRASH = 12325;
    public static final int MESSAGE_SHOW_DIALOG_RATEME = 12329;
    public static final int MESSAGE_DIALOG_HELP = 14329;
    public static final int MESSAGE_SUBMIT_SCORELOOP = 15678;
    public static final int MESSAGE_SHOW_SCORES = 15569;
    
    public static final int MESSAGE_LEVEL_UP = 17045;
    public static final int MESSAGE_CONGRATULATIONS = 17231;
    public static final int MESSAGE_CONTINUE = 17841;
    public static final int MESSAGE_HELP_PUZZLE = 17884;
    public static final int MESSAGE_HELP_BONUS = 17542;
    public static final int MESSAGE_REPEAT_BONUS = 17481;
    public static final int MESSAGE_ADD_MONEY = 17145;
    public static final int MESSAGE_HELP_BASIC = 93145;
    public static final int MESSAGE_HELP_PINGUIN = 93146;
    public static final int MESSAGE_HELP_STONE = 93443;
    public static final int MESSAGE_HELP_SHAKES = 93111;
    public static final int MESSAGE_HELP_WELCOME_BONUS = 93893;
    public static final int MESSAGE_HELP_MODE = 74541;
        
    public static Random rand = new Random();
}
