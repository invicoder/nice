/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.settings;
/**
 * 
 * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
 */

public class BonusUpgradeConstants {
	
	private final static String mPrefix = "BONUS_COST" + "_";
	
	//BONUSES WITH UPGRADE LEVELS
	public static int[] moneyForBombUpgradeLevel = {
		10,
		100,
		1000
	};
	
	public static int[] moneyForPowerGemUpgradeLevel = {
		20,
		200,
		700,
		1500
	};

	//BONUSES WITHOUT UPGRADE LEVELS
	public static int moneyForColorBombUpgrade = 500;
	
	public static int moneyForYellowUpgrade = 500;
	public static int moneyForBlueUpgrade = 200;
	public static int moneyForRedUpgrade = 1000;
	public static int moneyForGreenUpgrade = 50;
	public static int moneyForPurpleUpgrade = 100;
	public static int moneyForOrangeUpgrade = 50;
	public static int moneyForWhiteUpgrade = 1000;
	
	public static final int x1AddMoney = 10; //$0.99
	public static final int x2AddMoney = 50; //$1.99
	public static final int x3AddMoney = 100; //$2.99
	public static final int x4AddMoney = 1000; //$9.99
	
	public static final int x1id = 0;
	public static final int x2id = 1;
	public static final int x3id = 2;
	public static final int x4id = 3;
	
	
	
}
