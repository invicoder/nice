/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.level;

import com.boolbalabs.nice.settings.Settings;
/**
 * 
 * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
 */

public class GameLevel {

	private int[][] levelTypes = new int[Settings.ROWS][Settings.COLUMNS];
	private int[][] levelPowers = new int[Settings.ROWS][Settings.COLUMNS];
	
	public int levelId;
	
	public GameLevel(int[][] levelTypes, int[][] levelPowers, int id) {
		levelId = id;
		
		for (int i = 0; i < Settings.ROWS; i++) {
			for (int j = 0; j < Settings.COLUMNS; j++) {
				this.levelTypes[i][j] = levelTypes[i][j];
				this.levelPowers[i][j] = levelPowers[i][j];
			}
		}
	}
	
	public int[][] getLevelTypes() {
		return levelTypes;
	}
	
	public int[][] getLevelPowers() {
		return levelPowers;
	}
	
}
