package com.boolbalabs.nice.level;

import java.util.ArrayList;

import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.utils.ZLog;

public class GameLevelFactory {

	protected static GameLevelFactory gameLevelFactory;
	protected static boolean isInitialised = false;
	protected static final Object isInitialisedLock = new Object();	
	
	protected ArrayList<GameLevel> allLevels;
	protected int numberOfLevels;
	
	public static GameLevelFactory getInstance() {
		return gameLevelFactory;
	}
	
    public static void initialize() { //in NiceGame()
        synchronized (isInitialisedLock) {
            if (isInitialised) {
                return;
            }
            if(ScreenMetrics.resolution_postfix.equals("hres") || ScreenMetrics.resolution_postfix.equals("xhres")) {
            	ZLog.i("level xml loading");
            	gameLevelFactory = new GameLevelFactoryXML();
            } else {
            	ZLog.i("level txt loading");
            	gameLevelFactory = new GameLevelFactoryTXT();
            }
            
			isInitialised = true;
        }
    }
	
	public GameLevel getCurrentLevel() {
		return allLevels.get(PlayerProfile.getInstance().currentPuzzleLevel);
	}
	
	public int getNumberOfLastLevel() {
		return allLevels.size()-1;
	}
	
}
