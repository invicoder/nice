/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.level;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.gamecomponents.IcicleStatic;
import com.boolbalabs.nice.settings.Settings;
/**
 * 
 * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
 */

public class GameLevelFactoryTXT extends GameLevelFactory{
	
	private final static int LEVEL_SIZE_BYTE = 80;
	
	private InputStream inputTypes;
	private InputStream inputPowers;
	
	private byte[] byteArrayTypes = new byte[LEVEL_SIZE_BYTE];
	private byte[] byteArrayPowers = new byte[LEVEL_SIZE_BYTE];
	
	private int[][] levelTypes = new int[Settings.ROWS][Settings.COLUMNS];
	private int[][] levelPowers = new int[Settings.ROWS][Settings.COLUMNS];
	
	public GameLevelFactoryTXT() {
		
		allLevels = new ArrayList<GameLevel>();
		numberOfLevels = 0;
		inputTypes = Gdx.app.getFiles().internal("levels/levels_types.txt").read();
		inputPowers = Gdx.app.getFiles().internal("levels/levels_powers.txt").read();
		
		initAllLevels();
	}
	
	private void initAllLevels() {
		try {
			while(inputTypes.read(byteArrayTypes) != -1 && inputPowers.read(byteArrayPowers) != -1) {

				String levelStringTypes = new String(byteArrayTypes);
				String levelStringPowers = new String(byteArrayPowers);

				String dataTypes = "";
				String dataPowers = "";
				
				for(int i = 0; i < Settings.ROWS; i++) {
					dataTypes += levelStringTypes.substring(10*i+0, 10*i+8);
				}
				
				for(int i = 0; i < Settings.ROWS; i++) {
					dataPowers += levelStringPowers.substring(10*i+0, 10*i+8);
				}
				
				GameLevel level  = getLevel(dataTypes, dataPowers);

				allLevels.add(level);
				
				inputTypes.read(new byte[2]);
				inputPowers.read(new byte[2]);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private GameLevel getLevel(String dataTypes, String dataPowers) {
		int rows = Settings.ROWS;
		int columns = Settings.COLUMNS;
		
		byte[][] tempLevelTypes = new byte[rows][columns];
		byte[][] tempLevelPowers = new byte[rows][columns];
		
		int tempXIndex = 0;
		int tempYIndex = 0;
		
		for (int i = 0; i < dataTypes.length(); i++) {
			if (dataTypes.charAt(i) >= 48 && dataTypes.charAt(i) <= 54) {
				tempLevelTypes[tempXIndex][tempYIndex] = (byte) (dataTypes.charAt(i) - 48);
				tempYIndex++;
				
			} else if (dataTypes.charAt(i) == 45) // 45 means "-"
			{
				tempLevelTypes[tempXIndex][tempYIndex] = (byte) IcicleStatic.EMPTY_CELL;
				tempYIndex++;
			}
			if (tempYIndex == columns) {
				tempXIndex++;
				tempYIndex = 0;
				if (tempXIndex == rows) {
					tempXIndex = 0;
					tempYIndex = 0;
				}
			}
		}
		
		for (int i = 0; i < dataPowers.length(); i++) {
			if (dataPowers.charAt(i) >= 49 && dataPowers.charAt(i) <= 51) {
				if(tempLevelTypes[tempXIndex][tempYIndex] == IcicleStatic.EMPTY_CELL) {
					tempLevelPowers[tempXIndex][tempYIndex] = (byte) 0;
				} else {
					tempLevelPowers[tempXIndex][tempYIndex] = (byte) (dataPowers.charAt(i) - 48);
				}	
				tempYIndex++;
				
			} else if (dataPowers.charAt(i) == 45) // 45 means "-"
			{
				tempLevelPowers[tempXIndex][tempYIndex] = (byte) 0;
				tempYIndex++;
			}
			if (tempYIndex == columns) {
				tempXIndex++;
				tempYIndex = 0;
				if (tempXIndex == rows) {
					numberOfLevels++;
					
					for(int k = 0; k < Settings.ROWS; k++) {
						for(int j = 0; j < Settings.COLUMNS; j++) {
							levelTypes[k][j] = (int) tempLevelTypes[k][j];
							levelPowers[k][j] = (int) tempLevelPowers[k][j] + 100;
						}
					}
					
					return (new GameLevel(levelTypes, levelPowers, numberOfLevels));
				}
			}
		}
		return (new GameLevel(levelTypes, levelPowers, allLevels.size()));
	}
	
	public GameLevel getCurrentLevel() {
		return allLevels.get(PlayerProfile.getInstance().currentPuzzleLevel);
	}
	
	public int getNumberOfLastLevel() {
		return allLevels.size()-1;
	}
	
}
