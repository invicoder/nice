/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.level;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.badlogic.gdx.Gdx;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.settings.Settings;
/**
 * 
 * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
 */

public class GameLevelFactoryXML extends GameLevelFactory{	
	
	private InputStream input;
	
	private int[][] icicleType;
	private int[][] iciclePower;

	private int row;
	private int column;
	private int type;
	private int power;

	public GameLevelFactoryXML() {
		
		allLevels = new ArrayList<GameLevel>();
		
		numberOfLevels = 0;
		input = Gdx.app.getFiles().internal("levels/levels.xml").read();
		
		icicleType = new int[Settings.ROWS][Settings.COLUMNS];	
    	iciclePower = new int[Settings.ROWS][Settings.COLUMNS];
    	
		initAllLevels();
	}
	
	private static int getTypeByName(String name) {
		if(name.equals("yellow")) {
			return 1;
		} else if(name.equals("red")) {
			return 3;
		} else if(name.equals("white")) {
			return 5;
		} else if(name.equals("green")) {
			return 6;
		} else if(name.equals("blue")) {
			return 0;
		} else if(name.equals("purple")) {
			return 2;
		} else if(name.equals("orange")) {
			return 4;
		} else if(name.equals("stone")) {
			return 8;
		} else if(name.equals("empty")) {
			return 7;
		} else {
			return -1;
		}
	}
	
	private static int getPowerByName(String name) {
		if(name.equals("bomb")) {
			return 101;
		} else if(name.equals("light")) {
			return 102;
		} else if(name.equals("color")) {
			return 103;
		} else if(name.equals("none")) {
			return 100;
		} else {
			return -1;
		}
	}
	
	private void initAllLevels() {
	
		try {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(input);
		doc.getDocumentElement().normalize();
    	
		NodeList nList = doc.getElementsByTagName("level");
    	
		for (int temp = 0; temp < nList.getLength(); temp++) {
			 
			   Node nNode = nList.item(temp);
			   if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	 
			      Element eElement = (Element) nNode;
			      NodeList nlList = eElement.getElementsByTagName("icicle");
			      
			      for(int i = 0; i < nlList.getLength(); i++) {
			    	   NamedNodeMap attributes = nlList.item(i).getAttributes();
			    	   
			    	   for(int j = 0; j < attributes.getLength(); j++) {
			    		   Attr attribute = (Attr)attributes.item(j);
			    		   
			    		   if(attribute.getName().equals("row")) {
			    			   row = Integer.parseInt(attribute.getValue());
			    		   } else if(attribute.getName().equals("column")) {
			    			   column = Integer.parseInt(attribute.getValue());
			    		   } else if(attribute.getName().equals("type")) {
			    			   type = getTypeByName(attribute.getValue());
			    		   } else if(attribute.getName().equals("power")) {
			    			   power = getPowerByName(attribute.getValue());
			    		   }				    		   
			    	   }
			    	   
			    	   icicleType[row][column] = type;
			    	   iciclePower[row][column] = power;
			      }

	 
			  }
			   GameLevel level = new GameLevel(icicleType, iciclePower, numberOfLevels);
			   allLevels.add(level);
			   
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public GameLevel getCurrentLevel() {
		return allLevels.get(PlayerProfile.getInstance().currentPuzzleLevel);
	}
	
	@Override
	public int getNumberOfLastLevel() {
		return allLevels.size()-1;
	}
	
}
