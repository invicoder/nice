/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.menu;

import java.util.ArrayList;

import android.graphics.PointF;

import com.badlogic.gdx.math.Rectangle;
import com.boolbalabs.lib.elements.ZButton;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.graphics.ZNumberView;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.nice.R;
import com.boolbalabs.nice.SceneBonus;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.extra.SoundService;
import com.boolbalabs.nice.settings.BonusUpgradeConstants;
import com.boolbalabs.nice.settings.Settings;
import com.boolbalabs.nice.settings.StaticConstants;
import com.boolbalabs.nice.utils.ZLog;
/**
 * 
 * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
 * Bonus Menu consists of:
 * player money (balanceView)
 * addMoneyButton (addMoney)
 * buyButton (buy Button)
 * buttons for each bonus.
 * Each bonus should have: Rectangle, CostAndCoin, ZButton, id
 * methods: setButtonRegionsAvailable/Unavailable, update, upgrade, bonusButtonClick.
 */

public class BonusMenu extends ZNode{
	
	private static final int BOMB = 1;
	private static final int POWER_GEM = 2;
	private static final int COLOR_BOMB = 3;
	private static final int YELLOW = 4;
	private static final int BLUE = 5;
	private static final int GREEN = 6;
	private static final int RED = 7;
	private static final int ORANGE = 8;
	private static final int PURPLE = 9;
	private static final int WHITE = 0;
	
	private static final int top = ScreenMetrics.screenHeightRip;
	
	private static int upgradedBonus = -1;
	
    private final String lightBlueDigitsFramenames[] = {"silverdigits00d", "silverdigits01d", "silverdigits02d",
            "silverdigits03d", "silverdigits04d", "silverdigits05d", "silverdigits06d",
            "silverdigits07d", "silverdigits08d", "silverdigits09d"};
	
    public static final int[] bonusHelpCaptionId = {
    	R.string.caption_white,
		R.string.caption_bomb,
		R.string.caption_lightning,
		R.string.caption_color_bomb,
		R.string.caption_yellow,
		R.string.caption_blue,
		R.string.caption_green,
		R.string.caption_red,
		R.string.caption_orange,
		R.string.caption_purple
    };
    
    public static final int[] bonusHelpImageId = {
    	R.drawable.white,
    	R.drawable.bonus_bomb,
    	R.drawable.bonus_lightning,
    	R.drawable.bonus_color_bomb,
    	R.drawable.yellow,
    	R.drawable.blue,
    	R.drawable.green,
    	R.drawable.red,
    	R.drawable.orange,
    	R.drawable.purple
    };
    
	public static final int[] bonusHelpTextId = {
		R.string.bonus_white,
		R.string.bonus_bomb,
		R.string.bonus_lightning,
		R.string.bonus_color_bomb,
		R.string.bonus_yellow,
		R.string.bonus_blue,
		R.string.bonus_green,
		R.string.bonus_red,
		R.string.bonus_orange,
		R.string.bonus_purple
	};
	
	public static boolean[] isHelpShouldBeShown = {
		true, true,	true, true,	true, true,	true, true,	true, true
	};
	
	boolean isAddMoneyDialogShouldShown = false;
		
	public static final String NAME = "bonus_menu";
    private TexturesManager texturesManager;
    private PlayerProfile playerProfile;
    private SceneBonus sceneBonus;
    private Rectangle screenRectRip = new Rectangle(0, 0, ScreenMetrics.screenWidthRip, ScreenMetrics.screenHeightRip);
    
    private ArrayList<ZButton> bonusButtons;
    
    private ZButton bombBonusButton;
    private ZButton powerGemBonusButton;
    private ZButton colorBombBonusButton;
    private ZButton yellowBonusButton;
    private ZButton blueBonusButton;
    private ZButton whiteBonusButton;
    private ZButton greenBonusButton;
    private ZButton orangeBonusButton;
    private ZButton purpleBonusButton;
    private ZButton redBonusButton;
    
    private ZButton buyButton;
    
    private ZButton.ClickListener clickListener;
    
    private ZNode selectView;
    
    private ZNode bombBought;
    private ZNode powerGemBought;
    private ZNode colorBombBought;
    private ZNode yellowBought;
    private ZNode blueBought;
    private ZNode greenBought;
    private ZNode orangeBought;
    private ZNode whiteBought;
    private ZNode purpleBought;
    private ZNode redBought;
        
    private Rectangle tempRect = new Rectangle(0, 0, 1, 1);
    private Rectangle bombBonusRect = new Rectangle(13, top - 203, 67, 73);
    private Rectangle powerGemBonusRect = new Rectangle(120, top - 203, 67, 73);
    private Rectangle colorBombBonusRect = new Rectangle(230, top - 203, 67, 73);
    private Rectangle whiteBonusRect = new Rectangle(13, top - 283, 67, 73);
    private Rectangle yellowBonusRect = new Rectangle(120, top - 283, 67, 73);
    private Rectangle blueBonusRect = new Rectangle(230, top - 283, 67, 73);
    private Rectangle greenBonusRect = new Rectangle(13, top - 366, 67, 73);
    private Rectangle redBonusRect = new Rectangle(120, top - 366, 67, 73);
    private Rectangle purpleBonusRect = new Rectangle(230, top - 366, 67, 73);
    private Rectangle orangeBonusRect = new Rectangle(13, top - 446, 67, 73);
    
    private Rectangle buyButtonRect = new Rectangle(167, top - 466, 142, 79);
    
    private CostAndCoin bombNumberView;
    private CostAndCoin powerGemNumberView;
    private CostAndCoin colorBombNumberView;
    private CostAndCoin yellowNumberView;
    private CostAndCoin blueNumberView;
    private CostAndCoin whiteNumberView;
    private CostAndCoin greenNumberView;
    private CostAndCoin orangeNumberView;
    private CostAndCoin purpleNumberView;
    private CostAndCoin redNumberView;
    
    private PointF bombPosition = new PointF(13 + 67, top - 203);
    private PointF powerGemPosition = new PointF(120 + 67, top - 203);
    private PointF colorBombPosition = new PointF(230 + 67, top - 203);
    private PointF yellowPosition = new PointF(120 + 67, top - 283);
    private PointF bluePosition = new PointF(230 + 67, top - 283);
    private PointF whitePosition = new PointF(13 + 67, top - 283);
    private PointF greenPosition = new PointF(13 + 67, top - 366);
    private PointF orangePosition = new PointF(13 + 67, top - 456);
    private PointF purplePosition = new PointF(230 + 67, top - 366);
    private PointF redPosition = new PointF(120 + 67, top - 366);
    
    private BonusMenuNumberView balance;
    private AddMoney addMoney;

	public BonusMenu(SceneBonus sceneBonus) {
		super(NAME);
        this.sceneBonus = sceneBonus;
        parentScene = sceneBonus;
        sceneBonus.addActor(this);
        texturesManager = TexturesManager.getInstance();
        playerProfile = PlayerProfile.getInstance();
        bonusButtons = new ArrayList<ZButton>();
        setScreenFrame(screenRectRip);
        setRegion();
        
        createSelectView();
        createBoughtNodes();
        createButtons();
        addBoughtNodes();
        createCostNumberViews();
        createBalanceView();
	}
	
	private void setRegion() {
        region.setRegion(texturesManager.getRegionByName("bonus_bg"));
        region.setRegion(region.getRegionX(), region.getRegionY(),
                region.getRegionWidth(), Math.min(800, ScreenMetrics.getHeightProportionalToWidth(region.getRegionWidth())));
    }
	
	private void createSelectView() {
		selectView = new ZNode("selectView");
        selectView.initWithFrame(tempRect, texturesManager.getRegionByName("select"));
        selectView.visible = false;
	}
	
	private void createBoughtNodes() {
		bombBought = new ZNode("bomb_bought");
		bombBought.initWithFrame(tempRect, texturesManager.getRegionByName("bought"));
		bombBought.visible = false;
		
		powerGemBought = new ZNode("power_gem_bought");
		powerGemBought.initWithFrame(tempRect, texturesManager.getRegionByName("bought"));
		powerGemBought.visible = false;
		
		colorBombBought = new ZNode("color_bomb_bought");
		colorBombBought.initWithFrame(tempRect, texturesManager.getRegionByName("bought"));
		colorBombBought.visible = false;
		
		yellowBought = new ZNode("yellow_bought");
		yellowBought.initWithFrame(tempRect, texturesManager.getRegionByName("bought"));
		yellowBought.visible = false;
		
		blueBought = new ZNode("bomb_bought");
		blueBought.initWithFrame(tempRect, texturesManager.getRegionByName("bought"));
		blueBought.visible = false;
		
		redBought = new ZNode("bomb_bought");
		redBought.initWithFrame(tempRect, texturesManager.getRegionByName("bought"));
		redBought.visible = false;
		
		whiteBought = new ZNode("bomb_bought");
		whiteBought.initWithFrame(tempRect, texturesManager.getRegionByName("bought"));
		whiteBought.visible = false;
		
		orangeBought = new ZNode("bomb_bought");
		orangeBought.initWithFrame(tempRect, texturesManager.getRegionByName("bought"));
		orangeBought.visible = false;
		
		purpleBought = new ZNode("bomb_bought");
		purpleBought.initWithFrame(tempRect, texturesManager.getRegionByName("bought"));
		purpleBought.visible = false;
		
		greenBought = new ZNode("bomb_bought");
		greenBought.initWithFrame(tempRect, texturesManager.getRegionByName("bought"));
		greenBought.visible = false;
		
	}
	
	private void createButtons() {
        clickListener = new ZButton.ClickListener() {

            public void clicked(ZButton button) {
            	SoundService.playButtonClick();
                if (button == bombBonusButton) {
                	bombBonusButtonClick();
                } else if (button == powerGemBonusButton) {
                	powerGemBonusButtonClick();
                } else if (button == colorBombBonusButton) {
                	colorBombBonusButtonClick();
                } else if (button == yellowBonusButton) {
                	yellowBonusButtonClick();
                } else if (button == redBonusButton) {
                	redBonusButtonClick();
                } else if (button == greenBonusButton) {
                	greenBonusButtonClick();
                } else if (button == blueBonusButton) {
                	blueBonusButtonClick();
                } else if (button == whiteBonusButton) {
                	whiteBonusButtonClick();
                } else if (button == orangeBonusButton) {
                	orangeBonusButtonClick();
                } else if (button == purpleBonusButton) {
                	purpleBonusButtonClick();
                } else if(button == buyButton) {
                	buyButtonClick();
                }
            }

            public void touchDown(ZButton button) {
            }
        };

        bombBonusButton = new ZButton("bombBonusButton", texturesManager.getRegionByName("bomb_3_cant"));
        updateButton(bombBonusButton);
        bombBonusButton.setScreenFrame(bombBonusRect);
        bombBonusButton.clickListener = clickListener;
        
        powerGemBonusButton = new ZButton("powerGemBonusButton", texturesManager.getRegionByName("power_gem_4_cant"));
        updateButton(powerGemBonusButton);
        powerGemBonusButton.setScreenFrame(powerGemBonusRect);
        powerGemBonusButton.clickListener = clickListener;

        colorBombBonusButton = new ZButton("colorBombBonusButton", texturesManager.getRegionByName("color_bomb_cant"));
        updateButton(colorBombBonusButton);
        colorBombBonusButton.setScreenFrame(colorBombBonusRect);
        colorBombBonusButton.clickListener = clickListener;
        
        yellowBonusButton = new ZButton("yellowBonusButton", texturesManager.getRegionByName("yellow_cant"));
        updateButton(yellowBonusButton);
        yellowBonusButton.setScreenFrame(yellowBonusRect);
        yellowBonusButton.clickListener = clickListener;
        
        blueBonusButton = new ZButton("blueBonusButton", texturesManager.getRegionByName("blue_cant"));
        updateButton(blueBonusButton);
        blueBonusButton.setScreenFrame(blueBonusRect);
        blueBonusButton.clickListener = clickListener;
        
        redBonusButton = new ZButton("redBonusButton", texturesManager.getRegionByName("red_cant"));
        updateButton(redBonusButton);
        redBonusButton.setScreenFrame(redBonusRect);
        redBonusButton.clickListener = clickListener;
        
        greenBonusButton = new ZButton("greenBonusButton", texturesManager.getRegionByName("green_cant"));
        updateButton(greenBonusButton);
        greenBonusButton.setScreenFrame(greenBonusRect);
        greenBonusButton.clickListener = clickListener;
        
        whiteBonusButton = new ZButton("whiteBonusButton", texturesManager.getRegionByName("white_cant"));
        updateButton(whiteBonusButton);
        whiteBonusButton.setScreenFrame(whiteBonusRect);
        whiteBonusButton.clickListener = clickListener;
        
        orangeBonusButton = new ZButton("orangeBonusButton", texturesManager.getRegionByName("orange_cant"));
        updateButton(orangeBonusButton);
        orangeBonusButton.setScreenFrame(orangeBonusRect);
        orangeBonusButton.clickListener = clickListener;
        
        purpleBonusButton = new ZButton("purpleBonusButton", texturesManager.getRegionByName("purple_cant"));
        updateButton(purpleBonusButton);
        purpleBonusButton.setScreenFrame(purpleBonusRect);
        purpleBonusButton.clickListener = clickListener;
        
        buyButton = new ZButton("buyButton", texturesManager.getRegionByName("buy"), 
        		texturesManager.getRegionByName("buy_pressed"));
        buyButton.setScreenFrame(buyButtonRect);
        buyButton.clickListener = clickListener;
 
        parentScene.addActor(bombBonusButton);
        parentScene.addActor(powerGemBonusButton);
        parentScene.addActor(colorBombBonusButton);
        parentScene.addActor(yellowBonusButton);
        parentScene.addActor(blueBonusButton);
        parentScene.addActor(redBonusButton);
        parentScene.addActor(greenBonusButton);
        parentScene.addActor(whiteBonusButton);
        parentScene.addActor(orangeBonusButton);
        parentScene.addActor(purpleBonusButton);
        parentScene.addActor(buyButton);
        
        bonusButtons.add(bombBonusButton);
        bonusButtons.add(powerGemBonusButton);
        bonusButtons.add(colorBombBonusButton);
        bonusButtons.add(yellowBonusButton);
        bonusButtons.add(blueBonusButton);
        bonusButtons.add(redBonusButton);
        bonusButtons.add(greenBonusButton);
        bonusButtons.add(whiteBonusButton);
        bonusButtons.add(orangeBonusButton);
        bonusButtons.add(purpleBonusButton);
        
    }
	
	private void addBoughtNodes() {
		sceneBonus.addActor(bombBought);
		sceneBonus.addActor(powerGemBought);
		sceneBonus.addActor(colorBombBought);
		sceneBonus.addActor(yellowBought);
		sceneBonus.addActor(purpleBought);
		sceneBonus.addActor(blueBought);
		sceneBonus.addActor(redBought);
		sceneBonus.addActor(greenBought);
		sceneBonus.addActor(whiteBought);
		sceneBonus.addActor(orangeBought);
        sceneBonus.addActor(selectView);
	}
		
	private void createCostNumberViews() {
		bombNumberView = new CostAndCoin("bombNumberView");
		bombNumberView.initNumberView(bombPosition, lightBlueDigitsFramenames, ZNumberView.ALIGN_RIGHT, 16);
		bombNumberView.drawingMode = ZNumberView.NumberDrawingMode.NDISCRETE;
		if(playerProfile.isBombUpgradeAvailable()) {
			bombNumberView.setNumberToDraw(BonusUpgradeConstants.moneyForBombUpgradeLevel[playerProfile.getBombUpgradeLevel()]);
		} else {
			bombNumberView.visible = false;
			bombNumberView.setCoinVisibility(false);
		}
		
		powerGemNumberView = new CostAndCoin("powerGemNumberView");
		powerGemNumberView.initNumberView(powerGemPosition, lightBlueDigitsFramenames, ZNumberView.ALIGN_RIGHT, 16);
		powerGemNumberView.drawingMode = ZNumberView.NumberDrawingMode.NDISCRETE;
		if(playerProfile.isPowerGemUpgradeAvailable()) {
			powerGemNumberView.setNumberToDraw(BonusUpgradeConstants.moneyForPowerGemUpgradeLevel[playerProfile.getPowerGemUpgradeLevel()]);
		} else {
			powerGemNumberView.visible = false;
			powerGemNumberView.setCoinVisibility(false);
		}
		
		colorBombNumberView = new CostAndCoin("colorBombNumberView");
		colorBombNumberView.initNumberView(colorBombPosition, lightBlueDigitsFramenames, ZNumberView.ALIGN_RIGHT, 16);
		colorBombNumberView.drawingMode = ZNumberView.NumberDrawingMode.NDISCRETE;
		colorBombNumberView.setNumberToDraw(BonusUpgradeConstants.moneyForColorBombUpgrade);
		if(!playerProfile.isColorBombUpgradeAvailable()) {
			colorBombNumberView.visible = false;
			colorBombNumberView.setCoinVisibility(false);
		}
		
		yellowNumberView = new CostAndCoin("yellowNumberView");
		yellowNumberView.initNumberView(yellowPosition, lightBlueDigitsFramenames, ZNumberView.ALIGN_RIGHT, 16);
		yellowNumberView.drawingMode = ZNumberView.NumberDrawingMode.NDISCRETE;
		yellowNumberView.setNumberToDraw(BonusUpgradeConstants.moneyForYellowUpgrade);
		if(!playerProfile.isYellowUpgradeAvailable()) {
			yellowNumberView.visible = false;
			yellowNumberView.setCoinVisibility(false);
		}
		
		blueNumberView = new CostAndCoin("blueNumberView");
		blueNumberView.initNumberView(bluePosition, lightBlueDigitsFramenames, ZNumberView.ALIGN_RIGHT, 16);
		blueNumberView.drawingMode = ZNumberView.NumberDrawingMode.NDISCRETE;
		blueNumberView.setNumberToDraw(BonusUpgradeConstants.moneyForBlueUpgrade);
		if(!playerProfile.isBlueUpgradeAvailable()) {
			blueNumberView.visible = false;
			blueNumberView.setCoinVisibility(false);
		}
		
		whiteNumberView = new CostAndCoin("whiteNumberView");
		whiteNumberView.initNumberView(whitePosition, lightBlueDigitsFramenames, ZNumberView.ALIGN_RIGHT, 16);
		whiteNumberView.drawingMode = ZNumberView.NumberDrawingMode.NDISCRETE;
		whiteNumberView.setNumberToDraw(BonusUpgradeConstants.moneyForWhiteUpgrade);
		if(!playerProfile.isWhiteUpgradeAvailable()) {
			whiteNumberView.visible = false;
			whiteNumberView.setCoinVisibility(false);
		}
		
		greenNumberView = new CostAndCoin("greenNumberView");
		greenNumberView.initNumberView(greenPosition, lightBlueDigitsFramenames, ZNumberView.ALIGN_RIGHT, 16);
		greenNumberView.drawingMode = ZNumberView.NumberDrawingMode.NDISCRETE;
		greenNumberView.setNumberToDraw(BonusUpgradeConstants.moneyForGreenUpgrade);
		if(!playerProfile.isGreenUpgradeAvailable()) {
			greenNumberView.visible = false;
			greenNumberView.setCoinVisibility(false);
		}
		
		orangeNumberView = new CostAndCoin("orangeNumberView");
		orangeNumberView.initNumberView(orangePosition, lightBlueDigitsFramenames, ZNumberView.ALIGN_RIGHT, 16);
		orangeNumberView.drawingMode = ZNumberView.NumberDrawingMode.NDISCRETE;
		orangeNumberView.setNumberToDraw(BonusUpgradeConstants.moneyForOrangeUpgrade);
		if(!playerProfile.isOrangeUpgradeAvailable()) {
			orangeNumberView.visible = false;
			orangeNumberView.setCoinVisibility(false);
		}
		
		purpleNumberView = new CostAndCoin("purpleNumberView");
		purpleNumberView.initNumberView(purplePosition, lightBlueDigitsFramenames, ZNumberView.ALIGN_RIGHT, 16);
		purpleNumberView.drawingMode = ZNumberView.NumberDrawingMode.NDISCRETE;
		purpleNumberView.setNumberToDraw(BonusUpgradeConstants.moneyForPurpleUpgrade);
		if(!playerProfile.isPurpleUpgradeAvailable()) {
			purpleNumberView.visible = false;
			purpleNumberView.setCoinVisibility(false);
		}
		
		redNumberView = new CostAndCoin("redNumberView");
		redNumberView.initNumberView(redPosition, lightBlueDigitsFramenames, ZNumberView.ALIGN_RIGHT, 16);
		redNumberView.drawingMode = ZNumberView.NumberDrawingMode.NDISCRETE;
		redNumberView.setNumberToDraw(BonusUpgradeConstants.moneyForRedUpgrade);
		if(!playerProfile.isRedUpgradeAvailable()) {
			redNumberView.visible = false;
			redNumberView.setCoinVisibility(false);
		}
		
		parentScene.addActor(bombNumberView);
		parentScene.addActor(powerGemNumberView);
		parentScene.addActor(colorBombNumberView);
		parentScene.addActor(yellowNumberView);
		parentScene.addActor(blueNumberView);
		parentScene.addActor(whiteNumberView);
		parentScene.addActor(greenNumberView);
		parentScene.addActor(orangeNumberView);
		parentScene.addActor(purpleNumberView);
		parentScene.addActor(redNumberView);
	}
	
	private void createBalanceView() {
		balance = new BonusMenuNumberView(sceneBonus);
        parentScene.addActor(balance);
        addMoney = new AddMoney(sceneBonus);
        parentScene.addActor(addMoney);
	}
	
	/*
	 * UPDATE
	 */
	
	@Override
	public void update() {
		super.update();
		if(Settings.isBonusShouldBeUpdated) {
			updateAllButtons();
			Settings.isBonusShouldBeUpdated = false;
		}
	}

	private void updateButton(ZButton button) {
		if(button == bombBonusButton) {
			updateBombButton();
		} else if(button == powerGemBonusButton) {
			updatePowerGemButton();
		} else if(button == colorBombBonusButton) {
			updateColorBombButton();
		} else if(button == yellowBonusButton) {
			updateYellowButton();
		} else if(button == blueBonusButton) {
			updateBlueButton();
		} else if(button == whiteBonusButton) {
			updateWhiteButton();
		} else if(button == purpleBonusButton) {
			updatePurpleButton();
		} else if(button == redBonusButton) {
			updateRedButton();
		} else if(button == greenBonusButton) {
			updateGreenButton();
		} else if(button == orangeBonusButton) {
			updateOrangeButton();
		}
	}
	
	private void updateAllButtons() {
		updateBombButton();
		updatePowerGemButton();
		updateColorBombButton();
		updateYellowButton();
		updateBlueButton();
		updateWhiteButton();
		updatePurpleButton();
		updateRedButton();
		updateGreenButton();
		updateOrangeButton();
	}
	
	private void updateBombButton() {
		if(PlayerProfile.getInstance().isBombUpgradeAvailable()) {
        	if(PlayerProfile.getInstance().getCurrentBalance() < BonusUpgradeConstants.moneyForBombUpgradeLevel[PlayerProfile.getInstance().getBombUpgradeLevel()]) {
        		setBombButtonRegionsUnavailable();
        	} else {
        		setBombButtonRegionsAvailable();
        	}
        } else {
        	setButtonRegionesByName(bombBonusButton, "bomb_3_cant");
        	bombBought.setScreenFrame(bombBonusRect);
        	bombBought.visible = true;
        }
	}
	
	private void updatePowerGemButton() {
		if(PlayerProfile.getInstance().isPowerGemUpgradeAvailable()) {
        	if(PlayerProfile.getInstance().getCurrentBalance() < BonusUpgradeConstants.moneyForPowerGemUpgradeLevel[PlayerProfile.getInstance().getPowerGemUpgradeLevel()]) {
        		setPowerGemButtonRegionsUnavailable();
        	} else {
        		setPowerGemButtonRegionsAvailable();
        	}
        } else {
        	setButtonRegionesByName(powerGemBonusButton, "power_gem_4_cant");
        	powerGemBought.setScreenFrame(powerGemBonusRect);
        	powerGemBought.visible = true;
        }
	}
	
	private void updateColorBombButton() {
		if(PlayerProfile.getInstance().isColorBombUpgradeAvailable()) {
        	if(PlayerProfile.getInstance().getCurrentBalance() < BonusUpgradeConstants.moneyForColorBombUpgrade) {
        		setColorBombButtonRegionsUnavailable();
        	} else {
        		setColorBombButtonRegionsAvailable();
        	}
        } else {
        	setButtonRegionesByName(colorBombBonusButton, "color_bomb_cant");
        	colorBombBought.setScreenFrame(colorBombBonusRect);
        	colorBombBought.visible = true;
        }
	}
	
	private void updateYellowButton() {
		if(PlayerProfile.getInstance().isYellowUpgradeAvailable()) {
        	if(PlayerProfile.getInstance().getCurrentBalance() < BonusUpgradeConstants.moneyForYellowUpgrade) {
        		setYellowButtonRegionsUnavailable();
        	} else {
        		setYellowButtonRegionsAvailable();
        	}
        } else {
        	setButtonRegionesByName(yellowBonusButton, "yellow_cant");
        	yellowBought.setScreenFrame(yellowBonusRect);
        	yellowBought.visible = true;
        }
	}
	
	private void updatePurpleButton() {
		if(PlayerProfile.getInstance().isPurpleUpgradeAvailable()) {
        	if(PlayerProfile.getInstance().getCurrentBalance() < BonusUpgradeConstants.moneyForPurpleUpgrade) {
        		setPurpleButtonRegionsUnavailable();
        	} else {
        		setPurpleButtonRegionsAvailable();
        	}
        } else {
        	setButtonRegionesByName(purpleBonusButton, "purple_cant");
        	purpleBought.setScreenFrame(purpleBonusRect);
        	purpleBought.visible = true;
        }
	}
	
	private void updateRedButton() {
		if(PlayerProfile.getInstance().isRedUpgradeAvailable()) {
        	if(PlayerProfile.getInstance().getCurrentBalance() < BonusUpgradeConstants.moneyForRedUpgrade) {
        		setRedButtonRegionsUnavailable();
        	} else {
        		setRedButtonRegionsAvailable();
        	}
        } else {
        	setButtonRegionesByName(redBonusButton, "red_cant");
        	redBought.setScreenFrame(redBonusRect);
        	redBought.visible = true;
        }
	}
	
	private void updateBlueButton() {
		if(PlayerProfile.getInstance().isBlueUpgradeAvailable()) {
        	if(PlayerProfile.getInstance().getCurrentBalance() < BonusUpgradeConstants.moneyForBlueUpgrade) {
        		setBlueButtonRegionsUnavailable();
        	} else {
        		setBlueButtonRegionsAvailable();
        	}
        } else {
        	setButtonRegionesByName(blueBonusButton, "blue_cant");
        	blueBought.setScreenFrame(blueBonusRect);
        	blueBought.visible = true;
        }
	}
	
	private void updateOrangeButton() {
		if(PlayerProfile.getInstance().isOrangeUpgradeAvailable()) {
        	if(PlayerProfile.getInstance().getCurrentBalance() < BonusUpgradeConstants.moneyForOrangeUpgrade) {
        		setOrangeButtonRegionsUnavailable();
        	} else {
        		setOrangeButtonRegionsAvailable();
        	}
        } else {
        	setButtonRegionesByName(orangeBonusButton, "orange_cant");
        	orangeBought.setScreenFrame(orangeBonusRect);
        	orangeBought.visible = true;
        }
	}
	
	private void updateGreenButton() {
		if(PlayerProfile.getInstance().isGreenUpgradeAvailable()) {
        	if(PlayerProfile.getInstance().getCurrentBalance() < BonusUpgradeConstants.moneyForGreenUpgrade) {
        		setGreenButtonRegionsUnavailable();
        	} else {
        		setGreenButtonRegionsAvailable();
        	}
        } else {
        	setButtonRegionesByName(greenBonusButton, "green_cant");
        	greenBought.setScreenFrame(greenBonusRect);
        	greenBought.visible = true;
        }
	}
	
	private void updateWhiteButton() {
		if(PlayerProfile.getInstance().isWhiteUpgradeAvailable()) {
        	if(PlayerProfile.getInstance().getCurrentBalance() < BonusUpgradeConstants.moneyForWhiteUpgrade) {
        		setWhiteButtonRegionsUnavailable();
        	} else {
        		setWhiteButtonRegionsAvailable();
        	}
        } else {
        	setButtonRegionesByName(whiteBonusButton, "white_cant");
        	whiteBought.setScreenFrame(whiteBonusRect);
        	whiteBought.visible = true;
        }
	}

	/*
	 * BUTTON CLICK
	 */
    private void bombBonusButtonClick() {
    	if(!playerProfile.isBombUpgradeAvailable()) {
    		return;
    	}
    		isAddMoneyDialogShouldShown = !isHelpShouldBeShown[BOMB];
    	if(isHelpShouldBeShown[BOMB]) {
    		upgradedBonus = BOMB;
    		isHelpShouldBeShown[BOMB] = false;
    		sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_BONUS);
    		upgradedBonus = -1;
    	}
    	if(playerProfile.getCurrentBalance() >= 
        	BonusUpgradeConstants.moneyForBombUpgradeLevel[playerProfile.getBombUpgradeLevel()]) {
        	upgradedBonus = BOMB;
        	ZLog.i("bomb pressed");
        	pressButton(bombBonusButton);	
        } else {
        	if(isAddMoneyDialogShouldShown) {
        		sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_ADD_MONEY);
        	}
        	upgradedBonus = -1;
        }
    }

    private void powerGemBonusButtonClick() {
    	if(!playerProfile.isPowerGemUpgradeAvailable()) {
    		return;
    	}
    	isAddMoneyDialogShouldShown = !isHelpShouldBeShown[POWER_GEM];
    	if(isHelpShouldBeShown[POWER_GEM]) {
    		upgradedBonus = POWER_GEM;
    		isHelpShouldBeShown[POWER_GEM] = false;
    		sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_BONUS);
    		upgradedBonus = -1;
    	}
    	if(playerProfile.getCurrentBalance() >= 
        	BonusUpgradeConstants.moneyForPowerGemUpgradeLevel[playerProfile.getPowerGemUpgradeLevel()]) {
        	upgradedBonus = POWER_GEM;
        	pressButton(powerGemBonusButton);
        } else {
        	if(isAddMoneyDialogShouldShown) {
        		sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_ADD_MONEY);
        	}
        	upgradedBonus = -1;
        }
    }

    private void colorBombBonusButtonClick() {
    	if(!playerProfile.isColorBombUpgradeAvailable()) {
    		return;
    	}
    		isAddMoneyDialogShouldShown = !isHelpShouldBeShown[COLOR_BOMB];
    	if(isHelpShouldBeShown[COLOR_BOMB]) {
    		upgradedBonus = COLOR_BOMB;
    		isHelpShouldBeShown[COLOR_BOMB] = false;
    		sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_BONUS);
    		upgradedBonus = -1;
    	}
    	if(playerProfile.getCurrentBalance() >= 
        	BonusUpgradeConstants.moneyForColorBombUpgrade) {
        	upgradedBonus = COLOR_BOMB;
        	pressButton(colorBombBonusButton);
        } else {
        	if(isAddMoneyDialogShouldShown) {
        		sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_ADD_MONEY);
        	}
        	upgradedBonus = -1;
        }
    }
    
    private void yellowBonusButtonClick() {
    	if(!playerProfile.isYellowUpgradeAvailable()) {
    		return;
    	}
    	isAddMoneyDialogShouldShown = !isHelpShouldBeShown[YELLOW];
		if(isHelpShouldBeShown[YELLOW]) {
			upgradedBonus = YELLOW;
    		isHelpShouldBeShown[YELLOW] = false;
    		sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_BONUS);
    		upgradedBonus = -1;
    	}
    	if(playerProfile.getCurrentBalance() >= BonusUpgradeConstants.moneyForYellowUpgrade) {
    		upgradedBonus = YELLOW;
    		pressButton(yellowBonusButton);
    	} else {
    		if(isAddMoneyDialogShouldShown) {
    			sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_ADD_MONEY);
    		}
    		upgradedBonus = -1;
    	}
    }
    
    private void blueBonusButtonClick() {
    	if(!playerProfile.isBlueUpgradeAvailable()) {
    		return;
    	}
    	isAddMoneyDialogShouldShown = !isHelpShouldBeShown[BLUE];
		if(isHelpShouldBeShown[BLUE]) {
			upgradedBonus = BLUE;
    		isHelpShouldBeShown[BLUE] = false;
    		sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_BONUS);
    		upgradedBonus = -1;
    	}
    	if(playerProfile.getCurrentBalance() >= BonusUpgradeConstants.moneyForBlueUpgrade) {
    		upgradedBonus = BLUE;
    		pressButton(blueBonusButton);
    	} else {
    		if(isAddMoneyDialogShouldShown) {
    			sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_ADD_MONEY);
    		}
    		upgradedBonus = -1;
    	}
    }
    
    private void whiteBonusButtonClick() {
    	if(!playerProfile.isWhiteUpgradeAvailable()) {
    		return;
    	}
    	isAddMoneyDialogShouldShown = !isHelpShouldBeShown[WHITE];
		if(isHelpShouldBeShown[WHITE]) {
			upgradedBonus = WHITE;
    		isHelpShouldBeShown[WHITE] = false;
    		sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_BONUS);
    		upgradedBonus = -1;
    	}
    	if(playerProfile.getCurrentBalance() >= BonusUpgradeConstants.moneyForWhiteUpgrade) {
    		upgradedBonus = WHITE;
    		pressButton(whiteBonusButton);
    	} else {
    		if(isAddMoneyDialogShouldShown) {
    			sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_ADD_MONEY);
    		}
    		upgradedBonus = -1;
    	}
    }
    
    private void greenBonusButtonClick() {
    	if(!playerProfile.isGreenUpgradeAvailable()) {
    		return;
    	}
    	isAddMoneyDialogShouldShown = !isHelpShouldBeShown[GREEN];
		if(isHelpShouldBeShown[GREEN]) {
			upgradedBonus = GREEN;
    		isHelpShouldBeShown[GREEN] = false;
    		sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_BONUS);
    		upgradedBonus = -1;
    	}
    	if(playerProfile.getCurrentBalance() >= BonusUpgradeConstants.moneyForGreenUpgrade) {
    		upgradedBonus = GREEN;
    		pressButton(greenBonusButton);
    	} else {
    		if(isAddMoneyDialogShouldShown) {
    			sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_ADD_MONEY);
    		}
    		upgradedBonus = -1;
    	}
    }
    
    private void purpleBonusButtonClick() {
    	if(!playerProfile.isPurpleUpgradeAvailable()) {
    		return;
    	}
    	isAddMoneyDialogShouldShown = !isHelpShouldBeShown[PURPLE];
		if(isHelpShouldBeShown[PURPLE]) {
			upgradedBonus = PURPLE;
    		isHelpShouldBeShown[PURPLE] = false;
    		sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_BONUS);
    		upgradedBonus = -1;
    	}
    	if(playerProfile.getCurrentBalance() >= BonusUpgradeConstants.moneyForPurpleUpgrade) {
    		upgradedBonus = PURPLE;
    		pressButton(purpleBonusButton);
    	} else {
    		if(isAddMoneyDialogShouldShown) {
    			sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_ADD_MONEY);
    		}
    		upgradedBonus = -1;
    	}
    }
    
    private void orangeBonusButtonClick() {
    	if(!playerProfile.isOrangeUpgradeAvailable()) {
    		return;
    	}
    	isAddMoneyDialogShouldShown = !isHelpShouldBeShown[ORANGE];
		if(isHelpShouldBeShown[ORANGE]) {
			upgradedBonus = ORANGE;
    		isHelpShouldBeShown[ORANGE] = false;
    		sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_BONUS);
    		upgradedBonus = -1;
    	}
    	if(playerProfile.getCurrentBalance() >= BonusUpgradeConstants.moneyForOrangeUpgrade) {
    		upgradedBonus = ORANGE;
    		pressButton(orangeBonusButton);
    	} else {
    		if(isAddMoneyDialogShouldShown) {
    			sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_ADD_MONEY);
    		}
    		upgradedBonus = -1;
    	}
    }
    
    private void redBonusButtonClick() {
    	if(!playerProfile.isRedUpgradeAvailable()) {
    		return;
    	}
    	isAddMoneyDialogShouldShown = !isHelpShouldBeShown[RED];
		if(isHelpShouldBeShown[RED]) {
			upgradedBonus = RED;
    		isHelpShouldBeShown[RED] = false;
    		sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_BONUS);
    		upgradedBonus = -1;
    	}
    	if(playerProfile.getCurrentBalance() >= BonusUpgradeConstants.moneyForRedUpgrade) {
    		upgradedBonus = RED;
    		pressButton(redBonusButton);
    	} else {
    		if(isAddMoneyDialogShouldShown) {
    			sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_ADD_MONEY);
    		}
    		upgradedBonus = -1;
    	}
    }
    
    private void buyButtonClick() {
    	if(upgradedBonus == -1) {
    		return;
    	}
    	upgradeCurrentBonus();
    	upgradedBonus = -1;
    	selectView.visible = false;
    	selectView.setScreenFrame(tempRect);
    	updateAllButtons();
    }
    
    
    /*
     * PRESS BUTTON
     */
    private void pressButton(ZButton button) {
    	if(button == bombBonusButton) {
			selectView.setScreenFrame(bombBonusRect);
			selectView.visible = true;
		} else if(button == powerGemBonusButton) {
			selectView.setScreenFrame(powerGemBonusRect);
			selectView.visible = true;
		} else if(button == colorBombBonusButton) {
			selectView.setScreenFrame(colorBombBonusRect);
			selectView.visible = true;
		} else if(button == yellowBonusButton) {
			selectView.setScreenFrame(yellowBonusRect);
			selectView.visible = true;
		} else if(button == blueBonusButton) {
			selectView.setScreenFrame(blueBonusRect);
			selectView.visible = true;
		} else if(button == whiteBonusButton) {
			selectView.setScreenFrame(whiteBonusRect);
			selectView.visible = true;
		} else if(button == purpleBonusButton) {
			selectView.setScreenFrame(purpleBonusRect);
			selectView.visible = true;
		} else if(button == redBonusButton) {
			selectView.setScreenFrame(redBonusRect);
			selectView.visible = true;
		} else if(button == greenBonusButton) {
			selectView.setScreenFrame(greenBonusRect);
			selectView.visible = true;
		} else if(button == orangeBonusButton) {
			selectView.setScreenFrame(orangeBonusRect);
			selectView.visible = true;
		}
    }
    
    /*
     * UPGRADE
     */    
    private void upgradeCurrentBonus() {
    	switch(upgradedBonus) {
    	case BOMB: upgradeBomb(); break;
    	case POWER_GEM: upgradePowerGem(); break;
    	case COLOR_BOMB: upgradeColorBomb(); break;
    	case YELLOW: upgradeYellow(); break;
    	case BLUE: upgradeBlue(); break;
    	case WHITE: upgradeWhite(); break;
    	case GREEN: upgradeGreen(); break;
    	case ORANGE: upgradeOrange(); break;
    	case PURPLE: upgradePurple(); break;
    	case RED: upgradeRed(); break;
    	default: return;
    	}
    }
    
    private void upgradeBomb() {
    	playerProfile.increaseCurrentBalance(-BonusUpgradeConstants.moneyForBombUpgradeLevel[playerProfile.getBombUpgradeLevel()]);
    	playerProfile.upgradeBomb();
    	balance.resetBalance();
    	updateButton(bombBonusButton);
    	if(playerProfile.isBombUpgradeAvailable()) {
    		bombNumberView.setNumberToDraw(BonusUpgradeConstants.moneyForBombUpgradeLevel[playerProfile.getBombUpgradeLevel()]);
    	} else {
    		bombNumberView.visible = false;
    		bombNumberView.setCoinVisibility(false);
    	}
    }
    
    private void upgradePowerGem() {
    	playerProfile.increaseCurrentBalance(-BonusUpgradeConstants.moneyForPowerGemUpgradeLevel[playerProfile.getPowerGemUpgradeLevel()]);
    	playerProfile.upgradePowerGem();
    	balance.resetBalance();
    	updateButton(powerGemBonusButton);
    	if(playerProfile.isPowerGemUpgradeAvailable()) {
    		powerGemNumberView.setNumberToDraw(BonusUpgradeConstants.moneyForPowerGemUpgradeLevel[playerProfile.getPowerGemUpgradeLevel()]);
    	} else {
    		powerGemNumberView.visible = false;
    		powerGemNumberView.setCoinVisibility(false);
    	}
    }
    
    private void upgradeColorBomb() {
    	playerProfile.increaseCurrentBalance(-BonusUpgradeConstants.moneyForColorBombUpgrade);
    	playerProfile.upgradeColorBomb();
    	balance.resetBalance();
    	updateButton(colorBombBonusButton);
    	colorBombNumberView.visible = false;
    	colorBombNumberView.setCoinVisibility(false);
    }
    
    private void upgradeYellow() {
    	playerProfile.increaseCurrentBalance(-BonusUpgradeConstants.moneyForYellowUpgrade);
    	playerProfile.upgradeYellow();
    	balance.resetBalance();
    	updateButton(yellowBonusButton);
    	yellowNumberView.visible = false;
    	yellowNumberView.setCoinVisibility(false);
    }
    
    private void upgradeRed() {
    	playerProfile.increaseCurrentBalance(-BonusUpgradeConstants.moneyForRedUpgrade);
    	playerProfile.upgradeRed();
    	balance.resetBalance();
    	updateButton(redBonusButton);
    	redNumberView.visible = false;
    	redNumberView.setCoinVisibility(false);
    }
    
    private void upgradeBlue() {
    	playerProfile.increaseCurrentBalance(-BonusUpgradeConstants.moneyForBlueUpgrade);
    	playerProfile.upgradeBlue();
    	balance.resetBalance();
    	updateButton(blueBonusButton);
    	blueNumberView.visible = false;
    	blueNumberView.setCoinVisibility(false);
    }
    
    private void upgradeGreen() {
    	playerProfile.increaseCurrentBalance(-BonusUpgradeConstants.moneyForGreenUpgrade);
    	playerProfile.upgradeGreen();
    	balance.resetBalance();
    	updateButton(greenBonusButton);
    	greenNumberView.visible = false;
    	greenNumberView.setCoinVisibility(false);
    }
    
    private void upgradeWhite() {
    	playerProfile.increaseCurrentBalance(-BonusUpgradeConstants.moneyForWhiteUpgrade);
    	playerProfile.upgradeWhite();
    	balance.resetBalance();
    	updateButton(whiteBonusButton);
    	whiteNumberView.visible = false;
    	whiteNumberView.setCoinVisibility(false);
    }
    
    private void upgradeOrange() {
    	playerProfile.increaseCurrentBalance(-BonusUpgradeConstants.moneyForOrangeUpgrade);
    	playerProfile.upgradeOrange();
    	balance.resetBalance();
    	updateButton(orangeBonusButton);
    	orangeNumberView.visible = false;
    	orangeNumberView.setCoinVisibility(false);
    }
    
    private void upgradePurple() {
    	playerProfile.increaseCurrentBalance(-BonusUpgradeConstants.moneyForPurpleUpgrade);
    	playerProfile.upgradePurple();
    	balance.resetBalance();
    	updateButton(purpleBonusButton);
    	purpleNumberView.visible = false;
    	purpleNumberView.setCoinVisibility(false);
    }
    
    
    /*
     * BUTTON REGIONS UNAVAILABLE
     */
    private void setBombButtonRegionsUnavailable() {
    	switch(playerProfile.getBombUpgradeLevel()) {
    	case 0: setButtonRegionesByName(bombBonusButton, "bomb_1_cant"); break;
    	case 1: setButtonRegionesByName(bombBonusButton, "bomb_2_cant"); break;
    	case 2: setButtonRegionesByName(bombBonusButton, "bomb_3_cant"); break;
    	case 3: setButtonRegionesByName(bombBonusButton, "bomb_3_cant"); break;
    	default: return;
    	}
    }
    
    private void setColorBombButtonRegionsUnavailable() {
    	switch(playerProfile.getColorBombUpgradeLevel()) {
    	case 0: setButtonRegionesByName(colorBombBonusButton, "color_bomb_cant"); break;
    	case 1: setButtonRegionesByName(colorBombBonusButton, "color_bomb_cant"); break;
    	default: return;
    	}
    }
    
    private void setPowerGemButtonRegionsUnavailable() {
    	switch(playerProfile.getPowerGemUpgradeLevel()) {
    	case 0: setButtonRegionesByName(powerGemBonusButton, "power_gem_1_cant"); break;
    	case 1: setButtonRegionesByName(powerGemBonusButton, "power_gem_2_cant"); break;
    	case 2: setButtonRegionesByName(powerGemBonusButton, "power_gem_3_cant"); break;
    	case 3: setButtonRegionesByName(powerGemBonusButton, "power_gem_4_cant"); break;
    	case 4: setButtonRegionesByName(powerGemBonusButton, "power_gem_4_cant"); break;
    	default: return;
    	}
    }
    
    private void setYellowButtonRegionsUnavailable() {
    	switch(playerProfile.getYellowUpgradeLevel()) {
    	case 0: setButtonRegionesByName(yellowBonusButton, "yellow_cant"); break;
    	case 1: setButtonRegionesByName(yellowBonusButton, "yellow_cant"); break;
    	default: return;
    	}
    }
    
    private void setBlueButtonRegionsUnavailable() {
    	switch(playerProfile.getBlueUpgradeLevel()) {
    	case 0: setButtonRegionesByName(blueBonusButton, "blue_cant"); break;
    	case 1: setButtonRegionesByName(blueBonusButton, "blue_cant"); break;
    	default: return;
    	}
    }
    
    private void setRedButtonRegionsUnavailable() {
    	switch(playerProfile.getRedUpgradeLevel()) {
    	case 0: setButtonRegionesByName(redBonusButton, "red_cant"); break;
    	case 1: setButtonRegionesByName(redBonusButton, "red_cant"); break;
    	default: return;
    	}
    }
    
    private void setGreenButtonRegionsUnavailable() {
    	switch(playerProfile.getGreenUpgradeLevel()) {
    	case 0: setButtonRegionesByName(greenBonusButton, "green_cant"); break;
    	case 1: setButtonRegionesByName(greenBonusButton, "green_cant"); break;
    	default: return;
    	}
    }
    
    private void setWhiteButtonRegionsUnavailable() {
    	switch(playerProfile.getWhiteUpgradeLevel()) {
    	case 0: setButtonRegionesByName(whiteBonusButton, "white_cant"); break;
    	case 1: setButtonRegionesByName(whiteBonusButton, "white_cant"); break;
    	default: return;
    	}
    }
    
    private void setOrangeButtonRegionsUnavailable() {
    	switch(playerProfile.getOrangeUpgradeLevel()) {
    	case 0: setButtonRegionesByName(orangeBonusButton, "orange_cant"); break;
    	case 1: setButtonRegionesByName(orangeBonusButton, "orange_cant"); break;
    	default: return;
    	}
    }
    
    private void setPurpleButtonRegionsUnavailable() {
    	switch(playerProfile.getPurpleUpgradeLevel()) {
    	case 0: setButtonRegionesByName(purpleBonusButton, "purple_cant"); break;
    	case 1: setButtonRegionesByName(purpleBonusButton, "purple_cant"); break;
    	default: return;
    	}
    }
    
    /*
     * BUTTON REGIONS AVAILABLE
     */    
    private void setBombButtonRegionsAvailable() {
    	switch(playerProfile.getBombUpgradeLevel()) {
    	case 0: setButtonRegionesByName(bombBonusButton, "bomb_1_upgrade"); break;
    	case 1: setButtonRegionesByName(bombBonusButton, "bomb_2_upgrade"); break;
    	case 2: setButtonRegionesByName(bombBonusButton, "bomb_3_upgrade"); break;
    	case 3: setButtonRegionesByName(bombBonusButton, "bomb_3_upgrade"); break;
    	default: return;
    	}
    }
    
    private void setColorBombButtonRegionsAvailable() {
    	switch(playerProfile.getColorBombUpgradeLevel()) {
    	case 0: setButtonRegionesByName(colorBombBonusButton, "color_bomb"); break;
    	case 1: setButtonRegionesByName(colorBombBonusButton, "color_bomb"); break;
    	default: return;
    	}
    }
    
    private void setPowerGemButtonRegionsAvailable() {
    	switch(playerProfile.getPowerGemUpgradeLevel()) {
    	case 0: setButtonRegionesByName(powerGemBonusButton, "power_gem_1_upgrade"); break;
    	case 1: setButtonRegionesByName(powerGemBonusButton, "power_gem_2_upgrade"); break;
    	case 2: setButtonRegionesByName(powerGemBonusButton, "power_gem_3_upgrade"); break;
    	case 3: setButtonRegionesByName(powerGemBonusButton, "power_gem_4_upgrade"); break;
    	case 4: setButtonRegionesByName(powerGemBonusButton, "power_gem_4_upgrade"); break;
    	default: return;
    	}
    }
    
    private void setYellowButtonRegionsAvailable() {
    	switch(playerProfile.getYellowUpgradeLevel()) {
    	case 0: setButtonRegionesByName(yellowBonusButton, "yellow"); break;
    	case 1: setButtonRegionesByName(yellowBonusButton, "yellow"); break;
    	default: return;
    	}
    }
    
    private void setBlueButtonRegionsAvailable() {
    	switch(playerProfile.getBlueUpgradeLevel()) {
    	case 0: setButtonRegionesByName(blueBonusButton, "blue"); break;
    	case 1: setButtonRegionesByName(blueBonusButton, "blue"); break;
    	default: return;
    	}
    }
    
    private void setRedButtonRegionsAvailable() {
    	switch(playerProfile.getRedUpgradeLevel()) {
    	case 0: setButtonRegionesByName(redBonusButton, "red"); break;
    	case 1: setButtonRegionesByName(redBonusButton, "red"); break;
    	default: return;
    	}
    }
    
    private void setGreenButtonRegionsAvailable() {
    	switch(playerProfile.getGreenUpgradeLevel()) {
    	case 0: setButtonRegionesByName(greenBonusButton, "green"); break;
    	case 1: setButtonRegionesByName(greenBonusButton, "green"); break;
    	default: return;
    	}
    }
    
    private void setWhiteButtonRegionsAvailable() {
    	switch(playerProfile.getWhiteUpgradeLevel()) {
    	case 0: setButtonRegionesByName(whiteBonusButton, "white"); break;
    	case 1: setButtonRegionesByName(whiteBonusButton, "white"); break;
    	default: return;
    	}
    }
    
    private void setOrangeButtonRegionsAvailable() {
    	switch(playerProfile.getOrangeUpgradeLevel()) {
    	case 0: setButtonRegionesByName(orangeBonusButton, "orange"); break;
    	case 1: setButtonRegionesByName(orangeBonusButton, "orange"); break;
    	default: return;
    	}
    }
    
    private void setPurpleButtonRegionsAvailable() {
    	switch(playerProfile.getPurpleUpgradeLevel()) {
    	case 0: setButtonRegionesByName(purpleBonusButton, "purple"); break;
    	case 1: setButtonRegionesByName(purpleBonusButton, "purple"); break;
    	default: return;
    	}
    }

    private void setButtonRegionesByName(ZButton button, String textureName) {
    	button.pressedRegion = texturesManager.getRegionByName(textureName);
    	button.unpressedRegion = texturesManager.getRegionByName(textureName);
    }
    
    public static int getUpgradedBonus() {
    	return upgradedBonus;
    }
}
