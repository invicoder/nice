package com.boolbalabs.nice.menu;

import android.os.SystemClock;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.nice.SceneBonus;
import com.boolbalabs.nice.settings.StaticConstants;

public class AddMoney extends ZNode {

	private SceneBonus sceneBonus;
	
	private Rectangle rect = new Rectangle(10, ScreenMetrics.screenHeightRip - 130, 46, 46);
	
	private ZNode staticNode;
	private ZNode nonstaticNode;
	
	private long currentTime;
	private float deltaTime = 0.02f;
	private float currentAlpha;
	private float deltaAlpha = 0.02f;
	private boolean isAlphaUp = true;
	
	public AddMoney(SceneBonus sceneBonus) {
		super("add_money");
		
		this.sceneBonus = sceneBonus;
		
		staticNode = new ZNode("static_add");
		staticNode.initWithFrame(rect, TexturesManager.getInstance().getRegionByName("add"));
		
		nonstaticNode = new ZNode("nonstatic_add");
		nonstaticNode.initWithFrame(rect, TexturesManager.getInstance().getRegionByName("add_light"));
		currentAlpha = 0.0f;
		currentTime = SystemClock.uptimeMillis();
	}
	
	@Override
	protected boolean touchDown(float x, float y, int pointer) {
		if(x > rect.x && x < rect.x + rect.width && y > rect.y && y < rect.y + rect.height) {
			sceneBonus.sendEmptyMessageToActivity(StaticConstants.MESSAGE_ADD_MONEY);
			return true;
		}
		return false;
	}
	
	@Override
	public void update() {
		super.update();
		staticNode.update();
		nonstaticNode.update();
		if(SystemClock.uptimeMillis() - currentTime >= deltaTime) {
			if(currentAlpha >= 0.9f) {
				isAlphaUp = false;
			} else if(currentAlpha <= 0.1f) {
				isAlphaUp = true;
			}
			if(isAlphaUp) {
				currentAlpha += deltaAlpha;
			} else {
				currentAlpha -= deltaAlpha;
			}
			currentTime = SystemClock.uptimeMillis();
		}
		
	}
	
	@Override
	protected void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		staticNode.zdraw(batch, parentAlpha);
		nonstaticNode.zdraw(batch, currentAlpha);
	}

}
