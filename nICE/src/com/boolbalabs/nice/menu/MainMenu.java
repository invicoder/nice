/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.menu;

import android.content.IntentSender.SendIntentException;
import android.graphics.Point;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.boolbalabs.lib.elements.ZButton;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.nice.NiceGame;
import com.boolbalabs.nice.SceneMenu;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.extra.SoundService;
import com.boolbalabs.nice.settings.Settings;
import com.boolbalabs.nice.settings.StaticConstants;
import com.boolbalabs.nice.utils.ZLog;

/**
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class MainMenu extends ZNode {

    public static final String NAME = "main_menu";
    private TexturesManager texturesManager;
    private PlayerProfile playerProfile;
    private SceneMenu sceneMenu;
    private Rectangle screenRectRip = new Rectangle(0, 0, ScreenMetrics.screenWidthRip, ScreenMetrics.screenHeightRip);
    private ZButton modeClassicButton;
    private ZButton modeTimeButton;
    private ZButton modeQuickButton;
    private ZButton modePuzzleButton;
    private ZButton.ClickListener clickListener;
    private int minimalY;
    private Rectangle modeClassicRect;
    private Rectangle modeTimeRect;
    private Rectangle modeQuickRect;
    private Rectangle modePuzzleRect;
    //************** other buttons *****************//
//    private ZButton modeInfiniteButton;
    private ZButton settingsButton;
    private ZButton helpButton;
    private ZButton highScoresButton;
    
    private ZButton shopButton;
    private Rectangle shopButtonRect;
    
    private int skipFromBottom = ScreenMetrics.screenHeightRip - 505;
    private int skipFromRight = ScreenMetrics.screenWidthRip;
//    private Rectangle modeInfiniteRect = new Rectangle(51+5 + 51+5 + 51+5, ScreenMetrics.screenHeightRip - skipFromTop, 51, 44);
    private Rectangle settingsButtonRect;
    private Rectangle helpButtonRect;
    private Rectangle highScoresButtonRect;

    public MainMenu(SceneMenu sceneMenu) {
        super(NAME);
        
        createScreenRectangles();
        
        this.sceneMenu = sceneMenu;
        parentScene = sceneMenu;
        sceneMenu.addActor(this);
        texturesManager = TexturesManager.getInstance();
        playerProfile = PlayerProfile.getInstance();
        setScreenFrame(screenRectRip);
        setRegion();
        createButtons(); 
    }
    
    private void createScreenRectangles() {
    	minimalY = ScreenMetrics.screenHeightRip-430;
    	skipFromBottom = ScreenMetrics.screenHeightRip - 460;

    	modeClassicRect = new Rectangle(40, minimalY + 170, 148, 147);
        modeTimeRect = new Rectangle(0, minimalY, 139, 146);
        modeQuickRect = new Rectangle(45, minimalY + 85, 148, 147);
        modePuzzleRect = new Rectangle(110, minimalY + 15, 151, 148);
        
        settingsButtonRect = new Rectangle(skipFromRight - 120, skipFromBottom, 59, 57);
        helpButtonRect = new Rectangle(skipFromRight - 60, skipFromBottom + 30, 58, 57);
        highScoresButtonRect = new Rectangle(skipFromRight - 60, skipFromBottom - 25, 61, 51);
        
        shopButtonRect = new Rectangle(200, ScreenMetrics.screenHeightRip - 280, 122, 125);
    }

    private void setRegion() {
        region.setRegion(texturesManager.getRegionByName("menu_bg"));
        region.setRegion(region.getRegionX(), region.getRegionY(),
                region.getRegionWidth(), Math.min(854, ScreenMetrics.getHeightProportionalToWidth(region.getRegionWidth())));
    }

    private void classicButtonButtonClick() {
    	if(Settings.isHelpBasicShouldShown) {
    		sceneMenu.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_BASIC);
    		Settings.isHelpBasicShouldShown = false;
    	}
        playerProfile.setCurrentMode(PlayerProfile.MODE_CLASSIC);
        if(Settings.isHelpClassicShouldShown) {
        	sceneMenu.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_MODE);
        	Settings.isHelpClassicShouldShown = false;
        }
        sceneMenu.askGameToSwitchGameScene(NiceGame.SCENE_GAMEPLAY);
    }

    private void timeButtonButtonClick() {
    	if(Settings.isHelpBasicShouldShown) {
    		sceneMenu.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_BASIC);
    		Settings.isHelpBasicShouldShown = false;
    	}
        playerProfile.setCurrentMode(PlayerProfile.MODE_TIME);
        if(Settings.isHelpTimeShouldShown) {
        	sceneMenu.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_MODE);
        	Settings.isHelpTimeShouldShown = false;
        }
        sceneMenu.askGameToSwitchGameScene(NiceGame.SCENE_GAMEPLAY);
    }

    private void quickButtonButtonClick() {
    	if(Settings.isHelpBasicShouldShown) {
    		sceneMenu.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_BASIC);
    		Settings.isHelpBasicShouldShown = false;
    	}
        playerProfile.setCurrentMode(PlayerProfile.MODE_QUICK);
        if(Settings.isHelpQuickShouldShown) {
        	sceneMenu.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_MODE);
        	Settings.isHelpQuickShouldShown = false;
        }
        sceneMenu.askGameToSwitchGameScene(NiceGame.SCENE_GAMEPLAY);
    }

    private void puzzleButtonButtonClick() {
    	if(Settings.isHelpBasicShouldShown) {
    		sceneMenu.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_BASIC);
    		Settings.isHelpBasicShouldShown = false;
    	}
    	if(playerProfile.currentPuzzleLevel != 0) {
        	sceneMenu.sendEmptyMessageToActivity(StaticConstants.MESSAGE_CONTINUE);
        } else {
        	sceneMenu.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_PUZZLE);
        }
        playerProfile.setCurrentMode(PlayerProfile.MODE_PUZZLE);
        if(Settings.isHelpPuzzleShouldShown) {
        	sceneMenu.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_MODE);
        	Settings.isHelpPuzzleShouldShown = false;
        }
        sceneMenu.askGameToSwitchGameScene(NiceGame.SCENE_GAMEPLAY);
    }
    
    private void infiniteButtonButtonCLick() {
    	playerProfile.setCurrentMode(PlayerProfile.MODE_INFINITE);
        sceneMenu.askGameToSwitchGameScene(NiceGame.SCENE_GAMEPLAY);
    }

    private void settingsButtonClick() {
    	sceneMenu.sendEmptyMessageToActivity(StaticConstants.MESSAGE_SHOW_SETTINGS);
    }
    
    private void helpButtonClick() {
    	sceneMenu.sendEmptyMessageToActivity(StaticConstants.MESSAGE_DIALOG_HELP);
    }

    //TODO:
    private void highScoresButtonClick() {
    	//sceneMenu.sendEmptyMessageToActivity(StaticConstants.MESSAGE_SHOW_SCORES);
    }

    private void shopButtonClick() {
    	sceneMenu.askGameToSwitchGameScene(NiceGame.SCENE_BONUS);
    	if(Settings.isHelpWelcomeBonusShouldBeShown) {
    		Settings.isHelpWelcomeBonusShouldBeShown = false;
    		sceneMenu.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_WELCOME_BONUS);
    	}
    	Settings.isBonusShouldBeUpdated = true;  
    }
    
    private void createButtons() {
        clickListener = new ZButton.ClickListener() {

            public void clicked(ZButton button) {
            	SoundService.playButtonClick();
                if (button == modeClassicButton) {
                    classicButtonButtonClick();
                } else if (button == modeTimeButton) {
                    timeButtonButtonClick();
                } else if (button == modeQuickButton) {
                    quickButtonButtonClick();
                } else if (button == modePuzzleButton) {
                    puzzleButtonButtonClick();
                } else if (button == settingsButton) {
                    settingsButtonClick();
                } else if (button == helpButton) {
                    helpButtonClick();
                } else if (button == highScoresButton) {
                    highScoresButtonClick();
//                } else if(button == modeInfiniteButton) {
//                	infiniteButtonButtonCLick();
                } else if(button == shopButton) {
                	shopButtonClick();
                }
            }

            public void touchDown(ZButton button) {
            }
        };

        modeClassicButton = new ZButton("modeClassicButton", texturesManager.getRegionByName("button_classic"),
                texturesManager.getRegionByName("button_classic_pressed"));
        modeClassicButton.setScreenFrame(modeClassicRect);
        modeClassicButton.clickListener = clickListener;

        modeTimeButton = new ZButton("modeTimeButton", texturesManager.getRegionByName("button_time"),
                texturesManager.getRegionByName("button_time_pressed"));
        modeTimeButton.setScreenFrame(modeTimeRect);
        modeTimeButton.clickListener = clickListener;

        modeQuickButton = new ZButton("modeQuickButton", texturesManager.getRegionByName("button_quick"),
                texturesManager.getRegionByName("button_quick_pressed"));
        modeQuickButton.setScreenFrame(modeQuickRect);
        modeQuickButton.clickListener = clickListener;

        modePuzzleButton = new ZButton("modePuzzleButton", texturesManager.getRegionByName("button_puzzle"),
                texturesManager.getRegionByName("button_puzzle_pressed"));
        modePuzzleButton.setScreenFrame(modePuzzleRect);
        modePuzzleButton.clickListener = clickListener;
        
//        modeInfiniteButton = new ZButton("modeInfiniteButton", texturesManager.getRegionByName("button_settings_pressed"),
//                texturesManager.getRegionByName("button_settings_pressed"));
//        modeInfiniteButton.setScreenFrame(modeInfiniteRect);
//        modeInfiniteButton.clickListener = clickListener;

        settingsButton = new ZButton("settingsButton", texturesManager.getRegionByName("button_settings"),
                texturesManager.getRegionByName("button_settings_pressed"));
        settingsButton.setScreenFrame(settingsButtonRect);
        settingsButton.clickListener = clickListener;

        helpButton = new ZButton("helpButton", texturesManager.getRegionByName("button_help"),
                texturesManager.getRegionByName("button_help_pressed"));
        helpButton.setScreenFrame(helpButtonRect);
        helpButton.clickListener = clickListener;

        highScoresButton = new ZButton("highScoresButton", texturesManager.getRegionByName("button_score"),
                texturesManager.getRegionByName("button_score_pressed"));
        highScoresButton.setScreenFrame(highScoresButtonRect);
        highScoresButton.clickListener = clickListener;
        
        shopButton = new ZButton("shopButton", texturesManager.getRegionByName("button_shop"), 
        		texturesManager.getRegionByName("button_shop_pressed"));
        shopButton.setScreenFrame(shopButtonRect);
        shopButton.clickListener = clickListener;
 
        parentScene.addActor(modePuzzleButton);
        parentScene.addActor(modeTimeButton);
        parentScene.addActor(modeQuickButton);
        parentScene.addActor(modeClassicButton);
//        parentScene.addActor(modeInfiniteButton);

        parentScene.addActor(settingsButton);
        parentScene.addActor(helpButton);
        parentScene.addActor(highScoresButton);
        
        parentScene.addActor(shopButton);
    }
}
