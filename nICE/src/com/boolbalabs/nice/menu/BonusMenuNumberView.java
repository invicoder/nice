/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.menu;

import android.graphics.PointF;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.graphics.ZNumberView;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.nice.SceneBonus;
import com.boolbalabs.nice.extra.PlayerProfile;
/**
 * 
 * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
 */

public class BonusMenuNumberView extends ZNode{
	
	private Rectangle bgRectOnScreen = new Rectangle(2, ScreenMetrics.screenHeightRip - 20, 93, 17);
    private SceneBonus sceneBonus;
    private PlayerProfile playerProfile;
    private final String lightBlueDigitsFramenames[] = {"silverdigits00d", "silverdigits01d", "silverdigits02d",
        "silverdigits03d", "silverdigits04d", "silverdigits05d", "silverdigits06d",
        "silverdigits07d", "silverdigits08d", "silverdigits09d"};

    //private ZNode balanceTextView;
    private ZNumberView balanceNumberView;
    //private Rectangle balanceScreenRect = new Rectangle(ScreenMetrics.screenWidthRip - 71, ScreenMetrics.screenHeightRip - 20, 71, 17);
    private PointF balancePosition = new PointF(57, ScreenMetrics.screenHeightRip - 120);
    
	public BonusMenuNumberView(SceneBonus sceneBonus) {
		super("Score");
        parentScene = sceneBonus;
        this.sceneBonus = (SceneBonus) parentScene;
        playerProfile = PlayerProfile.getInstance();
        setScreenFrame(bgRectOnScreen);
        
        createNumberViews();
	}
	
	private void createNumberViews() {

        balanceNumberView = new ZNumberView("moneyBalancenNumberView");
        balanceNumberView.initNumberView(balancePosition, lightBlueDigitsFramenames, ZNumberView.ALIGN_LEFT, 20);
        balanceNumberView.drawingMode = ZNumberView.NumberDrawingMode.NCONTINUOUS;
        balanceNumberView.setContinuousDrawingParameters(15, -51);
        balanceNumberView.setNumberToDraw(playerProfile.getCurrentBalance());    }

    @Override
    public void onShow() {
        // initModeView();
        resetBalance();
        balanceNumberView.increaseInstantly();
    }

    @Override
    public void update() {
    	balanceNumberView.update();
    }

    @Override
    protected void draw(SpriteBatch batch, float parentAlpha) {
    	super.draw(batch, parentAlpha);
    	balanceNumberView.zdraw(batch, parentAlpha);
    }

    public void resetBalance() {
        balanceNumberView.setNumberToDraw(playerProfile.getCurrentBalance());
    }

    @Override
    public void onPause() {
        balanceNumberView.increaseInstantly();
    }

}
