/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.nice.utils;

import android.graphics.Point;
import android.graphics.Rect;
import com.boolbalabs.lib.utils.DebugLog;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 *         Date: 01/04/2011
 *         Time: 09:22
 */
public class ZLog extends DebugLog {

    private final static String TAG = "nICE";

    private ZLog() {
        super();
    }


    public static void i(String msg) {
        DebugLog.i(TAG, msg);
    }

    public static void i(int msg) {
        DebugLog.i(TAG, msg);
    }

    public static void i(float msg) {
        DebugLog.i(TAG, msg);
    }

    public static void i(double msg) {
        DebugLog.i(TAG, msg);
    }

    public static void i(Point msg) {
        DebugLog.i(TAG, "x: " + msg.x + " y: " + msg.y);
    }

    public static void i(Rect msg) {
        DebugLog.i(TAG, msg.toShortString());
    }

}
