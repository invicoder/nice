/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.utils;
/**
 * 
 * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
 */

import org.acra.*;
import org.acra.annotation.*;

import com.boolbalabs.nice.R;

import android.app.Application;

@ReportsCrashes(formKey="", //will not be used
		mailTo = "boolbalabs.errors@gmail.com",
        mode = ReportingInteractionMode.TOAST,
        forceCloseDialogAfterToast = false, // optional, default false
        resToastText = R.string.crash_toast_text)

public class CrashReports extends Application {
	@Override
    public void onCreate() {
		ZLog.i("ACRA init");
        ACRA.init(this); 
        ErrorReporter.getInstance().checkReportsOnApplicationStart();
        super.onCreate();
    }
}
