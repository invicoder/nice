/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.modes;

import com.boolbalabs.nice.R;
import com.boolbalabs.nice.gamecomponents.ProgressBar;
import com.boolbalabs.nice.settings.Settings;

/**
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class ModeQuick extends GameMode {

    
    private int totalSecondsPerBar;

    public ModeQuick(int id) {
        super(id);
        timedMode = true;
        initialProgressValue = ProgressBar.MAX_CAPACITY;
        unitsPerSecondDecrement = 800;
        hintDecrement=5;
        totalSecondsPerBar = ProgressBar.MAX_CAPACITY / unitsPerSecondDecrement;
        scoreloop_mode = 3;
      //  textRectOnTexture = TexturesManager.getInstance().getRectByFrameName("text_mode_quick.png");
    }

    @Override
    public int unitsForIncrementBar(int icicleNumber, int chainCount) {
        int iR = playerProfile.getIciclesRemovedOnCurrentLevel();
        int iTR = playerProfile.getIciclesToRemoveOnCurrentLevel();
        if (iR + icicleNumber >= iTR) {
            isLevelUp = true;
        } else {
            iR = iR + icicleNumber;
            isLevelUp = false;
            playerProfile.setIciclesRemovedOnCurrentLevel(iR);
        }
        return 0;
    }

    @Override
    public String getGameOverString() {
        String string = Settings.getInstance().getResources().getString(R.string.text_game_over_time);
        return string;
    }

   @Override
    public int useHint() {
        int secondsToSubtract = hintDecrement;
        secondsLeft = (int) ((float) progressUnits * totalSecondsPerBar / (float) ProgressBar.MAX_CAPACITY);
        if (secondsLeft - secondsToSubtract < 0) {
            secondsLeft = 0;
            return -progressUnits;
        } else {
            int unitsToAdd = (int) (((float) secondsToSubtract / (float) totalSecondsPerBar) * ProgressBar.MAX_CAPACITY);
            return -unitsToAdd;
        }
    }

    @Override
    public boolean hintAvailable() {
        return true;
    }

}
