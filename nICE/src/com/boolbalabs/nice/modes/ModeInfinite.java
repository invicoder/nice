/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.modes;

import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.nice.gamecomponents.ProgressBar;

/**
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class ModeInfinite extends GameMode {

    public ModeInfinite(int id) {
        super(id);
        timedMode = false;
        initialProgressValue = 0;
        unitsPerSecondDecrement = 0;
        hintDecrement = 5;
        scoreloop_mode = 4;
      //  textRectOnTexture = TexturesManager.getInstance().getRectByFrameName("text_mode_infinite.png");
    }

    @Override
    public int unitsForIncrementBar(int icicleNumber, int chainCount) {
        int iR = playerProfile.getIciclesRemovedOnCurrentLevel();
        int iTR = playerProfile.getIciclesToRemoveOnCurrentLevel();
        if (iR + icicleNumber >= iTR) {
            isLevelUp = true;
            return 0;
        } else {
            iR = iR + icicleNumber;
            isLevelUp = false;
            playerProfile.setIciclesRemovedOnCurrentLevel(iR);
            int unitsToAdd = (int) (((float) icicleNumber / (float) iTR) * ProgressBar.MAX_CAPACITY);
            return unitsToAdd;
        }
    }

    @Override
    public int useHint() {
        int iR = playerProfile.getIciclesRemovedOnCurrentLevel();
        int iTR = playerProfile.getIciclesToRemoveOnCurrentLevel();
        int iFH = hintDecrement;
        int unitsToAdd;
        if (iR - iFH < 0) {
            iR = 0;
            unitsToAdd = progressUnits;
        } else {
            iR = iR - iFH;
            unitsToAdd = (int) (((float) iFH / (float) iTR) * ProgressBar.MAX_CAPACITY);
        }
        playerProfile.setIciclesRemovedOnCurrentLevel(iR);
        return -unitsToAdd;
    }

    @Override
    public boolean hintAvailable() {
        return playerProfile.getIciclesRemovedOnCurrentLevel() >= hintDecrement;
    }
}
