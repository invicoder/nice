/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.modes;

import android.graphics.Rect;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.gamecomponents.IcicleStatic;
import com.boolbalabs.nice.gamecomponents.RepeatBonus;
import com.boolbalabs.nice.settings.Settings;

/**
 * 
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class GameMode {

	protected Rect textRectOnTexture;
	private int id;
	protected boolean timedMode;
	protected int initialProgressValue;
	protected int unitsPerSecondDecrement;
	protected PlayerProfile playerProfile;
	protected boolean isGameOver;
	protected boolean isLevelUp;
	public int progressUnits;
	protected int hintDecrement;
	protected int secondsLeft;
	// ** save/load data**//
	public boolean hasBeenPreviouslySaved;
	public int[][] icicleTypes;
	
	public int[][] iciclePowers;
	
	public int currentScore;
	public int maxScore = 0;
	public int currentBonus;
	public int maxChain;
	public int iciclesRemoved;
	public int scoreloop_mode = -1;
	
    public int stonesMax = 0;
	public int stonesOnField = 0;
	public RepeatBonus repeatBonus;

	public GameMode(int id) {
		this.id = id;
		isGameOver = false;
		isLevelUp = false;
		playerProfile = PlayerProfile.getInstance();
		icicleTypes = new int[Settings.ROWS][Settings.COLUMNS];
		
		iciclePowers = new int[Settings.ROWS][Settings.COLUMNS];
		repeatBonus = new RepeatBonus();
		Settings.getInstance().loadMode(this);
	}

	public int getId() {
		return id;
	}

	public boolean isTimedMode() {
		return timedMode;
	}

	public int getInitialProgressValue() {
		return initialProgressValue;
	}

	public int getUnitsPerSecondDecrement() {
		return unitsPerSecondDecrement;
	}

	/**
	 * Every mode should overwrite this method
	 * 
	 * @param icicleNumber
	 * @param chainCount
	 * @return
	 */
	public int unitsForIncrementBar(int icicleNumber, int chainCount) {
		return 0;
	}

	public void setCurrentProgressUnits(int progressUnits) {
		this.progressUnits = progressUnits;
	}

	public boolean isGameOver() {
		return isGameOver;
	}

	public void setIsGameOver(boolean isGameOver) {
		this.isGameOver = isGameOver;
	}

	public boolean isLevelUp() {
		return isLevelUp;
	}

	public Rect getTextureRect() {
		return textRectOnTexture;
	}

	public String getGameOverString() {
		return "";
	}

	public int useHint() {
		return 0;
	}

	public boolean hintAvailable() {
		return false;
	}

	public int getSecondsLeft() {
		return secondsLeft;
	}

	public void setData(int cScore, int cBonus, int maxChain, int iciclesRevoved) {
		this.currentScore = cScore;
		this.currentBonus = cBonus;
		this.maxChain = maxChain;
		this.iciclesRemoved = iciclesRevoved;

	}

	public void setIcicleTypes(int[][] indices) {
		for (int i = 0; i < Settings.ROWS; i++) {
			System.arraycopy(indices[i], 0, icicleTypes[i], 0, Settings.COLUMNS);
		}
	}
	
	public void setIciclePowers(int[][] indices) {
		for (int i = 0; i < Settings.ROWS; i++) {
			System.arraycopy(indices[i], 0, iciclePowers[i], 0, Settings.COLUMNS);
		}
	}
	
	protected void setAllIciclesUnpowered() {
		for (int i = 0; i < Settings.ROWS; i++) {
			for (int j = 0; j < Settings.COLUMNS; j++) {
				iciclePowers[i][j] = IcicleStatic.ICICLE;
			}
		}
	}

	public void resetMode() {
		IcicleStatic.generateHighEntropyIndices();
		int[][] indices = IcicleStatic.highEntropyIndices;
		setIcicleTypes(indices);
		setAllIciclesUnpowered();
		currentScore = 0;
		currentBonus = 1;
		iciclesRemoved = 0;
		progressUnits = getInitialProgressValue();
		maxChain = 0;
		Settings.SHAKES_LEFT = Settings.SHAKES_INITIAL;
		stonesMax = 0;
		stonesOnField = 0;
		repeatBonus.reset();
	}
	
	public void setLevel() {
		//Overrides in ModePuzzle
	}
}
