/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.modes;

import android.util.Log;
import android.widget.Toast;

import com.badlogic.gdx.Gdx;
import com.boolbalabs.nice.R;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.gamecomponents.IcicleStatic;
import com.boolbalabs.nice.gamecomponents.ProgressBar;
import com.boolbalabs.nice.level.GameLevelFactory;
import com.boolbalabs.nice.level.GameLevelFactoryXML;
import com.boolbalabs.nice.level.GameLevelStatic;
import com.boolbalabs.nice.settings.Settings;
import com.boolbalabs.nice.utils.ZLog;

/**
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class ModePuzzle extends GameMode {
	
    public ModePuzzle(int id) {
        super(id);

        Settings.SHAKES_LEFT = 0;
        timedMode = false;
        scoreloop_mode = 5;
    }

    @Override
    public int unitsForIncrementBar(int icicleNumber, int chainCount) {
        int iR = playerProfile.getIciclesRemovedOnCurrentLevel();
        int iTR = GameLevelStatic.iciclesToRemove[playerProfile.currentPuzzleLevel];
        if (iR + icicleNumber >= iTR) {
        		iciclesRemoved = 0;
        		Settings.SHAKES_LEFT = 0;
        		isLevelUp = true;
        } else {
            iR = iR + icicleNumber;
            isLevelUp = false;
            playerProfile.setIciclesRemovedOnCurrentLevel(iR);
        }
        return 0;
    }

    @Override
    public String getGameOverString() {
        String string = Settings.getInstance().getResources().getString(R.string.text_game_over_time);
        return string;
    }


    @Override
    public boolean hintAvailable() {
        return false;
    }
    
    @Override
    public void setLevel() {
    	int[][] indices = GameLevelFactory.getInstance().getCurrentLevel().getLevelTypes().clone();
    	setIcicleTypes(indices);
    	
		indices = GameLevelFactory.getInstance().getCurrentLevel().getLevelPowers().clone();
		setIciclePowers(indices);
    }
    
    @Override
	public void resetMode() {
    	setLevel();

		iciclesRemoved = 0;
		Settings.SHAKES_LEFT = 0;
	}

}
