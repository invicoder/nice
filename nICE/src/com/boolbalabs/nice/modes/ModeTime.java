/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.modes;

import com.boolbalabs.nice.R;
import com.boolbalabs.nice.gamecomponents.ProgressBar;
import com.boolbalabs.nice.settings.Settings;

/**
 * 
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class ModeTime extends GameMode {

	private int totalSecondsPerBar;

	public ModeTime(int id) {
		super(id);
		timedMode = true;
		initialProgressValue = ProgressBar.MAX_CAPACITY / 2;
		unitsPerSecondDecrement = 800 * 2;
		hintDecrement = 5;
		totalSecondsPerBar = ProgressBar.MAX_CAPACITY / unitsPerSecondDecrement;
		secondsLeft = totalSecondsPerBar / 2;
		scoreloop_mode = 2;
		// textRectOnTexture =
		// TexturesManager.getInstance().getRectByFrameName("text_mode_time.png");
	}

	@Override
	public int unitsForIncrementBar(int icicleNumber, int chainCount) {
		int secondsToAdd = Math.min(icicleNumber, 10);
		secondsLeft = (int) ((float) progressUnits * totalSecondsPerBar / (float) ProgressBar.MAX_CAPACITY);
		if (secondsLeft + secondsToAdd >= totalSecondsPerBar) {
			isLevelUp = true;
			return 0;
		} else {
			isLevelUp = false;
			int unitsToAdd = (int) (((float) secondsToAdd / (float) totalSecondsPerBar) * ProgressBar.MAX_CAPACITY);
			return unitsToAdd;
		}
	}

	@Override
	public String getGameOverString() {
		String string = Settings.getInstance().getResources().getString(R.string.text_game_over_time);
		return string;
	}

	@Override
	public int useHint() {
		int secondsToSubtract = hintDecrement;
		secondsLeft = (int) ((float) progressUnits * totalSecondsPerBar / (float) ProgressBar.MAX_CAPACITY);
		if (secondsLeft - secondsToSubtract < 0) {
			secondsLeft = 0;
			return -progressUnits;
		} else {
			int unitsToAdd = (int) (((float) secondsToSubtract / (float) totalSecondsPerBar) * ProgressBar.MAX_CAPACITY);
			return -unitsToAdd;
		}
	}

	@Override
	public boolean hintAvailable() {
		return true;
	}
}
