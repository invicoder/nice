/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.boolbalabs.lib.utils.ErrorReporter;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.extra.ShakeListener;
import com.boolbalabs.nice.extra.SoundService;
import com.boolbalabs.nice.settings.BonusUpgradeConstants;
import com.boolbalabs.nice.settings.Settings;

/**
 * 
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class NiceDialogs {

	public static final int DIALOG_MENU = 0;
	public static final int DIALOG_GAME_OVER = 1;
	public static final int DIALOG_EXIT = 2;
	public static final int DIALOG_SETTINGS = 3;
	public static final int DIALOG_CRASH = 4;
	public static final int DIALOG_RATEME = 5;
	public static final int DIALOG_HELP = 6;
	public static final int DIALOG_LEVEL_UP = 7;
	public static final int DIALOG_CONGRATULATIONS = 8;
	public static final int DIALOG_CONTINUE = 9;
	public static final int DIALOG_HELP_PUZZLE = 10;
	public static final int DIALOG_HELP_BONUS = 11;
	public static final int DIALOG_REPEAT_BONUS = 12;
	public static final int DIALOG_GET_MONEY = 13;
	public static final int DIALOG_HELP_BASIC = 14;
	public static final int DIALOG_HELP_PINGUIN = 15;
	public static final int DIALOG_HELP_STONE = 16;
	public static final int DIALOG_HELP_SHAKES = 17;
	public static final int DIALOG_HELP_WELCOME_BONUS = 18;
	public static final int DIALOG_HELP_MODE = 19;
	
	private NiceActivity niceActivity;

	// private final int buttonClickSoundId = R.raw.button_click;
	public NiceDialogs(NiceActivity niceActivity) {
		this.niceActivity = niceActivity;
	}

	protected Dialog createMenuDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);

		final View dialogView = factory.inflate(R.layout.dialog_menu, null);

		// ((TextView)
		// dialogView.findViewById(R.id.textview_exit)).setTypeface(Settings.MAIN_TYPEFACE);

		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);

		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
			}
		});

		Button btnMenu = (Button) dialogView.findViewById(R.id.btn_menu);
		btnMenu.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.showMainMenu();
			}
		});

		Button btnResume = (Button) dialogView.findViewById(R.id.btn_resume);
		btnResume.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
			}
		});

		Button btnRestart = (Button) dialogView.findViewById(R.id.btn_restart);
		btnRestart.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.restartMode();
			}
		});

		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

			public void onCancel(DialogInterface dialog) {
				niceActivity.resumeGame();
			}
		});

		dialog.setOnShowListener(new DialogInterface.OnShowListener() {

			public void onShow(DialogInterface dialog) {
				// dialogView.startAnimation(inFromTop);
			}
		});

		return dialog;
	}

	protected Dialog createGameOverDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_gameover, null);

		final TextView textView = ((TextView) dialogView.findViewById(R.id.textview_gameover));

		textView.setTypeface(Settings.MAIN_TYPEFACE);

		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(false);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.showMainMenu();
			}
		});

		Button btnMenu = (Button) dialogView.findViewById(R.id.btn_menu);
		btnMenu.setTypeface(Settings.MAIN_TYPEFACE);
		btnMenu.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.showMainMenu();
			}
		});

		Button btnRestart = (Button) dialogView.findViewById(R.id.btn_restart);
		btnRestart.setTypeface(Settings.MAIN_TYPEFACE);
		btnRestart.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.restartMode();
			}
		});

		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

			public void onCancel(DialogInterface dialog) {
				niceActivity.removeDialog(DIALOG_GAME_OVER);
			}
		});

		dialog.setOnShowListener(new DialogInterface.OnShowListener() {

			public void onShow(DialogInterface dialog) {
				textView.setText(PlayerProfile.getInstance().getCurrentMode().getGameOverString());
			}
		});

		return dialog;
	}

	protected Dialog createExitDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_exit, null);

		((TextView) dialogView.findViewById(R.id.textview_exit)).setTypeface(Settings.MAIN_TYPEFACE);

		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
			}
		});

		Button btnYes = (Button) dialogView.findViewById(R.id.btn_yes);
		btnYes.setTypeface(Settings.MAIN_TYPEFACE);
		btnYes.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.finish();
			}
		});

		Button btnNo = (Button) dialogView.findViewById(R.id.btn_no);
		btnNo.setTypeface(Settings.MAIN_TYPEFACE);
		btnNo.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
			}
		});

		return dialog;
	}

	protected Dialog createSettingsDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_settings, null);

		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);

		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
			}
		});

		Button soundButton = (Button) dialogView.findViewById(R.id.btn_settings_sound);
		int soundBgRes = ZageCommonSettings.soundEnabled ? R.drawable.btn_sound_on : R.drawable.btn_sound_off;
		soundButton.setBackgroundResource(soundBgRes);
		soundButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				ZageCommonSettings.soundEnabled = !ZageCommonSettings.soundEnabled;
				int soundBgRes = ZageCommonSettings.soundEnabled ? R.drawable.btn_sound_on : R.drawable.btn_sound_off;
				arg0.setBackgroundResource(soundBgRes);
			}
		});

		Button musicButton = (Button) dialogView.findViewById(R.id.btn_settings_music);
		int musicBgRes = ZageCommonSettings.musicEnabled ? R.drawable.btn_music_on : R.drawable.btn_music_off;
		musicButton.setBackgroundResource(musicBgRes);
		musicButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				ZageCommonSettings.musicEnabled = !ZageCommonSettings.musicEnabled;
				int musicBgRes = ZageCommonSettings.musicEnabled ? R.drawable.btn_music_on : R.drawable.btn_music_off;
				arg0.setBackgroundResource(musicBgRes);
				if (ZageCommonSettings.musicEnabled) {
					SoundService.startMusic01();
				} else {
					SoundService.pauseMusic();

				}

			}
		});

		final TextView shakeBarTextView = (TextView) dialogView.findViewById(R.id.seekbar_text);
		shakeBarTextView.setTypeface(Settings.MAIN_TYPEFACE);

		SeekBar sensitivityBar = (SeekBar) dialogView.findViewById(R.id.SensitivitySeekBar);
		sensitivityBar.setMax(100);
		sensitivityBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				ShakeListener.setSensitivity(progress);
				String shakeValue;
				if (progress < 40) {
					shakeValue = niceActivity.getString(R.string.shake_value_low);
				} else if (progress >= 40 && progress < 75) {
					shakeValue = niceActivity.getString(R.string.shake_value_meduim);
				} else {
					shakeValue = niceActivity.getString(R.string.shake_value_high);
				}
				String progressText = String.format(shakeValue, progress);
				shakeBarTextView.setText(progressText);
				Settings.SHAKE_SENSITIVITY = progress;
			}

			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			public void onStopTrackingTouch(SeekBar seekBar) {
			}

		});
		sensitivityBar.setProgress(Settings.SHAKE_SENSITIVITY);

		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

			public void onCancel(DialogInterface dialog) {
				Settings.getInstance().saveSharedPreferences();
				niceActivity.removeDialog(DIALOG_SETTINGS);
			}
		});
		
		final TextView noAds = (TextView) dialogView.findViewById(R.id.buynoads);
		noAds.setTypeface(Settings.MAIN_TYPEFACE);
		
		Button noAdsButton = (Button) dialogView.findViewById(R.id.noadsbutton);
		noAdsButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				niceActivity.buyNoAds();
			}
		});

		return dialog;
	}

	protected Dialog createCrashDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_crash, null);

		((TextView) dialogView.findViewById(R.id.textview_crash)).setTypeface(Settings.MAIN_TYPEFACE);

		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
			}
		});

		Button btnYes = (Button) dialogView.findViewById(R.id.btn_yes);
		btnYes.setTypeface(Settings.MAIN_TYPEFACE);
		btnYes.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				ErrorReporter.getInstance().sendReport(niceActivity);
				dialog.cancel();
			}
		});

		Button btnNo = (Button) dialogView.findViewById(R.id.btn_no);
		btnNo.setTypeface(Settings.MAIN_TYPEFACE);
		btnNo.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
			}
		});

		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

			public void onCancel(DialogInterface dialog) {
				ErrorReporter.getInstance().clearErrorsWithoutSending();
			}
		});

		return dialog;
	}

	protected Dialog createRateMeDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_rateme, null);

		((TextView) dialogView.findViewById(R.id.textview_rateme)).setTypeface(Settings.MAIN_TYPEFACE);

		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);

		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
			}
		});

		Button btnYes = (Button) dialogView.findViewById(R.id.btn_yes);
		btnYes.setTypeface(Settings.MAIN_TYPEFACE);
		btnYes.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				Settings.rateMeDialogHasBeenShown = true;
				Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(Settings.MARKET_URL));
				niceActivity.startActivity(i);
			}
		});

		Button btnNo = (Button) dialogView.findViewById(R.id.btn_no);
		btnNo.setTypeface(Settings.MAIN_TYPEFACE);
		btnNo.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
			}
		});

		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

			public void onCancel(DialogInterface dialog) {
				Settings.currentLaunch = 2;
			}
		});

		return dialog;
	}

	protected Dialog createHelpDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_help, null);

		((TextView) dialogView.findViewById(R.id.help_caption_1)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView)
		 dialogView.findViewById(R.id.help_caption_2)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView)
		 dialogView.findViewById(R.id.help_caption_3)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_caption_5)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_caption_6)).setTypeface(Settings.MAIN_TYPEFACE);
		 
//		 ((TextView)
//		 dialogView.findViewById(R.id.help_caption_4)).setTypeface(Settings.MAIN_TYPEFACE);
		((TextView) dialogView.findViewById(R.id.help_text_1)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView)
		 dialogView.findViewById(R.id.help_text_2)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView)
		 dialogView.findViewById(R.id.help_text_3)).setTypeface(Settings.MAIN_TYPEFACE);
//		 ((TextView)
//		 dialogView.findViewById(R.id.help_text_4)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_text_5)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_text_6)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_bonus_bomb)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_bonus_color_bomb)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_bonus_lightning)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_bonus_blue)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_bonus_yellow)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_bonus_purple)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_bonus_red)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_bonus_orange)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_bonus_white)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_bonus_green)).setTypeface(Settings.MAIN_TYPEFACE);

		 ((TextView) dialogView.findViewById(R.id.help_repeat_caption)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_repeat_text)).setTypeface(Settings.MAIN_TYPEFACE);
		 
		 ((TextView) dialogView.findViewById(R.id.help_caption_bomb)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_caption_color_bomb)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_caption_lightning)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_caption_blue)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_caption_yellow)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_caption_red)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_caption_orange)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_caption_white)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_caption_purple)).setTypeface(Settings.MAIN_TYPEFACE);
		 ((TextView) dialogView.findViewById(R.id.help_caption_green)).setTypeface(Settings.MAIN_TYPEFACE);
		 
		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
			}
		});

		return dialog;
	}
	
	protected Dialog createLevelUpDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_level_up, null);

		final TextView textView = ((TextView) dialogView.findViewById(R.id.textview_level_up));

		textView.setTypeface(Settings.MAIN_TYPEFACE);

		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(false);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.showMainMenu();
			}
		});

		Button btnMenu = (Button) dialogView.findViewById(R.id.btn_menu);
		btnMenu.setTypeface(Settings.MAIN_TYPEFACE);
		btnMenu.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.showMainMenu();
			}
		});

		Button btnRestart = (Button) dialogView.findViewById(R.id.btn_next);
		btnRestart.setTypeface(Settings.MAIN_TYPEFACE);
		btnRestart.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.restartMode();
			}
		});

		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

			public void onCancel(DialogInterface dialog) {
				niceActivity.removeDialog(DIALOG_LEVEL_UP);
			}
		});

		dialog.setOnShowListener(new DialogInterface.OnShowListener() {

			public void onShow(DialogInterface dialog) {
				textView.setText(R.string.text_level_up);
			}
		});

		return dialog;
	}

	protected Dialog createCongratulationsDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_congratulations, null);

		((TextView) dialogView.findViewById(R.id.textview_congratulations)).setTypeface(Settings.MAIN_TYPEFACE);

		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);

		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(false);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.showMainMenu();
			}
		});

		Button btnYes = (Button) dialogView.findViewById(R.id.btn_yes);
		btnYes.setTypeface(Settings.MAIN_TYPEFACE);
		btnYes.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.showMainMenu();
				Settings.rateMeDialogHasBeenShown = true;
				Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(Settings.MARKET_URL));
				niceActivity.startActivity(i);
			}
		});

		Button btnNo = (Button) dialogView.findViewById(R.id.btn_no);
		btnNo.setTypeface(Settings.MAIN_TYPEFACE);
		btnNo.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.showMainMenu();
			}
		});

		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

			public void onCancel(DialogInterface dialog) {
				Settings.currentLaunch = 2;
			}
		});

		return dialog;
	}
	
	protected Dialog createContinueDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_continue, null);

		((TextView) dialogView.findViewById(R.id.textview_continue)).setTypeface(Settings.MAIN_TYPEFACE);

		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
			}
		});

		Button btnYes = (Button) dialogView.findViewById(R.id.btn_yes);
		btnYes.setTypeface(Settings.MAIN_TYPEFACE);
		btnYes.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
			}
		});

		Button btnNo = (Button) dialogView.findViewById(R.id.btn_no);
		btnNo.setTypeface(Settings.MAIN_TYPEFACE);
		btnNo.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				PlayerProfile.getInstance().currentPuzzleLevel = 0;
				niceActivity.restartMode();
				niceActivity.showDialog(DIALOG_HELP_PUZZLE);
			}
		});

		return dialog;
	}
	
	protected Dialog createHelpPuzzleDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_help_puzzle, null);

		((TextView) dialogView.findViewById(R.id.help_puzzle_caption_1)).setTypeface(Settings.MAIN_TYPEFACE);
		
		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
			}
		});

		return dialog;
	}
	
	protected Dialog createBonusDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_help_bonus, null);

		((TextView) dialogView.findViewById(R.id.help_caption_bonus)).setText("This string will be changed");
		((TextView) dialogView.findViewById(R.id.help_caption_bonus)).setTypeface(Settings.MAIN_TYPEFACE);
		
		((ImageView) dialogView.findViewById(R.id.help_image)).setBackgroundResource(R.drawable.blue);
		
		((TextView) dialogView.findViewById(R.id.help_bonus)).setText("This string will be changed");
		((TextView) dialogView.findViewById(R.id.help_bonus)).setTypeface(Settings.MAIN_TYPEFACE);

		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
			}
		});

		return dialog;
	}
	
	protected Dialog createRepeatBonusDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_repeat_bonus, null);

		 ((TextView) dialogView.findViewById(R.id.help_repeat_caption)).setTypeface(Settings.MAIN_TYPEFACE);

		 ((TextView) dialogView.findViewById(R.id.help_repeat_text)).setTypeface(Settings.MAIN_TYPEFACE);

		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.resumeGame();
			}
		});

		return dialog;
	}
	
	protected Dialog createGetMoneyDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_get_money, null);

		((TextView) dialogView.findViewById(R.id.get_money)).setTypeface(Settings.MAIN_TYPEFACE);
		((TextView) dialogView.findViewById(R.id.x100)).setTypeface(Settings.MAIN_TYPEFACE);
		((TextView) dialogView.findViewById(R.id.x500)).setTypeface(Settings.MAIN_TYPEFACE);
		((TextView) dialogView.findViewById(R.id.x1000)).setTypeface(Settings.MAIN_TYPEFACE);
		((TextView) dialogView.findViewById(R.id.x10000)).setTypeface(Settings.MAIN_TYPEFACE);

		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
			}
		});
		
		//TODO: set events onClick for getting game coins
		Button btnX100 = (Button) dialogView.findViewById(R.id.btn_buy_x100);
		btnX100.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				niceActivity.addMoney(BonusUpgradeConstants.x1id);
				
			}
		});
		
		Button btnX500 = (Button) dialogView.findViewById(R.id.btn_buy_x500);
		btnX500.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				niceActivity.addMoney(BonusUpgradeConstants.x2id);
				
			}
		});
		
		Button btnX1000 = (Button) dialogView.findViewById(R.id.btn_buy_x1000);
		btnX1000.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				niceActivity.addMoney(BonusUpgradeConstants.x3id);
				
			}
		});
		
		Button btnX10000 = (Button) dialogView.findViewById(R.id.btn_buy_x10000);
		btnX10000.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				niceActivity.addMoney(BonusUpgradeConstants.x4id);
				
			}
		});

		return dialog;
	}
	
	protected Dialog createHelpBasicDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_help_basic, null);

		((TextView) dialogView.findViewById(R.id.help_caption_1)).setTypeface(Settings.MAIN_TYPEFACE);
		
		((TextView) dialogView.findViewById(R.id.help_text_1)).setTypeface(Settings.MAIN_TYPEFACE);
		
		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.resumeGame();
			}
		});

		return dialog;
	}
	
	protected Dialog createHelpPinguinDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_help_pinguin, null);

		 ((TextView) dialogView.findViewById(R.id.help_caption_3)).setTypeface(Settings.MAIN_TYPEFACE);

		 ((TextView) dialogView.findViewById(R.id.help_text_3)).setTypeface(Settings.MAIN_TYPEFACE);

		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.resumeGame();
			}
		});

		return dialog;
	}
	
	protected Dialog createHelpStoneDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_help_stone, null);

		 ((TextView) dialogView.findViewById(R.id.help_caption_5)).setTypeface(Settings.MAIN_TYPEFACE);

		 ((TextView) dialogView.findViewById(R.id.help_text_5)).setTypeface(Settings.MAIN_TYPEFACE);

		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.resumeGame();
			}
		});

		return dialog;
	}
	
	protected Dialog createHelpShakesDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_help_shakes, null);

		 ((TextView) dialogView.findViewById(R.id.help_caption_6)).setTypeface(Settings.MAIN_TYPEFACE);

		 ((TextView) dialogView.findViewById(R.id.help_text_6)).setTypeface(Settings.MAIN_TYPEFACE);

		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.resumeGame();
			}
		});

		return dialog;
	}
	
	protected Dialog createHelpWelcomeBonusDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_welcome_bonus, null);

		((TextView) dialogView.findViewById(R.id.help_welcome_caption)).setTypeface(Settings.MAIN_TYPEFACE);
		
		((TextView) dialogView.findViewById(R.id.help_welcome_text)).setTypeface(Settings.MAIN_TYPEFACE);
		
		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.resumeGame();
			}
		});

		return dialog;
	}
	
	protected Dialog createHelpModeDialog() {

		LayoutInflater factory = LayoutInflater.from(niceActivity);
		final View dialogView = factory.inflate(R.layout.dialog_help_mode, null);

		 ((TextView) dialogView.findViewById(R.id.mode_caption)).setTypeface(Settings.MAIN_TYPEFACE);

		 ((TextView) dialogView.findViewById(R.id.mode_text)).setTypeface(Settings.MAIN_TYPEFACE);

		final Dialog dialog = new Dialog(niceActivity, R.style.Dialog_NICE);
		dialog.setContentView(dialogView);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		Button btnCancel = (Button) dialogView.findViewById(R.id.btn_settings_cross);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				playClick();
				dialog.cancel();
				niceActivity.resumeGame();
			}
		});

		return dialog;
	}
	
	// ************** TOASTS *******************//
	public Toast createLoadingToast() {
		Toast loadingToast;
		LayoutInflater factory = niceActivity.getLayoutInflater();
		View layout = factory.inflate(R.layout.loading_toast, null);
		loadingToast = new Toast(niceActivity);
		loadingToast.setDuration(Toast.LENGTH_LONG);
		loadingToast.setView(layout);
		return loadingToast;
	}

	private void playClick() {
		SoundService.playButtonClick();
	}
}
