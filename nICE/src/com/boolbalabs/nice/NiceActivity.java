/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.boolbalabs.lib.utils.Constants;
import com.boolbalabs.lib.utils.ErrorReporter;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.extra.SoundService;
import com.boolbalabs.nice.menu.BonusMenu;
import com.boolbalabs.nice.settings.BonusUpgradeConstants;
import com.boolbalabs.nice.settings.Settings;
import com.boolbalabs.nice.settings.StaticConstants;
import com.boolbalabs.nice.utils.ZLog;
import com.google.ads.*;

public class NiceActivity extends AndroidApplication {

	private LinearLayout parentView;
	private NiceGame niceGame;
	private Settings settings;
	private Handler activityHandler;
	// **dialogs**//
	private NiceDialogs niceDialogs;
	private int currentDialogId;
	private Dialog menuDialog;
	private Dialog gameOverDialog;
	private Dialog exitDialog;
	private Dialog settingsDialog;
	private Dialog crashDialog;
	private Dialog ratemeDialog;
	private Dialog helpDialog;
	private Dialog levelUpDialog;
	private Dialog congratulationsDialog;
	private Dialog continueDialog;
	private Dialog helpPuzzleDialog;
	private Dialog helpBonusDialog;
	private Dialog helpRepeatBonusDialog;
	private Dialog getMoneyDialog;
	private Dialog helpBasicDialog;
	private Dialog helpPinguinDialog;
	private Dialog helpStoneDialog;
	private Dialog helpShakesDialog;
	private Dialog helpWelcomeBonusDialog;
	private Dialog helpModeDialog;
	
	private AdView adView;
	
	@Override
	public void onCreate(android.os.Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		parentView = new LinearLayout(this);
		
		initServices();
		createDialogsAndToasts();
		
		NiceToasts.showToast(NiceToasts.LOADING_TOAST);
		
		Settings.currentLaunch++;

		// ****** nICE GAME *****//
		niceGame = new NiceGame();
		/* creating handler */
		activityHandler = createHandler();
		niceGame.setActivityHandler(activityHandler);
		initialize(niceGame, true);
		
		initializeAds();
		
		InAppBillingManager.getInstance().createInAppBillingService(this);
//		ScoreloopManagerSingleton.init(this);
	}
	
	private void initServices() {
		SharedPreferences prefs = getSharedPreferences(StaticConstants.TAG, MODE_PRIVATE);
		ScreenMetrics.initScreenMetrics(this);
		SoundService.initialize(this);
		ErrorReporter.getInstance().initialize(this);
		Settings.initialise(prefs, getApplicationContext().getResources());
		settings = Settings.getInstance();

		TexturesManager.init(getResources());
		ZLog.i(ScreenMetrics.resolution_postfix);
		// using MDPI resources as LDPI
		if (ScreenMetrics.resolution_postfix.equals("lres")) {
			ScreenMetrics.resolution_postfix = "mres";
		} else if (ScreenMetrics.resolution_postfix.equals("xhres")) {
			ScreenMetrics.resolution_postfix = "hres";
		}

		settings.loadSharedPreferences();
		settings.getDeviceInfo();
		PlayerProfile.initialise(prefs);
		settings.loadBalanceData();
		
		InAppBillingManager.initialise();

	}
	
	private void initializeAds() {
		Settings.getInstance().loadAdsState();
		if(!Settings.isAdsEnabled) {
			return;
		}
		adView = new AdView(this, AdSize.BANNER, "a14cb2cefabffdb");
		parentView.setGravity(Gravity.BOTTOM);
		parentView.addView(adView);
		rootLayout.addView(parentView);
		AdRequest adRequest = new AdRequest();
		//adRequest.addTestDevice("FF809D446CA19259E162A37765DF311D"); 
		adView.loadAd(adRequest);
	}

	@Override
	protected void onPause() {
		// Debug.stopMethodTracing();
		settings.saveBalanceData();
		if (niceGame != null) {
			if (niceGame.isPaused()) {
				return;
			}
			niceGame.pause();
		}
		super.onPause();
		
	}

	@Override
	protected void onResume() {
		super.onResume();
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		resumeGame();
		// Debug.startMethodTracing("nICE");
	}

	@Override
	protected void onDestroy() {
		adView.destroy();
		InAppBillingManager.getInstance().unbindServices();
		SoundService.dispose();
		super.onDestroy();
		System.runFinalization();
		System.exit(0);
	}

	@Override
	public void onStart() {
		super.onStart();
		InAppBillingManager.getInstance().registerServices();
//		 FlurryAgent.onStartSession(this, "SWSPCJII9XFND5HWPYXI");
	}

	@Override
	public void onStop() {
		settings.saveBalanceData();
		super.onStop();
		InAppBillingManager.getInstance().unregisterServices();
//		 FlurryAgent.onEndSession(this);
	}

	private Handler createHandler() {
		return new Handler() {

			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case StaticConstants.MESSAGE_EXIT_APPLICATION:
					finish();
					break;
				case Constants.MESSAGE_RESTART_SOUND_MANAGER:
					SoundService.restartSoundManager(msg, getApplicationContext(), activityHandler);
					break;
				case StaticConstants.MESSAGE_SHOW_MENU:
					// showMenu();
					break;
				case StaticConstants.MESSAGE_GAME_OVER:
					showGameOverDialog();
					break;
				case StaticConstants.MESSAGE_SHOW_SETTINGS:
					showSettingsDialog();
					break;
				case StaticConstants.MESSAGE_DIALOG_HELP:
					showHelpDialog();
					break;
				case StaticConstants.MESSAGE_SHOW_DIALOG_CRASH:
					showDialogCrash();
					break;
				case StaticConstants.MESSAGE_SHOW_DIALOG_RATEME:
					showRatemeDialog();
					break;
				case StaticConstants.MESSAGE_SUBMIT_SCORELOOP:
					submitScore();
					break;		
				case StaticConstants.MESSAGE_SHOW_SCORES:
					showHighScores();
					break;
				case StaticConstants.MESSAGE_LEVEL_UP:
					showLevelUpDialog();
					break;
				case StaticConstants.MESSAGE_CONGRATULATIONS:
					showCongratulationsDialog();
					break;
				case StaticConstants.MESSAGE_CONTINUE:
					showContinueDialog();
					break;
				case StaticConstants.MESSAGE_HELP_PUZZLE:
					showHelpPuzzleDialog();
					break;
				case StaticConstants.MESSAGE_HELP_BONUS:
					showHelpBonusDialog();
					break;
				case StaticConstants.MESSAGE_REPEAT_BONUS:
					showRepeatBonusDialog();
					break;
				case StaticConstants.MESSAGE_ADD_MONEY:
					showGetMoneyDialog();
					break;
				case StaticConstants.MESSAGE_HELP_BASIC:
					showHelpBasicDialog();
					break;
				case StaticConstants.MESSAGE_HELP_PINGUIN:
					showHelpPinguinDialog();
					break;
				case StaticConstants.MESSAGE_HELP_STONE:
					showHelpStoneDialog();
					break;
				case StaticConstants.MESSAGE_HELP_SHAKES:
					showHelpShakesDialog();
					break;
				case StaticConstants.MESSAGE_HELP_WELCOME_BONUS:
					showHelpWelcomeBonusDialog();
					break;
				case StaticConstants.MESSAGE_HELP_MODE:
					showHelpModeDialog();
					break;
				default:
					break;
				}
			}
		};
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (!settings.isKeyAllowed(keyCode)) {
			return false;
		}
		if(niceGame == null) {
			return false;
		}
		if ((keyCode == KeyEvent.KEYCODE_SEARCH)) {
			// do nothing
			return false;
		} else if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			switch (niceGame.getCurrentGameSceneId()) {
			case NiceGame.SCENE_GAMEPLAY:
				showMenu();
				return true;
			case NiceGame.SCENE_MENU:
				showExitDialog();
				return true;
			case NiceGame.SCENE_BONUS:
				settings.saveBalanceData();
				niceGame.switchGameScene(NiceGame.SCENE_MENU);
				return true;
			default:
				return false;
				//return super.onKeyDown(keyCode, event);
			}
		}
		// default behaviour
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (!isMenuAvailable()) {
			return false;
		}
		onKeyDown(KeyEvent.KEYCODE_BACK, null);
		return true;
//		switch (niceGame.getCurrentGameSceneId()) {
//		case NiceGame.SCENE_GAMEPLAY:
//			showMenu();
//			return true;
//		case NiceGame.SCENE_MENU:
//			showExitDialog();
//			return true;
//		default:
//			return true;
//		}
	}

	private boolean isMenuAvailable() {
		if (niceGame == null) {
			return false;
		}
		return true;
	}

	// ****************** DIALOGS ****************//
	private void createDialogsAndToasts() {
		niceDialogs = new NiceDialogs(this);
		menuDialog = niceDialogs.createMenuDialog();
		gameOverDialog = niceDialogs.createGameOverDialog();
		exitDialog = niceDialogs.createExitDialog();
		settingsDialog = niceDialogs.createSettingsDialog();
		crashDialog = niceDialogs.createCrashDialog();
		ratemeDialog = niceDialogs.createRateMeDialog();
		helpDialog = niceDialogs.createHelpDialog();
		levelUpDialog = niceDialogs.createLevelUpDialog();
		congratulationsDialog = niceDialogs.createCongratulationsDialog();
		continueDialog = niceDialogs.createContinueDialog();
		helpPuzzleDialog = niceDialogs.createHelpPuzzleDialog();
		helpBonusDialog = niceDialogs.createBonusDialog();
		helpRepeatBonusDialog = niceDialogs.createRepeatBonusDialog();
		getMoneyDialog = niceDialogs.createGetMoneyDialog();
		helpBasicDialog = niceDialogs.createHelpBasicDialog();
		helpPinguinDialog = niceDialogs.createHelpPinguinDialog();
		helpStoneDialog = niceDialogs.createHelpStoneDialog();
		helpShakesDialog = niceDialogs.createHelpShakesDialog();
		helpWelcomeBonusDialog = niceDialogs.createHelpWelcomeBonusDialog();
		helpModeDialog = niceDialogs.createHelpModeDialog();
		
		NiceToasts.initialize(this);
	}

	private void showMenu() {
		if (niceGame != null) {
			if (niceGame.isPaused()) {
				return;
			}
			niceGame.pause();
		}
		showDialog(NiceDialogs.DIALOG_MENU);
	}

	public void resumeGame() {
		if (niceGame != null && niceGame.isPaused()) {
			niceGame.resume();
		}
	}

	private void showGameOverDialog() {
		showDialog(NiceDialogs.DIALOG_GAME_OVER);
	}

	private void showSettingsDialog() {
		showDialog(NiceDialogs.DIALOG_SETTINGS);
	}

	private void showExitDialog() {
		showDialog(NiceDialogs.DIALOG_EXIT);
	}

	private void showDialogCrash() {
		showDialog(NiceDialogs.DIALOG_CRASH);
	}

	private void showHelpDialog() {
		showDialog(NiceDialogs.DIALOG_HELP);
	}

	private void showRatemeDialog() {
		showDialog(NiceDialogs.DIALOG_RATEME);
	}
	
	private void showLevelUpDialog() {
		showDialog(NiceDialogs.DIALOG_LEVEL_UP);
	}
	
	private void showCongratulationsDialog() {
		showDialog(NiceDialogs.DIALOG_CONGRATULATIONS);
	}
	
	private void showContinueDialog() {
		showDialog(NiceDialogs.DIALOG_CONTINUE);
	}
	
	private void showHelpPuzzleDialog() {
		showDialog(NiceDialogs.DIALOG_HELP_PUZZLE);
	}
	
	private void showHelpBonusDialog() {
		if(BonusMenu.getUpgradedBonus() != -1) {
			((TextView) helpBonusDialog.findViewById(R.id.help_bonus)).setText(BonusMenu.bonusHelpTextId[BonusMenu.getUpgradedBonus()]);
			((TextView) helpBonusDialog.findViewById(R.id.help_caption_bonus)).setText(BonusMenu.bonusHelpCaptionId[BonusMenu.getUpgradedBonus()]);
			
			((ImageView) helpBonusDialog.findViewById(R.id.help_image)).setBackgroundResource(BonusMenu.bonusHelpImageId[BonusMenu.getUpgradedBonus()]);
		}
		showDialog(NiceDialogs.DIALOG_HELP_BONUS);
	}
	
	private void showGetMoneyDialog() {
		showDialog(NiceDialogs.DIALOG_GET_MONEY);
	}
	
	private void showHelpBasicDialog() {
		if (niceGame != null) {
			if (niceGame.isPaused()) {
				return;
			}
			niceGame.pause();
		}
		showDialog(NiceDialogs.DIALOG_HELP_BASIC);
	}
	
	private void showRepeatBonusDialog() {
		if (niceGame != null) {
			if (niceGame.isPaused()) {
				return;
			}
			niceGame.pause();
		}
		showDialog(NiceDialogs.DIALOG_REPEAT_BONUS);
	}
	
	private void showHelpPinguinDialog() {
		if (niceGame != null) {
			if (niceGame.isPaused()) {
				return;
			}
			niceGame.pause();
		}
		showDialog(NiceDialogs.DIALOG_HELP_PINGUIN);
	}
	
	private void showHelpStoneDialog() {
		if (niceGame != null) {
			if (niceGame.isPaused()) {
				return;
			}
			niceGame.pause();
		}
		showDialog(NiceDialogs.DIALOG_HELP_STONE);
	}
	
	private void showHelpShakesDialog() {
		if (niceGame != null) {
			if (niceGame.isPaused()) {
				return;
			}
			niceGame.pause();
		}
		showDialog(NiceDialogs.DIALOG_HELP_SHAKES);
	}
	
	private void showHelpWelcomeBonusDialog() {
		showDialog(NiceDialogs.DIALOG_HELP_WELCOME_BONUS);
	}
	
	private void showHelpModeDialog() {
		if (niceGame != null) {
			if (niceGame.isPaused()) {
				return;
			}
			niceGame.pause();
		}
		switch(PlayerProfile.getInstance().getCurrentModeId())
		{
		case PlayerProfile.MODE_CLASSIC:
			((TextView) helpModeDialog.findViewById(R.id.mode_caption)).setText(R.string.classic_caption);
			((TextView) helpModeDialog.findViewById(R.id.mode_text)).setText(R.string.classic_text);
			((ImageView) helpModeDialog.findViewById(R.id.mode_image)).setBackgroundResource(R.drawable.classic);
			showDialog(NiceDialogs.DIALOG_HELP_MODE);
			break;
		case PlayerProfile.MODE_TIME:
			((TextView) helpModeDialog.findViewById(R.id.mode_caption)).setText(R.string.time_caption);
			((TextView) helpModeDialog.findViewById(R.id.mode_text)).setText(R.string.time_text);
			((ImageView) helpModeDialog.findViewById(R.id.mode_image)).setBackgroundResource(R.drawable.time);
			showDialog(NiceDialogs.DIALOG_HELP_MODE);
			break;
		case PlayerProfile.MODE_QUICK:
			((TextView) helpModeDialog.findViewById(R.id.mode_caption)).setText(R.string.quick_caption);
			((TextView) helpModeDialog.findViewById(R.id.mode_text)).setText(R.string.quick_text);
			((ImageView) helpModeDialog.findViewById(R.id.mode_image)).setBackgroundResource(R.drawable.quick);
			showDialog(NiceDialogs.DIALOG_HELP_MODE);
			break;
		case PlayerProfile.MODE_PUZZLE:
			((TextView) helpModeDialog.findViewById(R.id.mode_caption)).setText(R.string.puzzle_caption);
			((TextView) helpModeDialog.findViewById(R.id.mode_text)).setText(R.string.puzzle_text);
			((ImageView) helpModeDialog.findViewById(R.id.mode_image)).setBackgroundResource(R.drawable.puzzle);
			showDialog(NiceDialogs.DIALOG_HELP_MODE);
			break;
		default:
			return;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		final Dialog dialog;
		currentDialogId = id;
		switch (id) {
		case NiceDialogs.DIALOG_MENU:
			return menuDialog;
		case NiceDialogs.DIALOG_GAME_OVER:
			return gameOverDialog;
		case NiceDialogs.DIALOG_EXIT:
			return exitDialog;
		case NiceDialogs.DIALOG_SETTINGS:
			return settingsDialog;
		case NiceDialogs.DIALOG_CRASH:
			return crashDialog;
		case NiceDialogs.DIALOG_RATEME:
			return ratemeDialog;
		case NiceDialogs.DIALOG_HELP:
			return helpDialog;
		case NiceDialogs.DIALOG_LEVEL_UP:
			return levelUpDialog;
		case NiceDialogs.DIALOG_CONGRATULATIONS:
			return congratulationsDialog;
		case NiceDialogs.DIALOG_CONTINUE:
			return continueDialog;
		case NiceDialogs.DIALOG_HELP_PUZZLE:
			return helpPuzzleDialog;
		case NiceDialogs.DIALOG_HELP_BONUS:
			return helpBonusDialog;
		case NiceDialogs.DIALOG_REPEAT_BONUS:
			return helpRepeatBonusDialog;
		case NiceDialogs.DIALOG_GET_MONEY:
			return getMoneyDialog;
		case NiceDialogs.DIALOG_HELP_BASIC:
			return helpBasicDialog;
		case NiceDialogs.DIALOG_HELP_PINGUIN:
			return helpPinguinDialog;
		case NiceDialogs.DIALOG_HELP_STONE:
			return helpStoneDialog;
		case NiceDialogs.DIALOG_HELP_SHAKES:
			return helpShakesDialog;
		case NiceDialogs.DIALOG_HELP_WELCOME_BONUS:
			return helpWelcomeBonusDialog;
		case NiceDialogs.DIALOG_HELP_MODE:
			return helpModeDialog;
		default:
			dialog = null;
			break;
		}
		return dialog;
	}

	public void showMainMenu() {
		if (niceGame != null) {
			resumeGame();
			niceGame.switchGameScene(NiceGame.SCENE_MENU);

		}
	}

	public void restartMode() {
		if (niceGame != null) {
			niceGame.restartMode();
		}
	}
	
	
	
	//****** SCORELOOP START *****//
	private final int SCORELOOP_LEVEL=1;
	private void showHighScores() {
//		final Intent intent = new Intent(NiceActivity.this, LeaderboardsScreenActivity.class);
//		// optionally specify the leaderboard that will open by default
//		intent.putExtra(LeaderboardsScreenActivity.LEADERBOARD, LeaderboardsScreenActivity.LEADERBOARD_GLOBAL);
//		intent.putExtra(LeaderboardsScreenActivity.MODE, SCORELOOP_LEVEL);
//		submitScore();
//		startActivity(intent);
	}


	protected void submitScore() {
//		GameMode m = PlayerProfile.getInstance().getCurrentMode();
//		if(m==null){
//			return;
//		}
//		double score = m.maxScore;
//		com.scoreloop.client.android.core.model.Score s = new com.scoreloop.client.android.core.model.Score(score, null);
//		s.setLevel(SCORELOOP_LEVEL);
//		s.setMode(m.scoreloop_mode);
//		ScoreloopManagerSingleton.get().onGamePlayEnded(s);
	}
	
	//****** SCORELOOP END *****//
	
	public void buyNoAds() {
		InAppBillingManager.getInstance().buyNoAds();
	}
	
	public void addMoney(int id) {
		InAppBillingManager.getInstance().addMoney(id);
	}
}
