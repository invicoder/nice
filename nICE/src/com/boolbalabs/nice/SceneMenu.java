/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice;

import com.boolbalabs.lib.game.ZScene;
import com.boolbalabs.lib.utils.ErrorReporter;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.menu.MainMenu;
import com.boolbalabs.nice.settings.Settings;
import com.boolbalabs.nice.settings.StaticConstants;

/**
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class SceneMenu extends ZScene {

    private PlayerProfile playerProfile;
    private Settings settings;
    private boolean hasCrashed = false;

    private MainMenu mainMenu;


    public SceneMenu(float width, float height, boolean stretch, int id) {
        super(width, height, stretch, id);
        playerProfile = PlayerProfile.getInstance();
        settings = Settings.getInstance();

        mainMenu = new MainMenu(this);

    }
    
    @Override
    public void onShow() {

        checkCrash();
        checkRateMe();
    }    

    private void checkCrash() {
        hasCrashed = ErrorReporter.getInstance().hasCrashed();
        if (hasCrashed) {
            sendEmptyMessageToActivity(StaticConstants.MESSAGE_SHOW_DIALOG_CRASH);
        }
    }
    
    private void checkRateMe() {
        if (Settings.currentLaunch > Settings.LAUNCHES_BEFORE_SHOW_RATE_ME && !hasCrashed && !Settings.rateMeDialogHasBeenShown) {
            sendEmptyMessageToActivity(StaticConstants.MESSAGE_SHOW_DIALOG_RATEME);
        }
    }
}
