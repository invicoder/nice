/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice;

import com.boolbalabs.lib.game.ZScene;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.menu.BonusMenu;
import com.boolbalabs.nice.settings.Settings;
/**
 * 
 * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
 */
public class SceneBonus extends ZScene{

	 private PlayerProfile playerProfile;
	    private Settings settings;
	    
	    private BonusMenu bonusMenu;
	
	public SceneBonus(float width, float height, boolean stretch, int id) {
		super(width, height, stretch, id);
		playerProfile = PlayerProfile.getInstance();
        settings = Settings.getInstance();

        bonusMenu = new BonusMenu(this);
	}
	
	@Override
	public void draw() {
		super.draw();
	}

}
