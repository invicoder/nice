package com.boolbalabs.nice;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

/**
 * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
 */

public class NiceToasts {

	public static final int LOADING_TOAST = 0;
	
	private static Toast loadingToast;
	private static boolean isRunning = false;
	
	public static void initialize(NiceActivity niceActivity) {
		createLoadingToast(niceActivity);
	}

	public static Toast createLoadingToast(NiceActivity niceActivity) {
		LayoutInflater factory = niceActivity.getLayoutInflater();
		View layout = factory.inflate(R.layout.loading_toast, null);
		loadingToast = new Toast(niceActivity);
		loadingToast.setDuration(Toast.LENGTH_LONG);
		loadingToast.setView(layout);
		return loadingToast;
	}
	
	public static void showToast(int id) {
		switch(id) {
		case LOADING_TOAST:
			showLoadingToast();
			break;
		default:
			return;
		}
	}
	
	/*
	 * WARNING! IS A HACK!
	 * This function hacks Toast for showing it custom duration
		because Toast.setDuration(int) can show only LENGTH_LONG and LENGTH_SMALL.
		All other toasts should be shown only like: toast.show().
		if you need to show customDuration toast, you should add isRunning2
		and create new Thread.
		BUT AVOID THIS! This is made JUST for slow devices, because loading in this case
		is very very very long...*/
	private static void showLoadingToast() {
		isRunning = true;
		
		Thread t = new Thread() {			
			public void run() {
		        try {
		        	while (isRunning) {
		        		loadingToast.show();
			            sleep(1850);
                    }
		        } catch (Exception e) {
		        }
			}
		};
		t.start();
	}
	
	public static void hideLoadingToast() {
		isRunning = false;
	}
	
}
