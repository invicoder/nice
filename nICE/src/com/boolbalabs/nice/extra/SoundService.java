/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.extra;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.boolbalabs.lib.managers.SoundManager;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.nice.R;

/**
 * 
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class SoundService {

	private static SoundManager soundManager;

	public static void initialize(Context context) {
		SoundManager.init(context);
		soundManager = SoundManager.getInstance();

		int[] soundRefs = new int[] { R.raw.button_click, R.raw.gameover, R.raw.hint, R.raw.icedrop, R.raw.levelup, R.raw.nohint, R.raw.remove,
				R.raw.startgame, R.raw.wrongmove, R.raw.bomb, R.raw.color_bomb, R.raw.repeat_bonus, R.raw.power_icicle };
		soundManager.addShortSounds(soundRefs);

		soundManager.addLoopingSound(R.raw.music01);
	}

	private static void playSound(int soundRef) {
		if (soundManager != null && ZageCommonSettings.soundEnabled) {
			soundManager.playShortSound(soundRef);
		}
	}
	
	public static void playRepeatBonus() {
		playSound(R.raw.repeat_bonus);
	}
	
	public static void playBomb() {
		playSound(R.raw.bomb);
	}
	
	public static void playPowerIcicle() {
		playSound(R.raw.power_icicle);
	}
	
	public static void playColorBomb() {
		playSound(R.raw.color_bomb);
	}

	public static void playIceDrop() {
		playSound(R.raw.icedrop);
	}

	public static void playButtonClick() {
		playSound(R.raw.button_click);
	}

	public static void playGameOver() {
		playSound(R.raw.gameover);
	}

	public static void playHint() {
		playSound(R.raw.hint);
	}

	public static void playNoHint() {
		playSound(R.raw.nohint);
	}

	public static void playLevelUp() {
		playSound(R.raw.levelup);
	}

	public static void playRemove(float playSpeed) {
		if (soundManager != null && ZageCommonSettings.soundEnabled) {
			soundManager.playShortSound(R.raw.remove, playSpeed);
		}
	}

	public static void playStartGame() {
		playSound(R.raw.startgame);
	}

	public static void playWrongMove() {
		playSound(R.raw.wrongmove);
	}

	public static void dispose() {
		SoundManager.release();
	}

	// ********** music *********//
	public static void startMusic01() {
		if (soundManager != null && ZageCommonSettings.musicEnabled && !soundManager.isPlaying()) {
			soundManager.playLoopingSound(R.raw.music01);
		}
	}

	public static void pauseMusic() {
		if (soundManager != null) {
			soundManager.pauseLoopingSounds();
		}
	}

	public static void stopMusic() {
		if (soundManager != null) {
			soundManager.stopAllPlayingSounds();
		}
	}

	// ******* SOUND MANAGER ******//
	public static void restartSoundManager(Message msg, Context ctx, Handler activityHandler) {
		String error = msg.getData().getString("error");
		soundManager = SoundManager.getInstance();
		try {
			if (soundManager != null) {
				soundManager.stopAllPlayingSounds();
			}
			SoundManager.release();
			SoundService.initialize(ctx);
			soundManager.setHandler(activityHandler);
			if (soundManager != null && ZageCommonSettings.musicEnabled) {
				// reset the mediaplayer as it is in error state
				soundManager.playLoopingSound(R.raw.music01);
			}
		} catch (Exception e) {
			if (e != null) {
				if (e.getMessage() == null) {
					Log.w("SOUNDMANAGER", "e.getMessage() is null");
				} else {
					Log.w("SOUNDMANAGER", e.getMessage());
				}
			}

		}
	}
	
}
