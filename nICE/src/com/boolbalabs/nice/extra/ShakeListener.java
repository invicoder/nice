/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.extra;

import android.hardware.SensorManager;
import android.os.SystemClock;
import com.badlogic.gdx.Gdx;

public class ShakeListener {

    private static int FORCE_THRESHOLD = 320;/* between 220 and 420 */

    private static int TIME_THRESHOLD = 150;/* between 50 and 250 */

    private static int SHAKE_TIMEOUT = 500;/* between 350 and 650 */

    private static int SHAKE_DURATION = 500;/* between 350 and 650 */

    private static int SHAKE_COUNT = 3;
    private float mLastX = -1.0f, mLastY = -1.0f, mLastZ = -1.0f;
    private long mLastTime;
    private int mShakeCount = 0;
    private long mLastShake;
    private long mLastForce;
    // ** params **//
    private static final int FORCE_THRESHOLD_MIN = 220;
    private static final int FORCE_THRESHOLD_MAX = 420;
    private static final int TIME_THRESHOLD_MIN = 50;
    private static final int TIME_THRESHOLD_MAX = 250;
    private static final int SHAKE_TIMEOUT_MIN = 350;
    private static final int SHAKE_TIMEOUT_MAX = 650;
    private static final int SHAKE_DURATION_MIN = 350;
    private static final int SHAKE_DURATION_MAX = 650;

    public static void setSensitivity(int percentage) {
        /* alpha = 0 - low sensitive, alpha = 1 - very sensitive */
        float alpha = 0.5f;
        if (percentage < 0) {
            alpha = 0;
        } else if (percentage > 100) {
            alpha = 1;
        } else {
            alpha = percentage / 100.0f;
        }
        FORCE_THRESHOLD = (int) (alpha * FORCE_THRESHOLD_MIN + (1 - alpha) * FORCE_THRESHOLD_MAX);
        TIME_THRESHOLD = (int) (alpha * TIME_THRESHOLD_MIN + (1 - alpha) * TIME_THRESHOLD_MAX);
        SHAKE_TIMEOUT = (int) (alpha * SHAKE_TIMEOUT_MAX + (1 - alpha) * SHAKE_TIMEOUT_MIN);
        SHAKE_DURATION = (int) (alpha * SHAKE_DURATION_MIN + (1 - alpha) * SHAKE_DURATION_MAX);
    }

    public ShakeListener() {
    }

    public boolean onSensorChanged() {

        boolean result = false;

        long currentTime = SystemClock.uptimeMillis();

        if ((currentTime - mLastForce) > SHAKE_TIMEOUT) {
            mShakeCount = 0;
        }

        float dataX = Gdx.input.getAccelerometerX();
        float dataY = Gdx.input.getAccelerometerY();
        float dataZ = Gdx.input.getAccelerometerZ();

        if ((currentTime - mLastTime) > TIME_THRESHOLD) {
            long diff = currentTime - mLastTime;
            float speed = Math.abs(dataX + dataY + dataZ - mLastX - mLastY - mLastZ)
                    / diff * 10000;
            if (speed > FORCE_THRESHOLD) {
                if ((++mShakeCount >= SHAKE_COUNT) && (currentTime - mLastShake > SHAKE_DURATION)) {
                    mLastShake = currentTime;
                    mShakeCount = 0;
                    result = true;
                }
                mLastForce = currentTime;
            }
            mLastTime = currentTime;
            mLastX = dataX;
            mLastY = dataY;
            mLastZ = dataZ;
        }

        return result;
    }
}
