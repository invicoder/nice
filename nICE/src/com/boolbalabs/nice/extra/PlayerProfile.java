/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.extra;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.boolbalabs.nice.gamecomponents.Icicle;
import com.boolbalabs.nice.gamecomponents.ProgressBar;
import com.boolbalabs.nice.gamecomponents.RepeatBonus;
import com.boolbalabs.nice.level.GameLevelFactoryXML;
import com.boolbalabs.nice.modes.GameMode;
import com.boolbalabs.nice.modes.ModeClassic;
import com.boolbalabs.nice.modes.ModeInfinite;
import com.boolbalabs.nice.modes.ModePuzzle;
import com.boolbalabs.nice.modes.ModeQuick;
import com.boolbalabs.nice.modes.ModeTime;
import com.boolbalabs.nice.settings.Settings;
import com.boolbalabs.nice.utils.ZLog;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com> Date: 03/03/2011 Time: 15:08
 */
public class PlayerProfile {

	private static final int MAX_BOMB_UPGRADE_LEVEL = 3;
	private static final int MAX_POWER_GEM_UPGRADE_LEVEL = 4;
	private static final int MAX_COLOR_BOMB_UPGRADE_LEVEL = 1;
	
	// ****** PROFILE *****//
	private int currentScore;
	public int longestChain;
	public int bonusMultiplier;
	/**
	 * how many icicles have been removed on current level
	 */
	private int iciclesRemovedOnCurrentLevel;
	/**
	 * how many icicles a player should remove to reach next level
	 */
	private int iciclesToRemoveOnCurrentLevel;
	/**
	 * units per 50 ms to subtract
	 */
	private static PlayerProfile playerProfile;
	private static boolean isInitialised = false;
	private static final Object isInitialisedLock = new Object();
	private SharedPreferences sharedPreferences;
	private Settings settings;
	// ****** GAME MODES *****//
	public static final int MODE_CLASSIC = 0;
	public static final int MODE_TIME = 1;
	public static final int MODE_QUICK = 2;
	public static final int MODE_INFINITE = 3;
	public static final int MODE_PUZZLE = 4;
	private ModeClassic gameModeClassic;
	private ModeInfinite gameModeInfinite;
	private ModeQuick gameModeQuick;
	private ModeTime gameModeTime;
	private ModePuzzle gameModePuzzle;
	private GameMode currentMode;
	// for 10 points give additional 3 seconds
	public float secondsToPointsRatio = 0.33f;
	public float secondsLeft;
	public static final float SECONDS_MAX = 60;
	public final static int I_TO_REMOVE = 80;
	
    //** bonuses **//
    private int currentBalance = 0;
   
    private int bombUpgradeLevel = 0;
    private int powerGemUpgradeLevel = 0;
    private int colorBombUpgradeLevel = 0;
    private int yellowUpgradeLevel = 0;
    private int purpleUpgradeLevel = 0;
    private int redUpgradeLevel = 0;
    private int blueUpgradeLevel = 0;
    private int whiteUpgradeLevel = 0;
    private int greenUpgradeLevel = 0;
    private int orangeUpgradeLevel = 0;
    public int currentPuzzleLevel = 0;
    public boolean hasBeenPreviouslySaved = false;

	// ****************************************************************************************************//
	public static PlayerProfile getInstance() {
		return playerProfile;
	}

	public static void initialise(SharedPreferences sharedPreferences) {
		synchronized (isInitialisedLock) {
			if (isInitialised) {
				return;
			}
			playerProfile = new PlayerProfile();
			playerProfile.sharedPreferences = sharedPreferences;
			playerProfile.createGameModes();
			playerProfile.loadProfile();
			isInitialised = true;
		}
	}

	public static void release() {
		synchronized (isInitialisedLock) {
			if (!isInitialised) {
				return;
			}
			if (playerProfile != null) {
				playerProfile = null;
			}
			isInitialised = false;
		}

	}

	private PlayerProfile() {
		settings = Settings.getInstance();
	}

	private void createGameModes() {
		gameModeClassic = new ModeClassic(MODE_CLASSIC);
		gameModeInfinite = new ModeInfinite(MODE_INFINITE);
		gameModeQuick = new ModeQuick(MODE_QUICK);
		gameModeTime = new ModeTime(MODE_TIME);
		gameModePuzzle = new ModePuzzle(MODE_PUZZLE);
		currentMode = gameModeClassic;
	}

	// /***** SAVE/LOAD ****///
	public void saveProfile() {
		if (currentMode != null) {
			settings.saveMode(currentMode);
		}
		settings.saveBalanceData();
	}

	public void loadProfile() {
		if (currentMode == null) {
			return;
		}
		currentScore = currentMode.currentScore;
		bonusMultiplier = currentMode.currentBonus;
		iciclesRemovedOnCurrentLevel = currentMode.iciclesRemoved;
		iciclesToRemoveOnCurrentLevel = I_TO_REMOVE * bonusMultiplier;
		longestChain = currentMode.maxChain;
	}

	// **** OTHER ***//
	public void increaseCurrentBalance(int number) {
		currentBalance += number;
	}
	
	public void addStoneToField(int stones) {
		if(stones == -1) {
			currentMode.stonesMax -= 1;
		}
		currentMode.stonesOnField += stones;
	}
	
	public int getStonesMax() {
		return currentMode.stonesMax;
	}
	
	public int getStonesOnField() {
		return currentMode.stonesOnField;
	}
	
	public int getCurrentBalance() {
		return currentBalance;
	}

	public int getBombUpgradeLevel() {
		return bombUpgradeLevel;
	}
	
	public int getPowerGemUpgradeLevel() {
		return powerGemUpgradeLevel;
	}
	
	public int getColorBombUpgradeLevel() {
		return colorBombUpgradeLevel;
	}
	
	public int getYellowUpgradeLevel() {
		return yellowUpgradeLevel;
	}
	
	public int getPurpleUpgradeLevel() {
		return purpleUpgradeLevel;
	}
	
	public int getRedUpgradeLevel() {
		return redUpgradeLevel;
	}
	
	public int getBlueUpgradeLevel() {
		return blueUpgradeLevel;
	}
	
	public int getWhiteUpgradeLevel() {
		return whiteUpgradeLevel;
	}
	
	public int getGreenUpgradeLevel() {
		return greenUpgradeLevel;
	}
	
	public int getOrangeUpgradeLevel() {
		return orangeUpgradeLevel;
	}
		
	public void setCurrentBalance(int balance) {
		currentBalance = balance;
	}
	
	public void setBombUpgradeLevel(int level) {
		bombUpgradeLevel = level;
	}
	
	public void setPowerGemUpgradeLevel(int level) {
		powerGemUpgradeLevel = level;
	}
	
	public void setColorBombUpgradeLevel(int level) {
		colorBombUpgradeLevel = level;
	}
	
	public void setYellowUpgradeLevel(int yellowUpgradeLevel) {
		this.yellowUpgradeLevel = yellowUpgradeLevel;
	}

	public void setPurpleUpgradeLevel(int purpleUpgradeLevel) {
		this.purpleUpgradeLevel = purpleUpgradeLevel;
	}

	public void setRedUpgradeLevel(int redUpgradeLevel) {
		this.redUpgradeLevel = redUpgradeLevel;
	}

	public void setBlueUpgradeLevel(int blueUpgradeLevel) {
		this.blueUpgradeLevel = blueUpgradeLevel;
	}

	public void setWhiteUpgradeLevel(int whiteUpgradeLevel) {
		this.whiteUpgradeLevel = whiteUpgradeLevel;
	}

	public void setGreenUpgradeLevel(int greenUpgradeLevel) {
		this.greenUpgradeLevel = greenUpgradeLevel;
	}

	public void setOrangeUpgradeLevel(int orangeUpgradeLevel) {
		this.orangeUpgradeLevel = orangeUpgradeLevel;
	}
	
	public boolean isBombUpgradeAvailable() {
		return bombUpgradeLevel < MAX_BOMB_UPGRADE_LEVEL;
	}
	
	public boolean isPowerGemUpgradeAvailable() {
		return powerGemUpgradeLevel < MAX_POWER_GEM_UPGRADE_LEVEL;
	}
	
	public boolean isColorBombUpgradeAvailable() {
		return colorBombUpgradeLevel < MAX_COLOR_BOMB_UPGRADE_LEVEL;
	}
	
	public boolean isYellowUpgradeAvailable() {
		return yellowUpgradeLevel < MAX_COLOR_BOMB_UPGRADE_LEVEL;
	}
	
	public boolean isRedUpgradeAvailable() {
		return redUpgradeLevel < MAX_COLOR_BOMB_UPGRADE_LEVEL;
	}
	
	public boolean isGreenUpgradeAvailable() {
		return greenUpgradeLevel < MAX_COLOR_BOMB_UPGRADE_LEVEL;
	}
	
	public boolean isPurpleUpgradeAvailable() {
		return purpleUpgradeLevel < MAX_COLOR_BOMB_UPGRADE_LEVEL;
	}
	
	public boolean isWhiteUpgradeAvailable() {
		return whiteUpgradeLevel < MAX_COLOR_BOMB_UPGRADE_LEVEL;
	}
	
	public boolean isOrangeUpgradeAvailable() {
		return orangeUpgradeLevel < MAX_COLOR_BOMB_UPGRADE_LEVEL;
	}
	
	public boolean isBlueUpgradeAvailable() {
		return blueUpgradeLevel < MAX_COLOR_BOMB_UPGRADE_LEVEL;
	}
	
	public void upgradeBomb() {
		bombUpgradeLevel++;
	}
	
	public void upgradePowerGem() {
		powerGemUpgradeLevel++;
	}
	
	public void upgradeColorBomb() {
		colorBombUpgradeLevel++;
	}
	
	public void upgradeYellow() {
		yellowUpgradeLevel++;
	}
	
	public void upgradeRed() {
		redUpgradeLevel++;
	}
	
	public void upgradeGreen() {
		greenUpgradeLevel++;
	}
	
	public void upgradeBlue() {
		blueUpgradeLevel++;
	}
	
	public void upgradePurple() {
		purpleUpgradeLevel++;
	}
	
	public void upgradeOrange() {
		orangeUpgradeLevel++;
	}
	
	public void upgradeWhite() {
		whiteUpgradeLevel++;
	}
	
    public boolean isBombAvailable() {
    	return (bombUpgradeLevel != 0);
    }
    
    public boolean isColorBombAvailable() {
    	return (colorBombUpgradeLevel != 0);
    }
    
    public boolean isPowerGemAvailable() {
    	return (powerGemUpgradeLevel != 0);
    }
    
    public boolean isYellowAvailable() {
    	return (yellowUpgradeLevel != 0);
    }
    
    public boolean isRedAvailable() {
    	return (redUpgradeLevel != 0);
    }
    
    public boolean isBlueAvailable() {
    	return (blueUpgradeLevel != 0);
    }
    
    public boolean isGreenAvailable() {
    	return (greenUpgradeLevel != 0);
    }
    
    public boolean isWhiteAvailable() {
    	return (whiteUpgradeLevel != 0);
    }
    
    public boolean isOrangeAvailable() {
    	return (orangeUpgradeLevel != 0);
    }
    
    public boolean isPurpleAvailable() {
    	return (purpleUpgradeLevel != 0);
    }
	
	public int getCurrentScore() {
		return currentScore;
	}

	public void onLevelUp() {
		bonusMultiplier++;
		iciclesRemovedOnCurrentLevel = 0;
		iciclesToRemoveOnCurrentLevel = I_TO_REMOVE * bonusMultiplier;
	}

	public int getIciclesToRemoveOnCurrentLevel() {
		return iciclesToRemoveOnCurrentLevel;
	}

	public int getIciclesRemovedOnCurrentLevel() {
		return iciclesRemovedOnCurrentLevel;
	}

	public void setIciclesRemovedOnCurrentLevel(int iciclesRemovedOnCurrentLevel) {
		this.iciclesRemovedOnCurrentLevel = iciclesRemovedOnCurrentLevel;
	}

	public int getUnitsPerSecondDecrement() {
		return currentMode.getUnitsPerSecondDecrement();
	}

	public int getCurrentModeId() {
		if (currentMode != null) {
			return currentMode.getId();
		}
		return -1;
	}

	public GameMode getCurrentMode() {
		return currentMode;
	}

	public void setCurrentMode(int newMode) {
		switch (newMode) {
		case MODE_CLASSIC:
			currentMode = gameModeClassic;
			break;
		case MODE_INFINITE:
			currentMode = gameModeInfinite;
			break;
		case MODE_TIME:
			currentMode = gameModeTime;
			break;
		case MODE_QUICK:
			currentMode = gameModeQuick;
			break;
		case MODE_PUZZLE:
			gameModePuzzle.setLevel();
			currentMode = gameModePuzzle;
			break;
		default:
			break;
		}
	}

	public void resetSession() {
		if (currentMode == null) {
			return;
		}
		currentMode.resetMode();
		currentScore = 0;
		bonusMultiplier = 1;
		longestChain = 0;
		iciclesRemovedOnCurrentLevel = 0;
		iciclesToRemoveOnCurrentLevel = I_TO_REMOVE * bonusMultiplier;
	}

	public void incrementScore(int scoreToAdd) {
		currentScore += scoreToAdd;
		if (currentScore >= currentMode.maxScore) {
			currentMode.maxScore = currentScore;
		}
	}

	public boolean isTimedMode() {
		return currentMode.isTimedMode();
	}

	// ***** MODES ****//
	public int getInitialProgressValue() {
		return currentMode.getInitialProgressValue();
	}

}
