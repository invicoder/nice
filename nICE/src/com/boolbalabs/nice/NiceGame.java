/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.FPSLogger;
import com.boolbalabs.lib.game.ZGame;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.nice.extra.SoundService;
import com.boolbalabs.nice.level.GameLevelFactory;

/**
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class NiceGame extends ZGame {

    FPSLogger fpsLogger = new FPSLogger();
    public final static int SCENE_MENU = 1;
    public final static int SCENE_GAMEPLAY = 2;
    public final static int SCENE_BONUS = 3;
    private SceneGameplay sceneGameplay;
    private SceneMenu sceneMenu;
    private SceneBonus sceneBonus;

    /**
     * called in onSurfaceCreated()
     */
    @Override
    public void create() {    	
        Gdx.input.setInputProcessor(this);

        final String[] names = new String[]{"nicetex_bg_", "nicetex_bg2_", "nicetex_gameplay_",
            "nicetex_icicles_", "nicetex_bonus_", "nicetex_menu_","nicetex_menubg_", "nicetex_water_", "nicetex_bonus_bg_"};
        TexturesManager.getInstance().loadTexturesFromFiles("img", names, true);
        
        sceneGameplay = new SceneGameplay(ScreenMetrics.screenWidthRip, ScreenMetrics.screenHeightRip, false, SCENE_GAMEPLAY);
        registerGameScene(sceneGameplay);
        
        NiceToasts.hideLoadingToast();
        
        sceneMenu = new SceneMenu(ScreenMetrics.screenWidthRip, ScreenMetrics.screenHeightRip, false, SCENE_MENU);
        registerGameScene(sceneMenu);
        
        
        sceneBonus = new SceneBonus(ScreenMetrics.screenWidthRip, ScreenMetrics.screenHeightRip, false, SCENE_BONUS);
        registerGameScene(sceneBonus);

        switchGameScene(SCENE_MENU);
        
        SoundService.startMusic01();
        
		GameLevelFactory.initialize();
        
        
    }

    @Override
    public void render() {
        super.render();
        fpsLogger.log();
    }

    @Override
    public void resume() {
        super.resume();
        if (currentScene != null) {
            currentScene.onResume();
        }
        SoundService.startMusic01();
    }

    @Override
    public void pause() {
        if (currentScene != null) {
            currentScene.onPause();
        }
        SoundService.pauseMusic();
        super.pause();
    }

    @Override
    public void dispose() {
    }

    public void restartMode() {
        if (sceneGameplay != null && currentScene == sceneGameplay) {
            sceneGameplay.restartScene();
        }
    }
}
