package com.boolbalabs.nice;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.Log;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.iab.BillingService;
import com.boolbalabs.nice.iab.BillingService.RequestPurchase;
import com.boolbalabs.nice.iab.BillingService.RestoreTransactions;
import com.boolbalabs.nice.iab.Consts;
import com.boolbalabs.nice.iab.Consts.PurchaseState;
import com.boolbalabs.nice.iab.Consts.ResponseCode;
import com.boolbalabs.nice.iab.PurchaseObserver;
import com.boolbalabs.nice.iab.ResponseHandler;
import com.boolbalabs.nice.settings.BonusUpgradeConstants;
import com.boolbalabs.nice.settings.Settings;

public class InAppBillingManager {
	// Singleton
	private static InAppBillingManager inAppBillingManager;
	private static boolean isInitialised = false;
	private static final Object isInitialisedLock = new Object();

	private Activity activity;
	// --------- In App billing -------------
	private static final String TAG = "nICE";

	private BillingService mBillingService;
	private NicePurchaseObserver mNicePurchaseObserver;
	private Handler mHandler;

	private boolean billingAvailable = false;

	/** An array of product list entries for the products that can be purchased. */
	private static final CatalogEntry[] CATALOG = new CatalogEntry[] { 
			new CatalogEntry("com.boolbalabs.nice.coinpack01", Managed.UNMANAGED), 
			new CatalogEntry("com.boolbalabs.nice.coinpack02", Managed.UNMANAGED), 
			new CatalogEntry("com.boolbalabs.nice.coinpack03", Managed.UNMANAGED), 
			new CatalogEntry("com.boolbalabs.nice.coinpack05", Managed.UNMANAGED),
			new CatalogEntry("com.boolbalabs.nice.noads", Managed.MANAGED)
		};

	public static InAppBillingManager getInstance() {
		return inAppBillingManager;
	}

	public static void initialise() {
		synchronized (isInitialisedLock) {
			if (isInitialised) {
				return;
			}
			inAppBillingManager = new InAppBillingManager();
			isInitialised = true;
		}
	}
	
	public void createInAppBillingService(Activity niceActivity) {
		mHandler = new Handler();
		mNicePurchaseObserver = new NicePurchaseObserver(niceActivity,
				mHandler);
		mBillingService = new BillingService();
		mBillingService.setContext(niceActivity);
		
		activity = niceActivity;

		// Check if billing is supported.
		ResponseHandler.register(mNicePurchaseObserver);
		if (!mBillingService.checkBillingSupported()) {
			billingAvailable = false;
			Log.i(TAG, "NO IAB");
		} else {
			billingAvailable = true;
		}
	}
	
	public void registerServices() {
		ResponseHandler.register(mNicePurchaseObserver);
	}
	
	public void unregisterServices() {
		ResponseHandler.unregister(mNicePurchaseObserver);
	}
	
	public void unbindServices(){
		mBillingService.unbind();
	}

	public boolean billingAlailable() {
		return isOnline() && billingAvailable;
	}

	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm == null) {
			return false;
		} else {
			NetworkInfo info = cm.getActiveNetworkInfo();
			if (info == null){
				return false;
			}
			return info.isConnectedOrConnecting();
		}
	}

	// /*********************** IN APP BILLING ********************////////
	/**
	 * Each product in the catalog is either MANAGED or UNMANAGED. MANAGED means
	 * that the product can be purchased only once per user (such as a new level
	 * in a game). The purchase is remembered by Android Market and can be
	 * restored if this application is uninstalled and then re-installed.
	 * UNMANAGED is used for products that can be used up and purchased multiple
	 * times (such as poker chips). It is up to the application to keep track of
	 * UNMANAGED products for the user.
	 */
	private enum Managed {
		MANAGED, UNMANAGED
	}

	/**
	 * A {@link PurchaseObserver} is used to get callbacks when Android Market
	 * sends messages to this application so that we can update the UI.
	 */
	private class NicePurchaseObserver extends PurchaseObserver {
		public NicePurchaseObserver(Activity niceActivity, Handler handler) {
			super(niceActivity, handler);
		}

		@Override
		public void onBillingSupported(boolean supported) {
			if (Consts.DEBUG) {
				Log.i(TAG, "supported: " + supported);
			}
			if (supported) {
				billingAvailable = true;
				restorePurchases();
			} else {
				billingAvailable = false;
			}
		}

		@Override
		public void onPurchaseStateChange(PurchaseState purchaseState,
				String itemId, int quantity, long purchaseTime,
				String developerPayload) {
			if (Consts.DEBUG) {
				Log.i(TAG, "onPurchaseStateChange() itemId: " + itemId + " "
						+ purchaseState);
			}

			if (purchaseState == PurchaseState.PURCHASED) {
				// mOwnedItems.add(itemId);
				Log.i("itemId", itemId);
				// NoAds
				if (itemId.equals(CATALOG[0].sku)) {
					PlayerProfile.getInstance().increaseCurrentBalance(BonusUpgradeConstants.x1AddMoney);
					//Settings.ADS_ENABLED = false;
					//TODO: Settings.getInstance().saveAdsEnabled();
					//FlurryAgent.onEvent("NoAds purchased)");
				} else if (itemId.equals(CATALOG[1].sku)) {
					PlayerProfile.getInstance().increaseCurrentBalance(BonusUpgradeConstants.x2AddMoney);
					//Settings.ADS_ENABLED = false;
					//TODO: Settings.getInstance().saveAdsEnabled();
					//FlurryAgent.onEvent("NoAds purchased)");
				} else if (itemId.equals(CATALOG[2].sku)) {
					PlayerProfile.getInstance().increaseCurrentBalance(BonusUpgradeConstants.x3AddMoney);
					//Settings.ADS_ENABLED = false;
					//TODO: Settings.getInstance().saveAdsEnabled();
					//FlurryAgent.onEvent("NoAds purchased)");
				} else if (itemId.equals(CATALOG[3].sku)) {
					PlayerProfile.getInstance().increaseCurrentBalance(BonusUpgradeConstants.x4AddMoney);
					//Settings.ADS_ENABLED = false;
					//TODO: Settings.getInstance().saveAdsEnabled();
					//FlurryAgent.onEvent("NoAds purchased)");
				} else if(itemId.equals(CATALOG[4].sku)) {
					Settings.isAdsEnabled = false;
					Settings.getInstance().saveAdsState();
				}
				// else if(itemId.equals(CATALOG[1].sku)){
				// playerProfile.increaseCurrentBalance(10000);
				// }
				// playerProfile.saveProfile();
				// if(paperJetGame!=null) {
				// paperJetGame.adjustShop();
				// }
			}
			// mCatalogAdapter.setOwnedItems(mOwnedItems);
			// mOwnedItemsCursor.requery();
		}

		@Override
		public void onRequestPurchaseResponse(RequestPurchase request,
				ResponseCode responseCode) {
			if (Consts.DEBUG) {
				Log.d(TAG, request.mProductId + ": " + responseCode);
			}
			if (responseCode == ResponseCode.RESULT_OK) {
				if (Consts.DEBUG) {
					Log.i(TAG, "purchase was successfully sent to server");
				}
				Log.i(request.mProductId, "sending purchase request");
			} else if (responseCode == ResponseCode.RESULT_USER_CANCELED) {
				if (Consts.DEBUG) {
					Log.i(TAG, "user canceled purchase");
				}
				Log.i(request.mProductId, "dismissed purchase dialog");
			} else {
				if (Consts.DEBUG) {
					Log.i(TAG, "purchase failed");
				}
				Log.i(request.mProductId, "request purchase returned "
						+ responseCode);
			}
		}

		@Override
		public void onRestoreTransactionsResponse(RestoreTransactions request,
				ResponseCode responseCode) {
			if (responseCode == ResponseCode.RESULT_OK) {
				if (Consts.DEBUG) {
					Log.d(TAG, "completed RestoreTransactions request");
				}
				Settings.getInstance().savePurchasesInitialized();
			} else {
				if (Consts.DEBUG) {
					Log.d(TAG, "RestoreTransactions error: " + responseCode);
				}
			}
		}
		
		
	}
	
	private void restorePurchases() {
        boolean initialized = Settings.getInstance().purchasesInitialized();
        if (!initialized) {
            mBillingService.restoreTransactions();
        }
    }

	private static class CatalogEntry {
		public String sku;
		public Managed managed;

		public CatalogEntry(String sku, Managed managed) {
			this.sku = sku;
			this.managed = managed;
		}
	}

	//
	// public void buyCoinPack(int coinPackIndex) {
	// CatalogEntry entry = CATALOG[coinPackIndex];
	// if (Consts.DEBUG) {
	// Log.d(TAG, "buying: " + coinPackIndex + " sku: " + entry.sku);
	// }
	// if (!mBillingService.requestPurchase(entry.sku, null)) {
	// Log.i(TAG,"Cannot make purchase");
	// }
	// }

	public void buyNoAds() {
		CatalogEntry entry = CATALOG[4];
		if (Consts.DEBUG) {
			Log.d(TAG, "buying noads. sku: " + entry.sku);
		}
		if (!mBillingService.requestPurchase(entry.sku, null)) {
			Log.i(TAG, "Cannot make purchase: " + entry.sku);
		}
	}
	
	public void addMoney(int id) {
		CatalogEntry entry = CATALOG[id];
		if (Consts.DEBUG) {
			Log.d(TAG, "buying coins. sku: " + entry.sku);
		}
		if (!mBillingService.requestPurchase(entry.sku, null)) {
			Log.i(TAG, "Cannot make purchase: " + entry.sku);
		}
	}
}
