/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.OnActionCompleted;
import com.badlogic.gdx.scenes.scene2d.actions.Parallel;
import com.badlogic.gdx.scenes.scene2d.actions.RotateBy;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateInterpolator;
import com.badlogic.gdx.scenes.scene2d.interpolators.LinearInterpolator;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.nice.SceneGameplay;
import com.boolbalabs.nice.utils.ZLog;

/**
 *
 * @author Igor Trubnikov <itrubnikov@gmail.com>
 */
public class StarGlitter extends ZNode {

    private Rectangle starRectOnScreen;
    private SceneGameplay sceneGameplay;
    //*******************ACTIONS**************************//
    private boolean isActing;
    private ScaleTo scaleIn;
    private ScaleTo scaleOut;
    private Sequence scaleSequence;
    private RotateBy rotateBy;
    private Parallel totalAction;
    private OnActionCompleted onActionCompleted;
    private float centerX;
    private float centerY;
    //***********************************************************//

    public StarGlitter(SceneGameplay sceneGameplay) {
        super("StarGlitter");
        parentScene = sceneGameplay;
        this.sceneGameplay = (SceneGameplay) parentScene;
        region = TexturesManager.getInstance().getRegionByName("star_glitter");
        starRectOnScreen = new Rectangle(0, 0, 33, 33);
        setScreenFrame(starRectOnScreen);
        visible = false;
    }

    @Override
    public void update() {
        if (!isActing) {
            return;
        }
        originX = width / 2;
        originY = height / 2;
        act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
    }

    private void startAction() {
        scaleIn = ScaleTo.$(1, 1, 0.8f);
        scaleOut = ScaleTo.$(0, 0, 0.8f);
        scaleSequence = Sequence.$(scaleIn, scaleOut);
        rotateBy = RotateBy.$(360, 1.6f);
        totalAction = Parallel.$(rotateBy, scaleSequence);
        scaleIn.setInterpolator(AccelerateInterpolator.$());
        scaleOut.setInterpolator(AccelerateInterpolator.$());
        rotateBy.setInterpolator(LinearInterpolator.$());
        totalAction.setCompletionListener(new OnActionCompleted() {

            public void completed(Action action) {
                if (action == totalAction) {
                    stopZoom();
                }
            }
        });
        action(totalAction);
    }

    public void startZoom(float xPos, float yPos) {
        isActing = true;
        centerX = xPos;
        centerY = yPos;
        x = centerX - width / 2;
        y = centerY - height / 2;
        scaleX = 0;
        scaleY = 0;
        visible = true;
        startAction();
    }

    public void stopZoom() {
        visible = false;
        rotation = 0;
        isActing = false;
        clearActions();
    }
//    @Override
//    public void onPause() {
//        stopZoom();
//        super.onPause();
//    }
}
