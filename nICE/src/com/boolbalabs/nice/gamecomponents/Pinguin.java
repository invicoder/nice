/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import android.os.SystemClock;
import android.util.Log;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.boolbalabs.lib.elements.ZButton;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.nice.SceneGameplay;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.gamecomponents.IceFactory.IceFactoryState;
import com.boolbalabs.nice.modes.ModePuzzle;
import com.boolbalabs.nice.settings.Settings;
import com.boolbalabs.nice.utils.ZLog;

/**
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class Pinguin extends ZNode {
	
    private SceneGameplay sceneGameplay;
    private final int skipFromTop = 380;
    private Rectangle screenRect = new Rectangle(0, ScreenMetrics.screenHeightRip - skipFromTop - 93, 65, 93);
    private TexturesManager texturesManager;
    //** HINT TEXT **//
    private ZNode hintTextView;
    private Rectangle hintScreenRect = new Rectangle(50, ScreenMetrics.screenHeightRip - skipFromTop + 15 - 39, 68, 54);
    private long startTimeVisible;
    private long durationVisible = 1000;
    
    private boolean isNeededToRestart;
    
    private ZButton restartButton;
    private ZButton.ClickListener clickListener;
    private Rectangle restartButtonRect = new Rectangle(0, ScreenMetrics.screenHeightRip - skipFromTop - 15 - 70, 70, 84);

    //*******************************//
    public Pinguin(SceneGameplay sceneGameplay) {
        super("Pinguin");
        parentScene = sceneGameplay;
        this.sceneGameplay = (SceneGameplay) parentScene;
        texturesManager = TexturesManager.getInstance();
        initWithFrame(screenRect, texturesManager.getRegionByName("pinguin"));
        
        hintTextView = new ZNode("hintTextView");
        hintTextView.initWithFrame(hintScreenRect, texturesManager.getRegionByName("nohint"));
        hintTextView.visible = false;
        
        ZLog.i(ScreenMetrics.screenHeightRip);
        
        createPuzzleRestartButton();
    }
    
    private void createPuzzleRestartButton() {
    	clickListener = new ZButton.ClickListener() {
			
			@Override
			public void touchDown(ZButton button) {
			}
			
			@Override
			public void clicked(ZButton button) {
				if(!restartButton.visible) {
					return;
				}
				
				if(button == restartButton) {
					isNeededToRestart = true;
				}
			}
		};
		
        restartButton = new ZButton("puzzleRestart", texturesManager.getRegionByName("puzzle_restart")
        		, texturesManager.getRegionByName("puzzle_restart_pressed"));
        restartButton.setScreenFrame(restartButtonRect);
        restartButton.clickListener = clickListener;
        sceneGameplay.addActor(restartButton);
    }

    @Override
    protected void draw(SpriteBatch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        hintTextView.zdraw(batch, parentAlpha);
        restartButton.zdraw(batch, parentAlpha);
    }

    @Override
    protected boolean touchDown(float x, float y, int pointer) {
    	if(PlayerProfile.getInstance().getCurrentModeId() == PlayerProfile.MODE_PUZZLE) {
    		return false;
    	} else if(pointInside(x, y) && IceFactory.getCurrentState() == IceFactoryState.INTERACTIVE) {
    		sceneGameplay.showHint();
    		return true;
    		}
        return false;
    }

    public void showNoHint() {
        hintTextView.visible = true;
        startTimeVisible = SystemClock.uptimeMillis();
    }

    @Override
    public void update() {
    	if(isNeededToRestart && IceFactory.getCurrentState() == IceFactoryState.INTERACTIVE) {
    		sceneGameplay.restartScene();
    		isNeededToRestart = false;
    	}
    	
        if (hintTextView.visible && SystemClock.uptimeMillis() - startTimeVisible > durationVisible) {
            hintTextView.visible = false;
        }
        if(PlayerProfile.getInstance().getCurrentModeId() == PlayerProfile.MODE_PUZZLE) {
        	this.visible = false;
        	restartButton.visible = true;
        } else {
        	this.visible = true;
        	restartButton.visible = false;
        }
        super.update();
    }
}
