/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import android.graphics.Rect;
import com.boolbalabs.nice.settings.Settings;
import com.boolbalabs.nice.settings.StaticConstants;
import java.util.HashMap;

/**
 *
 * @author Igor Trubnikov <itrubnikov@gmail.com>
 */
public class IcicleStatic {
	
	//** POWER **//
	public static final int ICICLE = 100;
	public static final int BOMB = 101;
	public static final int POWER_GEM = 102;
	public static final int COLOR_BOMB = 103;
	
	//** ICICLE TYPES **//
	public static final int EMPTY_CELL = 7;
	public static final int STONE = 8; 

    public static final int[] TYPES = {0, 1, 2, 3, 4, 5, 6, 7, 8};
    //** FRAMES **//
    private static HashMap<Integer, String[]> icicleFrameNames;
    private static final String[] frameType0 = {"icicle_type_0-0-0a", "icicle_type_0-1-0a", "icicle_type_0-2-0a",
        "icicle_type_0-3-0a", "icicle_type_0-4-0a", "icicle_type_0-5-0a",
        "icicle_type_0-6-0a", "icicle_type_0-7-0a", "icicle_type_0_white", "icicle_type_0_rot_1a", "icicle_type_0_rot_2a"};
    private static final String[] frameType1 = {"icicle_type_1-0-0a", "icicle_type_1-1-0a", "icicle_type_1-2-0a",
        "icicle_type_1-3-0a", "icicle_type_1-4-0a", "icicle_type_1-5-0a",
        "icicle_type_1-6-0a", "icicle_type_1-7-0a", "icicle_type_1_white", "icicle_type_1_rot_1a", "icicle_type_1_rot_2a"};
    private static final String[] frameType2 = {"icicle_type_2-0-0a", "icicle_type_2-1-0a", "icicle_type_2-2-0a",
        "icicle_type_2-3-0a", "icicle_type_2-4-0a", "icicle_type_2-5-0a",
        "icicle_type_2-6-0a", "icicle_type_2-7-0a", "icicle_type_2_white", "icicle_type_2_rot_1a", "icicle_type_2_rot_2a"};
    private static final String[] frameType3 = {"icicle_type_3-0-0a", "icicle_type_3-1-0a", "icicle_type_3-2-0a",
        "icicle_type_3-3-0a", "icicle_type_3-4-0a", "icicle_type_3-5-0a",
        "icicle_type_3-6-0a", "icicle_type_3-7-0a", "icicle_type_3_white", "icicle_type_3_rot_1a", "icicle_type_3_rot_2a"};
    private static final String[] frameType4 = {"icicle_type_4-0-0a", "icicle_type_4-1-0a", "icicle_type_4-2-0a",
        "icicle_type_4-3-0a", "icicle_type_4-4-0a", "icicle_type_4-5-0a",
        "icicle_type_4-6-0a", "icicle_type_4-7-0a", "icicle_type_4_white", "icicle_type_4_rot_1a", "icicle_type_4_rot_2a"};
    private static final String[] frameType5 = {"icicle_type_5-0-0a", "icicle_type_5-1-0a", "icicle_type_5-2-0a",
        "icicle_type_5-3-0a", "icicle_type_5-4-0a", "icicle_type_5-5-0a",
        "icicle_type_5-6-0a", "icicle_type_5-7-0a", "icicle_type_5_white", "icicle_type_5_rot_1a", "icicle_type_5_rot_2a"};
    private static final String[] frameType6 = {"icicle_type_6-0-0a", "icicle_type_6-1-0a", "icicle_type_6-2-0a",
        "icicle_type_6-3-0a", "icicle_type_6-4-0a", "icicle_type_6-5-0a",
        "icicle_type_6-6-0a", "icicle_type_6-7-0a", "icicle_type_6_white", "icicle_type_6_rot_1a", "icicle_type_6_rot_2a"};
    private static final String[] frameType7 = {};
    private static final String[] frameType8 = {"stone_0a", "stone_1a", "stone_2a", "stone_3a", "stone_4a", "stone_5a", "stone_6a", 
    	"stone_7a", "stone_0a", "stone_0a", "stone_0a"};
    
    public static int[][] highEntropyIndices = new int[Settings.ROWS][Settings.COLUMNS];

    public static void initialize() {
        createFrameNames();
    }

    private static void createFrameNames() {
        icicleFrameNames = new HashMap<Integer, String[]>();
        icicleFrameNames.put(TYPES[0], frameType0);
        icicleFrameNames.put(TYPES[1], frameType1);
        icicleFrameNames.put(TYPES[2], frameType2);
        icicleFrameNames.put(TYPES[3], frameType3);
        icicleFrameNames.put(TYPES[4], frameType4);
        icicleFrameNames.put(TYPES[5], frameType5);
        icicleFrameNames.put(TYPES[6], frameType6);
        icicleFrameNames.put(TYPES[7], frameType7);
        icicleFrameNames.put(TYPES[8], frameType8);
    }

    public static String[] getFrameNamesByType(int type) {
        if (icicleFrameNames.containsKey(type)) {
            return icicleFrameNames.get(type);
        }
        return null;
    }

    public static int getRandomType() {
        int index = StaticConstants.rand.nextInt(TYPES.length-2); //!!!!
        return TYPES[index];
    }

    public static boolean isValidType(int type) {
        for (int i = 0; i < TYPES.length; i++) {
            if (type == TYPES[i]) {
                return true;
            }
        }
        return false;
    }

    public static void generateHighEntropyIndices() {
        for (int i = 0; i < Settings.ROWS; i++) {
            for (int j = 0; j < Settings.COLUMNS; j++) {
                //even indices are random
                if ((i + j) % 2 == 0) {
                    highEntropyIndices[i][j] = getRandomType();
                }
            }
        }

        for (int i = 0; i < Settings.ROWS; i++) {
            for (int j = 0; j < Settings.COLUMNS; j++) {
                if ((i + j) % 2 == 1) {
                    highEntropyIndices[i][j] = getRandomType();
                    while (checkIndex(i, j)) {
                        highEntropyIndices[i][j] = getRandomType();
                    }
                }
            }
        }

    }

    private static boolean formTriple(int i1, int j1, int i2, int j2, int i3, int j3) {
        if (i1 > Settings.ROWS - 1 || i2 > Settings.ROWS - 1 || i3 > Settings.ROWS - 1
                || i1 < 0 || i2 < 0 || i3 < 0 || j1 < 0 || j2 < 0 || j3 < 0
                || j1 > Settings.COLUMNS - 1 || j2 > Settings.COLUMNS - 1 || j3 > Settings.COLUMNS - 1) {
            return false;
        } else {
            return (highEntropyIndices[i1][j1] == highEntropyIndices[i2][j2] && highEntropyIndices[i1][j1] == highEntropyIndices[i3][j3]);
        }
    }

    private static boolean checkIndex(int i, int j) {
        if (i > Settings.ROWS - 1 || i < 0 || j > Settings.COLUMNS - 1 || j < 0) {
            return false;
        }
        boolean left = formTriple(i, j, i, j - 1, i, j - 2);
        boolean right = formTriple(i, j, i, j + 1, i, j + 2);
        boolean top = formTriple(i, j, i - 1, j, i - 2, j);
        boolean bottom = formTriple(i, j, i + 1, j, i + 2, j);
        boolean crossHor = formTriple(i, j - 1, i, j, i, j + 1);
        boolean crossVer = formTriple(i - 1, j, i, j, i + 1, j);
        return left || right || top || bottom || crossHor || crossVer;
    }

    //*** test figures **//
    public static void makeCorner() {
        highEntropyIndices[5][5] = 0;
        highEntropyIndices[4][5] = 1;
        highEntropyIndices[3][5] = 1;
        highEntropyIndices[5][4] = 1;
        highEntropyIndices[5][3] = 1;
        highEntropyIndices[5][6] = 1;
    }

    public static void makeSingleMoveLeft() {
        int[][] h = highEntropyIndices;
        //orange=4
        h[0][0] = h[0][3] = h[1][7] = h[2][4] = h[2][7] = h[3][3] = h[4][2] = h[4][5] = h[5][6] = h[6][4] = h[7][2] = h[7][5] = 4;
        //white=5
        h[0][1] = h[1][6] = h[2][1] = h[2][6] = h[5][5] = h[6][0] = h[6][3] = h[6][6] = h[7][4] = 5;
        //red = 3
        h[0][2] = h[1][1] = h[1][5] = h[3][4] = h[3][7] = h[4][1] = h[5][4] = h[6][2] = h[6][7] = h[7][3] = 3;
        //yellow=1
        h[0][4] = h[1][0] = h[1][3] = h[2][2] = h[3][0] = h[3][5] = h[3][6] = h[4][6] = h[5][0] = h[7][0] = h[7][1] = 1;
        // green = 6
        h[0][5] = h[1][4] = h[3][2] = h[4][0] = h[5][2] = h[7][6] = 6;
        //blue = 0
        h[0][6] = h[0][7] = h[2][0] = h[2][3] = h[2][5] = h[4][7] = h[5][1] = h[5][3] = 0;
        //purple= 2
        h[1][2] = h[3][1] = h[4][3] = h[4][4] = h[5][7] = h[6][1] = h[6][5] = h[7][7] = 2;

        h[1][5] = 0;
        h[2][5] = 3;
        h[2][6] = 2;
        h[2][7] = 6;
        h[1][4] = 2;

    }
}
