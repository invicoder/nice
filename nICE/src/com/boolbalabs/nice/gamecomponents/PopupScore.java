/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import android.graphics.Point;
import android.graphics.PointF;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.OnActionCompleted;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateInterpolator;
import com.badlogic.gdx.scenes.scene2d.interpolators.LinearInterpolator;
import com.boolbalabs.lib.graphics.ZNumberView;
import com.boolbalabs.nice.R;
import com.boolbalabs.nice.SceneGameplay;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.utils.ZLog;

/**
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class PopupScore extends ZNumberView {

    private SceneGameplay sceneGameplay;
    private PlayerProfile playerProfile;
    private final String lightBlueDigitsFramenames[] = {"yellow_digits_00p", "yellow_digits_01p", "yellow_digits_02p",
        "yellow_digits_03p", "yellow_digits_04p", "yellow_digits_05p", "yellow_digits_06p",
        "yellow_digits_07p", "yellow_digits_08p", "yellow_digits_09p"};
    private boolean available;
    //*******************ACTIONS**************************//
    private ScaleTo scaleIn;
    private ScaleTo scaleOut;
    private Sequence scaleSequence;
    private OnActionCompleted onActionCompleted;
    private float centerX;
    private float centerY;

    public PopupScore(SceneGameplay sceneGameplay) {
        super("PopupScore");
        this.sceneGameplay = sceneGameplay;
        initNumberView(new PointF(-50, -50), lightBlueDigitsFramenames, ZNumberView.ALIGN_CENTER, 30);
        visible = false;
        available = true;
    }

    @Override
    public void update() {
        if (available) {
            return;
        }

        act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
    }

    private void startAction() {
        scaleIn = ScaleTo.$(1, 1, 0.4f);
        scaleOut = ScaleTo.$(0, 0, 0.4f);
        scaleIn.setInterpolator(LinearInterpolator.$());
        scaleOut.setInterpolator(LinearInterpolator.$());
        scaleSequence = Sequence.$(scaleIn, scaleOut);
        scaleSequence.setCompletionListener(new OnActionCompleted() {

            public void completed(Action action) {
                if (action == scaleSequence) {
                    visible = false;
                    available = true;
                    clearActions();
                }
            }
        });
        action(scaleSequence);
    }

    public void showScore(int scoreToShow, PointF positionRip) {
        available = false;
        positionRip.set(positionRip.x, positionRip.y - getSizeRip() / 2);
        setFixedPointRip(positionRip);
        setNumberToDraw(scoreToShow);
        originX = width / 2;
        originY = height / 2;
        scaleX = 0;
        scaleY = 0;
    	if(PlayerProfile.getInstance().getCurrentModeId() == PlayerProfile.MODE_PUZZLE) {
    		visible = false;
    	} else {
        	visible = true;
    	}	
        startAction();
    }
//    @Override
//    public void onPause() {
//        Transition t = getTransition();
//        if (t != null) {
//            t.pause();
//        }
//        super.onPause();
//    }
//
//    @Override
//    public void onResume() {
//        Transition t = getTransition();
//        if (t != null) {
//            t.resume();
//        }
//        super.onPause();
//    }
}
