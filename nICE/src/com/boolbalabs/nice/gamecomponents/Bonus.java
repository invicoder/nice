/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.OnActionCompleted;
import com.badlogic.gdx.scenes.scene2d.actions.MoveTo;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateInterpolator;
import com.boolbalabs.lib.graphics.ZSprite2D;
import com.boolbalabs.lib.utils.ScreenMetrics;
/**
 * 
 * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
 */
public class Bonus extends ZSprite2D {

	//coordinates of a pinguin
	private static final float START_X = 0;
	private static final float START_Y = ScreenMetrics.screenHeightRip - 380 - 93;
	
	private static final String[] framesNames = {
		"icicle_bonus_bomb",
		"icicle_bonus_gem",
		"icicle_bonus_color_bomb"
	};
	private IceFactory iceFactory;
	
	private OnActionCompleted onActionCompleted;
	private MoveTo actionMoveTo;
	
	private Rectangle screenRectRip;
	
	public Bonus(IceFactory iceFactory, Icicle ice) {
		super("bonus" + ice.getRow() + "" + ice.getColumn());
		this.iceFactory = iceFactory;
		screenRectRip = ice.getScreenRectRip();
		initializeAnimation(framesNames, 50, screenRectRip, false);
		setScreenFrame(screenRectRip);
		setDefaultFrameIndexToDraw(0);
		
		createMoveToAction();
	}
	
	@Override
	public void update() {
		super.update();
		act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
	}
	
	@Override
	public void zdraw(SpriteBatch batch, float parentAlpha) {
		act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        super.zdraw(batch, parentAlpha);
	}
	
	public void setBomb(Icicle ice) {
		visible = true;
		setDefaultFrameIndexToDraw(0);
		
		startMotion(START_X, START_Y, ice.x, ice.y, 0.5f);
	}
	
	public void setPowerGem(Icicle ice) {
		visible = true;
		
		setDefaultFrameIndexToDraw(1);
		
		startMotion(START_X, START_Y, ice.x, ice.y, 0.5f);
	}
	
	public void setColorBomb(Icicle ice) {
		visible = true;
		
		setDefaultFrameIndexToDraw(2);
		
		startMotion(START_X, START_Y, ice.x, ice.y, 0.5f);
	}
	
	public void setIcicle() {
		visible = false;
	}
	
	/*--- TRANSITIONS ---*/
	public void startMotion(float fromX, float fromY, float toX, float toY, float durationSec) {
        if (iceFactory != null) {
            clearActions();
            x = fromX;
            y = fromY;
            actionMoveTo = MoveTo.$(toX, toY, durationSec);
            actionMoveTo.setInterpolator(AccelerateInterpolator.$());
            actionMoveTo.setCompletionListener(onActionCompleted);
            //iceFactory.incrementSharedFlag();
            action(actionMoveTo);
        }
    }
	
    private void createMoveToAction() {
        onActionCompleted = new OnActionCompleted() {

            public void completed(Action action) {
                if (action == actionMoveTo) {
                    //iceFactory.decrementSharedFlag();
                }
            }
        };
    }

}
