/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import android.graphics.PointF;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.graphics.ZNumberView;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.nice.SceneGameplay;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.level.GameLevelFactoryXML;
import com.boolbalabs.nice.settings.Settings;

/**
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class Score extends ZNode {

    private Rectangle bgRectOnScreen = new Rectangle(2, ScreenMetrics.screenHeightRip - 20, 93, 17);
    private SceneGameplay sceneGameplay;
    private PlayerProfile playerProfile;
//    private final String lightBlueDigitsFramenames[] = {"lightblue_digits_00d", "lightblue_digits_01d", "lightblue_digits_02d",
//        "lightblue_digits_03d", "lightblue_digits_04d", "lightblue_digits_05d", "lightblue_digits_06d",
//        "lightblue_digits_07d", "lightblue_digits_08d", "lightblue_digits_09d"};
    private final String lightBlueDigitsFramenames[] = {"silverdigits00d", "silverdigits01d", "silverdigits02d",
        "silverdigits03d", "silverdigits04d", "silverdigits05d", "silverdigits06d",
        "silverdigits07d", "silverdigits08d", "silverdigits09d"};
    private ZNumberView currentScoreView;
    private ZNumberView currentBonusView;
    private ZNumberView currentPuzzleLevelView;
    private PointF currentScoreViewPosition = new PointF(ScreenMetrics.screenWidthRip - 2, ScreenMetrics.screenHeightRip - 20);
    private PointF bonusViewPosition = new PointF(bgRectOnScreen.x + bgRectOnScreen.width + 5, bgRectOnScreen.y);
    private PointF puzzleLevelViewPosition = new PointF(ScreenMetrics.screenWidthRip/2, ScreenMetrics.screenHeightRip-40);
    
    //****** LONGEST CHAIN **//
    private ZNode chainTextView;
    private ZNumberView chainNumberView;
    private final int skipFromTop = 357;
    private Rectangle chainScreenRect = new Rectangle(0, ScreenMetrics.screenHeightRip - 20 - 20, 71, 17);
    private PointF chainPoint = new PointF(bgRectOnScreen.x + bgRectOnScreen.width + 5, chainScreenRect.y + 1);

    //***********************************************************//
    public Score(SceneGameplay sceneGameplay) {
        super("Score");
        parentScene = sceneGameplay;
        this.sceneGameplay = (SceneGameplay) parentScene;
        playerProfile = PlayerProfile.getInstance();
        region = TexturesManager.getInstance().getRegionByName("text_bonus");
        setScreenFrame(bgRectOnScreen);
        createNumberViews();
    }

    private void createNumberViews() {
        currentScoreView = new ZNumberView("currentScoreView");
        currentScoreView.initNumberView(currentScoreViewPosition, lightBlueDigitsFramenames, ZNumberView.ALIGN_RIGHT, 20);
        currentScoreView.drawingMode = ZNumberView.NumberDrawingMode.NCONTINUOUS;
        currentScoreView.setContinuousDrawingParameters(15, 3);
        currentScoreView.setNumberToDraw(0);

        currentBonusView = new ZNumberView("currentBonusView");
        currentBonusView.initNumberView(bonusViewPosition, lightBlueDigitsFramenames, ZNumberView.ALIGN_LEFT, 18);
        currentBonusView.setNumberToDraw(playerProfile.bonusMultiplier);

        chainNumberView = new ZNumberView("longestChainNumberView");
        chainNumberView.initNumberView(chainPoint, lightBlueDigitsFramenames, ZNumberView.ALIGN_LEFT, 18);
        chainNumberView.setNumberToDraw(0);
        
        currentPuzzleLevelView = new ZNumberView("currentPuzzleLevelView");
        currentPuzzleLevelView.initNumberView(puzzleLevelViewPosition, lightBlueDigitsFramenames, ZNumberView.ALIGN_CENTER, 24);
        currentPuzzleLevelView.setNumberToDraw(playerProfile.currentPuzzleLevel + 1);

        chainTextView = new ZNode("longestChainTextView");
        chainTextView.initWithFrame(chainScreenRect,
                TexturesManager.getInstance().getRegionByName("text_max_chain"));
    }

    @Override
    public void onShow() {
        // initModeView();
        resetScore();
        currentScoreView.increaseInstantly();
    }

    @Override
    public void update() {
    	if(PlayerProfile.getInstance().getCurrentModeId() == PlayerProfile.MODE_PUZZLE) {
    		this.visible = false;
    		currentBonusView.visible = false;
    		currentPuzzleLevelView.visible = true;
    	} else {
    		this.visible = true;
    		currentBonusView.visible = true;
    		currentPuzzleLevelView.visible = false;
    	}
        currentScoreView.update();
    }

    @Override
    protected void draw(SpriteBatch batch, float parentAlpha) {
    	super.draw(batch, parentAlpha);
    	currentBonusView.zdraw(batch, parentAlpha);
    	currentPuzzleLevelView.zdraw(batch, parentAlpha);
    	if(PlayerProfile.getInstance().getCurrentModeId() != PlayerProfile.MODE_PUZZLE) {
		currentScoreView.zdraw(batch, parentAlpha);
        chainNumberView.zdraw(batch, parentAlpha);
        chainTextView.zdraw(batch, parentAlpha);
    	}
    }

    public void incrementScore(int icicleNumber, int chainCount, PointF positionRip) {
        int scoreToAdd = 10 + (icicleNumber - 3) * 10 + (chainCount - 1) * icicleNumber * 10;
        scoreToAdd *= playerProfile.bonusMultiplier;
        playerProfile.incrementScore(scoreToAdd);
        currentScoreView.setNumberToDraw(playerProfile.getCurrentScore());
        sceneGameplay.onPopupScore(scoreToAdd, positionRip);
        if (chainCount > playerProfile.longestChain) {
            playerProfile.longestChain = chainCount;
            chainNumberView.setNumberToDraw(chainCount);
        }
    }

    public void resetScore() {
        currentScoreView.setNumberToDraw(playerProfile.getCurrentScore());
        currentBonusView.setNumberToDraw(playerProfile.bonusMultiplier);
        chainNumberView.setNumberToDraw(playerProfile.longestChain);
        currentPuzzleLevelView.setNumberToDraw(playerProfile.currentPuzzleLevel + 1);
    }

    public void adjustBonusMultiplier() {
        currentBonusView.setNumberToDraw(playerProfile.bonusMultiplier);
        currentPuzzleLevelView.setNumberToDraw(playerProfile.currentPuzzleLevel + 1);
    }

    @Override
    public void onPause() {
        currentScoreView.increaseInstantly();
    }
}
