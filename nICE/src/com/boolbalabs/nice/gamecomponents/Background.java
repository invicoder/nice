/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import android.text.format.Time;
import android.util.Log;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.OnActionCompleted;
import com.badlogic.gdx.scenes.scene2d.actions.FadeIn;
import com.badlogic.gdx.scenes.scene2d.actions.FadeOut;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.LinearInterpolator;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.nice.SceneGameplay;
import com.boolbalabs.nice.settings.StaticConstants;
import javax.microedition.khronos.opengles.GL11Ext;

/**
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class Background extends ZNode {

    public static final String NAME = "game_background";
    private final String[] bgNames = new String[]{"nice_bg-a", "nice_bg-b", "nice_bg-c", "nice_bg-d"};
//    private final String[] bgNames = new String[]{"menu_bg", "menu_bg", "menu_bg", "menu_bg"};
    private Rectangle screenRectRip = new Rectangle(0, 0, ScreenMetrics.screenWidthRip, ScreenMetrics.screenHeightRip);
    private TexturesManager texturesManager;
    private SceneGameplay sceneGameplay;
    //*******************ACTIONS**************************//
    private FadeIn fadeInAction;
    private FadeOut fadeOutAction;
    private Sequence fadeOutInSequence;
    private OnActionCompleted onActionCompleted;
    private boolean withBgChange = true;
    //***********************************************************//
//    private SpriteBatch spriteBatch;
    
    private Snow snow;

    public Background(SceneGameplay sceneGameplay, Snow snow) {
        super(NAME);
        parentScene = sceneGameplay;
        this.sceneGameplay = (SceneGameplay) parentScene;
        
        this.snow = snow;
        
        touchable = false;
        texturesManager = TexturesManager.getInstance();
        setScreenFrame(screenRectRip);
        setTimedBg();
        createActions();
        
    }

    private void createActions() {
        onActionCompleted = new OnActionCompleted() {

            public void completed(Action action) {
                if (action == fadeOutInSequence) {
                    sceneGameplay.stopFadingBackground();
                } else if (action == fadeOutAction) {
                    if (withBgChange) {
                        setRandomBg();
                    }
                }
            }
        };

    }

    private void startAction() {
        clearActions();
        fadeInAction = FadeIn.$(0.6f);
        fadeInAction.setInterpolator(LinearInterpolator.$());
        fadeOutAction = FadeOut.$(0.6f);
        fadeOutAction.setInterpolator(LinearInterpolator.$());
        fadeOutInSequence = Sequence.$(fadeOutAction, fadeInAction);
        fadeOutAction.setCompletionListener(onActionCompleted);
        fadeInAction.setCompletionListener(onActionCompleted);
        fadeOutInSequence.setCompletionListener(onActionCompleted);
        action(fadeOutInSequence);
    }

    public void setRandomBg() {
        int ind = StaticConstants.rand.nextInt(bgNames.length);
        setRegion(ind);
    }

    private void setRegion(int ind) {
        region.setRegion(texturesManager.getRegionByName(bgNames[ind]));
        region.setRegion(region.getRegionX(), region.getRegionY(),
                region.getRegionWidth(), Math.min(region.getRegionHeight(),
                ScreenMetrics.getHeightProportionalToWidth(region.getRegionWidth())));
    }

    public void setTimedBg() {
        int newIndex = 1;
        Time t = new Time();
        t.setToNow();
        int currentHours = t.hour;
        if ((currentHours >= 0 && currentHours < 7) || currentHours >= 22) {
            newIndex = 3;
        } else if (currentHours >= 7 && currentHours < 12) {
            newIndex = 1;
        } else if (currentHours >= 12 && currentHours < 19) {
            newIndex = 0;
        } else {
            newIndex = 2;
        }
        setRegion(newIndex);
    }

    public void onLevelUp() {
        sceneGameplay.startFadingBackground(true);
    }

    @Override
    public void onHide() {
    }

    @Override
    protected boolean touchDown(float x, float y, int pointer) {
//        if (pointInside(x, y)) {
//            startFading(true);
//            return true;
//        }
        return false;
    }
    int[] oesTextureCropRect = new int[]{0, 800, 480, -800};

    @Override
    protected void draw(SpriteBatch batch, float parentAlpha) {
        act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        super.draw(batch, parentAlpha);
    }

    public void startFading(boolean withBgChange) {
        this.withBgChange = withBgChange;
        startAction();
        snow.startAction();
    }

    public void stopFading() {
        if (fadeOutInSequence != null) {
            fadeOutInSequence.finish();
        }
    }
    
    public void freeze() {
    	clearActions();
        fadeOutAction = FadeOut.$(0.6f);
        fadeOutAction.setInterpolator(LinearInterpolator.$());
        fadeOutInSequence = Sequence.$(fadeOutAction);
        fadeOutAction.setCompletionListener(onActionCompleted);
        fadeOutInSequence.setCompletionListener(onActionCompleted);
        action(fadeOutInSequence);
        snow.freeze();
    }
    
    public void unfreeze() {
    	clearActions();
        fadeInAction = FadeIn.$(0.6f);
        fadeInAction.setInterpolator(LinearInterpolator.$());
        fadeOutInSequence = Sequence.$(fadeInAction);
        fadeInAction.setCompletionListener(onActionCompleted);
        fadeOutInSequence.setCompletionListener(onActionCompleted);
        action(fadeOutInSequence);
        snow.unfreeze();
    }
}
