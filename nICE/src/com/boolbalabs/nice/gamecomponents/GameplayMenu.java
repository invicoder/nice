/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import com.badlogic.gdx.math.Rectangle;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.nice.SceneGameplay;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 *         Date: 28/03/2011
 *         Time: 11:08
 */
public class GameplayMenu extends ZNode {

    private SceneGameplay sceneGameplay;
    private TexturesManager texturesManager;
    private Rectangle restartButtonRectOnScreenRip = new Rectangle(0, 10, 65, 65);

    public GameplayMenu(SceneGameplay sceneGameplay) {
        super("GameplayMenu");
        this.sceneGameplay = sceneGameplay;
        sceneGameplay.addActor(this);
        texturesManager = TexturesManager.getInstance();
        initWithFrame(restartButtonRectOnScreenRip, texturesManager.getRegionByName("reload"));
    }

    @Override
    public boolean touchDown(float x, float y, int pointer) {
        if (pointInside(x, y)) {
            sceneGameplay.restartScene();
            return true;
        }
        return false;
    }
}
