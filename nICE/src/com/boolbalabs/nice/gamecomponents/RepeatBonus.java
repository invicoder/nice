/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.boolbalabs.lib.elements.ZButton;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.nice.SceneGameplay;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.utils.ZLog;

/**
 * 
 * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
 */

public class RepeatBonus extends ZNode {

	private static final int BLUE = 0;
	private static final int YELLOW = 1;
	private static final int PURPLE = 2;
	private static final int RED = 3;
	private static final int ORANGE = 4;
	private static final int WHITE = 5;
	private static final int GREEN = 6;
	private static final int IS_NOT_AVAILABLE = 7;
	private static final String[] frameNames = {
		"icicle_type_0-0-0a", "icicle_type_1-0-0a", "icicle_type_2-0-0a", "icicle_type_3-0-0a",
		"icicle_type_4-0-0a", "icicle_type_5-0-0a", "icicle_type_6-0-0a", "nohint"};
	
	private TexturesManager texturesManager;
	private PlayerProfile playerProfile;
	
	private TextureRegion region;
	
	public int lastUsedIcicle;
	private boolean updatable;
	public boolean isAvailable;
		
	public RepeatBonus() {
		super("repeat_bonus");
        
        texturesManager = TexturesManager.getInstance();
        playerProfile = PlayerProfile.getInstance();

		lastUsedIcicle = IS_NOT_AVAILABLE;
		isAvailable = false;
		updatable = false;
        
        region = texturesManager.getRegionByName(frameNames[lastUsedIcicle]);
	}
	
    public void update(int icicleType) {
    	updateLastUsedIcicle(icicleType);
    	if(updatable && icicleType != IcicleStatic.STONE) {
    		lastUsedIcicle = icicleType;
    	} else {
    		lastUsedIcicle = IS_NOT_AVAILABLE;
    	}
    	region = texturesManager.getRegionByName(frameNames[lastUsedIcicle]);
    }
    
    private void updateLastUsedIcicle(int type) {
    	switch(type) {
    	case YELLOW:
    		updatable = playerProfile.isYellowAvailable() ? true : false;
    		break;
    	case BLUE:
    		updatable = playerProfile.isBlueAvailable() ? true : false;
    		break;
    	case RED:
    		updatable = playerProfile.isRedAvailable() ? true : false;
    		break;
    	case ORANGE:
    		updatable = playerProfile.isOrangeAvailable() ? true : false;
    		break;
    	case WHITE:
    		updatable = playerProfile.isWhiteAvailable() ? true : false;
    		break;
    	case GREEN:
    		updatable = playerProfile.isGreenAvailable() ? true : false;
    		break;
    	case PURPLE:
    		updatable = playerProfile.isPurpleAvailable() ? true : false;
    		break;
    	case IS_NOT_AVAILABLE:
    		updatable = true;
    		break;
    	default: 
    		return;
    	}
    }
    
    public void reset() {
    	update(IS_NOT_AVAILABLE);
    }

	@Override
	public void zdraw(SpriteBatch batch, float parentAlpha) {
		if(lastUsedIcicle == IS_NOT_AVAILABLE || !visible) {
			return;
		}
		//batch.draw(region, 60, ScreenMetrics.screenHeightRip - 420 - 56, 56, 56);
		
		batch.draw(region, 50, ScreenMetrics.screenHeightRip - 420 - 56, 0, 0, 56, 56, 1, 1, -10);
	}
}
