/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import android.graphics.PointF;
import android.os.SystemClock;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.utils.MathUtils;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.nice.SceneGameplay;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.extra.SoundService;
import com.boolbalabs.nice.level.GameLevelFactory;
import com.boolbalabs.nice.level.GameLevelFactoryXML;
import com.boolbalabs.nice.modes.GameMode;
import com.boolbalabs.nice.settings.Settings;
import com.boolbalabs.nice.settings.StaticConstants;
import com.boolbalabs.nice.utils.ZLog;

import java.util.ArrayList;

/**
 *
 * @author Igor Trubnikov <itrubnikov@gmail.com>
 */
public class IceFactory extends ZNode {

	private static final int DELETION_MODE_TRIPLE = 3;
	private static final int DELETION_MODE_QUAD = 4;
	private static final int DELETION_MODE_FIVE = 5;
	private static final boolean IS_STRAIGHT_DELETION = true;
	private static final int NUMBER_OF_SWAPPINGS_IN_FREEZE = 5;
	
	private static final int MOVES_TO_SHOW_PINGUIN_HELP = 10;
	private static final int MOVES_TO_SHOW_SHAKES_HELP = 30;
	private static final int MOVES_TO_SHOW_REPEAT_HELP = 50;
	private int numberOfMoves = 0;
		
    private SceneGameplay sceneGameplay;
    private ArrayList<Icicle> icicles;
    private ArrayList<Icicle> iciclesOriginal;
    private int rows;
    private int columns;
        
    //** board **//
    private static final int skipFromTop = 21 + 13 + 21;
    public static Rectangle screenRectRip = new Rectangle(0, ScreenMetrics.screenHeightRip - skipFromTop - ScreenMetrics.screenWidthRip,
            ScreenMetrics.screenWidthRip, ScreenMetrics.screenWidthRip);
    public static int gridSideLengthRip;
    private PointF lastTouchMovePoint = new PointF();
    //** selection **//
    private ZNode selectionView;
    private Rectangle selectionScreenRectRip = new Rectangle(0, 0, 40, 40);
    //** SWAPPING **//
    private Icicle firstIcicle;
    private Icicle secondIcicle;
    private boolean firstIcileSelected;
    //** FALLING **//
    private ArrayList<Icicle> movingIcicles;
    private ArrayList<Icicle> canBeUsedIcicles;
    private ArrayList<Icicle> iciclesBackup;
    private PointF[][] fixedPositions;
    //** GLOWING ANIMATION **//
    private int glowingRowIndex = 0;
    private long startGlowingTime = 0;
    private long minimalGlowingPeriod = 15000;
    //** DELETING **//
    private int chainCount;
    private PointF averagePositionRip = new PointF();
    private boolean isFiveToDelete = false;
    private boolean isThereIciclesToDelete = false;
    //**** GAME OVER ****//
    private boolean gameOver;
    //**** HINT ****//
    private ArrayList<Icicle> hintIcicles;
    //** STATES **//

    public static enum IceFactoryState {

        INTERACTIVE, SWAPPING_FORWARD, SWAPPING_BACKWARDS, DELETING, FALLING, GAME_OVER
    };
    private static IceFactoryState currentState;
    private int commonSharedFlag = 0;
    private final int checkTypeLeft = 0;
    private final int checkTypeRight = 1;
    private final int checkTypeTop = 2;
    private final int checkTypeBottom = 3;
    //** SETTINGS **//
    private Settings settings;
    private PlayerProfile playerProfile;
    private boolean shakeHappened = false;

    //*** GET BONUS ***//    
    private boolean isShake = false;
    public boolean isGameFreezed = false;
    private int amountSwappingInFreezeMode = 4;

    public static IceFactoryState getCurrentState() {
    	return currentState;
    }

    ///*********************************************************//
    public IceFactory(SceneGameplay sceneGameplay) {
        super("IceFactory");
        parentScene = sceneGameplay;
        this.sceneGameplay = sceneGameplay;
        IcicleStatic.initialize();
        IcicleStatic.generateHighEntropyIndices();
//        IcicleStatic.makeSingleMoveLeft();
        setScreenFrame(screenRectRip);
        createIcicles();
        createSelection();
        
        settings = Settings.getInstance();
        playerProfile = PlayerProfile.getInstance();
    }

    private void createIcicles() {
        icicles = new ArrayList<Icicle>();
        iciclesOriginal = new ArrayList<Icicle>();
        movingIcicles = new ArrayList<Icicle>();
        iciclesBackup = new ArrayList<Icicle>();
        canBeUsedIcicles = new ArrayList<Icicle>();
        hintIcicles = new ArrayList<Icicle>();
        rows = Settings.ROWS;
        columns = Settings.COLUMNS;
        gridSideLengthRip = ScreenMetrics.screenWidthRip / columns;
        fixedPositions = new PointF[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                Icicle ice = new Icicle(this, i, j);
                ice.touchable = false;
                icicles.add(ice);
                fixedPositions[i][j] = new PointF(ice.x, ice.y);
            }
        }
        iciclesOriginal.addAll(icicles);
        firstIcileSelected = false;
    }

    @Override
    public void onShow() {
        selectionView.visible = false;
        commonSharedFlag = 0;
        int[][] icicleTypes = playerProfile.getCurrentMode().icicleTypes;
        int[][] iciclePowers = playerProfile.getCurrentMode().iciclePowers;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                Icicle ice = getIcicle(i, j);
                ice.stopCurrentAnimation();
                ice.x = fixedPositions[i][j].x;
                ice.y = fixedPositions[i][j].y;
                ice.changeType(icicleTypes[i][j]);
                ice.changePower(iciclePowers[i][j]);
            }
        }
        currentState = IceFactoryState.INTERACTIVE;
    }

    //********** UPDATE ************//
    @Override
    public void update() {
        for (int i = 0; i < icicles.size(); i++) {
            icicles.get(i).update();
        }
        for (int i = 0; i < icicles.size(); i++) {
            icicles.get(i).getBonus().update();
        }
        for (int i = 0; i < icicles.size(); i++) {
            icicles.get(i).getLightning().update();
        }
        //playerProfile.getCurrentMode().repeatBonus.update();
        starGlitter();
    }

    @Override
    protected void draw(SpriteBatch batch, float parentAlpha) {
        for (int i = 0; i < icicles.size(); i++) {
            icicles.get(i).zdraw(batch, parentAlpha);
        }
        for (int i = 0; i < icicles.size(); i++) {
            icicles.get(i).getBonus().zdraw(batch, parentAlpha);
        }
        for (int i = 0; i < icicles.size(); i++) {
            icicles.get(i).getLightning().zdraw(batch, parentAlpha);
        }
        selectionView.zdraw(batch, parentAlpha);
        if(playerProfile.getCurrentModeId() != PlayerProfile.MODE_PUZZLE) {
        	playerProfile.getCurrentMode().repeatBonus.zdraw(batch, parentAlpha);
        }	
    }
    
///******************************************************//
    @Override
    protected boolean touchDown(float x, float y, int pointer) {
        if (!pointInside(x, y)) {
            return false;
        }
        if (currentState == IceFactoryState.INTERACTIVE) {
            Icicle ice = getTouchedIcicle(x, y);
            lastTouchMovePoint.set(x, y);   
            
            if (!firstIcileSelected) {
            	if(ice.getType() != IcicleStatic.EMPTY_CELL) {
            		selectFirstIcicle(ice);
                }
            } else {
                secondIcicle = ice;
                if (firstIcicle.isNeighbour(secondIcicle)) {
                    startSwappingForward();
                } else {
                    selectFirstIcicle(ice);
                }
            }
        }
        return true;
    }

    @Override
    protected boolean touchDragged(float x, float y, int pointer) {
        if (!pointInside(x, y)) {
            return false;
        }
        if (currentState == IceFactoryState.INTERACTIVE) {
            Icicle ice = getTouchedIcicle(x, y);
            if (ice == null) {
                return false;
            }
            float distance = MathUtils.vectorLength(x, y, lastTouchMovePoint.x, lastTouchMovePoint.y);
            if (distance < 10) {
                // phone screen is very sensitive!
                return false;
            }
            touchDown(x, y, pointer);
            lastTouchMovePoint.set(x, y);
        }
        return true;
    }

    private void selectFirstIcicle(Icicle ice) {
        if (firstIcicle != null) {
            firstIcicle.stopAnimation();
        }
        firstIcicle = ice;
        secondIcicle = null;
        showSelection(firstIcicle);
        firstIcicle.startAnimation(Icicle.ANIMATION_ROTATE);
        firstIcileSelected = true;
    }

    private void startSwappingForward() {
        sceneGameplay.starGlitterStop();
        firstIcicle.stopAnimation();
        gameOver = false;
        commonSharedFlag = 0;
        chainCount = 0;
        firstIcileSelected = false;
        selectionView.visible = false;
        currentState = IceFactoryState.SWAPPING_FORWARD;
        firstIcicle.startMotion(firstIcicle.x, firstIcicle.y, secondIcicle.x, secondIcicle.y, 0.3f);
        secondIcicle.startMotion(secondIcicle.x, secondIcicle.y, firstIcicle.x, firstIcicle.y, 0.3f);
        
        numberOfMoves++;
        if(Settings.isHelpPinguinShouldShown && playerProfile.getCurrentModeId() != PlayerProfile.MODE_PUZZLE && numberOfMoves >= MOVES_TO_SHOW_PINGUIN_HELP) {
        	sceneGameplay.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_PINGUIN);
        	Settings.isHelpPinguinShouldShown = false;
        }
        
        if(Settings.isHelpShakesShouldShown && playerProfile.getCurrentModeId() != PlayerProfile.MODE_PUZZLE && numberOfMoves >= MOVES_TO_SHOW_SHAKES_HELP) {
        	sceneGameplay.sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_SHAKES);
        	Settings.isHelpShakesShouldShown = false;
        }
        
        if(Settings.isHelpRepeatShouldShown && playerProfile.getCurrentModeId() != PlayerProfile.MODE_PUZZLE && numberOfMoves >= MOVES_TO_SHOW_REPEAT_HELP) {
        	sceneGameplay.sendEmptyMessageToActivity(StaticConstants.MESSAGE_REPEAT_BONUS);
        	Settings.isHelpRepeatShouldShown = false;
        }
    }

    private void startSwappingBackwards() {
        if (firstIcicle == null || secondIcicle == null) {
            return;
        }
        SoundService.playWrongMove();
        commonSharedFlag = 0;
        Icicle tmp = firstIcicle;
        firstIcicle = secondIcicle;
        secondIcicle = tmp;
        firstIcileSelected = false;
        selectionView.visible = false;
        currentState = IceFactoryState.SWAPPING_BACKWARDS;
        firstIcicle.startMotion(firstIcicle.x, firstIcicle.y, secondIcicle.x, secondIcicle.y, 0.3f);
        secondIcicle.startMotion(secondIcicle.x, secondIcicle.y, firstIcicle.x, firstIcicle.y, 0.3f);
    }

    private Icicle getTouchedIcicle(float x, float y) {
        int j = (int) Math.floor(x / (float) gridSideLengthRip);
        int i = rows - (int) Math.floor(y / (float) gridSideLengthRip) - 1;
        if (i > rows - 1) {
            i = rows - 1;
        } else if (i < 0) {
            i = 0;
        }
        if (j > columns - 1) {
            j = columns - 1;
        } else if (j < 0) {
            j = 0;
        }
        return icicles.get(i * columns + j);
    }

    //****** UPDATE ****//
    public void decrementSharedFlag() {
        commonSharedFlag--;
        if (commonSharedFlag == 0) {
            switch (currentState) {
                case SWAPPING_FORWARD:
                    exchangeSwappingIcicles();
                    currentState = IceFactoryState.INTERACTIVE;
                    checkForDeletion();
                    break;
                case SWAPPING_BACKWARDS:
                    exchangeSwappingIcicles();
                    currentState = IceFactoryState.INTERACTIVE;
                    break;
                case DELETING:
                    startFalling();
                    break;
                case FALLING:
                    onPuzzleLevelUp();
                    if(isIceDropSoundShouldPlay()) {
                    	SoundService.playIceDrop();
                    }
                    resetIcicles();
                    break;
                default:
                    break;
            }
        }
    }
    
    /**
     * 
     * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
     */
    private void onPuzzleLevelUp() {    	
    	 if(playerProfile.getCurrentModeId() == PlayerProfile.MODE_PUZZLE
         		&& playerProfile.getCurrentMode().isLevelUp()) {

         	if(playerProfile.currentPuzzleLevel == GameLevelFactory.getInstance().getNumberOfLastLevel()) {
         		sceneGameplay.sendEmptyMessageToActivity(StaticConstants.MESSAGE_CONGRATULATIONS);
         	} else {
         		sceneGameplay.sendEmptyMessageToActivity(StaticConstants.MESSAGE_LEVEL_UP);
         	}	

         	if(playerProfile.currentPuzzleLevel != GameLevelFactory.getInstance().getNumberOfLastLevel()) {
         		playerProfile.currentPuzzleLevel += 1;
             	playerProfile.getCurrentMode().setLevel();
 			} else {
 				playerProfile.currentPuzzleLevel = 0;
 			}
         }
    }
    
    //****** DELETING ****//
    private void checkForDeletion() {
    	if(isGameFreezed) {
    		amountSwappingInFreezeMode--;
    		if(amountSwappingInFreezeMode == 0) {
    			isGameFreezed = false;
    			sceneGameplay.unfreeze();
    			checkForDeletionWholeBoard();
    		} 
    		return;
    	}
        
    	checkForDeletion(firstIcicle, secondIcicle);
    	
    	playerProfile.getCurrentMode().repeatBonus.isAvailable = true;
        boolean startDeletion = startDeletion();
        //swapping backwards if nothing to delete
        if (!startDeletion) {
            startSwappingBackwards();
        } else {
            SoundService.playRemove(1);
            currentState = IceFactoryState.DELETING;
        }
    }
    
    /**
     * 
     * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
     */
    private void checkForDeletion(Icicle first, Icicle second) {
    	checkForTripleDeletion(first);
        checkForTripleDeletion(second);
        
        //TODO: in the next update in the mode puzzle should be upgrades - bombs, etc
        if(playerProfile.getCurrentModeId() == PlayerProfile.MODE_PUZZLE)
        {
        	return;
        }
        
        if(playerProfile.isPowerGemAvailable()) {
        	checkForNotStraightFiveDeletion(first);
        }
        if(playerProfile.isColorBombAvailable()) {
        	checkForStraightFiveDeletion(first);
        }
        if(!isFiveToDelete && playerProfile.isBombAvailable()) {
        	checkForQuadDeletion(first);
        }
        isFiveToDelete = false;
        
        if(playerProfile.isPowerGemAvailable()) {
        	checkForNotStraightFiveDeletion(second);
        }
        if(playerProfile.isColorBombAvailable()) {
        	checkForStraightFiveDeletion(second);
        }
        if(!isFiveToDelete && playerProfile.isBombAvailable()) {
        	checkForQuadDeletion(second);
        }
        isFiveToDelete = false;
    }
    
    /**
     * 
     * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
     */
    private void checkForStraightFiveDeletion(Icicle ice) {
    	int row = ice.getRow();
        int column = ice.getColumn();
    	checkDeletionHorizontal(DELETION_MODE_FIVE, row, column);
        checkDeletionVertical(DELETION_MODE_FIVE, row, column);
    }
    
    private void checkForNotStraightFiveDeletion(Icicle ice) {
    	int row = ice.getRow();
        int column = ice.getColumn();
        checkDeletionBroken(row, column);
    }
    
    private void checkForQuadDeletion(Icicle ice) {
    	int row = ice.getRow();
        int column = ice.getColumn();
    	checkDeletionHorizontal(DELETION_MODE_QUAD, row, column);
        checkDeletionVertical(DELETION_MODE_QUAD, row, column);
    }
    
    private void checkForTripleDeletion(Icicle ice) {
    	int row = ice.getRow();
        int column = ice.getColumn();
    	checkDeletionHorizontal(DELETION_MODE_TRIPLE, row, column);
        checkDeletionVertical(DELETION_MODE_TRIPLE, row, column);
    }
    
    /**
     * 
     * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
     */
    private void checkDeletionBroken(int row, int column) {
     /*  000
      *  0
      *  0
      */
    	checkDeletionForIndices(DELETION_MODE_FIVE, 
    			row, column, row + 1, column, row + 2, column, row, column + 1, row, column + 2, !IS_STRAIGHT_DELETION);

     /*    0
      *    0
      *  000
      */
    	checkDeletionForIndices(DELETION_MODE_FIVE, 
    			row, column, row - 1, column, row - 2, column, row, column + 1, row, column + 2, !IS_STRAIGHT_DELETION);
   
     /*  000
      *    0
      *    0
      */
    	checkDeletionForIndices(DELETION_MODE_FIVE, 
    			row, column, row - 1, column, row - 2, column, row, column - 1, row, column - 2, !IS_STRAIGHT_DELETION);
     
     /*  0
      *  0
      *  000
      */
    	checkDeletionForIndices(DELETION_MODE_FIVE, 
    			row, column, row + 1, column, row + 2, column, row, column - 1, row, column - 2, !IS_STRAIGHT_DELETION);
     
     /*   0
      *  000
      *   0
      */
    	checkDeletionForIndices(DELETION_MODE_FIVE, 
    			row, column, row - 1, column, row + 1, column, row, column + 1, row, column -1, !IS_STRAIGHT_DELETION);
     
     /*  000
      *   0
      *   0
      */
    	checkDeletionForIndices(DELETION_MODE_FIVE, 
    			row, column, row, column - 1, row, column + 1, row + 1, column, row + 2, column, !IS_STRAIGHT_DELETION);
     
     /*   0
      *   0
      *  000
      */
    	checkDeletionForIndices(DELETION_MODE_FIVE, 
    			row, column, row, column - 1, row, column + 1, row - 1, column, row - 2, column, !IS_STRAIGHT_DELETION);
     
     /*  0
      *  000
      *  0
      */
    	checkDeletionForIndices(DELETION_MODE_FIVE, 
    			row, column, row - 1, column, row + 1, column, row, column + 1, row, column + 2, !IS_STRAIGHT_DELETION);
     
     /*    0
      *  000
      *    0
      */
    	checkDeletionForIndices(DELETION_MODE_FIVE, 
    			row, column, row - 1, column, row + 1, column, row, column - 1, row, column - 2, !IS_STRAIGHT_DELETION);
    }
    
    /**
     * 
     * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
     */
    private void checkDeletionHorizontal(int deletionMode, int row, int column) {
    	checkDeletionForIndices(deletionMode, 
    			row, column, row, column - 1, row, column - 2, row, column - 3, row, column - 4, IS_STRAIGHT_DELETION);
        checkDeletionForIndices(deletionMode, 
        		row, column, row, column - 1, row, column - 2, row, column - 3, row, column + 1, IS_STRAIGHT_DELETION);
        checkDeletionForIndices(deletionMode, 
        		row, column, row, column - 1, row, column + 1, row, column + 2, row, column + 3, IS_STRAIGHT_DELETION);
        checkDeletionForIndices(deletionMode, 
        		row, column, row, column + 1, row, column + 2, row, column + 3, row, column + 4, IS_STRAIGHT_DELETION);
        checkDeletionForIndices(deletionMode, 
        		row, column, row, column - 1, row, column - 2, row, column + 1, row, column + 2, IS_STRAIGHT_DELETION);
    }
    
    /**
     * 
     * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
     */
    private void checkDeletionVertical(int deletionMode, int row, int column) {
    	checkDeletionForIndices(deletionMode, 
    			row, column, row - 1, column, row - 2, column, row - 3, column, row - 4, column, IS_STRAIGHT_DELETION);
        checkDeletionForIndices(deletionMode, 
        		row, column, row - 1, column, row - 2, column, row - 3, column, row + 1, column, IS_STRAIGHT_DELETION);
        checkDeletionForIndices(deletionMode, 
        		row, column, row - 1, column, row + 1, column, row + 2, column, row + 3, column, IS_STRAIGHT_DELETION);
        checkDeletionForIndices(deletionMode, 
        		row, column, row + 1, column, row + 2, column, row + 3, column, row + 4, column, IS_STRAIGHT_DELETION);
        checkDeletionForIndices(deletionMode, 
        		row, column, row - 1, column, row - 2, column, row + 1, column, row + 2, column, IS_STRAIGHT_DELETION);
    }

    private boolean checkForPossibleDeletion(int row, int column, int type, int checkType) {
        boolean left = checkForMatch(type, row, column - 1, row, column - 2);
        boolean right = checkForMatch(type, row, column + 1, row, column + 2);
        boolean top = checkForMatch(type, row - 1, column, row - 2, column);
        boolean bottom = checkForMatch(type, row + 1, column, row + 2, column);
        boolean crossHor = checkForMatch(type, row, column - 1, row, column + 1);
        boolean crossVer = checkForMatch(type, row - 1, column, row + 1, column);
        if (checkType == checkTypeLeft) {
            right = false;
            crossHor = false;
        } else if (checkType == checkTypeRight) {
            left = false;
            crossHor = false;
        } else if (checkType == checkTypeTop) {
            bottom = false;
            crossVer = false;
        } else if (checkType == checkTypeBottom) {
            top = false;
            crossVer = false;
        }
//        if (left) {
//            ZLog.i("check - LEFT");
//        }
//        if (right) {
//            ZLog.i("check - right");
//        }
//        if (top) {
//            ZLog.i("check - top");
//        }
//        if (bottom) {
//            ZLog.i("check - bottom");
//        }
//        if (crossHor) {
//            ZLog.i("check - crossHor");
//        }
//        if (crossVer) {
//            ZLog.i("check - crossVer");
//        }
        return left || right || top || bottom || crossHor || crossVer;
    }

    private boolean checkForMatch(int type, int i1, int j1, int i2, int j2) {
        Icicle ice1 = getIcicle(i1, j1);
        Icicle ice2 = getIcicle(i2, j2);
        if (ice1 == null || ice2 == null 
        		|| ice1.getType() == IcicleStatic.STONE || ice2.getType() == IcicleStatic.STONE) {
            return false;
        }
        return ice1.getType() == type && ice2.getType() == type;
    }
    
    private void checkForDeletionBroken(Icicle ice) {
    	int row = ice.getRow();
    	int column = ice.getColumn();
    	if(playerProfile.isPowerGemAvailable()) {
    		checkDeletionBroken(row, column);
    	}	
    }

    /**
     * 
     * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
     */
    private void checkForDeletionToRight(Icicle ice) {
        int row = ice.getRow();
        int column = ice.getColumn();
        //**** TO RIGHT ****//
        checkDeletionForIndices(DELETION_MODE_TRIPLE,
        		row, column, row, column + 1, row, column + 2, row, column + 3, row, column + 4, IS_STRAIGHT_DELETION);
        if(playerProfile.isColorBombAvailable()) {
        	checkDeletionForIndices(DELETION_MODE_FIVE, 
        			row, column, row, column + 1, row, column + 2, row, column + 3, row, column + 4, IS_STRAIGHT_DELETION);
        }	
        if(!isFiveToDelete && playerProfile.isBombAvailable()) {
        	checkDeletionForIndices(DELETION_MODE_QUAD, 
        			row, column, row, column + 1, row, column + 2, row, column + 3, row, column + 4, IS_STRAIGHT_DELETION);
        	isFiveToDelete = false;
        }	
    }

    /**
     * 
     * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
     */
    private void checkForDeletionToDown(Icicle ice) {
        int row = ice.getRow();
        int column = ice.getColumn();
        //**** TO DOWN ****//
        checkDeletionForIndices(DELETION_MODE_TRIPLE, 
        		row, column, row + 1, column, row + 2, column, row + 3, column, row + 4, column, IS_STRAIGHT_DELETION);
        if(playerProfile.isColorBombAvailable()) {
        	checkDeletionForIndices(DELETION_MODE_FIVE, 
        			row, column, row + 1, column, row + 2, column, row + 3, column, row + 4, column, IS_STRAIGHT_DELETION);
        }	
        if(!isFiveToDelete && playerProfile.isBombAvailable()) {
        	checkDeletionForIndices(DELETION_MODE_QUAD, 
        			row, column, row + 1, column, row + 2, column, row + 3, column, row + 4, column, IS_STRAIGHT_DELETION);
        	isFiveToDelete = false;
        }
    }
    
    /**
     * 
     * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
     */
    private void checkTripleDeletionForIndices(int i1, int j1, int i2, int j2, int i3, int j3) {
    	Icicle ice = getIcicle(i1, j1);
        if (ice == null) {
            return;
        }
        Icicle first = getIcicle(i2, j2);
        Icicle second = getIcicle(i3, j3);
        
        if (first == null) {
            return;
        }
        
        if (ice.formTripleToDelete(first, second)) {
        	
        	ZLog.i("TRIPLETODELETE");
        	
        	ice.toBeExterminated = true;
        	ice.isScored = true;
        	exterminateIfPowerful(ice);
        	
            first.toBeExterminated = true;
            first.isScored = true;
            exterminateIfPowerful(first);
            
            second.toBeExterminated = true;
            second.isScored = true;
            exterminateIfPowerful(second);
            
            isThereIciclesToDelete = true;
        }
    }
    
    /**
     * 
     * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
     */
    private void checkQuadDeletionForIndices(int i1, int j1, int i2, int j2, int i3, int j3, int i4, int j4) {
    	Icicle ice = getIcicle(i1, j1);
        if (ice == null) {
            return;
        }
        Icicle first = getIcicle(i2, j2);
        Icicle second = getIcicle(i3, j3);
        Icicle third = getIcicle(i4, j4);
        
        if (first == null) {
            return;
        }
        
        if(ice.formQuatroToDelete(first, second, third)) {
        	
        	ZLog.i("QUADTODELETE");
            
            exterminateIfPowerful(ice);
            ice.toBeExterminated = false;
            ice.isScored = true;
            ice.setBomb();

            isThereIciclesToDelete = true;
            return;
        }
    }
    
    /**
     * 
     * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
     */
    private void checkFiveStraightDeletionForIndices(int i1, int j1, int i2, int j2, int i3, int j3, int i4, int j4, int i5, int j5) {
    	Icicle ice = getIcicle(i1, j1);
        if (ice == null) {
            return;
        }
        Icicle first = getIcicle(i2, j2);
        Icicle second = getIcicle(i3, j3);
        Icicle third = getIcicle(i4, j4);
        Icicle fourth = getIcicle(i5, j5);
        
        if (first == null) {
            return;
        }
        
        if(ice.formFiveToDelete(first, second, third, fourth)) {

        	ZLog.i("FIVETODELETE STRAIGHT");

            exterminateIfPowerful(ice);
            ice.toBeExterminated = false;
            ice.isScored = true;
                
            ice.setColorBomb();
            isFiveToDelete = true;

            isThereIciclesToDelete = true;
        	return;
        }
    }
    
    /**
     * 
     * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
     */
    private void checkFiveNotStraightDeletionForIndices(int i1, int j1, int i2, int j2, int i3, int j3, int i4, int j4, int i5, int j5) {
    	Icicle ice = getIcicle(i1, j1);
        if (ice == null) {
            return;
        }
        Icicle first = getIcicle(i2, j2);
        Icicle second = getIcicle(i3, j3);
        Icicle third = getIcicle(i4, j4);
        Icicle fourth = getIcicle(i5, j5);
        
        if (first == null) {
            return;
        }
        
        if(ice.formFiveToDelete(first, second, third, fourth)) {

        	ZLog.i("FIVETODELETE NOT STRAIGHT");
            
            exterminateIfPowerful(ice);
            ice.toBeExterminated = false;
            ice.isScored = true;
            	
            ice.setPowerGem();
            isFiveToDelete = false;

            isThereIciclesToDelete = true;
        	return;
        }
    }

    /**
     * 
     * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
     */
    private void checkDeletionForIndices(int deletionMode, int i1, int j1, int i2, int j2, int i3, int j3, int i4, int j4, int i5, int j5, boolean isStraight) {
    	if(!isStraight) {
    		checkFiveNotStraightDeletionForIndices(i1, j1, i2, j2, i3, j3, i4, j4, i5, j5);
    		return;
    	}
    	switch(deletionMode) {
	    case DELETION_MODE_TRIPLE: checkTripleDeletionForIndices(i1, j1, i2, j2, i3, j3); break;
	    case DELETION_MODE_QUAD: checkQuadDeletionForIndices(i1, j1, i2, j2, i3, j3, i4, j4); break;
	    case DELETION_MODE_FIVE: checkFiveStraightDeletionForIndices(i1, j1, i2, j2, i3, j3, i4, j4, i5, j5); break;
	    default: return;
	    }
    }
    
    /**
     * 
     * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
     */
    private void exterminateIfPowerful(Icicle ice) {
    	switch(ice.getPower()) {
    	case IcicleStatic.BOMB: 
    		exterminateByBomb(ice);
    		break;
    	case IcicleStatic.COLOR_BOMB:
    		exterminateByColorBomb(ice);
    		break;
    	case IcicleStatic.POWER_GEM:
    		exterminateByPowerGem(ice);
    		break;
    	default:
    		return;
    	}
    }
    
    private void exterminateByBomb(Icicle bomb) {
    	bomb.setIcicle();
    	bomb.startExplosion();
    	SoundService.playBomb();
    	
    	switch(playerProfile.getBombUpgradeLevel()) {
    	case 1: exterminateByBombFirstUpgradeLevel(bomb); break;
    	case 2: exterminateByBombSecondUpgradeLevel(bomb); break;
    	case 3: exterminateByBombThirdUpgradeLevel(bomb); break;
    	default: exterminateByBombFirstUpgradeLevel(bomb); return;
    	}
    }
    
    private void exterminateByBombFirstUpgradeLevel(Icicle bomb) {
    	int row = bomb.getRow();
    	int column = bomb.getColumn();
    	
    	exterminateIcicle(row, column);
    	exterminateIcicle(row - 1, column);
    	exterminateIcicle(row + 1, column);
    	exterminateIcicle(row, column - 1);
    	exterminateIcicle(row, column + 1);
    }
    
    private void exterminateByBombSecondUpgradeLevel(Icicle bomb) {
    	int row = bomb.getRow();
    	int column = bomb.getColumn();
    	for(int i = row - 1; i <= row + 1; i++) {
    		for(int j = column - 1; j <= column + 1; j++) {
    			exterminateIcicle(i, j);
    		}
    	}
    }
    
    private void exterminateByBombThirdUpgradeLevel(Icicle bomb) {
    	int row = bomb.getRow();
    	int column = bomb.getColumn();
    	
    	exterminateByBombSecondUpgradeLevel(bomb);
    	exterminateIcicle(row - 2, column);
    	exterminateIcicle(row + 2, column);
    	exterminateIcicle(row, column - 2);
    	exterminateIcicle(row, column + 2);
    }
        
    private void exterminateByPowerGem(Icicle powerGem) {
    	powerGem.setIcicle();
    	SoundService.playPowerIcicle();
    	
    	switch(playerProfile.getPowerGemUpgradeLevel()) {
    	case 1: exterminateByPowerGemFirstUpgradeLevel(powerGem); break;
    	case 2: exterminateByPowerGemSecondUpgradeLevel(powerGem); break;
    	case 3: exterminateByPowerGemThirdUpgradeLevel(powerGem); break;
    	case 4: exterminateByPowerGemFourthUpgradeLevel(powerGem); break;
    	default: exterminateByPowerGemFirstUpgradeLevel(powerGem); return;
    	}
    }
    
    private void exterminateByPowerGemFirstUpgradeLevel(Icicle powerGem) {
    	powerGem.startLightningAnimationFirstLevel();
    	int row = powerGem.getRow();
    	int column = powerGem.getColumn();
    	for(int i = 0; i < row; i++) {
    		exterminateIcicle(i, column);	
    	}
    }
    
    private void exterminateByPowerGemSecondUpgradeLevel(Icicle powerGem) {
    	powerGem.startLightningAnimationSecondLevel();
    	int row = powerGem.getRow();
    	for(int i = 0; i < Settings.COLUMNS; i++) {
    		exterminateIcicle(row, i);	
    	}
    }
    
    private void exterminateByPowerGemThirdUpgradeLevel(Icicle powerGem) {
    	exterminateByPowerGemFirstUpgradeLevel(powerGem);
    	exterminateByPowerGemSecondUpgradeLevel(powerGem);
    }
    
    private void exterminateByPowerGemFourthUpgradeLevel(Icicle powerGem) {
    	exterminateByPowerGemSecondUpgradeLevel(powerGem);
    	powerGem.startLightningAnimationFourthLevel();
    	int column = powerGem.getColumn();
    	for(int i = 0; i < Settings.ROWS; i++) {
    		exterminateIcicle(i, column);	
    	}
    }
    

    private void exterminateIcicle(int row, int column) {
    	Icicle ice = getIcicle(row, column);
		if(ice != null && ice.getType() != IcicleStatic.EMPTY_CELL) {
			ice.toBeExterminated = true;
			ice.isScored = true;
			if(ice.getType() == IcicleStatic.STONE) {
				playerProfile.addStoneToField(-1);
			}
			exterminateIfPowerful(ice);
		}
    }
    
    private void exterminateByColorBomb(Icicle colorBomb) {
    	colorBomb.setIcicle();
    	SoundService.playColorBomb();
    	
    	int deletedType = colorBomb.getType();
    	for(int i = 0; i < icicles.size(); i++) {
    		Icicle ice = icicles.get(i);
    		if(ice.getType() == deletedType && !ice.isShining) {
    			ice.startShining();
    			ice.toBeExterminated = true;
    			ice.isScored = true;
    			exterminateIfPowerful(ice);
    		}
    	}
    }

    private boolean startDeletion() {
    	int emptyCellsToDelete = 0;
    	
        int size = icicles.size();
        boolean startDeletion = false;

        if(playerProfile.getCurrentMode().repeatBonus.isAvailable && playerProfile.getCurrentModeId() != PlayerProfile.MODE_PUZZLE && !isShake) {
        	checkForGettingBonus();
        }
        if(isShake) {
        	isShake = false;
        }
        canBeUsedIcicles.clear();
        int iciclesToDelete = 0;
        int iciclesToScore = 0;
        int xScore = 0;
        int yScore = 0;
        for (int i = 0; i < size; i++) {
            Icicle currentIcicle = icicles.get(i);
            currentIcicle.setTargetIcicle(currentIcicle);
            
            if(isEmptyCellUnderIcicle(currentIcicle) && isThereIciclesToDelete) {
            	currentIcicle.toBeExterminated = true;
            	currentIcicle.isScored = true;
            	emptyCellsToDelete++;
            }
            
            if (currentIcicle.isScored) {
                iciclesToScore++;

                xScore += currentIcicle.getCenterX();
                yScore += currentIcicle.getCenterY();
                
                if(currentIcicle.toBeExterminated) {
                	iciclesToDelete++;
                	canBeUsedIcicles.add(currentIcicle);
                	currentIcicle.startAnimation(Icicle.ANIMATION_EXPLOSION);
                }	
                startDeletion = true;
            }
        }
        
//        deletedIcicles += iciclesToDelete;

        if (startDeletion && !shakeHappened) {
            chainCount++;
            if(playerProfile.getCurrentModeId() != PlayerProfile.MODE_PUZZLE) {
	            xScore = xScore / iciclesToScore;
	            yScore = yScore / iciclesToScore;
	            averagePositionRip.set(xScore, yScore);
	            sceneGameplay.incrementScore(iciclesToScore - emptyCellsToDelete, chainCount, averagePositionRip);
            } else {
            	sceneGameplay.incrementScore(iciclesToDelete - emptyCellsToDelete, chainCount, averagePositionRip);
            }
            playerProfile.increaseCurrentBalance(iciclesToDelete);
            isThereIciclesToDelete = false;
        }
        shakeHappened = false;
        return startDeletion;
    }
    
    /**
     * 
     * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
     */    
	private void checkForGettingBonus() {
    	if(isBonusGet(firstIcicle) || isBonusGet(secondIcicle)) {
    		setBonuses();
    		SoundService.playRepeatBonus();
        } else {
        	if(firstIcicle.isScored) {
        		playerProfile.getCurrentMode().repeatBonus.update(firstIcicle.getType());
            } else if(secondIcicle.isScored) {
            	playerProfile.getCurrentMode().repeatBonus.update(secondIcicle.getType());
            }
        }
    }
	
    private boolean isBonusGet(Icicle ice) {
    	if(ice == null) {
    		return false;
    	}
    	return (ice.isScored && ice.getType() == playerProfile.getCurrentMode().repeatBonus.lastUsedIcicle);
    }
    
    public void setBonuses() {
    	int row = StaticConstants.rand.nextInt(Settings.ROWS);
    	int column = StaticConstants.rand.nextInt(Settings.COLUMNS);
    	switch(playerProfile.getCurrentMode().repeatBonus.lastUsedIcicle) {
    	case 0: 
    		exploseRandomPlace(row, column);
    		SoundService.playBomb();
    		break;
    	case 1: 
    		setRandomBonus();    		
    		break;
    	case 2: 
    		sceneGameplay.incrementScore(100, 0, averagePositionRip);
    		break;
    	case 3:
    		repaintIcicles();
    		break;
    	case 4:
    		sceneGameplay.addShakes(Settings.SHAKES_MAX);
    		break;
    	case 5:
    		freezeGameField();
    		break;
    	case 6:
    		deleteAllStones();
    		break;
    	default:
    		return;
    	}
    	playerProfile.getCurrentMode().repeatBonus.reset();
    }
    
    private void exploseRandomPlace(int row, int column) {
    	Icicle bomb = getIcicle(row, column);
    	bomb.setIcicle();
    	bomb.startExplosion();
    	exterminateByBombSecondUpgradeLevel(bomb);
    }
    
    private void setRandomBonus() {
    	int row;
    	int column;
    	Icicle ice;
    	do {
			row = StaticConstants.rand.nextInt(Settings.ROWS);
	    	column = StaticConstants.rand.nextInt(Settings.COLUMNS);
	    	ice = getIcicle(row, column);
		} while(ice.getType() == IcicleStatic.STONE);
		
		switch(StaticConstants.rand.nextInt(3)) {
		case 0: ice.setBomb(); break;
		case 1: ice.setPowerGem(); break;
		case 2: ice.setColorBomb(); break;
		default: return;
    	}
    }
    
    private void repaintIcicles() {
    	int repaintColor = IcicleStatic.getRandomType();
    	int row;
    	int column;
    	for(int i = 0; i < rows; i++) {
    		row = StaticConstants.rand.nextInt(rows);
    		column = StaticConstants.rand.nextInt(columns);
    		Icicle ice = getIcicle(row, column);
    		if(ice.getType() != IcicleStatic.STONE && !ice.isShining) {
    			ice.startShining();
    			ice.changeType(repaintColor);
    		} else {
    			i--;
    		}
    	}
    }

    private void freezeGameField() {
    	isGameFreezed = true;
		sceneGameplay.freeze();
		amountSwappingInFreezeMode = NUMBER_OF_SWAPPINGS_IN_FREEZE;
    }
    
    private void deleteAllStones() {
    	for(int i = 0; i < icicles.size(); i++) {
    			Icicle ice = icicles.get(i);
        		if(ice.getType() == IcicleStatic.STONE) {
        			ice.toBeExterminated = true;
        			ice.isScored = true;
        			playerProfile.addStoneToField(-1);
    		}
    	}
    }
    
    //****** FALLING ****//
    private void startFalling() {
        movingIcicles.clear();
        iciclesBackup.clear();
        commonSharedFlag = 0;

        int useIndex = 0;
        for (int j = 0; j < columns; j++) {
            int negativeDec = 1;
            for (int i = rows - 1; i >= 0; i--) {
                Icicle ice = getIcicle(i, j);
//                isMovingEmpty(ice);
                if (ice.toBeExterminated) {
                	ice.toBeExterminated = true;
                    boolean foundClosest = false;
                    if (i != 0) {
                        for (int k = i - 1; k >= 0; k--) {
                            Icicle topNeighbour = getIcicle(k, j);
                            //found nonempty icicle
                            if (!topNeighbour.toBeExterminated) {
                                topNeighbour.toBeExterminated = true;
                                topNeighbour.setTargetIcicle(ice);
                                PointF from = fixedPositions[topNeighbour.getRow()][topNeighbour.getColumn()];
                                PointF to = fixedPositions[ice.getRow()][ice.getColumn()];
                                topNeighbour.startMotion(from.x, from.y, to.x, to.y, 0.2f);
                                currentState = IceFactoryState.FALLING;
                                movingIcicles.add(topNeighbour);
                                foundClosest = true;
                                break;
                            }
                        }
                    }
                    if (!foundClosest) {
                        Icicle auxIcicle = canBeUsedIcicles.get(useIndex);
                        useIndex++;
                        auxIcicle.setTargetIcicle(ice);
                        float fromX = fixedPositions[ice.getRow()][ice.getColumn()].x;
                        float fromY = ScreenMetrics.screenHeightRip
                                + negativeDec * gridSideLengthRip - gridSideLengthRip;
                        
                        if(PlayerProfile.getInstance().getCurrentModeId() == PlayerProfile.MODE_PUZZLE) {
                        	auxIcicle.changeType(IcicleStatic.EMPTY_CELL);
                        } else if(playerProfile.getStonesOnField() < playerProfile.getStonesMax()
                        		&& StaticConstants.rand.nextBoolean()) {
                        	auxIcicle.changeType(IcicleStatic.STONE);
                        	playerProfile.addStoneToField(1);
                        } else {
                        	auxIcicle.changeType(IcicleStatic.getRandomType());
                        } 
                        
                        
                        
//   if(deletedIcicles < deleteToGetMegaIcicle)                     else {
//                        	auxIcicle.changeType(IcicleStatic.MEGA_ICICLE);
//                        	deletedIcicles = 0;
//                        }
                        
                        PointF to = fixedPositions[ice.getRow()][ice.getColumn()];
                        auxIcicle.startMotion(fromX, fromY, to.x, to.y, 0.2f);
                        currentState = IceFactoryState.FALLING;
                        movingIcicles.add(auxIcicle);
                        negativeDec++;
                    }
                }
            }
        }

        int sizeOriginal = iciclesOriginal.size();
        for (int i = 0; i < sizeOriginal; i++) {
            Icicle ice = iciclesOriginal.get(i);
            adjustTargetPositionInArray(ice);
        }
        
        


        if (playerProfile.getCurrentModeId() == PlayerProfile.MODE_CLASSIC) {
            if (!areThereMovesLeft(false)) {
                //ZLog.i("MODE_CLASSIC,  NO MOVES LEFT");
                gameOver = true;
            }
        } else {
        while (!areThereMovesLeft(false)) {
            ZLog.i("GENERATING NEW MOVES");
            int length = canBeUsedIcicles.size();
            for (int i = 0; i < length; i++) {
                canBeUsedIcicles.get(i).changeType(IcicleStatic.getRandomType());
            }
        }
        }

        for (int k = 0; k < canBeUsedIcicles.size(); k++) {
            canBeUsedIcicles.get(k).visible = true;
        }

    }
    
    private boolean isEmptyCellUnderIcicle(Icicle ice) {
    	if(ice.getType() == IcicleStatic.EMPTY_CELL) {
    		for( int i = ice.getRow()-1; i >= 0; i--) {
    			Icicle ice2 = getIcicle(i, ice.getColumn());
    			if(ice2.getType() != IcicleStatic.EMPTY_CELL) {
    				return true;
    			}
    		}
    		return false;
    	}
    	return false;
    }
    
    private boolean isIceDropSoundShouldPlay() {
    	for(int i = 0; i < movingIcicles.size(); i++) {
        	if(movingIcicles.get(i).getType() != IcicleStatic.EMPTY_CELL) {
        		return true;
        	}
        }
    	return false;
    }

    private void resetIcicles() {
        int size = iciclesOriginal.size();
        for (int i = 0; i < size; i++) {
            Icicle ice = iciclesOriginal.get(i);
            ice.toBeExterminated = false;
            ice.isScored = false;
            adjustTargetPositionInArray(ice);
        }
        checkForDeletionWholeBoard();
    }

    //** MOVES **//
    private void checkForDeletionWholeBoard() {
        int size = icicles.size();
        boolean deleting = false;
        for (int i = 0; i < size; i++) {
            Icicle ice = icicles.get(i);
            checkForDeletionToRight(ice);
            checkForDeletionToDown(ice);
            checkForDeletionBroken(ice);
	
            if (ice.toBeExterminated) {
                deleting = true;
            }
        }
        
        if (deleting) {
            currentState = IceFactoryState.DELETING;
            float playSpeed = 1.2f + 0.2f * (chainCount - 2);
            SoundService.playRemove(playSpeed);
            playerProfile.getCurrentMode().repeatBonus.isAvailable = false;
            startDeletion();
        } else {
            currentState = IceFactoryState.INTERACTIVE;
            if (gameOver) {
                sceneGameplay.onGameOver();
            } else {
                startGlowingWave();
            }
        }
    }

    public boolean areThereMovesLeft(boolean findAllPossibleMoves) {
        boolean movesLeft = false;
        hintIcicles.clear();
        int size = icicles.size();
        for (int i = 0; i < size; i++) {
            Icicle ice = icicles.get(i);
            int row = ice.getRow();
            int column = ice.getColumn();
            Icicle leftNeighbour = getIcicle(row, column - 1);
            Icicle rightNeighbour = getIcicle(row, column + 1);
            Icicle topNeighbour = getIcicle(row - 1, column);
            Icicle bottomNeighbour = getIcicle(row + 1, column);
            boolean left = false;
            boolean right = false;
            boolean top = false;
            boolean bottom = false;
            if (leftNeighbour != null) {
                left = checkForPossibleDeletion(leftNeighbour.getRow(), leftNeighbour.getColumn(), ice.getType(), checkTypeLeft);
            }
            if (rightNeighbour != null) {
                right = checkForPossibleDeletion(rightNeighbour.getRow(), rightNeighbour.getColumn(), ice.getType(), checkTypeRight);
            }
            if (topNeighbour != null) {
                top = checkForPossibleDeletion(topNeighbour.getRow(), topNeighbour.getColumn(), ice.getType(), checkTypeTop);
            }
            if (bottomNeighbour != null) {
                bottom = checkForPossibleDeletion(bottomNeighbour.getRow(), bottomNeighbour.getColumn(), ice.getType(), checkTypeBottom);
            }

            if (left || right || top || bottom) {
                movesLeft = true;
                hintIcicles.add(ice);
                if (!findAllPossibleMoves) {
                    break;
                }
            }
        }
        return movesLeft;
    }

    ///*** OTHER ***///
    private void createSelection() {
        selectionView = new ZNode("selectionView");
        selectionView.visible = false;
        selectionView.touchable = false;
        selectionView.initWithFrame(selectionScreenRectRip, TexturesManager.getInstance().getRegionByName("selection"));
    }

    private void showSelection(Icicle ice) {
        selectionView.setSameCenter(ice);
        selectionView.visible = true;
    }

    public void incrementSharedFlag() {
        commonSharedFlag++;
//        ZLog.i("++ " + commonSharedFlag);
    }

    private void exchangeSwappingIcicles() {
        if (firstIcicle != null && secondIcicle != null) {
            firstIcicle.exchangeInformation(secondIcicle);
            icicles.set(firstIcicle.getIndex(), firstIcicle);
            icicles.set(secondIcicle.getIndex(), secondIcicle);
        }
    }

    private void adjustTargetPositionInArray(Icicle ice) {
        ice.setTargetCoordinates();
        icicles.set(ice.getIndex(), ice);
    }

    private Icicle getIcicle(int i, int j) {
        if (i >= 0 && i < rows && j >= 0 && j < columns) {
            return icicles.get(i * columns + j);
        } else {
            return null;
        }
    }
    //** HINT **//

    public void showHint() {
        if (hintIcicles == null) {
            return;
        }
        boolean hintAvailable = playerProfile.getCurrentMode().hintAvailable();
        if (!hintAvailable) {
            SoundService.playNoHint();
            sceneGameplay.showNoHint();
            return;
        }
        if (!areThereMovesLeft(true)) {
            SoundService.playNoHint();
            return;
        }
        int ind = StaticConstants.rand.nextInt(hintIcicles.size());
        Icicle ice = hintIcicles.get(ind);
        selectFirstIcicle(ice);
        SoundService.playHint();
        sceneGameplay.useHint();
    }

    //** GLOWING ANIMATION **//
    private void startGlowingWave() {
        Runtime.getRuntime().gc();
        if (SystemClock.uptimeMillis() - startGlowingTime < minimalGlowingPeriod) {
            return;
        }
        startGlowingTime = SystemClock.uptimeMillis();
        glowingRowIndex = 0;
        Icicle ice = icicles.get(0);
        if(ice.isGlowingAnimationAvailable) {
        	ice.startAnimation(Icicle.ANIMATION_GLOW);
        }
    }

    public void triggerNextGlowingWave() {
        glowingRowIndex++;
        int size = icicles.size();
        for (int i = 0; i < size; i++) {
            Icicle ice = icicles.get(i);
            if (ice.getRow() + ice.getColumn() == glowingRowIndex
            		&& ice.isGlowingAnimationAvailable) {
            	ice.startAnimation(Icicle.ANIMATION_GLOW);
            }
        }
    }
    //** GAMEPLAY DATA **//

    public void saveIcicleTypes() {
        GameMode m = playerProfile.getCurrentMode();
        for (int i = 0; i < icicles.size(); i++) {
            Icicle ice = icicles.get(i);
            m.icicleTypes[ice.getRow()][ice.getColumn()] = ice.getType();
            m.iciclePowers[ice.getRow()][ice.getColumn()] = ice.getPower();
        }
    }
////
////    private boolean checkForFirstTime(int[][] icicleTypes) {
////        if (icicleTypes == null) {
////            return true;
////        }
////        boolean firstTime = true;
////        int valueDefault = icicleTypes[0][0];
////        for (int j = 1; j < 6; j++) {
////            int value = icicleTypes[0][j];
////            if (valueDefault != value) {
////                firstTime = false;
////            }
////        }
////        return firstTime;
////    }
    private long glitterTime = 0;
    private long glitterMinimalPeriod = 6000;

    private void starGlitter() {
        if (currentState == IceFactoryState.INTERACTIVE && SystemClock.uptimeMillis() > glitterTime) {
            int ind = StaticConstants.rand.nextInt(rows * columns - 1);
            Icicle mIce = icicles.get(ind);
            if(mIce.getType() == IcicleStatic.EMPTY_CELL) {
            	return;
            }
            float xRip = mIce.getCenterX() + 6;
            float yRip = mIce.getCenterY() + 8;
            sceneGameplay.starGlitterStart(xRip, yRip);
            glitterTime = SystemClock.uptimeMillis() + glitterMinimalPeriod + StaticConstants.rand.nextInt(3000);
        }
    }

    public void hideIcicles() {
        selectionView.visible = false;
        for (int i = 0; i < icicles.size(); i++) {
            icicles.get(i).visible = false;
            icicles.get(i).getBonus().visible = false;
        }
    }

    public void showIcicles() {
        for (int i = 0; i < icicles.size(); i++) {
            icicles.get(i).visible = true;
            if(icicles.get(i).getPower() != IcicleStatic.ICICLE) {
            	icicles.get(i).getBonus().visible = true;
            }
        }
    }

    public void onShakeEvent() {
    	isShake = true;
        if (currentState == IceFactoryState.INTERACTIVE) {
            markRandomIciclesToDelete(6);
            shakeHappened = true;
            boolean startDeletion = startDeletion();
            if (startDeletion) {
                SoundService.playRemove(1);
                currentState = IceFactoryState.DELETING;
            }
        }
    }

    private void markRandomIciclesToDelete(int number) {
        for (int i = 0; i < number; i++) {
            int row = i % rows;
            int col = StaticConstants.rand.nextInt(columns);
            Icicle ice = getIcicle(row, col);
            if (ice != null) {
                ice.toBeExterminated = true;
                ice.isScored = true;
                if(ice.getType() == IcicleStatic.STONE) {
                	playerProfile.addStoneToField(-1);
                }
            }
        }
    }

    public void onGameOver() {
        currentState = IceFactoryState.GAME_OVER;
    }
}
