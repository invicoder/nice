/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import android.graphics.PointF;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.boolbalabs.lib.graphics.ZSprite2D;
import com.boolbalabs.nice.settings.Settings;
/**
 * 
 * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
 */

public class LightningVertical extends ZSprite2D {

	private static final String[] frameNames = {"lightning_1v", "lightning_2v", "lightning_3v"};
	public static final int ANIMATION_LIGHTNING = 10;
	
	private int row;
	private int column;
	private int gridSideLengthRip;
	private PointF fixedCenterRip;
	private int realSideLengthRip = 25;
	private int delay = 50;
	
	private Rectangle screenRectRip;
	
	public LightningVertical(IceFactory iceFactory, Icicle ice) {
		super("vert" + ice.getRow() + "" + ice.getColumn());
        this.row = ice.getRow();
        this.column = ice.getColumn();
        //** POSITION **//
        gridSideLengthRip = IceFactory.gridSideLengthRip;
        setPositionFirstLevel(ice);

        initializeAnimation(frameNames, new int[]{0, 1, 2, 0, 1, 2}, new int[]{delay, 2*delay, 3*delay, 4*delay, 5*delay, 6*delay}, screenRectRip, false);
	}
	
	public void setPositionFirstLevel(Icicle ice) {
		this.row = ice.getRow();
        this.column = ice.getColumn();
		
		fixedCenterRip = new PointF(IceFactory.screenRectRip.x + column * gridSideLengthRip + gridSideLengthRip / 2,
                IceFactory.screenRectRip.y + (Settings.ROWS - row - 1) * gridSideLengthRip + gridSideLengthRip / 2);
        screenRectRip = new Rectangle(fixedCenterRip.x - realSideLengthRip / 2,
        		fixedCenterRip.y - realSideLengthRip / 2,
                realSideLengthRip, realSideLengthRip * Settings.COLUMNS);
        setScreenFrame(screenRectRip);
	}
	
	public void setPositionFourthLevel(Icicle ice) {
		this.row = ice.getRow();
        this.column = ice.getColumn();
		
		fixedCenterRip = new PointF(IceFactory.screenRectRip.x + column * gridSideLengthRip + gridSideLengthRip / 2,
                IceFactory.screenRectRip.y + (Settings.ROWS - row - 1) * gridSideLengthRip + gridSideLengthRip / 2);
        screenRectRip = new Rectangle(fixedCenterRip.x - realSideLengthRip / 2,
        		fixedCenterRip.y - realSideLengthRip / 2 - (Settings.ROWS - row - 1) * realSideLengthRip,
                realSideLengthRip, realSideLengthRip * Settings.COLUMNS);
        setScreenFrame(screenRectRip);
	}
	
	@Override
    public void update() {
    	super.update();
    	act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
    }

    @Override
    public void zdraw(SpriteBatch batch, float parentAlpha) {
        act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        super.draw(batch, parentAlpha);
    }
    
    public void startAnimationFirstLevel(Icicle ice) {
    	setPositionFirstLevel(ice);
    	startAnimation();
    }
    
    public void startAnimationFourthLevel(Icicle ice) {
    	setPositionFourthLevel(ice);
    	startAnimation();
    }

    @Override
    public void onAnimationStarted(int state) {
   		visible = true;
    }

    @Override
    public void onAnimationFinished(int state) {
    	resetAnimation();
       	visible = false;
    }
}
