/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import com.badlogic.gdx.math.Rectangle;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.TexturesManager;

/**
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class VisualBoard extends ZNode {

    public static final String NAME = "visual_board";
    private final int skipFromTop = 21 + 13 + 21;
    private Rectangle screenRectRip = new Rectangle(0, ScreenMetrics.screenHeightRip - skipFromTop - ScreenMetrics.screenWidthRip,
            ScreenMetrics.screenWidthRip, ScreenMetrics.screenWidthRip);
    private TexturesManager texturesManager;

    //***********************************************************//
    public VisualBoard() {
        super(NAME);
        touchable = false;
        texturesManager = TexturesManager.getInstance();
        region.setRegion(texturesManager.getRegionByName("board_bg"));
        setScreenFrame(screenRectRip);
    }

    public void onLevelUp() {
    }

    @Override
    public void onShow() {
    }

    @Override
    public void onHide() {
    }

//    @Override
//    protected void draw(SpriteBatch batch, float parentAlpha) {
//        //batch.setColor(1.0f, 0, 0, 0.2f);
//        GL11 gl = Gdx.gl11;
//        gl.glColor4f(1.0f, 0, 0, 0.5f);
//        batch.setBlendFunction(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);
//        super.draw(batch, parentAlpha);
//        batch.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
//        batch.setColor(1, 1, 1, 1);
//    }
}
