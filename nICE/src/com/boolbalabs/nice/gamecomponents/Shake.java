/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.nice.SceneGameplay;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.extra.ShakeListener;
import com.boolbalabs.nice.settings.Settings;
import com.boolbalabs.nice.utils.ZLog;

/**
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class Shake extends ZNode {

    private SceneGameplay sceneGameplay;
    private int rightX = ScreenMetrics.screenWidthRip;
    private int yPosition = ScreenMetrics.screenHeightRip - 42;
    private int singleShakeWidth = 20;
    private int singleShakeHeight = 22;
    private int shakesVisisble = Settings.SHAKES_INITIAL;
    private ShakeListener shakeListener;
    private PlayerProfile playerProfile;

    //***********************************************************//
    public Shake(SceneGameplay sceneGameplay) {
        super("Shake");
        parentScene = sceneGameplay;
        this.sceneGameplay = (SceneGameplay) parentScene;
        region = TexturesManager.getInstance().getRegionByName("shake_icon_small");
        visible = true;
        shakesVisisble = Settings.SHAKES_LEFT;
        shakeListener = new ShakeListener();
        ShakeListener.setSensitivity(Settings.SHAKE_SENSITIVITY);
        playerProfile = PlayerProfile.getInstance();
    }

    @Override
    public void onShow() {
        shakesVisisble = Settings.SHAKES_LEFT;
    }

    @Override
    protected void draw(SpriteBatch batch, float parentAlpha) {
        for (int i = 0; i < shakesVisisble; i++) {
            int xPos = rightX - singleShakeWidth * (i + 1) - 3;
            batch.draw(region, xPos, yPosition, singleShakeWidth, singleShakeHeight);
        }
    }

    @Override
    public void update() {
        if (shakeListener.onSensorChanged()) {
            onShakeEvent();
        }
    }

    public void onShakeEvent() {
        if (isShakesLeft()) {
            addShakes(-1);
            sceneGameplay.onShakeEvent();
        }
    }

    public void addShakes(int toAdd) {
        int newShakes = Math.min(Settings.SHAKES_MAX, Settings.SHAKES_LEFT + toAdd);
        if (newShakes < 0) {
            newShakes = 0;
        }
        Settings.SHAKES_LEFT = newShakes;
        shakesVisisble = Settings.SHAKES_LEFT;
    }

    public boolean isShakesLeft() {
        return Settings.SHAKES_LEFT > 0;
    }
}
