/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import android.graphics.Rect;
import android.os.SystemClock;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.graphics.AnimationState;
import com.boolbalabs.lib.graphics.ZSprite2D;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.nice.R;
import com.boolbalabs.nice.SceneGameplay;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.modes.GameMode;
import com.boolbalabs.nice.settings.StaticConstants;
import com.boolbalabs.nice.utils.ZLog;

/**
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class ProgressBar extends ZSprite2D {

    private TexturesManager texturesManager;
    private ZNode fillView;
    //*******SCREEN/TEXTURE*******//
    private final int skipFromTop = 21 + 21;//345;
    private Rectangle bgScreenRect = new Rectangle(0, ScreenMetrics.screenHeightRip - skipFromTop - 13, ScreenMetrics.screenWidthRip, 13);
    private Rectangle fillScreenRect = new Rectangle(bgScreenRect);
    private TextureRegion[] fillRegions;
    private TextureRegion currentFillRegion;
    private TextureRegion tmpFillRegion = new TextureRegion();
    private boolean isInitialized = false;
    /**
     * in some conventional units
     */
    public static final int MAX_CAPACITY = 100000;
    private int currentUnits;
    private int currentRealUnits;
    //***** animation **//
    public static final int ANIMATION_BLINK = 1;
    private final int incrementValue = 300;
    private boolean shouldCallUpdate;
    //***** other **//
    private PlayerProfile playerProfile;
    private SceneGameplay sceneGameplay;
    private GameMode currentMode;
    //***** Timing **//
    private long startTimeMs;
    private long previousTimeMs;
    private final long updateIntervalMs = 50;
    private int timeDecrement;
    private boolean timedMode;
    private long startWaitingTime;
    private long waitingDuration = 4000;
    private boolean waiting = false;

    //********************************************//
    public ProgressBar(SceneGameplay sceneGameplay) {
        super("ProgressBar");
        parentScene = sceneGameplay;
        this.sceneGameplay = (SceneGameplay) parentScene;
        texturesManager = TexturesManager.getInstance();
        playerProfile = PlayerProfile.getInstance();
        setDefaultFrameIndexToDraw(0);
        setScreenFrame(bgScreenRect);
        initAnimationStates();
        initializeAnimation(new String[]{"pb_empty", "pb_1_highlight", "pb_2_highlight"}, 50, bgScreenRect, false);
        //***** FILL VIEW ****//
        createFillViews();
        shouldCallUpdate = false;
    }

    private void createFillViews() {
        fillView = new ZNode("fillView");
        fillRegions = new TextureRegion[3];
        fillRegions[0] = texturesManager.getRegionByName("pb_1_full");
        fillRegions[1] = texturesManager.getRegionByName("pb_2_full");
        fillRegions[2] = texturesManager.getRegionByName("pb_3_full");
        setRandomFillRect();
    }

    private void initAnimationStates() {
        final int delay = 120;
        AnimationState blinkAnimation = new AnimationState(ANIMATION_BLINK, new int[]{2, 1, 2, 1, 2, 1, 2, 1, 2, 1},
                new int[]{delay, 2 * delay,
                    3 * delay, 4 * delay, 5 * delay, 6 * delay, 7 * delay, 8 * delay,
                    9 * delay, 10 * delay}, false);
        addState(blinkAnimation);
    }

    @Override
    public void onShow() {
        restart();
    }

    @Override
    protected void draw(SpriteBatch batch, float parentAlpha) {
    	if(PlayerProfile.getInstance().getCurrentModeId() == PlayerProfile.MODE_PUZZLE) {
    		return;
    	}
        super.draw(batch, parentAlpha);
        fillView.zdraw(batch, parentAlpha);
    }

    public void restart() {
        currentMode = playerProfile.getCurrentMode();
        timedMode = playerProfile.isTimedMode();
        resetBar(currentMode.progressUnits);
        timeDecrement = (int) ((float) playerProfile.getUnitsPerSecondDecrement() * updateIntervalMs / 1000.0f);
        if (timedMode) {
            startTimeMs = SystemClock.uptimeMillis();
            previousTimeMs = startTimeMs;
            if (playerProfile.getCurrentModeId() == PlayerProfile.MODE_TIME) {
                resetBar(0);
                addUnits(currentMode.progressUnits);
                startWaitingTime = SystemClock.uptimeMillis();
                waiting = true;
            }
        }
        isInitialized = true;
    }

    public void incrementBar(int icicleNumber, int chainCount) {
    	if(sceneGameplay.isOnPause) {
    		return;
    	}
        currentMode.setCurrentProgressUnits(currentRealUnits);
        int unitsToAdd = currentMode.unitsForIncrementBar(icicleNumber, chainCount);
        if (currentMode.isLevelUp()) {
            onLevelUp();
        } else {
            addUnits(unitsToAdd);
        }
    }

    public void addUnits(int unitsToAdd) {
        currentRealUnits += unitsToAdd;
        if (currentRealUnits > MAX_CAPACITY) {
            currentRealUnits = MAX_CAPACITY;
        }
        if (currentRealUnits < 0) {
            currentRealUnits = 0;
        }
        shouldCallUpdate = true;
    }

    private void setRatio() {
        float alpha = (float) currentUnits / (float) MAX_CAPACITY;
        fillScreenRect.setWidth(bgScreenRect.width * alpha);
        tmpFillRegion.setRegionWidth((int) (currentFillRegion.getRegionWidth() * alpha));
        fillView.initWithFrame(fillScreenRect, tmpFillRegion);
    }

    private void onFillMax() {
    }

    @Override
    public void onAnimationStarted(int state) {
        switch (state) {
            case ANIMATION_BLINK:
                fillView.visible = false;
                break;
            default:
                break;

        }
    }

    @Override
    public void onAnimationFinished(int state) {
        switch (state) {
            case ANIMATION_BLINK:
                fillView.visible = true;
                break;
            default:
                break;

        }
    }

    private void onEmpty() {
        if (isInitialized && timedMode) {
            ZLog.i("onEmpty");
            timedMode = false;
            isInitialized = false;
            sceneGameplay.onGameOver();
        }
    }

    public void onLevelUp() {
        sceneGameplay.onLevelUp();
        if (playerProfile.getCurrentModeId() != PlayerProfile.MODE_QUICK) {
            startAnimation(ANIMATION_BLINK);
            resetBar(playerProfile.getInitialProgressValue());
            setRandomFillRect();
        }
    }

    public void useHint() {
        currentMode.setCurrentProgressUnits(currentRealUnits);
        int unitsToAdd = currentMode.useHint();
        addUnits(unitsToAdd);
    }

    @Override
    public void update() {
        updateTimeData();
        if (shouldCallUpdate && !isStarted()) {
            int sign = (int) Math.signum(currentRealUnits - currentUnits);
            currentUnits += sign * incrementValue;
            if (currentUnits >= MAX_CAPACITY && currentRealUnits == MAX_CAPACITY) {
                currentUnits = MAX_CAPACITY;
                shouldCallUpdate = false;
                onFillMax();
            } else if (currentUnits <= 0 && currentRealUnits == 0) {
                currentUnits = 0;
                shouldCallUpdate = false;
                onEmpty();
            }
            if ((currentUnits >= currentRealUnits && sign >= 0) || (currentUnits <= currentRealUnits && sign < 0)) {
                currentUnits = currentRealUnits;
                shouldCallUpdate = false;
            }
            setRatio();
        }
        super.update();
    }

    public void resetBar(int initialValue) {
        currentRealUnits = initialValue;
        currentUnits = initialValue;
        addUnits(0);
    }

    public int getCurrentRealUnits() {
        return currentRealUnits;
    }

    public void setRandomFillRect() {
        int ind = StaticConstants.rand.nextInt(100) % 3;
        currentFillRegion = fillRegions[ind];
        tmpFillRegion.setRegion(currentFillRegion);
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
        fillView.visible = visible;
    }

    private void updateTimeData() {
        if (!timedMode) {
            return;
        }
        long currentTime = SystemClock.uptimeMillis();
        if (waiting) {
            if (currentTime - startWaitingTime > waitingDuration) {
                waiting = false;
                startTimeMs = currentTime;
                previousTimeMs = currentTime;
            } else {
                return;
            }
        }
        if (currentTime - previousTimeMs > updateIntervalMs) {
            long timeError = currentTime - previousTimeMs - updateIntervalMs;
            float alpha = Math.min(1, (float) timeError / (float) updateIntervalMs);
            int dec = (int) (-timeDecrement * (1 + alpha));
            addUnits(dec);
            previousTimeMs = currentTime;
        }

    }
}
