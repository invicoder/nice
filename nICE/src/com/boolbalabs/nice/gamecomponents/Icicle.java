/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import java.util.ArrayList;

import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.SystemClock;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.OnActionCompleted;
import com.badlogic.gdx.scenes.scene2d.actions.MoveTo;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateInterpolator;
import com.badlogic.gdx.utils.Array;
import com.boolbalabs.lib.graphics.AnimationState;
import com.boolbalabs.lib.graphics.ZSprite2D;
import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.nice.R;
import com.boolbalabs.nice.settings.Settings;
import com.boolbalabs.nice.settings.StaticConstants;
import com.boolbalabs.nice.utils.ZLog;

/**
 *
 * @author Igor Trubnikov <itrubnikov@gmail.com>
 */
public class Icicle extends ZSprite2D {
	
    private TexturesManager texturesManager;
    private IceFactory iceFactory;
    //** coordinates **//
    private int row;
    private int column;
    private PointF fixedCenterRip;
    private int gridSideLengthRip;
    private int realSideLengthRip = 56;
    //** rects **//
    private Rectangle screenRectRip;
    //*** type **//
    private int icicleType;
    private int iciclePower;
    public int targetType;
    //*** Movement **//
    private MoveTo actionMoveTo;
    private OnActionCompleted onActionCompleted;
    private AccelerateInterpolator accelerateInterpolator;
    //*** Deleting, hello to Daleks **//
    public boolean toBeExterminated = false;
    public boolean isScored = false;
    //** falling **//
    private int targetRow;
    private int targetColumn;
    //***** animation **//
    public static final int ANIMATION_EXPLOSION = 1;
    public static final int ANIMATION_GLOW = 2;
    public static final int ANIMATION_ROTATE = 3;
    
    private Bonus bonusView;
    public boolean isGlowingAnimationAvailable = true;
    public boolean isExplodes = false;
    public boolean isShining = false;
    public Lightning lightning;

    //**********************************//
    public Icicle(IceFactory iceFactory, int row, int column) {    	
        super("icicle" + row + "" + column);
        this.row = row;
        this.column = column;
        this.iceFactory = iceFactory;

        lightning = new Lightning(iceFactory, this);
        
        icicleType = IcicleStatic.highEntropyIndices[row][column];
        //** POSITION **//
        gridSideLengthRip = IceFactory.gridSideLengthRip;
        fixedCenterRip = new PointF(IceFactory.screenRectRip.x + column * gridSideLengthRip + gridSideLengthRip / 2,
                IceFactory.screenRectRip.y + (Settings.ROWS - row - 1) * gridSideLengthRip + gridSideLengthRip / 2);
        screenRectRip = new Rectangle(fixedCenterRip.x - realSideLengthRip / 2, fixedCenterRip.y - realSideLengthRip / 2,
                realSideLengthRip, realSideLengthRip);
        setScreenFrame(screenRectRip);
        
        bonusView = new Bonus(iceFactory, this);

        setIcicle();

        initializeAnimation(IcicleStatic.getFrameNamesByType(icicleType), 50, screenRectRip, false);
        initAnimationStates();
        createMoveToAction();
        createParticles();
    }

    private void initAnimationStates() {
        final int delay = 50;
        AnimationState explosionAnimation = new AnimationState(ANIMATION_EXPLOSION, new int[]{0, 1, 2, 3, 4, 5, 6, 7}, new int[]{delay, 2 * delay,
                    3 * delay, 4 * delay, 5 * delay, 6 * delay, 7 * delay, 8 * delay}, false);
        AnimationState glowAnimation = new AnimationState(ANIMATION_GLOW, new int[]{8}, new int[]{20}, false);
        AnimationState rotationAnimation = new AnimationState(ANIMATION_ROTATE, new int[]{0, 9, 10}, new int[]{delay, 2 * delay,
                    3 * delay}, true);
        addState(explosionAnimation);
        addState(glowAnimation);
        addState(rotationAnimation);
    }
    
    public void changePower(int newPower) {
    	switch(newPower) {
    	case IcicleStatic.BOMB: setBomb(); break;
    	case IcicleStatic.COLOR_BOMB: setColorBomb(); break;
    	case IcicleStatic.POWER_GEM: setPowerGem(); break;
    	case IcicleStatic.ICICLE: setIcicle(); break;
    	default: return;
    	}
    }

    public void changeType(int newType) {
        if (IcicleStatic.isValidType(newType)) {
            icicleType = newType;
            setFrameNames(IcicleStatic.getFrameNamesByType(icicleType));
        }
    }

    public void switchToTargetType() {
        changeType(targetType);
    }
    
    public Rectangle getScreenRectRip() {
    	return screenRectRip;
    }

    //** ANIMATION **//
    @Override
    public void update() {
    	super.update();
    	act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
    	//bonusView.update();
    	if(isExplodes) {
    		explosion.setPosition(x + 32, y + 32);
    	}	
    	color.setPosition(x + 28, y + 28);
    	//lightning.update();
    }

    @Override
    public void zdraw(SpriteBatch batch, float parentAlpha) {
        act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        if(icicleType != IcicleStatic.EMPTY_CELL) {
        	super.zdraw(batch, parentAlpha);
        }
        //bonusView.zdraw(batch, parentAlpha);
        if(isExplodes) {
        	explosion.draw(batch, Gdx.graphics.getDeltaTime());
        	if(explosion.isComplete()) {
        		isExplodes = false;
        		iceFactory.decrementSharedFlag();
        	}	
        }	
        if(isShining) {
        	color.draw(batch, Gdx.graphics.getDeltaTime());
        	if(color.isComplete()) {
        		isShining = false;
        		iceFactory.decrementSharedFlag();
        	}	
        }	
        //lightning.zdraw(batch, parentAlpha);
    }
    
    public Bonus getBonus() {
    	return bonusView;
    }
    
    public Lightning getLightning() {
    	return lightning;
    }

    @Override
    public void onAnimationStarted(int state) {
        if (state == ANIMATION_EXPLOSION) {
            if (iceFactory != null) {
                iceFactory.incrementSharedFlag();
            }
            isGlowingAnimationAvailable = false;
        }
    }

    @Override
    public void onAnimationFinished(int state) {
        resetAnimation();
        if (state == ANIMATION_EXPLOSION) {
            if (iceFactory != null) {
                visible = false;
                iceFactory.decrementSharedFlag();
                isGlowingAnimationAvailable = true;
            }
        } else if (state == ANIMATION_GLOW) {
            if ((column == 0 && row != Settings.ROWS - 1) || (row == Settings.ROWS - 1)) {
                if (column != Settings.COLUMNS - 1 && iceFactory != null) {
                     iceFactory.triggerNextGlowingWave();
                }
            }
        }
    }

    //** TRANSITIONS **//
    private void createMoveToAction() {
        onActionCompleted = new OnActionCompleted() {

            public void completed(Action action) {
                if (action == actionMoveTo) {
                    iceFactory.decrementSharedFlag();
                }
            }
        };
    }

    public void startMotion(float fromX, float fromY, float toX, float toY, float durationSec) {
        if (iceFactory != null) {
            clearActions();
            x = fromX;
            y = fromY;
            actionMoveTo = MoveTo.$(toX, toY, durationSec);
            actionMoveTo.setInterpolator(AccelerateInterpolator.$());
            actionMoveTo.setCompletionListener(onActionCompleted);
            iceFactory.incrementSharedFlag();
            action(actionMoveTo);
            bonusView.startMotion(fromX, fromY, toX, toY, durationSec);
        }
    }

    //****** GETTERS / SETTERS *****//
    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    public int getTargetColumn() {
        return targetColumn;
    }

    public int getTargetRow() {
        return targetRow;
    }

    public int getIndex() {
        return row * Settings.COLUMNS + column;
    }

    public int getType() {
        return icicleType;
    }
    
    public int getPower() {
    	return iciclePower;
    }

    public void setRowAndColumn(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public void setTargetIcicle(Icicle ice) {
        targetRow = ice.row;
        targetColumn = ice.column;
    }

    public void setTargetCoordinates() {
        row = targetRow;
        column = targetColumn;
    }

    public boolean isNeighbour(Icicle ice) {
        int dist = Math.abs(row - ice.getRow()) + Math.abs(column - ice.getColumn());
        return dist == 1;
    }
    
    public boolean formFiveToDelete(Icicle ice1, Icicle ice2, Icicle ice3, Icicle ice4) {
    	 if (ice1 == null || ice2 == null || ice3 == null || ice4 == null
    			||this.icicleType == IcicleStatic.EMPTY_CELL
          		|| ice1.icicleType == IcicleStatic.EMPTY_CELL 
          		|| ice2.icicleType == IcicleStatic.EMPTY_CELL
          		|| ice3.icicleType == IcicleStatic.EMPTY_CELL
          		|| ice4.icicleType == IcicleStatic.EMPTY_CELL
          		|| this.icicleType == IcicleStatic.STONE
        		|| ice1.icicleType == IcicleStatic.STONE
        		|| ice2.icicleType == IcicleStatic.STONE
        		|| ice3.icicleType == IcicleStatic.STONE
        		|| ice4.icicleType == IcicleStatic.STONE) {
              return false;
          }
    	 
    	 return icicleType == ice1.getType() && icicleType == ice2.getType() 
    	 			&& icicleType == ice3.getType() && icicleType == ice4.getType();
    }
    
    public boolean formQuatroToDelete(Icicle ice1, Icicle ice2, Icicle ice3) {
    	 if (ice1 == null || ice2 == null || ice3 == null || this.icicleType == IcicleStatic.EMPTY_CELL
         		|| ice1.icicleType == IcicleStatic.EMPTY_CELL 
         		|| ice2.icicleType == IcicleStatic.EMPTY_CELL
         		|| ice3.icicleType == IcicleStatic.EMPTY_CELL
         		|| this.icicleType == IcicleStatic.STONE
        		|| ice1.icicleType == IcicleStatic.STONE
        		|| ice2.icicleType == IcicleStatic.STONE
        		|| ice3.icicleType == IcicleStatic.STONE) {
             return false;
         }

         return icicleType == ice1.getType() && icicleType == ice2.getType() && icicleType == ice3.getType();
    }

    public boolean formTripleToDelete(Icicle ice1, Icicle ice2) {
        if (ice1 == null || ice2 == null 
        		|| this.icicleType == IcicleStatic.EMPTY_CELL
        		|| ice1.icicleType == IcicleStatic.EMPTY_CELL 
        		|| ice2.icicleType == IcicleStatic.EMPTY_CELL
        		|| this.icicleType == IcicleStatic.STONE
        		|| ice1.icicleType == IcicleStatic.STONE
        		|| ice2.icicleType == IcicleStatic.STONE) {
            return false;
        }
//        if(icicleType == IcicleStatic.MEGA_ICICLE && ice1.getType() == ice2.getType()
//        		|| ice1.getType() == IcicleStatic.MEGA_ICICLE && icicleType == ice2.getType()
//        		|| ice2.getType() == IcicleStatic.MEGA_ICICLE && ice1.getType() == icicleType
//        		|| icicleType == IcicleStatic.MEGA_ICICLE && ice1.getType() == IcicleStatic.MEGA_ICICLE
//        		|| icicleType == IcicleStatic.MEGA_ICICLE && ice2.getType() == IcicleStatic.MEGA_ICICLE
//        		|| ice1.getType() == IcicleStatic.MEGA_ICICLE && ice2.getType() == IcicleStatic.MEGA_ICICLE) {
//        	return true;
//        }
        return icicleType == ice1.getType() && icicleType == ice2.getType();
    }

    public void exchangeInformation(Icicle ice) {
        int tmpRow = row;
        int tmpColumn = column;
        row = ice.getRow();
        column = ice.getColumn();
        ice.setRowAndColumn(tmpRow, tmpColumn);
    }
    
    //*** POWER ***//
    public void setBomb() {
    	iciclePower = IcicleStatic.BOMB;
    	bonusView.setBomb(this);
    }
    
    public void setPowerGem() {
    	iciclePower = IcicleStatic.POWER_GEM;
    	bonusView.setPowerGem(this);
    }
    
    public void setColorBomb() {
    	iciclePower = IcicleStatic.COLOR_BOMB;
    	bonusView.setColorBomb(this);
    }
    
    public void setIcicle() {
    	iciclePower = IcicleStatic.ICICLE;
    	bonusView.setIcicle();
    	setDefaultFrameIndexToDraw(0);
    }
    
    //********** pause / resume ************//
//    @Override
//    public void onPause() {
//        if (movement != null) {
//            movement.pause();
//        }
//        pause();
//        super.onPause();
//    }
//
//    @Override
//    public void onResume() {
//        if (movement != null) {
//            movement.resume();
//        }
//        resume();
//        super.onResume();
//    }
    
    private ParticleEffect explosion;
    private ParticleEffect color;

    private void createParticles() {
        explosion = new ParticleEffect();
        explosion.load(Gdx.files.internal("data/explosion.p"), Gdx.files.internal("data"));
        color = new ParticleEffect();
        color.load(Gdx.files.internal("data/shining.p"), Gdx.files.internal("data"));
    }

    public void startShining() {
    	if(isShining) {
    		return;
    	}
    	isShining = true;
    	color.start();
    	iceFactory.incrementSharedFlag();
    }
    
    public void startExplosion() {
    	if(isExplodes) {
    		return;
    	}
    	isExplodes = true;
    	explosion.start();
    	iceFactory.incrementSharedFlag();
    }
    
    public void startLightningAnimationFirstLevel() {
    	lightning.startAnimationFirstLevel(this);
    }
    
    public void startLightningAnimationSecondLevel() {
    	lightning.startAnimationSecondLevel(this);
    }
    
    public void startLightningAnimationFourthLevel() {
    	lightning.startAnimationFourthLevel(this);
    }
    
}
