/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.boolbalabs.lib.graphics.ZSprite2D;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.nice.SceneGameplay;

/**
 * 
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class Water extends ZSprite2D {

	private final int skipFromTop = 372;
	private Rectangle screenRectRip = new Rectangle(-1, ScreenMetrics.screenHeightRip - skipFromTop - 67, ScreenMetrics.screenWidthRip + 1, 67);
	private ZSprite2D waterOverlay;
	private SceneGameplay sceneGameplay;
	private boolean showWater = true;

	public Water(SceneGameplay sceneGameplay) {
		super("water_component");
		parentScene = sceneGameplay;
		this.sceneGameplay = (SceneGameplay) parentScene;
		initializeAnimation(new String[] { "waterframes-0-0w", "waterframes-0-1w", "waterframes-0-2w", "waterframes-0-3w", "waterframes-0-4w",
				"waterframes-0-5w", "waterframes-0-6w", "waterframes-0-7w" }, 100, screenRectRip, true);
		setDefaultFrameIndexToDraw(0);
		waterOverlay = new ZSprite2D("water_overlay");
		waterOverlay.initializeAnimation(new String[] { "wateroverlay-0-0w", "wateroverlay-0-1w", "wateroverlay-0-2w" }, 150, screenRectRip, true);
		waterOverlay.setDefaultFrameIndexToDraw(0);

		// TODO: fix for nonstandard aspect ratios (HTC Sensation)
		showWater = !(ScreenMetrics.aspectRatio < 0.58);
		
//		visible = showWater;
//		waterOverlay.visible = showWater;
	}

	
	@Override
	protected void draw(SpriteBatch batch, float parentAlpha) {
		if(!showWater) {
			return;
		}
		super.draw(batch, parentAlpha);
	}
	
	@Override
	public void initialize() {
		// parent.addActor(waterOverlay);
		super.initialize();
	}

	@Override
	public void onShow() {
		if (showWater) {
			startWaterAnimation();
		}
	}

	@Override
	public void onHide() {
		if (showWater) {
			stopWaterAnimation();
		}
	}

	@Override
	public void onPause() {
		if (showWater) {
			stopWaterAnimation();
		}
	}

	@Override
	public void onResume() {
		if (showWater) {
			startWaterAnimation();
		}
	}

	private void startWaterAnimation() {
		if (showWater) {
			startAnimation();
		}
	}

	private void stopWaterAnimation() {
		if (showWater) {
			stopAnimation();
		}
	}
}
