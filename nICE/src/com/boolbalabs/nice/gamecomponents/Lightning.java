/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice.gamecomponents;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.boolbalabs.lib.game.ZNode;
/**
 * 
 * @author Vasya Drobushkov <vasya.drobushkov@gmail.com>
 */

public class Lightning extends ZNode {

	private LightningHorizontal horizontal;
	private LightningVertical vertical;
	
	public Lightning(IceFactory iceFactory, Icicle ice) {
		super("lightning" + ice.getRow() + "" + ice.getColumn());
		
		horizontal = new LightningHorizontal(iceFactory, ice);
		vertical = new LightningVertical(iceFactory, ice);
	}
	
	@Override
	public void update() {
		horizontal.update();
		vertical.update();
	}
	
	@Override
	public void zdraw(SpriteBatch batch, float parentAlpha) {
		horizontal.zdraw(batch, parentAlpha);
		vertical.zdraw(batch, parentAlpha);
	}
	
	public void startAnimationFirstLevel(Icicle ice) {
		vertical.startAnimationFirstLevel(ice);
	}
	
	public void startAnimationSecondLevel(Icicle ice) {
		horizontal.startAnimation(ice);
	}
	
	public void startAnimationThirdLevel(Icicle ice) {//TODO: this is unused. third level = first + second
		vertical.startAnimationFirstLevel(ice);
		horizontal.startAnimation(ice);
	}
	
	public void startAnimationFourthLevel(Icicle ice) {
		horizontal.startAnimation(ice);
		vertical.startAnimationFourthLevel(ice);
	}
}
