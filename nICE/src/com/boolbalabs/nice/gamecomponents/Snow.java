package com.boolbalabs.nice.gamecomponents;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.OnActionCompleted;
import com.badlogic.gdx.scenes.scene2d.actions.FadeIn;
import com.badlogic.gdx.scenes.scene2d.actions.FadeOut;
import com.badlogic.gdx.scenes.scene2d.actions.FadeTo;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.interpolators.LinearInterpolator;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.TexturesManager;
import com.boolbalabs.nice.SceneGameplay;
import com.boolbalabs.nice.extra.PlayerProfile;

public class Snow extends ZNode {
	
    private Rectangle snowRectangle;
    
    private FadeTo fadeInAction;
    private FadeTo fadeOutAction;
    private Sequence fadeOutInSequence;
    private OnActionCompleted onActionCompleted;
    private boolean withBgChange = true;
    
    public Snow(SceneGameplay sceneGameplay) {
    	super("snow");
    	snowRectangle = new Rectangle(40, ScreenMetrics.screenHeightRip - 420 - 56, 78, 26);
    	touchable = false;
        setScreenFrame(snowRectangle);
        region.setRegion(TexturesManager.getInstance().getRegionByName("snow"));
        createActions();
    }
    
    private void createActions() {
        onActionCompleted = new OnActionCompleted() {

            public void completed(Action action) {
                if (action == fadeOutInSequence) {
                	PlayerProfile.getInstance().getCurrentMode().repeatBonus.visible = true;
                } else if (action == fadeOutAction) {
                	visible = true;
                	
                } else if(action == fadeInAction) {
                }
            }
        };

    }

    public void startAction() {
        clearActions();
        fadeInAction = FadeTo.$(1.0f, 0.6f);
        fadeInAction.setInterpolator(LinearInterpolator.$());
        fadeOutAction = FadeTo.$(0.0f, 0.6f);
        fadeOutAction.setInterpolator(LinearInterpolator.$());
        fadeOutInSequence = Sequence.$(fadeOutAction, fadeInAction);
        fadeOutAction.setCompletionListener(onActionCompleted);
        fadeInAction.setCompletionListener(onActionCompleted);
        fadeOutInSequence.setCompletionListener(onActionCompleted);
        visible = false;
        PlayerProfile.getInstance().getCurrentMode().repeatBonus.visible = false;
        action(fadeOutInSequence);
    }
    
    public void onLevelUp() {
    }

    @Override
    public void onHide() {
    }

    @Override
    protected boolean touchDown(float x, float y, int pointer) {
//        if (pointInside(x, y)) {
//            startFading(true);
//            return true;
//        }
        return false;
    }

    @Override
    protected void draw(SpriteBatch batch, float parentAlpha) {
        act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        super.draw(batch, parentAlpha);
    }

    public void startFading(boolean withBgChange) {
        this.withBgChange = withBgChange;
        startAction();
    }

    public void stopFading() {
        if (fadeOutInSequence != null) {
            fadeOutInSequence.finish();
        }
    }
    
    public void freeze() {
    	clearActions();
    	visible = false;
    	PlayerProfile.getInstance().getCurrentMode().repeatBonus.visible = false;
        fadeOutAction = FadeTo.$(0.0f, 0.6f);
        fadeOutAction.setInterpolator(LinearInterpolator.$());
        fadeOutInSequence = Sequence.$(fadeOutAction);
        fadeOutAction.setCompletionListener(onActionCompleted);
        fadeOutInSequence.setCompletionListener(onActionCompleted);
        action(fadeOutInSequence);
    }
    
    public void unfreeze() {
    	clearActions();
        fadeInAction = FadeTo.$(1.0f, 0.6f);
        fadeInAction.setInterpolator(LinearInterpolator.$());
        fadeOutInSequence = Sequence.$(fadeInAction);
        fadeInAction.setCompletionListener(onActionCompleted);
        fadeOutInSequence.setCompletionListener(onActionCompleted);
        action(fadeOutInSequence);
    }

	
}
