/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.nice;

import android.graphics.PointF;
import android.util.Log;
import com.boolbalabs.lib.game.ZScene;
import com.boolbalabs.nice.extra.PlayerProfile;
import com.boolbalabs.nice.extra.SoundService;
import com.boolbalabs.nice.gamecomponents.Background;
import com.boolbalabs.nice.gamecomponents.GameplayMenu;
import com.boolbalabs.nice.gamecomponents.IceFactory;
import com.boolbalabs.nice.gamecomponents.IceFactory.IceFactoryState;
import com.boolbalabs.nice.gamecomponents.Pinguin;
import com.boolbalabs.nice.gamecomponents.PopupScore;
import com.boolbalabs.nice.gamecomponents.ProgressBar;
import com.boolbalabs.nice.gamecomponents.RepeatBonus;
import com.boolbalabs.nice.gamecomponents.Score;
import com.boolbalabs.nice.gamecomponents.Shake;
import com.boolbalabs.nice.gamecomponents.Snow;
import com.boolbalabs.nice.gamecomponents.StarGlitter;
import com.boolbalabs.nice.gamecomponents.VisualBoard;
import com.boolbalabs.nice.gamecomponents.Water;
import com.boolbalabs.nice.level.GameLevelFactoryXML;
import com.boolbalabs.nice.modes.GameMode;
import com.boolbalabs.nice.settings.Settings;
import com.boolbalabs.nice.settings.StaticConstants;
import com.boolbalabs.nice.utils.ZLog;

/**
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class SceneGameplay extends ZScene {

    private PlayerProfile playerProfile;
    private Settings settings;
    private Background background;
    private VisualBoard visualBoard;
    private Water water;
    private IceFactory iceFactory;
    private StarGlitter starGlitter;
    private Score score;
    private ProgressBar progressBar;
    private Pinguin pinguin;
    private PopupScore popupScore;
    private Shake shake;
    private Snow snow;
    
    public boolean isOnPause;

    public SceneGameplay(float width, float height, boolean stretch, int id) {
        super(width, height, stretch, id);

        playerProfile = PlayerProfile.getInstance();
        settings = Settings.getInstance();

        snow = new Snow(this);
        background = new Background(this, snow);
        addActor(background);

        visualBoard = new VisualBoard();
        addActor(visualBoard);

        water = new Water(this);
        addActor(water);
        water.initialize();

        iceFactory = new IceFactory(this);
        addActor(iceFactory);
        
        //DO NOT MOVE!
        addActor(snow);

        starGlitter = new StarGlitter(this);
        addActor(starGlitter);

        score = new Score(this);
        addActor(score);

        shake = new Shake(this);
        addActor(shake);

        progressBar = new ProgressBar(this);
        addActor(progressBar);

        pinguin = new Pinguin(this);
        addActor(pinguin);

        popupScore = new PopupScore(this);
        addActor(popupScore);

        isOnPause = false;
        //GameplayMenu gameplayMenu = new GameplayMenu(this);
    }

    @Override
    public void draw() {

        super.draw();

    }

    @Override
    public void onHide() {
        saveCurrentMode();
        sendEmptyMessageToActivity(StaticConstants.MESSAGE_SUBMIT_SCORELOOP);
    }

    public void saveCurrentMode() {
        GameMode m = playerProfile.getCurrentMode();
        if (m == null) {
            return;
        }
        iceFactory.saveIcicleTypes();
        m.currentScore = playerProfile.getCurrentScore();
        m.currentBonus = playerProfile.bonusMultiplier;
        m.iciclesRemoved = playerProfile.getIciclesRemovedOnCurrentLevel();
        m.progressUnits = progressBar.getCurrentRealUnits();
        m.maxChain = playerProfile.longestChain;
        settings.saveMode(m);
        settings.saveBalanceData();
    }

    @Override
    public void onShow() {
    	SoundService.playStartGame();
        water.onShow();
        startFadingBackground(false);

        playerProfile.resetSession();
        if (settings.wasGameOver(playerProfile.getCurrentMode())) {
            settings.setGameOver(playerProfile.getCurrentMode(), false);
        } else {
            settings.loadMode(playerProfile.getCurrentMode());
        }
        playerProfile.loadProfile();
        iceFactory.onShow();
        snow.onShow();
        progressBar.onShow();
        score.onShow();
        shake.onShow();
    }

    @Override
    public void onPause() {
        super.onPause();
        isOnPause = true;
        iceFactory.hideIcicles();
        onHide();
    }

    @Override
    public void onResume() {
    	isOnPause = false;
        iceFactory.showIcicles();
        super.onResume();
    }

    public void startFadingBackground(boolean withBgChange) {
        water.visible = false;
        background.startFading(withBgChange);
    }

    public void stopFadingBackground() {
        water.visible = true;
    }

    public void starGlitterStart(float x, float y) {
        if (starGlitter != null) {
            starGlitter.startZoom(x, y);
        }
    }

    public void starGlitterStop() {
        if (starGlitter != null) {
            starGlitter.stopZoom();
        }
    }

    public void incrementScore(int icicleNumber, int chainCount, PointF averagePositionRip) {
        score.incrementScore(icicleNumber, chainCount, averagePositionRip);
        progressBar.incrementBar(icicleNumber, chainCount);
    }

    public void onLevelUp() {
        ZLog.i("onLevelUp");
        SoundService.playLevelUp();
        
        score.adjustBonusMultiplier();
        if(!iceFactory.isGameFreezed) {
        	background.onLevelUp();
        }
        playerProfile.onLevelUp();
        if(PlayerProfile.getInstance().getCurrentModeId() != PlayerProfile.MODE_PUZZLE) {
        	shake.addShakes(1);
        }
        if(Settings.isHelpStoneShouldShown && PlayerProfile.getInstance().getCurrentModeId() != PlayerProfile.MODE_PUZZLE) {
        	sendEmptyMessageToActivity(StaticConstants.MESSAGE_HELP_STONE);
        	Settings.isHelpStoneShouldShown = false;
        }
        playerProfile.getCurrentMode().stonesMax++;
    }

    public void onPopupScore(int scoreToShow, PointF averagePositionRip) {
        popupScore.showScore(scoreToShow, averagePositionRip);
    }

    public void onGameOver() {
        SoundService.playGameOver();
        settings.setGameOver(playerProfile.getCurrentMode(), true);
        iceFactory.onGameOver();
        sendEmptyMessageToActivity(StaticConstants.MESSAGE_GAME_OVER);
    }

    public void restartScene() {
        SoundService.playStartGame();
        playerProfile.resetSession();
        iceFactory.onShow();
        score.resetScore();
        progressBar.restart();
        background.onLevelUp();
        shake.onShow();
    }

    public void showHint() {
        if (iceFactory != null) {
            iceFactory.showHint();
        }
    }

    public void showNoHint() {
        if (pinguin != null) {
            pinguin.showNoHint();
        }
    }

    public void useHint() {
        if (progressBar != null) {
            progressBar.useHint();
        }
    }


    public void onShakeEvent(){
        iceFactory.onShakeEvent();
    }
    
    public void addShakes(int toAdd) {
    	shake.addShakes(toAdd);
    }
    
    public void freeze() {
    	background.freeze();
    }
    
    public void unfreeze() {
    	background.unfreeze();
    }
}
