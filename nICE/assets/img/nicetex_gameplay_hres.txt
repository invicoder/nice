nicetex_gameplay_hres.png
format: RGBA8888
filter: Linear,Linear
repeat: none
stone_7a
  rotate: false
  xy: 626, 275
  size: 84, 84
  orig: 84, 84
  offset: 0, 0
  index: -1
pb_1_full
  rotate: false
  xy: 484, 224
  size: 480, 19
  orig: 480, 19
  offset: 0, 0
  index: -1
lightning_1v
  rotate: false
  xy: 41, 536
  size: 37, 480
  orig: 37, 480
  offset: 0, 0
  index: -1
stone_0a
  rotate: false
  xy: 484, 478
  size: 84, 84
  orig: 84, 84
  offset: 0, 0
  index: -1
yellow_digits_08p
  rotate: false
  xy: 78, 484
  size: 35, 45
  orig: 35, 45
  offset: 0, 0
  index: -1
pb_2_full
  rotate: false
  xy: 484, 182
  size: 480, 19
  orig: 480, 19
  offset: 0, 0
  index: -1
puzzle_restart_pressed
  rotate: false
  xy: 816, 417
  size: 70, 84
  orig: 70, 84
  offset: 0, 0
  index: -1
yellow_digits_01p
  rotate: false
  xy: 40, 484
  size: 36, 45
  orig: 36, 45
  offset: 0, 0
  index: -1
silverdigits04d
  rotate: false
  xy: 936, 332
  size: 22, 30
  orig: 22, 30
  offset: 0, 0
  index: -1
pb_empty
  rotate: false
  xy: 484, 119
  size: 480, 19
  orig: 480, 19
  offset: 0, 0
  index: -1
yellow_digits_00p
  rotate: false
  xy: 584, 320
  size: 34, 45
  orig: 34, 45
  offset: 0, 0
  index: -1
silverdigits09d
  rotate: false
  xy: 706, 361
  size: 22, 30
  orig: 22, 30
  offset: 0, 0
  index: -1
snow
  rotate: false
  xy: 484, 273
  size: 105, 38
  orig: 105, 38
  offset: 0, 0
  index: -1
lightning_3v
  rotate: false
  xy: 966, 54
  size: 37, 480
  orig: 37, 480
  offset: 0, 0
  index: -1
yellow_digits_06p
  rotate: false
  xy: 570, 508
  size: 29, 45
  orig: 29, 45
  offset: 0, 0
  index: -1
silverdigits01d
  rotate: false
  xy: 912, 396
  size: 22, 30
  orig: 22, 30
  offset: 0, 0
  index: -1
stone_5a
  rotate: false
  xy: 484, 392
  size: 84, 84
  orig: 84, 84
  offset: 0, 0
  index: -1
pb_3_full
  rotate: false
  xy: 484, 140
  size: 480, 19
  orig: 480, 19
  offset: 0, 0
  index: -1
lightning_2h
  rotate: false
  xy: 484, 41
  size: 480, 37
  orig: 480, 37
  offset: 0, 0
  index: -1
selection
  rotate: false
  xy: 714, 465
  size: 60, 60
  orig: 60, 60
  offset: 0, 0
  index: -1
silverdigits06d
  rotate: false
  xy: 690, 457
  size: 22, 30
  orig: 22, 30
  offset: 0, 0
  index: -1
stone_6a
  rotate: false
  xy: 620, 361
  size: 84, 84
  orig: 84, 84
  offset: 0, 0
  index: -1
pb_1_highlight
  rotate: false
  xy: 484, 203
  size: 480, 19
  orig: 480, 19
  offset: 0, 0
  index: -1
pb_2_highlight
  rotate: false
  xy: 484, 161
  size: 480, 19
  orig: 480, 19
  offset: 0, 0
  index: -1
yellow_digits_07p
  rotate: false
  xy: 584, 367
  size: 32, 45
  orig: 32, 45
  offset: 0, 0
  index: -1
pinguin
  rotate: false
  xy: 734, 245
  size: 90, 132
  orig: 90, 132
  offset: 0, 0
  index: -1
yellow_digits_04p
  rotate: false
  xy: 591, 273
  size: 33, 45
  orig: 33, 45
  offset: 0, 0
  index: -1
lightning_1h
  rotate: false
  xy: 484, 80
  size: 480, 37
  orig: 480, 37
  offset: 0, 0
  index: -1
silverdigits03d
  rotate: false
  xy: 912, 364
  size: 22, 30
  orig: 22, 30
  offset: 0, 0
  index: -1
stone_3a
  rotate: false
  xy: 826, 331
  size: 84, 84
  orig: 84, 84
  offset: 0, 0
  index: -1
silverdigits08d
  rotate: false
  xy: 706, 393
  size: 22, 30
  orig: 22, 30
  offset: 0, 0
  index: -1
text_bonus
  rotate: false
  xy: 484, 245
  size: 140, 26
  orig: 140, 26
  offset: 0, 0
  index: -1
text_max_chain
  rotate: false
  xy: 626, 245
  size: 106, 28
  orig: 106, 28
  offset: 0, 0
  index: -1
nohint
  rotate: false
  xy: 484, 313
  size: 98, 77
  orig: 98, 77
  offset: 0, 0
  index: -1
lightning_2v
  rotate: false
  xy: 2, 536
  size: 37, 480
  orig: 37, 480
  offset: 0, 0
  index: -1
stone_1a
  rotate: false
  xy: 604, 447
  size: 84, 84
  orig: 84, 84
  offset: 0, 0
  index: -1
puzzle_restart
  rotate: false
  xy: 888, 428
  size: 70, 84
  orig: 70, 84
  offset: 0, 0
  index: -1
yellow_digits_05p
  rotate: false
  xy: 912, 245
  size: 30, 45
  orig: 30, 45
  offset: 0, 0
  index: -1
silverdigits00d
  rotate: false
  xy: 936, 396
  size: 22, 30
  orig: 22, 30
  offset: 0, 0
  index: -1
stone_4a
  rotate: false
  xy: 826, 245
  size: 84, 84
  orig: 84, 84
  offset: 0, 0
  index: -1
yellow_digits_02p
  rotate: false
  xy: 2, 484
  size: 36, 45
  orig: 36, 45
  offset: 0, 0
  index: -1
silverdigits05d
  rotate: false
  xy: 912, 332
  size: 22, 30
  orig: 22, 30
  offset: 0, 0
  index: -1
shake_icon_small
  rotate: false
  xy: 912, 292
  size: 30, 38
  orig: 30, 38
  offset: 0, 0
  index: -1
star_glitter
  rotate: false
  xy: 966, 2
  size: 50, 50
  orig: 50, 50
  offset: 0, 0
  index: -1
yellow_digits_09p
  rotate: false
  xy: 570, 461
  size: 29, 45
  orig: 29, 45
  offset: 0, 0
  index: -1
board_bg
  rotate: false
  xy: 2, 2
  size: 480, 480
  orig: 480, 480
  offset: 0, 0
  index: -1
yellow_digits_03p
  rotate: false
  xy: 570, 414
  size: 32, 45
  orig: 32, 45
  offset: 0, 0
  index: -1
silverdigits02d
  rotate: false
  xy: 936, 364
  size: 22, 30
  orig: 22, 30
  offset: 0, 0
  index: -1
stone_2a
  rotate: false
  xy: 730, 379
  size: 84, 84
  orig: 84, 84
  offset: 0, 0
  index: -1
lightning_3h
  rotate: false
  xy: 484, 2
  size: 480, 37
  orig: 480, 37
  offset: 0, 0
  index: -1
silverdigits07d
  rotate: false
  xy: 706, 425
  size: 22, 30
  orig: 22, 30
  offset: 0, 0
  index: -1
