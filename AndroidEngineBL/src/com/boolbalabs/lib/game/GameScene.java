/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.game;

import android.graphics.Point;
import android.os.Handler;
import android.os.Message;

import javax.microedition.khronos.opengles.GL10;
import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 *         Date: 15/03/2011
 *         Time: 09:07
 */
public abstract class GameScene implements ActionPerformer {

    /**
     * scene id
     */
    private int id;

    /**
     * parent game
     */
    protected Game mainGame;

    /**
     * indicates if a scene is registered within a Game
     */
    private boolean isRegistered;

    /**
     * indicates if a scene is initialized
     */
    private boolean isSceneInitialized;

    private ArrayList<ZNode> drawables;
    private ArrayList<ZNode> touchables;

    /**
     * main activity handler
     */
    private Handler activityHandler;


    public GameScene(Game game, int sceneId) {
        mainGame = game;
        id = sceneId;
        drawables = new ArrayList<ZNode>();
        touchables = new ArrayList<ZNode>();
        onCreate();
        mainGame.registerGameScene(this);
    }

    /**
     * registers ZNode within a GameScene
     *
     * @param zNode
     */
    public void registerGameComponent(ZNode zNode) {
        if (zNode == null) {
            return;
        }
        if (!drawables.contains(zNode)) {
            drawables.add(zNode);
            touchables.add(zNode);
            zNode.register(this);
        }
    }

    /**
     * unregisters ZNode
     *
     * @param zNode
     */
    public void unregisterGameComponent(ZNode zNode) {
        if (zNode == null) {
            return;
        }
        if (drawables.contains(zNode)) {
            zNode.unregister();
            drawables.remove(zNode);
            touchables.remove(zNode);
        }
    }


    /**
     * Loads scene content
     */
    public void loadContent() {
        if (drawables != null && !drawables.isEmpty()) {
            int size = drawables.size();
            for (int i = 0; i < size; i++) {
                ZNode currentNode = drawables.get(i);
                currentNode.loadContent();
            }
        }
    }

    /**
     * This event occurs after a game (more precisely, a ContentLoader) has
     * loaded all textures and coordinates;
     */
    public void onAfterLoad() {
        adjustNodes();
        isSceneInitialized = true;
    }

    /**
     * unloads game content
     */
    public void unloadContent() {
        if (drawables != null && !drawables.isEmpty()) {
            int size = drawables.size();
            for (int i = 0; i < size; i++) {
                ZNode currentNode = drawables.get(i);
                currentNode.unloadContent();
            }
        }
    }

    private void adjustNodes() {
        touchables.clear();
        touchables.addAll(drawables);
        Collections.reverse(touchables);
    }


    /**
     * Performs on-going game logic: calculates current positions, physics,
     * collisions and states; collects input information from the various input
     * devices; plays audio, and so on.
     */
    public void update() {
        if (!isSceneInitialized) {
            return;
        }
        synchronized (drawables) {
            int size = drawables.size();
            for (int i = 0; i < size; i++) {
                drawables.get(i).protectedUpdate();
            }
        }
    }

    public void draw(GL10 gl) {
        if (!isSceneInitialized) {
            return;
        }
        synchronized (drawables) {
            int size = drawables.size();
            for (int i = 0; i < size; i++) {
                drawables.get(i).drawNode(gl);
            }
        }
    }


    public ZNode findComponentByClass(Class c) {
        int size = drawables.size();
        for (int i = 0; i < size; i++) {
            ZNode zNode = drawables.get(i);
            if (c.isAssignableFrom(zNode.getClass())) {
                return zNode;
            }
        }
        return null;
    }


    public void askGameToSwitchGameScene(int newGameSceneId) {
        mainGame.switchGameScene(newGameSceneId);
    }

    // ************** TOUCH EVENTS ****************//

    protected boolean onTouchDown(Point touchDownPoint) {
        touchDownAction(touchDownPoint);
        int size = touchables.size();
        for (int i = 0; i < size; i++) {
            ZNode cNode = touchables.get(i);
            if (cNode.userInteractionEnabled) {
                boolean touched = cNode.onTouchDown(touchDownPoint);
                if (touched) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Override this method to provide a custom behaviour for onTouchDown event
     */
    public void touchDownAction(Point touchDownPoint) {

    }

    protected boolean onTouchMove(Point touchDownPoint, Point currentPoint) {
        touchMoveAction(touchDownPoint, currentPoint);
        int size = touchables.size();
        for (int i = 0; i < size; i++) {
            ZNode cNode = touchables.get(i);
            if (cNode.userInteractionEnabled) {
                boolean touched = cNode.onTouchMove(touchDownPoint, currentPoint);
                if (touched) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Override this method to provide a custom behaviour for onTouchMove event
     */
    public void touchMoveAction(Point touchDownPoint, Point currentPoint) {

    }

    protected boolean onTouchUp(Point touchDownPoint, Point touchUpPoint) {
        touchUpAction(touchDownPoint, touchUpPoint);
        int size = touchables.size();
        for (int i = 0; i < size; i++) {
            ZNode cNode = touchables.get(i);
            if (cNode.userInteractionEnabled) {
                boolean touched = cNode.onTouchUp(touchDownPoint, touchUpPoint);
                if (touched) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Override this method to provide a custom behaviour for onTouchUp event
     */
    public void touchUpAction(Point touchDownPoint, Point currentPoint) {

    }


    //********* events *********//
    //TODO
    public void onCreate() {

    }

    //TODO
    public void onResume() {

    }

    //TODO
    public void onPause() {

    }

    //TODO
    public void onDestroy() {

    }

    //TODO
    public void onShow() {

    }

    //TODO
    public void onHide() {

    }

    //******* GETTERS/SETTERS ********//
    public int getSceneId() {
        return id;
    }

    public boolean isSceneInitialized() {
        return isSceneInitialized;
    }

    public ArrayList<ZNode> getSceneDrawables() {
        return drawables;
    }

    public ArrayList<ZNode> getSceneTouchables() {
        return touchables;
    }

    public void setActivityHandler(Handler activityHandler) {
        this.activityHandler = activityHandler;
    }

    //******* messages to activity *****//
    public void sendMessageToActivity(Message msg) {
        if (activityHandler != null) {
            try {
                activityHandler.sendMessage(msg);
            } catch (Exception e) {
            }
        }
    }

    public void sendEmptyMessageToActivity(int messageId) {
        if (activityHandler != null) {
            try {
                activityHandler.sendEmptyMessage(messageId);
            } catch (Exception e) {
            }
        }
    }

}
