/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */
package com.boolbalabs.lib.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Point;
import android.opengl.GLSurfaceView;
import android.util.Log;
import com.boolbalabs.lib.managers.SoundManager;
import com.boolbalabs.lib.services.DisplayServiceOpenGL;

/**
 * Abstract class which represents Game
 * 
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public abstract class Game implements ContentLoader {

	private boolean isGameInitialized = false;
	public boolean userInteractionEnabled;
	/**
	 * a list of game scenes for this game
	 */
	private HashMap<Integer, GameScene> gameScenes;
	protected GameScene currentScene;

	public Game() {
		gameScenes = new HashMap<Integer, GameScene>();
		userInteractionEnabled = true;
		isGameInitialized = true;
	}

	/**
	 * This should be used in the instance of a Game only.
	 * 
	 * @return - the list of game components
	 */
	protected HashMap<Integer, GameScene> getGameScenes() {
		return gameScenes;
	}

	/**
	 * registers an existing game scene within a game
	 * 
	 * @param newGameScene
	 */
	public void registerGameScene(GameScene newGameScene) {
		if (newGameScene != null && gameScenes != null) {
			if (!gameScenes.containsKey(newGameScene.getSceneId())) {
				gameScenes.put(newGameScene.getSceneId(), newGameScene);
			} else {
				Log.i("WARNING", "There is already a scene with such ID");
			}
		}
	}

	public abstract void initialize();

	/**
	 * registers ZNode within a GameScene with ID=gameSceneId
	 * 
	 * @param zNode
	 * @param gameSceneId
	 */
	public void registerGameComponent(ZNode zNode, int gameSceneId) {
		if (zNode == null || gameScenes == null) {
			return;
		}
		if (!gameScenes.containsKey(gameSceneId)) {
			return;
		}
		GameScene scene = gameScenes.get(gameSceneId);
		scene.registerGameComponent(zNode);
	}

	/**
	 * unregisters ZNode within a GameScene with ID=gameSceneId
	 * 
	 * @param zNode
	 * @param gameSceneId
	 */
	public void unregisterGameComponent(ZNode zNode, int gameSceneId) {
		if (zNode == null || gameScenes == null) {
			return;
		}
		if (gameScenes.containsKey(gameSceneId)) {
			gameScenes.get(gameSceneId).unregisterGameComponent(zNode);
		}
	}

	/**
	 * Loads game content
	 */
	public void loadContent() {
		if (gameScenes != null && !gameScenes.isEmpty()) {
			for (int key : gameScenes.keySet()) {
				GameScene currentScene = gameScenes.get(key);
				currentScene.loadContent();
			}
		}
	}

	/**
	 * This event occurs after a game (more precisely, a ContentLoader) has
	 * loaded all textures and coordinates;
	 */
	public void onAfterLoad() {

	}

	/**
	 * unloads game content
	 */
	public void unloadContent() {
		if (gameScenes != null && !gameScenes.isEmpty()) {
			for (int key : gameScenes.keySet()) {
				GameScene currentScene = gameScenes.get(key);
				currentScene.unloadContent();
			}
		}
	}

	public void switchGameScene(int newGameSceneId) {
		if (!isGameInitialized) {
			return;
		}
		// synchronized (currentScene) {
		if (gameScenes.containsKey(newGameSceneId)) {
			GameScene newScene = gameScenes.get(newGameSceneId);
			if (newScene != currentScene) {
				if (currentScene != null && currentScene.isSceneInitialized()) {
					currentScene.onHide();
				}
				if (newScene.isSceneInitialized()) {
					newScene.onShow();
				}
				currentScene = newScene;
			}

		} else {
			throw new IllegalStateException("switchGameState (Game class) method");
		}
		// }
	}

	public int getCurrentGameSceneId() {
		if (currentScene != null) {
			return currentScene.getSceneId();
		}
		return -1;
	}

	/**
	 * Performs on-going game logic: calculates current positions, physics,
	 * collisions and states; collects input information from the various input
	 * devices; plays audio, and so on.
	 */
	public void update() {
		if (!isGameInitialized || currentScene == null) {
			return;
		}
		synchronized (currentScene) {
			currentScene.update();
		}
	}

	public void draw(GL10 gl) {
		if (!isGameInitialized) {
			return;
		}
		synchronized (currentScene) {
			currentScene.draw(gl);
		}
	}

	// ************** SPLASH SCREEN ****************//
	public abstract void drawSplashScreen();

	// ************** TOUCH EVENTS ****************//
	protected boolean onTouchDown(Point touchDownPoint) {
		if (!userInteractionEnabled || currentScene==null) {
			return false;
		}
		touchDownAction(touchDownPoint);
		return currentScene.onTouchDown(touchDownPoint);
	}

	/**
	 * Override this method to provide a custom behaviour for onTouchDown event
	 */
	public void touchDownAction(Point touchDownPoint) {
	}

	protected boolean onTouchMove(Point touchDownPoint, Point currentPoint) {
		if (!userInteractionEnabled || currentScene==null) {
			return false;
		}
		touchMoveAction(touchDownPoint, currentPoint);
		return currentScene.onTouchMove(touchDownPoint, currentPoint);
	}

	/**
	 * Override this method to provide a custom behaviour for onTouchMove event
	 */
	public void touchMoveAction(Point touchDownPoint, Point currentPoint) {
	}

	protected boolean onTouchUp(Point touchDownPoint, Point touchUpPoint) {
		if (!userInteractionEnabled || currentScene==null) {
			return false;
		}
		touchUpAction(touchDownPoint, touchUpPoint);
		return currentScene.onTouchUp(touchDownPoint, touchUpPoint);
	}

	/**
	 * Override this method to provide a custom behaviour for onTouchUp event
	 */
	public void touchUpAction(Point touchDownPoint, Point currentPoint) {
	}

	// ************** PAUSE/RESUME/EXIT ****************//

	public abstract void onPause();

	public abstract void onResume();

	public void onExit() {
		DisplayServiceOpenGL.release();
	}

	public boolean isGameInitialized() {
		return isGameInitialized;
	}

	/**
	 * Here one can modify OpenGL config and other parameters
	 * 
	 * @param gl
	 * @param parentView
	 */
	public void initCustomOpenGLParameters(GL10 gl, GLSurfaceView parentView) {
		// empty
	}

	// /***** SOUND *****///
	public void playSound(int soundId, boolean soundEnabled) {
		if (!soundEnabled) {
			return;
		}
		SoundManager soundManager = SoundManager.getInstance();
		if (soundManager != null) {
			soundManager.playShortSound(soundId);
		}
	}

	public void addShortSound(int soundId) {
		SoundManager soundManager = SoundManager.getInstance();
		if (soundManager != null) {
			soundManager.addShortSound(soundId, 1000);
		}
	}

	public void addLoopingSound(int soundId) {
		SoundManager soundManager = SoundManager.getInstance();
		if (soundManager != null) {
			soundManager.addLoopingSound(soundId);
		}
	}
}
