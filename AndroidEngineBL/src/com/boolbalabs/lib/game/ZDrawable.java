/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */
package com.boolbalabs.lib.game;

import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.opengl.GLU;

import com.boolbalabs.lib.graphics.Texture2D;
import com.boolbalabs.lib.transitions.Transition;
import com.boolbalabs.lib.utils.BufferUtils;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.ZageCommonSettings;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 *         Date: 16/03/2011
 *         Time: 09:18
 */
public class ZDrawable {

    /**
     * texture of the current view
     */
    private Texture2D drawableTexture;
    private int drawableTextureIndex = -1;
    private Rect rectOnTexture;
    public boolean visible;
    private Transition transition;
    /////**************************************/////
    /////**************************************/////
    /////********** COORDINATES ***************/////
    /////**************************************/////
    /////**************************************/////
    ///***** RIP ***///
    /**
     * Coordinate X on screen in RIP
     */
    private int xRip;
    /**
     * Coordinate Y on screen in RIP
     */
    private int yRip;
    private int widthRip;
    private int heightRip;
    private int centerRipX;
    private int centerRipY;
    private Point positionRip = new Point();
    private Point centerInRip = new Point();
    private Rect frameInRip = new Rect();
    ///***** PIX ***///
    /**
     * Coordinate X on screen in PIX
     */
    private int xPix;
    /**
     * Coordinate Y on screen in PIX
     */
    private int yPix;
    private int widthPix;
    private int heightPix;
    private int centerPixX;
    private int centerPixY;
    private Point positionPix = new Point();
    private Point centerInPix = new Point();
    private Rect frameInPix = new Rect();
    ///***** OPENGL COORDINATES for OES extension ***///
    private float xGL;
    private float yGL;
    private float zGL = 0;
    private int[] oesTextureCropRect;
    ///***** NORMAL OPENGL DRAWING ***///
    private FloatBuffer vertexBuffer;
    private ShortBuffer indexBuffer;
    protected FloatBuffer textureBuffer;
    private short[] quad_indices = {0, 1, 2, 0, 3, 2};
    private PointF rangeX;
    private PointF rangeY;
    private float rotationAngle = 0;
    private PointF pivotPoint = new PointF();
    private float originalX;//left
    private float originalY;//top
    private float affineShiftX;
    private float affineShiftY;
    public float defaultViewingDistance = ZageCommonSettings.openGLVertexModeDefaultViewDistance;
    private boolean vertexBufferCreated = false;
    //**other**//
    private final double degToRadCoeff = 360 / (2 * Math.PI);
    private final double radToDegCoeff = 1 / degToRadCoeff;
    /////**************************************/////
    /////********** DRAWING MODES *************/////
    /////**************************************/////
    public static final int MODE_OPENGL_OES_EXT = 0;
    public static final int MODE_OPENGL_VERTICES = 1;
    public static final int MODE_CANVAS = 2;
    private int drawingMode = MODE_OPENGL_OES_EXT;

    public ZDrawable(int drawingMode) {
        visible = true;
        setDrawingMode(drawingMode);
    }

    public void initWithFrame(Rect rectOnScreenRip, Rect rectOnTexture) {
        setFrameInRip(rectOnScreenRip);
        setRectOnTexture(rectOnTexture);
    }

    private void setDrawingMode(int drawingMode) {
        this.drawingMode = drawingMode;
        switch (drawingMode) {
            case MODE_OPENGL_OES_EXT:
                oesTextureCropRect = new int[4];
                break;
            case MODE_OPENGL_VERTICES:
                createDefaultRange();
                calculateRangeXY();
                createIndexBuffer();
                break;
            case MODE_CANVAS:
                break;
            default:
                this.drawingMode = MODE_OPENGL_OES_EXT;
                break;
        }
    }

    public void setRectOnTexture(Rect rectOnTexture) {
        this.rectOnTexture = rectOnTexture;
        switch (drawingMode) {
            case MODE_OPENGL_OES_EXT:
                oesTextureCropRect[0] = rectOnTexture.left;
                oesTextureCropRect[1] = rectOnTexture.bottom;
                oesTextureCropRect[2] = rectOnTexture.width();
                oesTextureCropRect[3] = -rectOnTexture.height();
                break;
            case MODE_OPENGL_VERTICES:
                if (drawableTexture != null) {
                    createTextureBuffer();
                }
                break;
            default:
                break;
        }


    }

    public void setTexture(Texture2D texture) {
        drawableTexture = texture;
        drawableTextureIndex = texture.getTextureGlobalIndex();
        if (drawingMode == MODE_OPENGL_VERTICES) {
            createTextureBuffer();
        }
    }

    public Texture2D getTexture() {
        return drawableTexture;
    }

    /////**************************************/////
    /////********** DRAW *************/////
    /////**************************************/////
    //
    //
    //
    //
    //
    //
    //
    //
    //
    public void draw(GL10 gl) {
        switch (drawingMode) {
            case MODE_OPENGL_OES_EXT:
                drawOES(gl);
                break;
            case MODE_OPENGL_VERTICES:
                drawVerts(gl);
                break;
            default:
                break;
        }
    }

    protected void drawOES(GL10 gl) {
        gl.glBindTexture(GL10.GL_TEXTURE_2D, drawableTextureIndex);
        ((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D,
                GL11Ext.GL_TEXTURE_CROP_RECT_OES, oesTextureCropRect, 0);
        ((GL11Ext) gl).glDrawTexfOES(xGL, yGL, zGL, widthPix, heightPix);
    }

    protected void drawVerts(GL10 gl) {
        if (textureBuffer == null) {
            return;
        }

        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();

        GLU.gluLookAt(gl, 0, 0, defaultViewingDistance, 0f, 0f,
                0f, 0f, 1.0f, 0.0f);

        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

        gl.glTranslatef(pivotPoint.x, pivotPoint.y, 0f);
        gl.glRotatef(rotationAngle, 0f, 0f, 1.0f);
        gl.glTranslatef(-pivotPoint.x, -pivotPoint.y, 0f);

        gl.glTranslatef(affineShiftX, affineShiftY, 0f);

        gl.glBindTexture(GL10.GL_TEXTURE_2D, drawableTextureIndex);
        gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S,
                GL10.GL_REPEAT);
        gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T,
                GL10.GL_REPEAT);

        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
        gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);
        gl.glDrawElements(GL10.GL_TRIANGLE_STRIP, 6, GL10.GL_UNSIGNED_SHORT,
                indexBuffer);

    }

    //
    //
    //
    //
    //
    //
    //
    //
    //
    /////**************************************/////
    /////********** UPDATE *************/////
    /////**************************************/////
    protected void update() {
        if (transition != null) {
            transition.innerUpdate();
        }
    }

    /////**************************************/////
    /////********** COORDINATES ***************/////
    /////**************************************/////
    public void setPivotPointRip(Point newPivotPointRip) {
        setPivotPointRip(newPivotPointRip.x, newPivotPointRip.y);
    }

    public void setPivotPointRip(int x, int y) {
        if (drawingMode == MODE_OPENGL_VERTICES) {
            pivotPoint.set(getScaledX(x), getScaledY(y));
        }
    }

    /**
     * Determines if point (in PIX) is inside a current PIX frame.
     * Used for detecting screen touch events.
     *
     * @param x,y
     * @return
     */
    public boolean pointInside(int x, int y) {
        return (widthPix > 0 && heightPix > 0 && (x >= xPix && x <= xPix + widthPix) && (y >= yPix && y <= yPix + heightPix));
    }

    /**
     * Determines if point (in PIX) is inside a current PIX frame.
     * Used for detecting screen touch events.
     *
     * @param point
     * @return
     */
    public boolean pointInside(Point point) {
        return pointInside(point.x, point.y);
    }

    /**
     * Determines if point (in RIP) is inside a current RIP frame.
     * Used for physics mainly.
     *
     * @param x,y
     * @return
     */
    public boolean pointInsideRip(int x, int y) {
        return (widthRip > 0 && heightRip > 0 && (x >= xRip && x <= xRip + widthRip) && (y >= yRip && y <= yRip + heightRip));
    }

    /**
     * Determines if point (in RIP) is inside a current RIP frame.
     * Used for physics mainly.
     *
     * @param pointInRip
     * @return
     */
    public boolean pointInsideRip(Point pointInRip) {
        return pointInsideRip(pointInRip.x, pointInRip.y);
    }

    public void setFrameInPix(Rect frame) {
        if (frame == null) {
            return;
        }
        widthPix = frame.width();
        heightPix = frame.height();
        setPositionInPix(frame.left, frame.top);
    }

    public void setFrameInRip(Rect frame) {
        if (frame == null) {
            return;
        }
        widthRip = frame.width();
        heightRip = frame.height();
        setPositionInRip(frame.left, frame.top);
    }

    public void setPositionInRip(int x, int y) {
        xRip = x;
        yRip = y;
        adjustCoordsPixToRip();
        adjustCenterRip();
        adjustCoordsOES();
        adjustVertexBuffer();
    }

    public void setPositionInRip(Point posInRip) {
        setPositionInRip(posInRip.x, posInRip.y);
    }

    public void setPositionInPix(int x, int y) {
        xPix = x;
        yPix = y;
        adjustCoordsRipToPix();
        adjustCenterPix();
        adjustCoordsOES();
        adjustVertexBuffer();
    }

    public void setPositionInPix(Point posInPix) {
        setPositionInPix(posInPix.x, posInPix.y);
    }

    public void setCenterInRip(int x, int y) {
        setPositionInRip(x - (widthRip >> 1), y - (heightRip >> 1));
    }

    /**
     * we have correct coordinates in RIP and want the PIX coords to
     * be synchronized with RIPs
     */
    private void adjustCoordsPixToRip() {
        xPix = ScreenMetrics.convertRipToPixel(xRip);
        yPix = ScreenMetrics.convertRipToPixel(yRip);
        widthPix = ScreenMetrics.convertRipToPixel(widthRip);
        heightPix = ScreenMetrics.convertRipToPixel(heightRip);
        adjustCenterPix();
    }

    /**
     * we have correct coordinates in PIX and want the RIP coords to
     * be synchronized with PIXs
     */
    private void adjustCoordsRipToPix() {
        xRip = ScreenMetrics.convertPixelToRip(xPix);
        yRip = ScreenMetrics.convertPixelToRip(yPix);
        widthRip = ScreenMetrics.convertPixelToRip(widthPix);
        heightRip = ScreenMetrics.convertPixelToRip(heightPix);
        adjustCenterRip();
    }

    private void adjustCenterRip() {
        centerRipX = xRip + (widthRip >> 1);
        centerRipY = yRip + (heightRip >> 1);
    }

    private void adjustCenterPix() {
        centerPixX = xPix + (widthPix >> 1);
        centerPixY = yPix + (heightPix >> 1);
    }

    private void adjustCoordsOES() {
        if (drawingMode == MODE_OPENGL_OES_EXT) {
            xGL = xPix;
            yGL = ScreenMetrics.screenHeightPix - yPix - heightPix;
        }
    }

    public void setZGL(float zGL) {
        this.zGL = zGL;
    }

    // TODO: fix zooming in MODE_OPENGL_VERTICES, doesn't work
    private void adjustVertexBuffer() {
        if (drawingMode == MODE_OPENGL_VERTICES) {
            if (!vertexBufferCreated) {
                createVertexBuffer();
            } else {
                affineShiftX = getScaledX(xRip) - originalX;
                affineShiftY = getScaledY(yRip) - originalY;
            }
        }
    }

    /////**************************************/////
    /////********** GETTERS/SETTERS ***********/////
    /////**************************************/////
    public int getTextureGlobalIndex() {
        if (drawableTexture != null) {
            return drawableTextureIndex;
        }
        return -1;
    }

    public Point getPositionRip() {
        positionRip.set(xRip, yRip);
        return positionRip;
    }

    public Point getPositionPix() {
        positionPix.set(xPix, yPix);
        return positionPix;
    }

    public Rect getFrameInPix() {
        frameInPix.set(xPix, yPix, xPix + widthPix, yPix + heightPix);
        return frameInPix;
    }

    public Rect getFrameInRip() {
        frameInRip.set(xRip, yRip, xRip + widthRip, yRip + heightRip);
        return frameInRip;
    }

    public Point getCenterInRip() {
        centerInRip.set(xRip + (widthRip >> 1), yRip + (heightRip >> 1));
        return centerInRip;
    }

    public Point getCenterInPix() {
        centerInPix.set(xPix + (widthPix >> 1), yPix + (heightPix >> 1));
        return centerInPix;
    }

    public float getCoordsOES_X() {
        return xGL;
    }

    public float getCoordsOES_Y() {
        return yGL;
    }

    public float getCoordsOES_Z() {
        return zGL;
    }

    protected int getDrawingMode() {
        return drawingMode;
    }

    ///*************** TRANSITION **************///
    public void setTransition(Transition transition) {
        this.transition = transition;
    }
    
    public Transition getTransition() {
    	return transition;
    }

    public void startTransition() {
        if (transition != null) {
            transition.start();
        }
    }

    public void stopTransition() {
        if (transition != null) {
            transition.stop();
        }
    }

    public void resetTransition() {
        if (transition != null) {
            transition.reset();
        }
    }

    ///*************** NORMAL OPEN GL DRAW **************///

    protected void createTextureBuffer() {
        if (rectOnTexture == null) {
            return;
        }

        float width = (float) drawableTexture.getWidth();
        float height = (float) drawableTexture.getHeight();


        float textCoords[] = {rectOnTexture.left / width, rectOnTexture.top / height,//  TOP LEFT
            rectOnTexture.left / width, rectOnTexture.bottom / height,// BOTTOM LEFT
            rectOnTexture.right / width, rectOnTexture.bottom / height,// BOTTOM RIGHT
            rectOnTexture.right / width, rectOnTexture.top / height // TOP RIGHT
        };

        ByteBuffer tbb = ByteBuffer.allocateDirect(textCoords.length * 4);
        tbb.order(ByteOrder.nativeOrder());
        textureBuffer = tbb.asFloatBuffer();
        textureBuffer.put(textCoords);
        textureBuffer.position(0);
    }

    protected void setTextureBuffer(FloatBuffer textureBuffer) {
        this.textureBuffer = textureBuffer;
    }

    private void createIndexBuffer() {
        // short is 2 bytes, therefore we multiply the number if
        // vertices with 2.
        ByteBuffer ibb = ByteBuffer.allocateDirect(quad_indices.length * 2);
        ibb.order(ByteOrder.nativeOrder());
        indexBuffer = ibb.asShortBuffer();
        indexBuffer.put(quad_indices);
        indexBuffer.position(0);
    }

    private void createVertexBuffer() {
        float vertices[] = {getScaledX(xRip), getScaledY(yRip), 0f, // 0, Top Left
            getScaledX(xRip), getScaledY(yRip + heightRip), 0f, // 1, Bottom Left
            getScaledX(xRip + widthRip), getScaledY(yRip + heightRip), 0f, // 2, Bottom Right
            getScaledX(xRip + widthRip), getScaledY(yRip), 0f // 3, Top Right
        };

        originalX = vertices[0];
        originalY = vertices[1];
        affineShiftX = 0;
        affineShiftY = 0;

        ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
        vbb.order(ByteOrder.nativeOrder());
        vertexBuffer = vbb.asFloatBuffer();
        vertexBuffer.put(vertices);
        vertexBuffer.position(0);
        vertexBufferCreated = true;
    }

    private float getScaledX(int xRip) {
        return xRip * (rangeX.y - rangeX.x)
                / ((float) ScreenMetrics.screenWidthRip) + rangeX.x;
    }

    private float getScaledY(int yRip) {
        return yRip * (rangeY.x - rangeY.y)
                / ((float) ScreenMetrics.screenHeightRip) - rangeY.x;
    }

    public FloatBuffer createVertexBufferForRect(Rect rect) {
        float vertices[] = {getScaledX(rect.left), getScaledY(rect.top), 0f, // 0, Top Left
            getScaledX(rect.left), getScaledY(rect.bottom), 0f, // 1, Bottom Left
            getScaledX(rect.right), getScaledY(rect.bottom), 0f, // 2, Bottom Right
            getScaledX(rect.right), getScaledY(rect.top), 0f // 3, Top Right
        };

        FloatBuffer fbb = BufferUtils.makeFloatBuffer(vertices);
        return fbb;
    }

    private void createDefaultRange() {
        rangeX = new PointF(-1, 1);
        rangeY = new PointF(-1, 1);
    }

    private void calculateRangeXY() {
        float screenRatio = ScreenMetrics.aspectRatioOriented;
        rangeY.y = 0.417f * (defaultViewingDistance - 1.4f) + 0.583f;
        rangeY.x = -rangeY.y;

        rangeX.x = rangeY.x * screenRatio;
        rangeX.y = rangeY.y * screenRatio;
    }

    /**
     * @param angleDeg - in degrees
     */
    public void setRotationAngleDeg(double angleDeg) {
        rotationAngle = (float) angleDeg;
    }

    public float getRotationAngleDeg() {
        return rotationAngle;
    }

    /**
     * @param angleRad - in radians
     */
    public void setRotationAngleRad(double angleRad) {
        rotationAngle = (float) (angleRad * degToRadCoeff);
    }

    /**
     * @param angleRad - radians
     */
    public void setPivotPointToCenterAndRotateByRad(double angleRad) {
        setPivotPointRip(centerRipX, centerRipY);
        setRotationAngleRad(angleRad);
    }

    /**
     * @param angleRad - degrees
     */
    public void setPivotPointToCenterAndRotateByDeg(double angleDeg) {
        setPivotPointRip(centerRipX, centerRipY);
        setRotationAngleDeg(angleDeg);
    }
}
