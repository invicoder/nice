/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.game;

/**
 * This interface is a link between Game and ContentLoaderService.
 * 
 * Created to avoid having a reference to a Game object in ContentLoaderService.
 * 
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */

public interface ContentLoader {

	public void loadContent();

	public void unloadContent();

    public void onAfterLoad();

}
