/*
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.game;

import java.lang.ref.WeakReference;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Semaphore;

import com.boolbalabs.lib.services.ContentLoaderServiceOpenGL;
import com.boolbalabs.lib.services.DisplayServiceOpenGL;
import com.boolbalabs.lib.utils.DebugLog;

import android.graphics.Point;
import android.util.Log;

/**
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */

/**
 * A generic GL Thread. Takes care of initializing EGL and GL. Delegates to a
 * Renderer instance to do the actual drawing.
 */

public class GameThreadGL extends Thread {

    private WeakReference<GameViewGL> gameViewGLWeak;
    private DisplayServiceOpenGL displayService;
    private Game game;

    private boolean mDone;
    private boolean mPaused;


    private static Semaphore sEglSemaphore;

    private final int THREAD_INPUT_QUEUE_SIZE = 20;
    private ArrayBlockingQueue<UserInputEvent> threadUserInputQueue = new ArrayBlockingQueue<UserInputEvent>(
            THREAD_INPUT_QUEUE_SIZE);
    private final Object threadUserInputQueueMutex = new Object();
    private final int ITERATION_TIME = 16;

    private Point lastTouchDownPoint = new Point(0, 0);
    private Point lastMovePoint = new Point(0, 0);
    private Point lastTouchUpPoint = new Point(0, 0);

    public GameThreadGL(GameViewGL gameViewGL) {
        this.gameViewGLWeak = new WeakReference<GameViewGL>(gameViewGL);
        sEglSemaphore = GameViewGL.getSemaphore();
        mDone = false;
        displayService = DisplayServiceOpenGL.getInstance();
    }

    public boolean isRunning() {
        return !mPaused;
    }

    public boolean isPaused() {
        return mPaused;
    }

    @Override
    public void run() {
        /*
           * When the android framework launches a second instance of an activity,
           * the new instance's onCreate() method may be called before the first
           * instance returns from onDestroy().
           *
           * This semaphore ensures that only one instance at a time accesses EGL.
           */
        try {
            try {
                sEglSemaphore.acquire();
            } catch (InterruptedException e) {
                return;
            }
            guardedRun();
        } catch (InterruptedException e) {
            // fall thru and exit normally
        } finally {
            sEglSemaphore.release();
        }
    }

    public void feedInput(UserInputEvent input) {
        synchronized (threadUserInputQueueMutex) {
            try {
                threadUserInputQueue.put(input);
            } catch (InterruptedException e) {
                Log.e("EXCEPTION", e.getMessage(), e);
            }
        }
    }

    private void guardedRun() throws InterruptedException {

        while (!mDone) {

            if (mPaused) {
                try {
                    Thread.sleep(100);
                    if (mDone) return;
                    continue;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    mDone = true;
                }
            }
            if (mDone) return;

            long startTime = System.currentTimeMillis();

            /* processing a touch here */
            processInput();
            /* updating */
            game.update();


            long actualIterationTime = System.currentTimeMillis() - startTime;
            long sleepTime = Math.max(ITERATION_TIME - actualIterationTime, 1);

            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException ex) {
            }
        }
    }

    @Override
    public void start() {
        mPaused = true;
//        resumeGame();
        //now mPaused = false;
        mDone = false;
        super.start();
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void pauseGame() {
        if (mPaused) {
            return;
        }
        DebugLog.i("THREAD", "pauseGame CALLED");
        mPaused = true;
        game.onPause();
    }

    public void resumeGame() {
        if (!mPaused) {
            return;
        }
        DebugLog.i("THREAD", "resumeGame CALLED");
        mPaused = false;
        game.onResume();
    }


    public void requestExitAndWait() {
        synchronized (this) {
            try {
                pauseGame();
                //now mPaused = true;
                mDone = true;
                interrupt();
            } catch (Exception e) {

            }
        }
    }

    /**
     * Called from main game loop, i.e. runs on main game loop thread
     * <p/>
     * Consumes (reads) the user input event from the thread's queue.
     */
    private void processInput() {
        synchronized (threadUserInputQueueMutex) {
            ArrayBlockingQueue<UserInputEvent> threadInputQueue = this.threadUserInputQueue;
            while (!threadInputQueue.isEmpty()) {
                try {
                    UserInputEvent inputEvent = threadInputQueue.take();
                    if (inputEvent.eventType == UserInputEvent.EVENT_TYPE_KEY) {
                        processKeyEvent(inputEvent);
                    } else if (inputEvent.eventType == UserInputEvent.EVENT_TYPE_TOUCH) {
                        processTouchEvent(inputEvent);
                    }
                    inputEvent.returnToPool();
                } catch (InterruptedException e) {
                    Log.e("EXCEPTION", e.getMessage(), e);
                }
            }
        }
    }

    private void processKeyEvent(UserInputEvent inputEvent) {
        // do nothing
    }

    /**
     * 1. Convert touch point to DIP 2. process touch in DIP
     *
     * @param inputEvent
     */
    private void processTouchEvent(UserInputEvent inputEvent) {
        int touchX = inputEvent.x;
        int touchY = inputEvent.y;


        if (inputEvent.action == UserInputEvent.ACTION_TOUCH_DOWN) {
            lastTouchDownPoint.set(touchX, touchY);
            game.onTouchDown(lastTouchDownPoint);
        } else if (inputEvent.action == UserInputEvent.ACTION_TOUCH_MOVE) {
            lastMovePoint.set(touchX, touchY);
            game.onTouchMove(lastTouchDownPoint, lastMovePoint);
        } else if (inputEvent.action == UserInputEvent.ACTION_TOUCH_UP) {
            lastTouchUpPoint.set(touchX, touchY);
            game.onTouchUp(lastTouchDownPoint, lastTouchUpPoint);
        }

    }

}
