/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.game;


import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Semaphore;

import android.opengl.GLSurfaceView;
import android.util.Log;
import com.boolbalabs.lib.graphics.Texture2D;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.services.ContentLoaderServiceOpenGL;
import com.boolbalabs.lib.services.DisplayServiceOpenGL;


import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;


import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


/**
 * An implementation of GLSurfaceView that uses the dedicated surface for
 * displaying an OpenGL animation.  This allows the animation to run in a
 * separate thread, without requiring that it be driven by the update mechanism
 * of the view hierarchy.
 *
 */

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */

public class GameViewGL extends GLSurfaceView {

    private boolean isGameInitialised = false;
    private GameThreadGL gameThreadGL;
    private static final Semaphore sEglSemaphore = new Semaphore(1);

    private ArrayBlockingQueue<UserInputEvent> mainGameUserInputPool;
    private final int MAIN_INPUT_POOL_SIZE = 20;

    private GameRenderer gameRenderer;
    private DisplayServiceOpenGL displayService;

    private boolean loadingFirstTime;


    // textures
    private TexturesManager texturesManager;


    public GameViewGL(Context context) {
        super(context);
        initialize();
    }

    public GameViewGL(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }


    private void initialize() {
        createInputObjectPool();
        gameRenderer = new GameRenderer();
        gameRenderer.initialize(this);

        setRenderer(gameRenderer);
        displayService = DisplayServiceOpenGL.getInstance();
        setFocusable(true);


    }


    public void startThread(Game game) {
        gameThreadGL = new GameThreadGL(this);
        gameThreadGL.setGame(game);
        gameThreadGL.start();
    }


    /**
     * Inform the view that the activity is paused.
     */
    public void pauseGame() {
        onPause();
        gameThreadGL.pauseGame();
    }

    public void stopThread() {
        try {
            gameThreadGL.requestExitAndWait();
            gameThreadGL = null;
        } catch (Exception e) {
        }
    }

    /**
     * Inform the view that the activity is resumed.
     */
    public void resumeGame() {
        gameThreadGL.resumeGame();
        onResume();
    }


    public static Semaphore getSemaphore() {
        return sEglSemaphore;
    }

    private void createInputObjectPool() {
        mainGameUserInputPool = new ArrayBlockingQueue<UserInputEvent>(MAIN_INPUT_POOL_SIZE);
        for (int i = 0; i < MAIN_INPUT_POOL_SIZE; i++) {
            mainGameUserInputPool.add(new UserInputEvent(mainGameUserInputPool));
        }
    }

    public void addTexture(Texture2D texture) {
        texturesManager.addTexture(texture);
    }

    // ********************* TOUCH EVENTS ***********************//

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        try {
            // history first
            int hist = event.getHistorySize();
            if (hist > 0) {
                // add from oldest to newest
                for (int i = 0; i < hist; i++) {
                    UserInputEvent input = mainGameUserInputPool.take();
                    input.initFromEventHistory(event, i);
                    gameThreadGL.feedInput(input);
                }
            }
            // current last
            UserInputEvent input = mainGameUserInputPool.take();
            input.initFromEvent(event);
            gameThreadGL.feedInput(input);
        } catch (InterruptedException e) {
        }
        // don't allow more than 60 motion events per second
        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
        }
        return true;
    }


}


class GameRenderer implements GLSurfaceView.Renderer {

    private DisplayServiceOpenGL displayService;
    private GLSurfaceView parentView;

    public void initialize(GLSurfaceView parentView) {
        this.parentView = parentView;
        displayService = DisplayServiceOpenGL.getInstance();
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        displayService.gl = gl;
        /* main content loading happens here */
        displayService.surfaceCreated();
        displayService.drawSplashScreen();
        ContentLoaderServiceOpenGL loader = ContentLoaderServiceOpenGL.getInstance();
        loader.loadContent();
    }

    public void onSurfaceChanged(GL10 gl, int w, int h) {
        gl.glViewport(0, 0, w, h);
        displayService.sizeChanged(w, h);
        displayService.initCustomOpenGLParameters(gl, parentView);
    }

    public void onDrawFrame(GL10 gl) {
        /* draw a game frame here */
        displayService.draw();
    }

}