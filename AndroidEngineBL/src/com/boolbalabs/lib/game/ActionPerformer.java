/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.game;

import android.os.Bundle;

/**
 * This interface is a link between GameComponent and Game. If a GameComponent
 * wants to perform an action on Game it can do it through this interface only.
 * 
 * Created to avoid having a reference to a Game object in a GameComponent.
 * 
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */

public interface ActionPerformer {

	public void performAction(int actionCode, Bundle actionParameters);

}
