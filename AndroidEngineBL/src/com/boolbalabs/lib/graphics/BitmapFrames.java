/*
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.graphics;

import com.boolbalabs.lib.managers.BitmapManager;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import com.boolbalabs.lib.utils.ScreenMetrics;

/**
 * 
 * @author Kiryl
 * 
 *         Utility class to get the rectangle enclosing a frame of a multiframe
 *         image given the index of the frame. The index starts from 0 and
 *         counts frames from left to right top to bottom.
 * 
 * 
 */
public class BitmapFrames {
	private BitmapManager bitmapManager;
	private int bitmapRef;
	private int bmWidthPix;
	private int bmHeightPix;

	private int horFrameCount;

	private float frameWidthPix;
	private float frameHeightPix;

	private Rect drawFrameSourceRect = new Rect();
	private Rect drawFrameDestRectArdp = new Rect();
	private Rect drawFrameInRectPix_SourceRect = new Rect();

	public BitmapFrames(int bitmapRef, int bitmapWidth, int bitmapHeight, int horFrameCount, int verFrameCount) {
		this.bitmapManager = BitmapManager.getInstance();
		this.bitmapRef = bitmapRef;
		this.bmWidthPix = bitmapWidth;
		this.bmHeightPix = bitmapHeight;

		this.horFrameCount = horFrameCount;

		this.frameWidthPix = this.bmWidthPix / horFrameCount;
		this.frameHeightPix = this.bmHeightPix / verFrameCount;
	}

	public void release() {
		bitmapManager = null;
	}

	// frame must be initialised
	private void getFrame(int index, Rect frame) {
		// integer division: 9 / 5= 1
		int frameRowIndex = index / horFrameCount;
		// remainder: 9 % 5 = 4
		int frameColumnIndex = index % horFrameCount;
		frame.left = (int) (0 + frameColumnIndex * frameWidthPix);
		frame.top = (int) (0 + frameRowIndex * frameHeightPix);
		frame.right = (int) (frame.left + frameWidthPix);
		frame.bottom = (int) (frame.top + frameHeightPix);
	}

	/*
	 * return top part of the frame (used for score change animation)
	 */
	private void getFrameTopPart(int frameIndex, int partsToGetCount, int totalPartsCount, Rect frame) {
		this.getFrame(frameIndex, frame);
		frame.bottom = frame.top + (int) (frameHeightPix * partsToGetCount / totalPartsCount);
	}

	/*
	 * return bottom part of the frame (used for score change animation)
	 */
	private void getFrameBottomPart(int frameIndex, int partsToGetCount, int totalPartsCount, Rect frame) {
		this.getFrame(frameIndex, frame);
		frame.top = frame.bottom - (int) (frameHeightPix * partsToGetCount / totalPartsCount);
	}

	/*
	 * draws the specified frame on the canvas at the specified position of top
	 * left corner.
	 * 
	 * The top left corner coordinate should be the coordinate of the object on
	 * medium resolution screen (320x480).
	 */
	public void drawFrame(Canvas canvas, int frameIndex, Point topLeftDip) {
		this.getFrame(frameIndex, drawFrameSourceRect);

		int drawFrameDestTopLeftPoint_X = ScreenMetrics.convertRipToPixel(topLeftDip.x);
		int drawFrameDestTopLeftPoint_Y = ScreenMetrics.convertRipToPixel(topLeftDip.y);

		drawFrameDestRectArdp.left = drawFrameDestTopLeftPoint_X;
		drawFrameDestRectArdp.top = drawFrameDestTopLeftPoint_Y;
		drawFrameDestRectArdp.right = drawFrameDestTopLeftPoint_X + (int) this.frameWidthPix;
		drawFrameDestRectArdp.bottom = drawFrameDestTopLeftPoint_Y + (int) this.frameHeightPix;

		bitmapManager.drawBitmap(canvas, bitmapRef, drawFrameSourceRect, drawFrameDestRectArdp);
	}

	/*
	 * draws the specified frame top part on the canvas at the position
	 * calculated based on partsToDrawCount, partsToDrawCount and
	 * totalPartsCount
	 * 
	 * 
	 * The top left corner coordinate should be the coordinate of the object on
	 * medium resolution screen (320x480).
	 */
	public void drawFrameTopPart(Canvas canvas, int frameIndex, Point topLeftDip, int partsToDrawCount,
			int totalPartsCount) {
		this.getFrameTopPart(frameIndex, partsToDrawCount, totalPartsCount, drawFrameSourceRect);

		int drawFrameDestTopLeftPoint_X = ScreenMetrics.convertRipToPixel(topLeftDip.x);
		int drawFrameDestTopLeftPoint_Y = ScreenMetrics.convertRipToPixel(topLeftDip.y);

		drawFrameDestTopLeftPoint_Y += (int) (frameHeightPix * (totalPartsCount - partsToDrawCount) / totalPartsCount);

		int destFrameWidth = drawFrameSourceRect.right - drawFrameSourceRect.left;
		int destFrameHeight = drawFrameSourceRect.bottom - drawFrameSourceRect.top;

		drawFrameDestRectArdp.left = drawFrameDestTopLeftPoint_X;
		drawFrameDestRectArdp.top = drawFrameDestTopLeftPoint_Y;
		drawFrameDestRectArdp.right = drawFrameDestTopLeftPoint_X + destFrameWidth;
		drawFrameDestRectArdp.bottom = drawFrameDestTopLeftPoint_Y + destFrameHeight;

		bitmapManager.drawBitmap(canvas, bitmapRef, drawFrameSourceRect, drawFrameDestRectArdp);
	}
	/*
	 * draws the specified frame bottom part on the canvas at the specified
	 * position of top left corner.
	 * 
	 * The top left corner coordinate should be the coordinate of the object on
	 * medium resolution screen (320x480).
	 */
	public void drawFrameBottomPart(Canvas canvas, int frameIndex, Point topLeftDip, int partsToDrawCount,
			int totalPartsCount) {
		this.getFrameBottomPart(frameIndex, partsToDrawCount, totalPartsCount, drawFrameSourceRect);

		int drawFrameDestTopLeftPoint_X = ScreenMetrics.convertRipToPixel(topLeftDip.x);
		int drawFrameDestTopLeftPoint_Y = ScreenMetrics.convertRipToPixel(topLeftDip.y);

		int destFrameWidth = drawFrameSourceRect.right - drawFrameSourceRect.left;
		int destFrameHeight = drawFrameSourceRect.bottom - drawFrameSourceRect.top;

		drawFrameDestRectArdp.left = drawFrameDestTopLeftPoint_X;
		drawFrameDestRectArdp.top = drawFrameDestTopLeftPoint_Y;
		drawFrameDestRectArdp.right = drawFrameDestTopLeftPoint_X + destFrameWidth;
		drawFrameDestRectArdp.bottom = drawFrameDestTopLeftPoint_Y + destFrameHeight;

		bitmapManager.drawBitmap(canvas, bitmapRef, drawFrameSourceRect, drawFrameDestRectArdp);
	}

	/*
	 * draws the specified frame on the canvas in the specified destination
	 * rectangle.
	 * 
	 * The rectangle coordinates should be the coordinate of the object on the
	 * actual screen, i.e density-dependent pixels.
	 */
	public void drawFrameInRectPix(Canvas canvas, int frameIndex, Rect destRectPix) {
		this.getFrame(frameIndex, drawFrameInRectPix_SourceRect);
		bitmapManager.drawBitmap(canvas, bitmapRef, drawFrameInRectPix_SourceRect, destRectPix);
	}


	public int getSingleFrameWidthPix() {
		return (int) this.frameWidthPix;
	}
	
	
	public int getSingleFrameHeightPix() {
		return (int) this.frameHeightPix;
	}


}
