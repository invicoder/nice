/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.graphics;

import android.graphics.Point;
import android.graphics.Rect;
import com.boolbalabs.lib.game.ZDrawable;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.utils.MathUtils;
import com.boolbalabs.lib.utils.ScreenMetrics;

import javax.microedition.khronos.opengles.GL10;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 *         Date: 27/03/2011
 *         Time: 12:14
 */
public class NumberFloatView2D extends NumberView2D {


    //****views****//
    private NumberView2D fractionalPartView;
    private ZNode pointView;

    //****other****//
    private String pointName;
    private TexturesManager texturesManager;
    private int digitsAfterPoint;

    //**** coordinates ****//
    private Rect pointRectOnTexture;
    private Rect pointRectOnScreenRip;
    private Point fixedFracPointPix;
    private int smallSkip;
    private int pWidth;


    public NumberFloatView2D(int resourceId) {
        super(resourceId);
        fractionalPartView = new NumberView2D(resourceId);
        pointView = new ZNode(resourceId, ZDrawable.MODE_OPENGL_OES_EXT);
        pointRectOnScreenRip = new Rect();
        fixedFracPointPix = new Point();
    }


    @Override
    public void initNumberView(Point fixedPointRip, String digitsFramesNames[], int align, int sizeRip) {
        throw new IllegalArgumentException("Don't use this method, use another initNumberView");
    }

    public void initNumberView(Point fixedPointRip, String digitsFramesNames[], String pointName, int align, int sizeRip, int digitsAfterPoint) {
        texturesManager = TexturesManager.getInstance();
        this.pointName = pointName;
        this.digitsAfterPoint = digitsAfterPoint;

        //** integer part **//

        super.initNumberView(fixedPointRip, digitsFramesNames, align, sizeRip);

        //** point **//
        pointRectOnTexture = texturesManager.getRectByFrameName(pointName);
        ScreenMetrics.convertRectPixToRip(pointRectOnTexture, pointRectOnScreenRip);
        pointView.initWithFrame(pointRectOnScreenRip, pointRectOnTexture);

        //**  fractional part **//
        fractionalPartView.initNumberView(fixedPointRip, digitsFramesNames, NumberView2D.ALIGN_LEFT, sizeRip);

        smallSkip = getSizePix() - pointView.getFrameInPix().height();
        pWidth = pointView.getFrameInPix().width();
    }

    public void setNumberToDrawF(float numberToDraw) {
        super.setNumberToDraw((int) numberToDraw);
        int fPartAsInt = MathUtils.getFractionalPartAsInt(numberToDraw, digitsAfterPoint);
        fractionalPartView.setNumberToDraw(fPartAsInt);
    }


    @Override
    public void setNumberToDraw(int numberToDraw) {
        setNumberToDrawF((float) numberToDraw);
    }

    @Override
    public void increaseNumberToDrawBy(int increment) {
        setNumberToDrawF(getRealValue() + increment);
    }


    @Override
    public void initialize() {
        super.initialize();
        fractionalPartView.initialize();
        pointView.initialize();
    }

    @Override
    public void loadContent() {
        super.loadContent();
        fractionalPartView.loadContent();
        pointView.loadContent();
    }

    @Override
    public void unloadContent() {
        fractionalPartView.unloadContent();
        pointView.unloadContent();
        super.unloadContent();
    }


    @Override
    public void update() {
        super.update();
        fractionalPartView.update();
    }

    @Override
    public void onAfterLoad() {
        super.onAfterLoad();
        pointView.setTexture(getTexture());
        fractionalPartView.setTexture(getTexture());
    }

    @Override
    public void draw(GL10 gl) {
        super.draw(gl);
        pointView.setPositionInPix(getFarRightPositionPix() + 1, getFixedPointPix_Y() + smallSkip);
        pointView.draw(gl);

        fixedFracPointPix.set(getFarRightPositionPix() + pWidth + 1, getFixedPointPix_Y());
        fractionalPartView.setFixedPointPix(fixedFracPointPix);
        fractionalPartView.draw(gl);
    }

}
