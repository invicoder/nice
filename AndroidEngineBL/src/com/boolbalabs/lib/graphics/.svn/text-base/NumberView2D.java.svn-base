/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */
package com.boolbalabs.lib.graphics;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

import android.graphics.Point;
import android.os.SystemClock;
import com.boolbalabs.lib.game.ZDrawable;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.managers.TexturesManager;

import android.graphics.Rect;
import android.util.Log;
import com.boolbalabs.lib.utils.ScreenMetrics;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class NumberView2D extends ZNode {

    public static final int NUMBER_OF_DIGITS = 10;
    private String digitsFramesNames[];
    private int[][] digitsCropWorkspace = new int[NUMBER_OF_DIGITS][4];// there are ten digits
    private int align;
    public static int ALIGN_LEFT = 0;
    public static int ALIGN_RIGHT = 1;
    public static int ALIGN_CENTER = 2;
    private Point fixedPointPix;
    private int sizePix;
    private int sizeRip;
    private int[] digits_width = new int[10];
    protected int oesY;
    private Rect fakeFrame;

    /* number 123 is stored as 3210000000 */
    public int[] digitsToDraw = new int[MAX_DIGIT_COUNT];
    public int lastDigitIndex = 0;// for number 007 lastdigitindex is 2
    public static final int MAX_DIGIT_COUNT = 10;
    private int theNumberToDraw = 0;
    private int theRealValue = 0;
    private int previousRealValue = 0;
    private long previousUpdateTime = 0;
    private boolean shouldCallUpdate = false;
    private int numberWidthPix = 0;
    private int farRightPositionPix = 0;
    private TexturesManager texturesManager;

    public enum NumberDrawingMode {

        NDISCRETE, NCONTINUOUS
    }
    public NumberDrawingMode drawingMode = NumberDrawingMode.NDISCRETE;
    private int incrementValue = 1;
    private int drawingSpeed = 50;

    public NumberView2D(int resourceId) {
        super(resourceId, ZDrawable.MODE_OPENGL_OES_EXT);
        userInteractionEnabled = false;
    }

    /**
     * @param fixedPointRip     - depending on this point
     *                          and "align" a rect on screen is calculated. Must be topleft if align is LEFT, center if align is CENTER
     *                          topright if align is RIGHT.
     * @param digitsFramesNames
     * @param align
     */
    public void initNumberView(Point fixedPointRip, String digitsFramesNames[], int align, int sizeRip) {
        this.sizeRip=sizeRip;
        this.sizePix = ScreenMetrics.convertRipToPixel(sizeRip);
        setNumberToDraw(0);
        this.digitsFramesNames = digitsFramesNames;
        setFixedPointRip(fixedPointRip);
        setAlignment(align);
    }

    @Override
    public void initialize() {
        texturesManager = TexturesManager.getInstance();
        initDigitsCropWorkspace();
        fakeFrame = new Rect(0, 0, sizePix, sizePix);
        super.setFrameInRip(fakeFrame);
    }

    private void initDigitsCropWorkspace() {
        setCorrectDigitsWidth();
        for (int i = 0; i < NUMBER_OF_DIGITS; i++) {
            Rect currentTextureRect = texturesManager.getRectByFrameName(digitsFramesNames[i]);
            if (currentTextureRect == null) {
                continue;
            }
            int frameWidthOnTexturePix = currentTextureRect.width();
            int frameHeightOnTexturePix = currentTextureRect.height();
            digitsCropWorkspace[i] = new int[]{currentTextureRect.left, currentTextureRect.bottom,
                        frameWidthOnTexturePix, -frameHeightOnTexturePix};
        }
    }

    private void setCorrectDigitsWidth() {
        for (int i = 0; i < NUMBER_OF_DIGITS; i++) {
            Rect currentTextureRect = texturesManager.getRectByFrameName(digitsFramesNames[i]);
            if (currentTextureRect == null) {
                continue;
            }
            int frameWidthOnTexturePix = currentTextureRect.width();
            int frameHeightOnTexturePix = currentTextureRect.height();
            float coeff = (float) sizePix / (float) frameHeightOnTexturePix;
            digits_width[i] = (int) (frameWidthOnTexturePix * coeff);
        }
    }

    public void setNumberToDraw(int theNumberToDraw) {
        theRealValue = Math.abs(theNumberToDraw);
        if (drawingMode == NumberView2D.NumberDrawingMode.NDISCRETE) {
            this.theNumberToDraw = theRealValue;
        } else if (previousRealValue != theRealValue || theRealValue == 0) {
            this.theNumberToDraw = previousRealValue;
            previousRealValue = theRealValue;
            shouldCallUpdate = true;
        }
        calculateDigits();
    }

    public int getRealValue() {
        return theRealValue;
    }

    public void increaseNumberToDrawBy(int increment) {
        setNumberToDraw(theNumberToDraw + increment);
    }

    public void increaseInstantly() {
        theNumberToDraw = theRealValue;
        previousRealValue = theRealValue;
        calculateDigits();
    }

    private void calculateDigits() {
        int curModulusDivider = 10;
        int curIntegerDivider = 1;
        numberWidthPix = 0;

        // clear digits array
        for (int i = 0; i < MAX_DIGIT_COUNT; i++) {
            digitsToDraw[i] = 0;
        }

        // calculate digits
        for (int i = 0; i < MAX_DIGIT_COUNT; i++) {
            digitsToDraw[i] = (theNumberToDraw % curModulusDivider) / curIntegerDivider;
            numberWidthPix += digits_width[digitsToDraw[i]];

            if (curModulusDivider > theNumberToDraw) {
                // we are done
                lastDigitIndex = i;
                return;
            }

            curModulusDivider *= 10;
            curIntegerDivider *= 10;
        }
        // if we reached here this means the number is larger than 10 to the
        // power of MAX_DIGIT_COUNT
        lastDigitIndex = MAX_DIGIT_COUNT - 1;


    }

    /**
     * @param drawingSpeed   - less is faster, in ms
     * @param incrementValue
     */
    public void setContinuousDrawingParameters(int drawingSpeed, int incrementValue) {
        this.drawingSpeed = drawingSpeed;
        this.incrementValue = incrementValue;
    }

    @Override
    public void update() {
        if (shouldCallUpdate && drawingMode == NumberView2D.NumberDrawingMode.NCONTINUOUS && SystemClock.uptimeMillis() - previousUpdateTime > drawingSpeed) {
            previousUpdateTime = SystemClock.uptimeMillis();
            theNumberToDraw += incrementValue;
            if ((theNumberToDraw > theRealValue && incrementValue >= 0) || (theNumberToDraw < theRealValue && incrementValue < 0)) {
                theNumberToDraw = theRealValue;
                shouldCallUpdate = false;
            }
            calculateDigits();
        }
    }
    private boolean digitsDrawingDirection = true;

    @Override
    protected void drawOES(GL10 gl) {
        gl.glBindTexture(GL10.GL_TEXTURE_2D, getTextureGlobalIndex());
        int currLeftPosX = 0;
        if (align == ALIGN_LEFT) {
            currLeftPosX = fixedPointPix.x;
            digitsDrawingDirection = true;
        } else if (align == ALIGN_RIGHT) {
            currLeftPosX = fixedPointPix.x;
            digitsDrawingDirection = false;
        } else if (align == ALIGN_CENTER) {
            digitsDrawingDirection = true;
            currLeftPosX = fixedPointPix.x - numberWidthPix / 2;
        }

        if (digitsDrawingDirection) {
            for (int i = lastDigitIndex; i >= 0; i--) {
                int digitToDraw = digitsToDraw[i];
                ((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D, GL11Ext.GL_TEXTURE_CROP_RECT_OES, digitsCropWorkspace[digitToDraw], 0);
                ((GL11Ext) gl).glDrawTexfOES(currLeftPosX, oesY, getCoordsOES_Z(),
                        digits_width[digitToDraw], sizePix);
                currLeftPosX = currLeftPosX + digits_width[digitToDraw] + 1;
            }
            farRightPositionPix = currLeftPosX;
        } else {
            farRightPositionPix = currLeftPosX;
            for (int i = 0; i <= lastDigitIndex; i++) {
                int digitToDraw = digitsToDraw[i];
                currLeftPosX = currLeftPosX - digits_width[digitToDraw] - 1;
                ((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D, GL11Ext.GL_TEXTURE_CROP_RECT_OES, digitsCropWorkspace[digitToDraw], 0);
                ((GL11Ext) gl).glDrawTexfOES(currLeftPosX, oesY, getCoordsOES_Z(),
                        digits_width[digitToDraw], sizePix);
            }
        }
    }

    public int getFarRightPositionPix() {
        return farRightPositionPix;
    }

    public void setAlignment(int align) {
        if (align >= 0 && align <= 2) {
            this.align = align;
        } else {
            this.align = ALIGN_LEFT;
        }

    }

    public void setFixedPointRip(Point fixedPointRip) {
        setCenterInRip(fixedPointRip.x, fixedPointRip.y);
        fixedPointPix = getCenterInPix();
        oesY = ScreenMetrics.screenHeightPix - fixedPointPix.y - sizePix;
    }

    @Override
    public void setFrameInRip(Rect frame) {
        sizePix = frame.height();
        setCorrectDigitsWidth();
        calculateDigits();
    }

    public void setFixedPointPix(Point fixedPPix) {
        fixedPointPix.set(fixedPPix.x, fixedPPix.y);
        oesY = ScreenMetrics.screenHeightPix - fixedPointPix.y - sizePix;
    }

    protected int getFixedPointPix_Y() {
        return fixedPointPix.y;
    }

    protected int getSizePix() {
        return sizePix;
    }

    protected int getSizeRip() {
        return sizeRip;
    }
}
