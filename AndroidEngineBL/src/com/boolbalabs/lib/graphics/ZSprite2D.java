/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */
package com.boolbalabs.lib.graphics;

import android.graphics.Rect;
import android.os.SystemClock;
import android.util.Log;
import com.boolbalabs.lib.game.ZDrawable;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.utils.BufferUtils;
import com.boolbalabs.lib.utils.DebugLog;
import com.boolbalabs.lib.utils.MathUtils;

import java.nio.FloatBuffer;
import java.util.HashMap;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 *         Date: 03/04/2011
 *         Time: 16:01
 */
public class ZSprite2D extends ZNode {

    private final static int maxStartDelayMs = 5000;
    private String framesNames[];
    private FloatBuffer frameTextureBuffers[];
    ///*****booleans****///
    private boolean isFinished = false;
    private boolean isStarted = false;
    private boolean updatable;
    private long pauseTime;
    private long resumeTime;
    private int totalFramesCount;
    private int curFrameIndexToDraw;
    private int defaultFrameIndexToDraw = -1;
    private long timeAnimationStartedMs = -1;
    private int startAnimationDelay = 0;
    private int visibleDelay = 0;
    private int[][] framesRectsOnTexture;
    private Rect tempRect = new Rect();
    //******ANIMATION STATES*****//
    private final int ANIMATION_DEFAULT_ID = 0;
    private AnimationState defaultAnimationState;
    private AnimationState currentAnimationState;
    private HashMap<Integer, AnimationState> states;

    public ZSprite2D(int resourceId, int drawingMode) {
        super(resourceId, drawingMode);
        states = new HashMap<Integer, AnimationState>();
    }

    public void initializeAnimation(String framesNames[], int frameIndices[], int frameDelays[], Rect screenRect, boolean isLooped) {
        if (framesNames == null || framesNames.length == 0) {
            DebugLog.i("ZSprite2D", "frameNames is empty");
            return;
        }
        if (MathUtils.getMaxElement(frameIndices) >= framesNames.length) {
            DebugLog.i("ZSprite2D", "frameIndices length is bigger than framesNames.length");
            return;
        }
        this.framesNames = framesNames;
        totalFramesCount = frameIndices.length;
        isFinished = false;
        isStarted = false;
        defaultAnimationState = new AnimationState(0, frameIndices, frameDelays, isLooped);
        currentAnimationState = defaultAnimationState;
        states.put(ANIMATION_DEFAULT_ID, defaultAnimationState);
        setFrameInRip(screenRect);
    }

    /**
     * sets uniform animation delays, and frameIndices are from 0 to frameNames.length-1
     *
     * @param framesNames
     * @param animationSpeed
     * @param isLooped
     */
    public void initializeAnimation(String framesNames[], int animationSpeed, Rect screenRect, boolean isLooped) {
        int length = framesNames.length;
        int[] frameIndices = new int[length];
        for (int i = 0; i < length; i++) {
            frameIndices[i] = i;
        }
        int[] frameDelays = new int[length];
        for (int i = 0; i < length; i++) {
            frameDelays[i] = 10 + i * animationSpeed;
        }
        initializeAnimation(framesNames, frameIndices, frameDelays, screenRect, isLooped);
    }

    public void addState(AnimationState state) {
        if (state == null) {
            DebugLog.i("ZSprite2D", "ERROR: state is NULL");
            return;
        }
        if (state.id == ANIMATION_DEFAULT_ID) {
            DebugLog.i("ZSprite2D", "defaultAnimationState.id cannot be equal to state.id");
            return;

        }
        states.put(state.id, state);
    }

    private void initRectsOnTexture() {
        TexturesManager texturesManager = TexturesManager.getInstance();
        int size = framesNames.length;
        framesRectsOnTexture = new int[size][4];
        if (getDrawingMode() == ZDrawable.MODE_OPENGL_VERTICES) {
            frameTextureBuffers = new FloatBuffer[size];
        }
        for (int i = 0; i < size; i++) {
            Rect currentTextureRect = texturesManager.getRectByFrameName(framesNames[i]);
            if (currentTextureRect == null) {
                continue;
            }
            int frameWidthOnTexturePix = currentTextureRect.width();
            int frameHeightOnTexturePix = currentTextureRect.height();
            framesRectsOnTexture[i] = new int[]{currentTextureRect.left, currentTextureRect.bottom,
                        frameWidthOnTexturePix, -frameHeightOnTexturePix};
            if (getDrawingMode() == ZDrawable.MODE_OPENGL_VERTICES) {
                frameTextureBuffers[i] = BufferUtils.makeTextureBuffer(currentTextureRect, getTexture().getWidth(), getTexture().getHeight());
            }
        }
    }

    @Override
    public void onAfterLoad() {
        initRectsOnTexture();
        super.onAfterLoad();
    }

    public void setRectOnTexture(int[] cropRect) {
        tempRect.set(cropRect[0], cropRect[1] + cropRect[3], cropRect[0] + cropRect[2], cropRect[1]);
        if (getDrawingMode() == ZDrawable.MODE_OPENGL_OES_EXT) {
            setRectOnTexture(tempRect);
        } else if (getDrawingMode() == ZDrawable.MODE_OPENGL_VERTICES) {
            setTextureBuffer(frameTextureBuffers[curFrameIndexToDraw]);
        }

    }

    public void setRectsForDefaultAnimationOES(int[][] framesRectsOnTexture) {
        if (MathUtils.getMaxElement(defaultAnimationState.indices) >= framesRectsOnTexture.length) {
            return;
        }
        this.framesRectsOnTexture = framesRectsOnTexture;
    }

    public void setFrameNames(String[] framesNames) {
        if (MathUtils.getMaxElement(defaultAnimationState.indices) >= framesNames.length) {
            return;
        }
        this.framesNames = framesNames;
    }

    public boolean isFinished() {
        return this.isFinished;
    }

    public boolean isStarted() {
        return this.isStarted;
    }

    public void startDefaultAnimation() {
        currentAnimationState = defaultAnimationState;
        isStarted = true;
        timeAnimationStartedMs = -1;
        onAnimationStarted(currentAnimationState.id);
    }

    public void stopCurrentAnimation() {
        isFinished = false;
        isStarted = false;
    }

    public void startAnimation() {
        startDefaultAnimation();
    }

    public void startAnimation(int stateId) {
        if (!states.containsKey(stateId)) {
            DebugLog.i("ZSprite2D", "Cannot start animation: no such state " + stateId);
            return;
        }
        currentAnimationState = states.get(stateId);
        if (MathUtils.getMaxElement(currentAnimationState.indices) >= framesNames.length) {
            DebugLog.i("ZSprite2D", "Cannot start animation: indices.length is bigger than framesNames.length");
            return;
        }
        isFinished = false;
        isStarted = true;
        timeAnimationStartedMs = -1;
        onAnimationStarted(stateId);
    }

    //TODO
    public void startWithRandomDelays(boolean visDelay, int maxVisibleDelayMs, boolean animDelay, int maxAnimationDelayMs) {
//        if (visDelay) {
//            drawableParent.visible = false;
//            visibleDelay = MathUtils.rand.nextInt(maxVisibleDelayMs);
//        } else {
//            visibleDelay = 0;
//        }
//        if (animDelay) {
//            startAnimationDelay = MathUtils.rand.nextInt(maxAnimationDelayMs);
//        } else {
//            startAnimationDelay = 0;
//        }
//        start();
    }

    public void resetAnimation() {
        stopCurrentAnimation();
    }

    public void stopAnimation() {
        stopCurrentAnimation();
    }

    @Override
    public void update() {

        synchronized (currentAnimationState) {

            if (!isStarted || isFinished) {
                // if no animation, but defaultFrameIndexToDraw is not -1
                if (defaultFrameIndexToDraw > -1) {
                    curFrameIndexToDraw = defaultFrameIndexToDraw;
                    setRectOnTexture(framesRectsOnTexture[curFrameIndexToDraw]);
                }
                return;
            }
            long timeMs = SystemClock.uptimeMillis();
            if (timeAnimationStartedMs == -1) {
                timeAnimationStartedMs = timeMs;
            }
            long deltaTime = timeMs - timeAnimationStartedMs;
            updatable = deltaTime >= visibleDelay;
            if (!updatable) {
                return;
            }

            if (deltaTime < startAnimationDelay) {
                curFrameIndexToDraw = currentAnimationState.indices[0];
                setRectOnTexture(framesRectsOnTexture[curFrameIndexToDraw]);
                return;
            }

            if (deltaTime > currentAnimationState.animationCycleLengthMs + startAnimationDelay) {
                if (currentAnimationState.isLooped) {
                    // finished but looped, just re-cycle animation
                    visibleDelay = 0;
                    deltaTime = startAnimationDelay
                            + (deltaTime - startAnimationDelay)
                            % currentAnimationState.animationCycleLengthMs;
                    timeAnimationStartedMs = timeMs;
                } else {
                    // finished and not looped => end animation
                    isFinished = true;
                    isStarted = false;
                    onAnimationFinished(currentAnimationState.id);
                    return;
                }
            }
            // now deltaTime is definitely less than animationCycleLengthMs
            // -> we need to find which frame to draw
            for (int i = 0; i < currentAnimationState.size; i++) {
                if (currentAnimationState.delays[i] > deltaTime - startAnimationDelay) {
                    curFrameIndexToDraw = currentAnimationState.indices[i];
                    setRectOnTexture(framesRectsOnTexture[curFrameIndexToDraw]);
                    return;
                }
            }
            // should not reach this, but just in case draw first frame
            curFrameIndexToDraw = currentAnimationState.indices[0];
        }
    }

    /**
     * This is called when animation is started
     */
    public void onAnimationStarted(int state) {
    }

    /**
     * This is called when animation is finished and is NOT looped
     */
    public void onAnimationFinished(int state) {
    }

    /**
     * This method helps make all views animate individually by randomizing the
     * time of the 1st frame.
     */
    //TODO
    @Deprecated
    private static int[] getRandomizedFrameDelays(int[] frameDelaysArray) {
        int randomOffsetMs = MathUtils.rand.nextInt(maxStartDelayMs);
        int framesCount = frameDelaysArray.length;
        int[] result = new int[framesCount];

        for (int i = 0; i < framesCount; i++) {
            result[i] = frameDelaysArray[i] + randomOffsetMs;
        }
        return result;
    }

    /**
     * displays frame with index=frameIndex, when there is no animation.
     * Set to -1 if you want view to be invisible before/after animation.
     *
     * @param frameIndex
     */
    public void setDefaultFrameIndexToDraw(int frameIndex) {
        defaultFrameIndexToDraw = frameIndex;
    }

    public int getCurrentAnimationStateId() {
        if (currentAnimationState != null) {
            return currentAnimationState.id;
        }
        return 0;
    }

    public void pause() {
        pauseTime = SystemClock.uptimeMillis();
    }

    public void resume() {
        resumeTime = SystemClock.uptimeMillis();
        timeAnimationStartedMs = timeAnimationStartedMs + resumeTime - pauseTime;
    }
}
