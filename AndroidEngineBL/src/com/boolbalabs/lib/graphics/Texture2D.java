/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.graphics;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

import android.graphics.BitmapFactory;
import com.boolbalabs.lib.managers.BitmapManager;
import com.boolbalabs.lib.managers.TexturesManager;

import android.graphics.Bitmap;
import android.opengl.GLU;
import android.opengl.GLUtils;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */

public class Texture2D {

    public enum TextureFilter {
        Nearest, Linear, MipMap
    }

    public enum TextureWrap {
        ClampToEdge, Wrap
    }

    private TextureFilter minFilter, maxFilter;
    private TextureWrap sWrap, tWrap;

    /**
     * the texture handle *
     */
    private int textureGlobalIndex = -1;

    /**
     * height of original image in pixels *
     */
    private int height;
    /**
     * width of original image in pixels *
     */
    private int width;

    private int resourceId;

    /**
     * Creates a new texture based on the given image
     */
    public Texture2D(int resId, TextureFilter minFilter, TextureFilter maxFilter, TextureWrap sWrap, TextureWrap tWrap) {

        resourceId = resId;
        this.minFilter = minFilter;
        this.maxFilter = maxFilter;
        this.sWrap = sWrap;
        this.tWrap = tWrap;

    }

    public void buildTexture(GL10 gl) {

        if (gl == null) {
            return;
        }

        int[] textures = new int[1];
        gl.glGenTextures(1, textures, 0);
        textureGlobalIndex = textures[0];


        Bitmap image = BitmapManager.decodeBitmap(resourceId, TexturesManager.textureBitmapOptions);

        this.width = image.getWidth();
        this.height = image.getHeight();

        gl.glBindTexture(GL10.GL_TEXTURE_2D, textureGlobalIndex);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, getTextureFilter(minFilter));
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, getTextureFilter(maxFilter));
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, getTextureWrap(sWrap));
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, getTextureWrap(tWrap));

        gl.glTexEnvf(GL10.GL_TEXTURE_ENV, GL10.GL_TEXTURE_ENV_MODE, GL10.GL_REPLACE);

        // Important: bitmap dimensions are only powers of 2 !!!
        GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, image, 0);


        image.recycle();

        gl.glMatrixMode(GL10.GL_TEXTURE);
        gl.glLoadIdentity();
        Log.i("TEXTURE", " TEXTURE #" + textureGlobalIndex + " created...");
        int error = gl.glGetError();
        if (error != GL10.GL_NO_ERROR) {
            Log.e("GLUtils", "ERROR: " + GLU.gluErrorString(error));
        }
    }

    private int getTextureFilter(TextureFilter filter) {
        if (filter == TextureFilter.Linear)
            return GL10.GL_LINEAR;
        else if (filter == TextureFilter.Nearest)
            return GL10.GL_NEAREST;
        else
            return GL10.GL_LINEAR_MIPMAP_NEAREST;
    }

    private int getTextureWrap(TextureWrap wrap) {
        if (wrap == TextureWrap.ClampToEdge)
            return GL10.GL_CLAMP_TO_EDGE;
        else
            return GL10.GL_REPEAT;
    }

    /**
     * Disposes the texture and frees the associated resources
     *
     * @param gl
     */
    public void dispose(GL10 gl) {
        int[] textures = {textureGlobalIndex};
        gl.glDeleteTextures(1, textures, 0);
        textureGlobalIndex = 0;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getResourceId() {
        return resourceId;
    }

    public int getTextureGlobalIndex() {
        return textureGlobalIndex;
    }

}
