/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.graphics;

import com.boolbalabs.lib.utils.DebugLog;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 *         Date: 04/04/2011
 *         Time: 09:24
 */
public class AnimationState {
    public int id;
    public int indices[];
    public int delays[];
    public boolean isLooped;
    public int animationCycleLengthMs;
    public int size;


    public AnimationState(int id, int frameIndices[], int frameDelays[], boolean isLooped) {
        if (frameIndices.length != frameDelays.length) {
            DebugLog.i("ZSprite2D", "frameIndices.length != frameDelays.length");
            return;
        }
        this.id = id;
        indices = frameIndices;
        delays = frameDelays;
        size = indices.length;
        animationCycleLengthMs = delays[size - 1];
        this.isLooped = isLooped;
    }

    public boolean isEqual(AnimationState anotherState) {
        return id == anotherState.id;
    }
}
