package com.boolbalabs.lib.managers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Rect;
import android.util.Log;
import com.boolbalabs.lib.graphics.Texture2D;
import com.boolbalabs.lib.services.DisplayServiceOpenGL;
import com.boolbalabs.lib.utils.CoordinatesParser;
import com.boolbalabs.lib.utils.DebugLog;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.SparseArray;

public class TexturesManager {

    private SparseArray<Integer> globalTextureIndices;
    private SparseArray<Texture2D> texturesList;
    private static TexturesManager texturesManager;
    private static boolean isInitialised = false;
    private static final Object isInitialisedLock = new Object();
    private static Resources appResources;
    // Specifies the format our textures should be converted to upon load.
    public static BitmapFactory.Options textureBitmapOptions = new BitmapFactory.Options();
    private GL10 gl;


    /**
     * we are using Zwoptex to create .png textures. It also generates an additional file with coordinates.
     * We parse this file upon a TexturesManager construction and store results in
     * HashMap framesRectangles.
     */
    private HashMap<String, Rect> allRectangles;

    private TexturesManager() {
        globalTextureIndices = new SparseArray<Integer>();
        texturesList = new SparseArray<Texture2D>();
        allRectangles = new HashMap<String, Rect>();
    }

    public static TexturesManager getInstance() {
        return texturesManager;
    }

    public static void init(Resources applicationResources) {
        synchronized (isInitialisedLock) {
            if (isInitialised) {
                return;
            }
            texturesManager = new TexturesManager();
            texturesManager.appResources = applicationResources;
            isInitialised = true;
        }

    }

    public static void release() {
        synchronized (isInitialisedLock) {
            if (!isInitialised || texturesManager.gl == null) {
                return;
            }
            if (texturesManager != null) {

//                int numberOfTextures = texturesManager.texturesList.size();
//                for (int i = 0; i < numberOfTextures; i++) {
//                    Texture2D currentTexture = texturesManager.texturesList.get(texturesManager.texturesList.keyAt(i));
//                    if (currentTexture != null) {
//                        currentTexture.dispose(texturesManager.gl);
//                    } else {
//                        DebugLog.i("TEXTURES", "TEXTURES DISPOSED " + currentTexture.getResourceId());
//                    }
//                }
//                DebugLog.i("TEXTURES", "ALL TEXTURES DISPOSED");
                texturesManager.texturesList = null;
                texturesManager.globalTextureIndices = null;
                texturesManager = null;
            }
            isInitialised = false;
        }

    }

    public void loadCoordinatesFromFile(String filename) {
        CoordinatesParser parser = CoordinatesParser.getInstance();
        allRectangles.putAll(parser.parseEntireFile(filename));
    }

    public void loadTextures() {
        GL10 gl = DisplayServiceOpenGL.getInstance().gl;
        this.gl = gl;
        // Set our bitmaps to 16-bit, 565 format.
        textureBitmapOptions.inPreferredConfig = Bitmap.Config.RGB_565;
        //textureBitmapOptions.inTempStorage = new byte[2 * 2048 * 2048];
        int numberOfTextures = texturesList.size();
        for (int i = 0; i < numberOfTextures; i++) {
            Texture2D currentTexture = texturesList.get(texturesList.keyAt(i));
            currentTexture.buildTexture(gl);
            addTextureIndex(currentTexture);
        }
    }

    private void addTextureIndex(Texture2D texture) {
        if (globalTextureIndices != null) {
            globalTextureIndices.put(texture.getResourceId(), texture.getTextureGlobalIndex());
        }
    }

    public int getGlobalIndexByResourceId(int resId) {
        if (globalTextureIndices != null) {
            return globalTextureIndices.get(resId, -1);
        }
        return -1;
    }

    public Texture2D getTextureByResourceId(int resId) {
        if (texturesList != null) {
            return texturesList.get(resId, null);
        }
        return null;
    }

    public Rect getRectByFrameName(String frameName) {
        if (allRectangles.containsKey(frameName)) {
            return allRectangles.get(frameName);
        }
        return null;
    }

    /**
     * @param texture - texture to add
     */
    public void addTexture(Texture2D texture) {
        if (texturesList != null) {
            int id = texture.getResourceId();
            Texture2D existingTexture = texturesList.get(id);
            if (existingTexture == null) {
                DebugLog.i("ADDING TEXTURE", " " + id);
                texturesList.put(id, texture);
            }
        }
    }


    public void showTextureIds() {
        int numberOfTextures = texturesList.size();
        for (int i = 0; i < numberOfTextures; i++) {
            Texture2D currentTexture = texturesList.get(texturesList.keyAt(i));
            Log.i("TEXTURE ID", "ID: " + currentTexture.getTextureGlobalIndex());
        }
    }

}
