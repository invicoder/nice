package com.boolbalabs.lib.managers;

import android.content.Context;
import android.os.Vibrator;

public class VibratorManager {

	private Vibrator vibrator;
	private static VibratorManager vibratorManager;
	private static boolean isInitialised = false;
	private long[] pattern = new long[2];

	private VibratorManager(Context context) {
		vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

	}

	public static VibratorManager getInstance() {
		return vibratorManager;
	}

	public static void init(Context context) {
		if (isInitialised) {
			return;
		}
		isInitialised = true;
		vibratorManager = new VibratorManager(context);
	}

	public static void release() {
		if (!isInitialised) {
			return;
		}
		if (vibratorManager != null) {
			vibratorManager.vibrator = null;
			vibratorManager = null;
		}
		isInitialised = false;
	}

	public void vibrate(long milliseconds) {
		if (vibrator != null) {
			vibrator.vibrate(milliseconds);
		}
	}

	public void vibrate(long delayMs, long vibrateTimeMs) {
		pattern[0] = delayMs;
		pattern[1] = vibrateTimeMs;
		// -1 means: do not repeat
		if (vibrator != null) {
			try {
				vibrator.vibrate(pattern, -1);
			} catch (Exception e) {
			}
		}
	}
}
