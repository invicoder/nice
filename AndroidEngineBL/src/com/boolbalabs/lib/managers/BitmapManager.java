/*
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.managers;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;

import com.boolbalabs.lib.graphics.BitmapFrames;

import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.util.Xml;

import com.boolbalabs.lib.utils.ScreenMetrics;

public class BitmapManager {

	private SparseArray<Bitmap> bitmaps = new SparseArray<Bitmap>();
	private SparseArray<BitmapFrames> bitmapFrames = new SparseArray<BitmapFrames>();
	private static BitmapManager bitmapManager;
	private static boolean isInitialised = false;
	private Matrix identityMatrix = new Matrix();
	private static final Object isInitialisedLock = new Object();
	private final static float DENSITY_FACTOR = ScreenMetrics.screenDensityX / 240;
	private static BitmapFactory.Options noScalingOptions = new BitmapFactory.Options();
	private static Resources appResources;

	public static BitmapManager getInstance() {
		return bitmapManager;
	}

	public static void init(Resources applicationResources) {
		synchronized (isInitialisedLock) {
			if (isInitialised) {
				return;
			}
			bitmapManager = new BitmapManager();
			appResources = applicationResources;
			isInitialised = true;
		}

	}

	public static void release() {
		synchronized (isInitialisedLock) {
			if (!isInitialised) {
				return;
			}
			if (bitmapManager != null) {
				bitmapManager.recycleBitmaps();
				bitmapManager.bitmaps = null;
				bitmapManager.bitmapFrames = null;
				bitmapManager = null;
			}
			isInitialised = false;
		}

	}

	private BitmapManager() {
		// setup noScalingOptions so that new devices do not scale the images
		try {
			Field inDensityField = BitmapFactory.Options.class
					.getField("inDensity");
			Field inTargetDensityField = BitmapFactory.Options.class
					.getField("inTargetDensity");
			Field inScaledField = BitmapFactory.Options.class
					.getField("inScaled");
			/* success, this is a newer device */

			inDensityField.setInt(noScalingOptions, 0);
			inTargetDensityField.setInt(noScalingOptions, 0);
			inScaledField.setBoolean(noScalingOptions, false);
		} catch (Exception e) {
			/* failure, must be older device */
		}

	}

	protected Bitmap getScaledBitmap(int bitmapRef) {
		Bitmap originalBitmap = BitmapFactory.decodeResource(appResources,
				bitmapRef, noScalingOptions);

		if (DENSITY_FACTOR == 1) {
			// we are on hdpi screen - no scaling required
			return originalBitmap;
		} else {

			Bitmap scaledBitmap = Bitmap.createScaledBitmap(originalBitmap,
					(int) (originalBitmap.getWidth() * DENSITY_FACTOR),
					(int) (originalBitmap.getHeight() * DENSITY_FACTOR), true);
			originalBitmap.recycle();

			return scaledBitmap;
		}
	}

	public static Bitmap decodeBitmap(int bitmapRef,
			BitmapFactory.Options options) {

		TypedValue value = new TypedValue();
		appResources.getValue(bitmapRef, value, true);
		String file = value.string.toString();
		BitmapDrawable dr;
		Bitmap originalBitmap;
		if (file.endsWith(".xml")) {
			int previousDensity = appResources.getDisplayMetrics().densityDpi;
			try {
				XmlResourceParser rp =appResources.getXml(bitmapRef);
				appResources.getDisplayMetrics().densityDpi=DisplayMetrics.DENSITY_MEDIUM;
				dr=(BitmapDrawable)Drawable.createFromXml(appResources, rp);
				rp.close();
			} catch (Exception e) {
				NotFoundException rnf = new NotFoundException("File " + file
						+ " from drawable resource ID #0x"
						+ Integer.toHexString(bitmapRef));
				rnf.initCause(e);
				throw rnf;
			}
			originalBitmap = ((BitmapDrawable) dr).getBitmap();
			appResources.getDisplayMetrics().densityDpi=previousDensity;
		} else {
			InputStream is = appResources.openRawResource(bitmapRef);
			try {
				originalBitmap = BitmapFactory.decodeStream(is, null, options);
			} finally {
				try {
					is.close();
				} catch (IOException e) {
					// Ignore.
				}
			}
		}

		return originalBitmap;

	}

	public void recycleBitmaps() {
		// recycle bitmaps

		int bitmapsCount = bitmaps.size();
		for (int i = 0; i < bitmapsCount; i++) {
			int curResourceId = bitmaps.keyAt(i);
			Bitmap bmToRelease = bitmaps.get(curResourceId);
			if (bmToRelease != null) {
				bmToRelease.recycle();
			}
		}
		bitmaps.clear();

		// release bitmap frames
		int bitmapsFramesCount = bitmapFrames.size();
		for (int i = 0; i < bitmapsFramesCount; i++) {
			int curResourceId = bitmapFrames.keyAt(i);
			BitmapFrames bmfToRelease = bitmapFrames.get(curResourceId);
			if (bmfToRelease != null) {
				bmfToRelease.release();
			}
		}
		bitmapFrames.clear();

	}

	public void drawBitmap(Canvas c, int bitmapRef, int leftDip, int topDip) {

		if (bitmaps == null) {
			Log.i("BitmapManager", "error: drawing recycled bitmap");
			return;
		}

		Bitmap bm = bitmaps.get(bitmapRef);
		if (bm != null && !bm.isRecycled()) {
			c.drawBitmap(bm, ScreenMetrics.convertRipToPixel(leftDip),
					ScreenMetrics.convertRipToPixel(topDip), null);
		} else {

			Log.i("BitmapManager", "error: drawing recycled bitmap");
		}

	}

	public void drawBitmapPix(Canvas c, int bitmapRef, int leftPix, int topPix) {

		if (bitmaps == null) {

			return;
		}
		Bitmap b = bitmaps.get(bitmapRef);
		if (b != null && !b.isRecycled()) {
			c.drawBitmap(b, leftPix, topPix, null);
		} else {

			Log.i("BitmapManager", "error: drawing recycled bitmap");
		}

	}

	public void drawBitmap(Canvas c, int bitmapRef, Rect sourceRectPix,
			Rect destRectPix) {

		if (bitmaps == null) {

			return;
		}
		Bitmap b = bitmaps.get(bitmapRef);
		if (b != null && !b.isRecycled()) {
			c.drawBitmap(b, sourceRectPix, destRectPix, null);
		} else {
			Log.i("BitmapManager", "error: drawing recycled bitmap");
		}

	}

	public void drawBitmap(Canvas c, int bitmapRef, Matrix m, Point posPix) {

		c.setMatrix(m);
		Bitmap b = bitmaps.get(bitmapRef);
		if (b != null && !b.isRecycled()) {
			c.drawBitmap(b, posPix.x, posPix.y, null);
		} else {
			// should not happen in theory but happens in practice
			Log.i("BitmapManager", "error: drawing recycled bitmap");
		}

		c.setMatrix(identityMatrix);
	}

	public void drawBitmapFrame(Canvas c, int bitmapFrameRef, int frameIndex,
			Point posDip) {

		if (bitmapFrames != null) {
			BitmapFrames bf = bitmapFrames.get(bitmapFrameRef);
			if (bf != null) {
				bf.drawFrame(c, frameIndex, posDip);
			}
		}

	}

	public void drawBitmapFrameTopPart(Canvas c, int bitmapFrameRef,
			int frameIndex, Point posDip, int partsToDrawCount,
			int totalPartsCount) {

		BitmapFrames bf = bitmapFrames.get(bitmapFrameRef);
		if (bf != null) {
			bf.drawFrameTopPart(c, frameIndex, posDip, partsToDrawCount,
					totalPartsCount);
		}
	}

	public void drawBitmapFrameBottomPart(Canvas c, int bitmapFrameRef,
			int frameIndex, Point posDip, int partsToDrawCount,
			int totalPartsCount) {

		BitmapFrames bf = bitmapFrames.get(bitmapFrameRef);
		if (bf != null) {
			bf.drawFrameBottomPart(c, frameIndex, posDip, partsToDrawCount,
					totalPartsCount);
		}
	}

	public void drawBitmapFrameInRectPix(Canvas c, int bitmapRef,
			int frameIndex, Rect rectToDrawInPix) {
		BitmapFrames bf = bitmapFrames.get(bitmapRef);
		if (bf != null) {
			bf.drawFrameInRectPix(c, frameIndex, rectToDrawInPix);
		}
	}

	public int getBitmapWidth(int bitmapRef) {
		return bitmaps.get(bitmapRef).getWidth();
	}

	public int getBitmapHeight(int bitmapRef) {
		return bitmaps.get(bitmapRef).getHeight();
	}

	public int getSingleBitmapFrameWidth(int bitmapFramesRef) {
		if (bitmapFrames != null && bitmapFrames.get(bitmapFramesRef) != null) {
			return bitmapFrames.get(bitmapFramesRef).getSingleFrameWidthPix();
		}
		return 0;
	}

	public SparseArray<Bitmap> getBitmaps() {
		return bitmaps;
	}

	public SparseArray<BitmapFrames> getBitmapFrames() {
		return bitmapFrames;
	}

	public void addBitmap(int bitmapRef) {
		if (appResources != null && bitmaps != null) {
			bitmaps.put(bitmapRef, getScaledBitmap(bitmapRef));
		}
	}

	public void addBitmapFrame(int bitmapFrameRef, int columns, int rows) {
		int ref = bitmapFrameRef;
		Bitmap b = getScaledBitmap(ref);
		bitmaps.put(ref, b);
		bitmapFrames.put(ref, new BitmapFrames(ref, b.getWidth(),
				b.getHeight(), columns, rows));
	}

	public void deleteBitmap(int bitmapRef) {
		Bitmap bmToRelease = bitmaps.get(bitmapRef);
		if (bmToRelease != null) {
			bmToRelease.recycle();
		}
		bitmaps.delete(bitmapRef);
	}

	public void deleteBitmapFrame(int bitmapFrameRef) {

		Bitmap bmToRelease = bitmaps.get(bitmapFrameRef);
		if (bmToRelease != null) {
			bmToRelease.recycle();
		}

		bitmapFrames.delete(bitmapFrameRef);
		BitmapFrames bmfToRelease = bitmapFrames.get(bitmapFrameRef);
		if (bmfToRelease != null) {
			bmfToRelease.release();
		}
		bitmapFrames.delete(bitmapFrameRef);
	}

	public Bitmap getBitmap(int bitmapRef) {
		return bitmaps.get(bitmapRef, null);
	}

	public BitmapFrames getBitmapFrame(int bitmapRef) {
		return bitmapFrames.get(bitmapRef, null);
	}

}
