/*
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */
package com.boolbalabs.lib.managers;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.SparseArray;

import java.io.IOException;
import java.util.Timer;

import com.boolbalabs.lib.utils.Constants;

/**
 * @author Kiryl, Igor
 *         <p/>
 *         The singleton class for playing sounds.
 */
public class SoundManager {

    // The SoundPool class manages and plays audio resources for applications
    private SoundPool soundPool;
    private SparseArray<Integer> soundPoolHandles = new SparseArray<Integer>();
    private SparseArray<Integer> playingSoundHandles = new SparseArray<Integer>();
    private final Object playingSoundHandlesLock = new Object();
    // maps sound resource id to the sound length in ms
    private SparseArray<Integer> soundLengths = new SparseArray<Integer>();
    private float currentVolume = 1;
    Timer soundTimer;
    private static SoundManager soundManager;
    private static boolean isInitialised = false;
    private static final Object isInitialisedLock = new Object();
    private MediaPlayer mediaPlayer = null;
    private Handler activityHandler;
    private Context context;
    
    // AudioManager instance for adjusting volume
    private static AudioManager audioManager; 

    public static SoundManager getInstance() {
        return soundManager;
    }

    public static void init(Context context) {
        synchronized (isInitialisedLock) {
            if (isInitialised) {
                return;
            }
            soundManager = new SoundManager(context);
            audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            // soundManager.addLoopingSound(context, R.raw.bgmusic);
            // soundManager.addShortSound(context, R.raw.reflection, 1000);
            // soundManager.addShortSound(context, R.raw.fire_blaster, 1000);
            // soundManager.addShortSound(context, R.raw.pop_single_delayed,
            // 1000);
            // soundManager.addShortSound(context, R.raw.pop_multiple_delayed,
            // 1000);
            //
            // soundManager.addShortSound(context, R.raw.gameover, 1000);
            // soundManager.addShortSound(context, R.raw.gamestart, 1000);
            //
            // soundManager.addShortSound(context, R.raw.levelup, 1000);
            // soundManager.addShortSound(context, R.raw.hitbrick, 1000);

            isInitialised = true;
        }

    }

    public void setHandler(Handler handler) {
        if (soundManager != null) {
            soundManager.activityHandler = handler;
        }
    }

    public static void release() {
        synchronized (isInitialisedLock) {
            if (!isInitialised) {
                return;
            }
            if (soundManager != null) {
                soundManager.soundPool.release();
                soundManager.soundPool = null;
                soundManager.soundPoolHandles.clear();
                soundManager.soundPoolHandles = null;
                synchronized (soundManager.playingSoundHandlesLock) {
                    soundManager.playingSoundHandles.clear();
                    soundManager.playingSoundHandles = null;
                }
                soundManager.soundLengths.clear();
                soundManager.soundLengths = null;
                soundManager.soundTimer.cancel();
                soundManager.soundTimer = null;
                soundManager = null;
            }
            isInitialised = false;
        }

    }

    private SoundManager(Context context) {
        // 4 below is the maximum number of simultaneous streams for this
        // SoundPool object
        soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
        soundPoolHandles = new SparseArray<Integer>();
        playingSoundHandles = new SparseArray<Integer>();
        soundLengths = new SparseArray<Integer>();
        this.context = context;
        // AudioManager provides access to volume and ringer mode control.
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        // get volume of the audio stream for music playback
        // ( as opposed to phone call, system sound, etc. volume)
        // You need to divide it by the streams maximum value in order to get
        // a float value between 0.0 and 1.0 that the play function requires
        currentVolume = 0.5f;
        if (audioManager != null) {
            try {
                currentVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)
                        / (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            } catch (Exception e) {
            }

        }

        if (currentVolume > 0.99f) {
            currentVolume = 0.99f;
        }
        soundTimer = new Timer();
        // delayedLoopedPlayTaskSoundHandles = new
        // HashMap<DelayedLoopedPlayTask, Integer>();

        // media players
        mediaPlayer = new MediaPlayer();

        // register mp error listener
        MediaPlayer.OnErrorListener mOnErrorListener = new MediaPlayer.OnErrorListener() {

            public boolean onError(MediaPlayer mp, int what, int extra) {
                if (activityHandler != null) {
                    Message msg = new Message();
                    Bundle error = new Bundle();
                    error.putString("error", "what: " + what + " extra: " + extra);
                    msg.what = Constants.MESSAGE_RESTART_SOUND_MANAGER;
                    msg.setData(error);
                    activityHandler.sendMessage(msg);
                }
                return false;
            }
        };

        mediaPlayer.setOnErrorListener(mOnErrorListener);

    }

    public void addShortSound(int resourceId, int soundLength) {
        // 1 below is the priority of the sound. Currently has no effect. Use a
        // value of 1 for future compatibility.
        // "load" returns a sound ID. This value can be used to play or unload
        // the sound.
        int soundID = soundPool.load(context, resourceId, 1);
        soundPoolHandles.put(resourceId, soundID);
        soundLengths.put(resourceId, soundLength);
    }

    public void addLoopingSound(int resourceId) {
        AssetFileDescriptor afd = context.getResources().openRawResourceFd(resourceId);
        if (afd == null) {
            // bad luck => do not prepare the sound
            return;
        }
        mediaPlayer.reset();
        try {
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            afd.close();
            mediaPlayer.setLooping(true);
            mediaPlayer.prepare();

        } catch (IOException ex) {
            Log.d("SoungManager", "addLoopingSound failed:", ex);
            // do not prepare the sound
            return;
        }
    }

    public void playShortSound(int resourceId, float playSpeed) {
        synchronized (isInitialisedLock) {
            if (!isInitialised) {
                return;
            }
        }

        /*
         * public final int play (int soundID, float leftVolume, float
         * rightVolume, int priority, int loop, float rate) priority stream
         * priority (0 = lowest priority) loop loop mode (0 = no loop, -1 = loop
         * forever) rate playback rate (1.0 = normal playback, range 0.5 to 2.0)
         */

        if (soundPoolHandles != null && soundPool != null) {

            try {
                int soundId = soundPoolHandles.get(resourceId);
                int streamId = soundPool.play(soundId, currentVolume, currentVolume, 1, 0, playSpeed);
                synchronized (playingSoundHandlesLock) {
                    playingSoundHandles.put(resourceId, streamId);
                }

            } catch (Exception e) {
            }

        }

    }

    public void playShortSound(int resourceId) {
        playShortSound(resourceId, 1);
    }

    public void stopShortSound(int resourceId) {
        synchronized (isInitialisedLock) {
            if (!isInitialised) {
                return;
            }
        }

        if (soundPoolHandles != null && soundPool != null) {
            if (playingSoundHandles.indexOfKey(resourceId) >= 0) {
                soundPool.stop(playingSoundHandles.get(resourceId));
                playingSoundHandles.delete(resourceId);
            }

        }

    }

    // public boolean isShortSoundPlaying(int resourceId){
    // if (soundPoolHandles != null && soundPool != null) {
    // if (playingSoundHandles.indexOfKey(resourceId) >= 0) {
    // return true;
    // }
    // }
    // return false;
    // }
    public void playLoopingSound(int resourceId) {
        synchronized (isInitialisedLock) {
            if (!isInitialised) {
                return;
            }
        }

        if (mediaPlayer != null) {

            synchronized (mediaPlayer) {
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.reset();
                    addLoopingSound(resourceId);
                    mediaPlayer.start();
                }
            }

        }
    }

    public void stopAllPlayingSounds() {
        synchronized (isInitialisedLock) {
            if (!isInitialised) {
                return;
            }
        }
        synchronized (playingSoundHandlesLock) {
            if (playingSoundHandles != null) {
                int playingSoundHandlesCount = playingSoundHandles.size();
                for (int i = 0; i < playingSoundHandlesCount; i++) {
                    int curResourceId = playingSoundHandles.keyAt(i);
                    soundPool.stop(playingSoundHandles.get(curResourceId));
                }
                playingSoundHandles.clear();
            }
        }

        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }

    }

    public void pauseLoopingSounds() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
            }
        }
    }
    
    /**
     * 
     * @param direction  AudioManager.ADJUST_LOWER or AudioManager.ADJUST_RAISE
     */
    public void adjustVolume(int direction) {
    	if (direction != AudioManager.ADJUST_LOWER && direction != AudioManager.ADJUST_RAISE) {
    		return;
    	}
    	
		if (audioManager != null) {
			audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, direction, 
					AudioManager.FLAG_PLAY_SOUND | AudioManager.FLAG_SHOW_UI );
		}
    }
}
