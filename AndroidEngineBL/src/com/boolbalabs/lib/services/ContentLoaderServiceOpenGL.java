/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.services;

import java.util.ArrayList;
import java.util.HashMap;

import com.boolbalabs.lib.game.*;
import com.boolbalabs.lib.managers.TexturesManager;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */

public class ContentLoaderServiceOpenGL {
    // ****************** Members *****************//

    private static ContentLoaderServiceOpenGL contentLoaderService;
    private static boolean isInitialised = false;
    private static final Object isInitialisedLock = new Object();
    private ContentLoader contentLoader;
    private HashMap<Integer, GameScene> gameScenes;

    // ****************** Methods *****************//

    public static ContentLoaderServiceOpenGL getInstance() {
        return contentLoaderService;
    }

    public static void initialise() {
        synchronized (isInitialisedLock) {
            if (isInitialised) {
                return;
            }
            contentLoaderService = new ContentLoaderServiceOpenGL();
            isInitialised = true;
        }

    }

    public static void release() {
        synchronized (isInitialisedLock) {
            if (!isInitialised) {
                return;
            }
            if (contentLoaderService != null) {
                contentLoaderService = null;
            }
            isInitialised = false;
        }

    }

    private ContentLoaderServiceOpenGL() {

    }

    public void setContentLoader(ContentLoader contentLoader) {
        this.contentLoader = contentLoader;
    }

    public void setGameScenes(HashMap<Integer, GameScene> gameScenes) {
        this.gameScenes = gameScenes;
    }

    public void loadContent() {
        loadContentOfGameComponents();
        TexturesManager texturesManager = TexturesManager.getInstance();
        texturesManager.loadTextures();
        assignIndicesToViews();
    }

    public void assignIndicesToViews() {
        TexturesManager texturesManager = TexturesManager.getInstance();
        if (gameScenes != null && !gameScenes.isEmpty()) {
            for (int key : gameScenes.keySet()) {
                GameScene scene = gameScenes.get(key);
                ArrayList<ZNode> currentList = scene.getSceneDrawables();
                int size = currentList.size();
                for (int i = 0; i < size; i++) {
                    ZNode cNode = currentList.get(i);
                    cNode.setTexture(texturesManager.getTextureByResourceId(cNode.getResourceId()));
                    cNode.onAfterLoad();
                }
                scene.onAfterLoad();
            }

        }
        contentLoader.onAfterLoad();
    }

    private void loadContentOfGameComponents() {
        contentLoader.loadContent();
    }

    public void unloadTexture() {
        contentLoader.unloadContent();
    }

}
