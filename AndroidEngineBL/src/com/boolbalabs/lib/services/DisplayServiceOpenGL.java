/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.services;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.boolbalabs.lib.game.Game;
import com.boolbalabs.lib.managers.TexturesManager;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */

public class DisplayServiceOpenGL {

	// ****************** Members *****************//

	private static DisplayServiceOpenGL displayService;
	private static boolean isInitialised = false;
	private static final Object isInitialisedLock = new Object();

	// ****** openGL *******//
	public GL10 gl;

	private Game game;

	private final float lightAmbient[] = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
	private final float lightDiffuse[] = new float[] { 1f, 1f, 1f, 1.0f };
	private final float[] lightPos = new float[] { 0, 0, 3, 1 };
	private final float matAmbient[] = new float[] { 1f, 1f, 1f, 1.0f };
	private final float matDiffuse[] = new float[] { 1f, 1f, 1f, 1.0f };

	// FPS
	private FrameLayout mainFrameLayout;
	private Activity mainActivity;
	private TextView fpsTextView;
	private boolean showFPS = false;
	private long currentTime;
	private short fpsCount;
	private long previousTime;
	private Runnable taskFPS;

	// ****************** Methods *****************//

	public static DisplayServiceOpenGL getInstance() {
		return displayService;
	}

	public static void initialise() {
		synchronized (isInitialisedLock) {
			if (isInitialised) {
				return;
			}
			displayService = new DisplayServiceOpenGL();
			isInitialised = true;
		}

	}

	public static void release() {
		synchronized (isInitialisedLock) {
			if (!isInitialised) {
				return;
			}
			if (displayService != null) {
				displayService = null;
			}
			isInitialised = false;
            TexturesManager.release();
		}

	}

	private DisplayServiceOpenGL() {

	}

	/**
	 * Called when the size of the window changes.
	 */
	public void sizeChanged(int width, int height) {

		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glViewport(0, 0, width, height);
		GLU.gluPerspective(gl, 45.0f, ((float) width) / height, 1f, 100f);
		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		gl.glEnable(GL10.GL_LIGHTING);
		gl.glEnable(GL10.GL_LIGHT0);
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_AMBIENT, matAmbient, 0);
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_DIFFUSE, matDiffuse, 0);

		gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_AMBIENT, lightAmbient, 0);
		gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_DIFFUSE, lightDiffuse, 0);
		gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, lightPos, 0);

		/* enables alpha layer */
		gl.glEnable(GL10.GL_BLEND);
		gl.glDisable(GL10.GL_DEPTH_TEST);

		/* enables alpha layer */
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);

		gl.glDepthFunc(GL10.GL_LEQUAL);

		gl.glEnable(GL10.GL_TEXTURE_2D);

		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		gl.glClearDepthf(1.0f);

		gl.glEnable(GL10.GL_CULL_FACE);
		gl.glShadeModel(GL10.GL_SMOOTH);

	}

	/**
	 * Called onSurfaceCreated.
	 */
	public void initCustomOpenGLParameters(GL10 gl, GLSurfaceView parentView) {
		if (game != null) {
			game.initCustomOpenGLParameters(gl, parentView);
		}
	}

	/**
	 * Called whenever the surface is created. This happens at startup, and may
	 * be called again at runtime if the device context is lost (the screen goes
	 * to sleep, etc). This function must fill the contents of vram with texture
	 * data and (when using VBOs) hardware vertex arrays.
	 */
	public void surfaceCreated() {
		initOpenGL();
	}

	private void initOpenGL() {
		/*
		 * Some one-time OpenGL initialization can be made here probably based
		 * on features of this particular context
		 */
		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);

		gl.glClearColor(0.5f, 0.5f, 0.5f, 1);
		gl.glShadeModel(GL10.GL_FLAT);
		gl.glDisable(GL10.GL_DEPTH_TEST);
		gl.glEnable(GL10.GL_TEXTURE_2D);
		/*
		 * By default, OpenGL enables features that improve quality but reduce
		 * performance. One might want to tweak that especially on software
		 * renderer.
		 */
		gl.glDisable(GL10.GL_DITHER);
		gl.glDisable(GL10.GL_LIGHTING);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

		gl.glEnable(GL10.GL_LINE_SMOOTH);
		gl.glHint(GL10.GL_POLYGON_SMOOTH_HINT, GL10.GL_NICEST);
		gl.glEnable(GL10.GL_POINT_SMOOTH);
		gl.glHint(GL10.GL_POINT_SMOOTH_HINT, GL10.GL_NICEST);

	}

	public void setGame(Game game) {
		this.game = game;
	}

	public void drawSplashScreen() {
		if (gl == null) {
			return;
		}

		game.drawSplashScreen();
	}

	/**
	 * to display FPS
	 * 
	 * @param mainFrameLayout
	 */
	public void setParameters(FrameLayout mainFrameLayout,
			Activity mainActivity, boolean showFPS) {
		this.showFPS = showFPS;
		this.mainFrameLayout = mainFrameLayout;
		this.mainActivity = mainActivity;
		if (showFPS) {
			fpsTextView = new TextView(mainFrameLayout.getContext());
			fpsTextView.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD);
			fpsTextView.setTextSize(22);
			fpsTextView.setTextColor(Color.RED);
			fpsCount = 0;
			fpsTextView.setText("FPS: " + fpsCount);
			mainFrameLayout.addView(fpsTextView);
			showFPS = true;
			previousTime = System.currentTimeMillis();

			taskFPS = new Runnable() {
				public void run() {
					currentTime = System.currentTimeMillis();
					fpsCount++;
					if (currentTime >= previousTime + 1000) {
						fpsTextView.setText("FPS: " + fpsCount);
						fpsCount = 0;
						previousTime = currentTime;
					}					
				}
			};
		}
	}

	private void updateFPS() {
		if (!showFPS) {
			return;
		}
		mainActivity.runOnUiThread(taskFPS);
	}

	public void draw() {

		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		gl.glMatrixMode(GL10.GL_MODELVIEW);
		game.draw(gl);
        //TODO switch off
		//updateFPS();
	}



}
