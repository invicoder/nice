/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.transitions;

import android.os.SystemClock;
import com.boolbalabs.lib.game.ZDrawable;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 *         Date: 20/03/2011
 *         Time: 14:59
 */
public abstract class Transition {

    protected ZDrawable parentView;

    private boolean isStarted = false;
    private boolean isFinished = false;

    protected long durationMs;
    protected long startTime;

    public Transition(ZDrawable view) {
        parentView = view;
    }

    public void start() {
        if(isStarted){
            return;
        }
        onTransitionStart();
        isStarted = true;
        startTime = SystemClock.uptimeMillis();
    }

    public void stop() {
        isFinished = true;
        onTransitionStop();
    }

    public void reset() {
        isStarted = false;
        isFinished = false;
        onTransitionReset();
    }

    public void onTransitionStart() {

    }

    public void onTransitionStop() {

    }

    public void onTransitionReset() {

    }

    /**
     * don't override this
     */
    public void innerUpdate() {
        if (!isStarted || isFinished) {
            return;
        }
        long timeSinceStartMs = SystemClock.uptimeMillis() - startTime;
        if (timeSinceStartMs > durationMs) {
            stop();
            return;
        }
        update(timeSinceStartMs);
    }

    public abstract void update(long timeSinceStartMs);


    public boolean isFinished() {
        return isFinished;
    }

    public boolean isStarted() {
        return isStarted;
    }

    public abstract Transition createCopy(ZDrawable view);

}
