/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.transitions;

import android.graphics.Point;
import com.boolbalabs.lib.game.ZDrawable;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 *         Date: 20/03/2011
 *         Time: 15:15
 */
public class TransitionMovement extends Transition {


    protected boolean isInitialized = false;

    protected float shiftStartRip_X;
    protected float shiftEndRip_X;
    protected float shiftStartRip_Y;
    protected float shiftEndRip_Y;

    public Point originalPosition;


    //*********** movement **********//
    private float initialVelocity;
    private float acceleration;
    private float accelerationSign;
    private boolean fixedOnFinish;

    public TransitionMovement(ZDrawable view) {
        super(view);
        originalPosition = new Point();
    }

    public void initialize(float shiftStartRip_X, float shiftEndRip_X, float shiftStartRip_Y, float shiftEndRip_Y, float accelerationSign, long durationMs, boolean fixedOnFinish) {
        this.shiftStartRip_X = shiftStartRip_X;
        this.shiftEndRip_X = shiftEndRip_X;
        this.shiftStartRip_Y = shiftStartRip_Y;
        this.shiftEndRip_Y = shiftEndRip_Y;
        this.durationMs = durationMs;
        this.accelerationSign = accelerationSign;
        this.fixedOnFinish = fixedOnFinish;
        float tMax = (float) durationMs / 1000f;
        if (accelerationSign > 0) {
            initialVelocity = 0;
            acceleration = 2 / (tMax * tMax);
        } else if (accelerationSign < 0) {
            initialVelocity = 5;
            acceleration = 2 / (tMax * tMax) - initialVelocity / tMax;
        } else {
            initialVelocity = 1 / tMax;
            acceleration = 0;
        }
        isInitialized = true;
    }

    @Override
    public void onTransitionStart() {
        Point posInRip = parentView.getPositionRip();
        originalPosition.set(posInRip.x, posInRip.y);
        moveAlongPath(0);
    }

    @Override
    public void onTransitionStop() {
        if (!fixedOnFinish && originalPosition!=null) {
            parentView.setPositionInRip(originalPosition);
        }
        reset();
    }


    @Override
    public void update(long timeSinceStartMs) {
        moveAlongPath(timeSinceStartMs);
    }


    private void moveAlongPath(long timeSinceStartMs) {
        float t = (float) timeSinceStartMs / 1000f;
        float alpha = initialVelocity * t + acceleration * t * t / 2;
        if (alpha < 0) alpha = 0;
        if (alpha > 1) alpha = 1;
        int xRip = originalPosition.x + (int) (shiftStartRip_X * (1 - alpha) + shiftEndRip_X * alpha);
        int yRip = originalPosition.y + (int) (shiftStartRip_Y * (1 - alpha) + shiftEndRip_Y * alpha);
        parentView.setPositionInRip(xRip, yRip);
    }

    @Override
    public Transition createCopy(ZDrawable view) {
        if (isInitialized) {
            TransitionMovement newTransition = new TransitionMovement(view);
            newTransition.initialize(shiftStartRip_X, shiftEndRip_X,
                    shiftStartRip_Y, shiftEndRip_Y,
                    accelerationSign, durationMs, fixedOnFinish);
            return newTransition;
        }
        return null;
    }
}
