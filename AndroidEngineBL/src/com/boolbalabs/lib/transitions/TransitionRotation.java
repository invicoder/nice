/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.lib.transitions;

import android.graphics.Point;
import android.graphics.Rect;
import android.os.SystemClock;
import android.util.Log;
import com.boolbalabs.lib.game.ZDrawable;

/**
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class TransitionRotation extends Transition {

    protected boolean isInitialized = false;
    public float originalAngleDeg;
    public float currentAngleDeg;
    private float angleIncrementDeg;
    private long incrementTime;
    private long previousTime;
    private boolean fixedOnFinish;

    public TransitionRotation(ZDrawable view) {
        super(view);
    }

    public void initialize(float angleIncrementDeg, long incrementTime, long durationMs, boolean fixedOnFinish) {
        this.angleIncrementDeg = angleIncrementDeg;
        this.durationMs = durationMs;
        this.fixedOnFinish = fixedOnFinish;
        this.incrementTime = incrementTime;
        isInitialized = true;
    }

    @Override
    public void onTransitionStart() {
        originalAngleDeg = parentView.getRotationAngleDeg();
        previousTime = 0;
        update(0);
    }

    @Override
    public void onTransitionStop() {
        if (!fixedOnFinish) {
            parentView.setPivotPointToCenterAndRotateByDeg(originalAngleDeg);
        }
        reset();
    }

    @Override
    public void update(long timeSinceStartMs) {
        if (timeSinceStartMs - previousTime > incrementTime) {
            currentAngleDeg = parentView.getRotationAngleDeg();
            parentView.setPivotPointToCenterAndRotateByDeg(currentAngleDeg + angleIncrementDeg);
            previousTime = timeSinceStartMs;
        }

    }

    @Override
    public Transition createCopy(ZDrawable view) {
        if (isInitialized) {
            TransitionRotation newTransition = new TransitionRotation(view);
            newTransition.initialize(angleIncrementDeg, incrementTime,
                    durationMs, fixedOnFinish);
            return newTransition;
        }
        return null;
    }
}
