/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.lib.transitions;

import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import com.boolbalabs.lib.game.ZDrawable;

/**
 *
 * @author Igor Trubnikov <itrubnikov@gmail.com>
 */
public class TransitionZoom extends Transition {

    protected boolean isInitialized = false;
    public Rect originalRect = new Rect();
    public Rect zoomRect = new Rect();
    //*********** zooming **********//
    private boolean fixedOnFinish;
    private float originalSizeCoefficient;
    private float endSizeCoefficient;
    //*** original width/height/center ***//
    private int halfWidth;
    private int halfHeight;
    private Point originalCenter;

    public TransitionZoom(ZDrawable view) {
        super(view);
    }

    public void initialize(float originalSizeCoefficient, float endSizeCoefficient, long durationMs, boolean fixedOnFinish) {
        this.endSizeCoefficient = endSizeCoefficient;
        this.originalSizeCoefficient = originalSizeCoefficient;
        this.durationMs = durationMs;
        this.fixedOnFinish = fixedOnFinish;
        isInitialized = true;
    }

    @Override
    public void onTransitionStart() {
        Rect frame = parentView.getFrameInRip();
        originalRect.set(frame);
        originalCenter = parentView.getCenterInRip();
        halfWidth = originalRect.width() / 2;
        halfHeight = originalRect.height() / 2;
        update(0);
    }

    @Override
    public void onTransitionStop() {
        if (!fixedOnFinish) {
            parentView.setFrameInRip(originalRect);
        }
        reset();
    }

    @Override
    public void update(long timeSinceStartMs) {
        float alpha = (float) timeSinceStartMs / (float) durationMs;
        if (alpha < 0) {
            alpha = 0;
        }
        if (alpha > 1) {
            alpha = 1;
        }
        float sizeCoefficient = originalSizeCoefficient * (1 - alpha) + endSizeCoefficient * alpha;
        zoomRect.left = (int) (originalCenter.x - halfWidth * sizeCoefficient);
        zoomRect.right = (int) (originalCenter.x + halfWidth * sizeCoefficient);
        zoomRect.top = (int) (originalCenter.y - halfHeight * sizeCoefficient);
        zoomRect.bottom = (int) (originalCenter.y + halfHeight * sizeCoefficient);
        parentView.setFrameInRip(zoomRect);
    }

    @Override
    public Transition createCopy(ZDrawable view) {
        if (isInitialized) {
            TransitionZoom newTransition = new TransitionZoom(view);
            newTransition.initialize(originalSizeCoefficient, endSizeCoefficient,
                    durationMs, fixedOnFinish);
            return newTransition;
        }
        return null;
    }
}
