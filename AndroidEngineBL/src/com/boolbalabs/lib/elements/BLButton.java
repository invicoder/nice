/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */
package com.boolbalabs.lib.elements;

import android.graphics.Point;
import android.graphics.Rect;

import com.boolbalabs.lib.game.ZDrawable;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.graphics.NumberView2D;
import com.boolbalabs.lib.managers.TexturesManager;

import javax.microedition.khronos.opengles.GL10;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class BLButton extends ZNode {

    private Rect rectOnScreenRip;
    private Rect rectOnTexture;
    private Rect rectOnTexturePressed;
    private Rect rectOnTextureDisabled;

    private Rect rectOnScreenOverlayRip;
    private Rect rectOnTextureOverlay;

    public boolean overlayDrawingMode = false;
    private ZNode overlayView;


    ///*********** NUMBERS ON BUTTON *************///
    private int digitsResourceId;

    private boolean numberDrawingMode = false;
    private NumberView2D numberView;
    private NumberView2D numberDisabledView;
    private NumberView2D currentNumberView;
    private Point fixedPointRip;
    private Point fixedPointOffset;


    private boolean enabled = true;
    
    
    // artificial workaround
    private boolean modeDisabledTextureAsMainTexture = false;

    public BLButton(int resourceId, int overlayResourceId, int digitsResourceId) {
        super(resourceId, ZDrawable.MODE_OPENGL_OES_EXT);
        overlayView = new ZNode(overlayResourceId, ZDrawable.MODE_OPENGL_OES_EXT);
        userInteractionEnabled = true;
        this.digitsResourceId = digitsResourceId;
    }

    public BLButton(int resourceId) {
        this(resourceId, resourceId, resourceId);
    }

    public void setRects(Rect rectOnScreen, Rect rectOnTexture, Rect rectOnTexture_pressed, Rect rectOnTexture_disabled) {
        this.rectOnScreenRip = rectOnScreen;
        this.rectOnTexture = rectOnTexture;
        this.rectOnTexturePressed = rectOnTexture_pressed;
        this.rectOnTextureDisabled = rectOnTexture_disabled;
    }

    public void setRectsOverlay(Rect rectOnScreenOverlayRip, Rect rectOnTextureOverlay) {
        this.rectOnScreenOverlayRip = rectOnScreenOverlayRip;
        this.rectOnTextureOverlay = rectOnTextureOverlay;
        overlayView.visible = false;
        if (overlayDrawingMode && rectOnTextureOverlay != null && rectOnScreenOverlayRip != null) {
            overlayView.initWithFrame(rectOnScreenOverlayRip, rectOnTextureOverlay);
        }
    }

    @Override
    public void initialize() {
        initWithFrame(rectOnScreenRip, rectOnTexture);

    }
    
    public void setDisabledTextureAsMainTexture(){
    	setRectOnTexture(rectOnTextureDisabled);
    	modeDisabledTextureAsMainTexture = true;
    	currentNumberView = numberDisabledView;
    }

    public void resumeNormalTextures(){
    	setRectOnTexture(rectOnTexture);
    	modeDisabledTextureAsMainTexture = false;
    	currentNumberView = numberView;
    }
    

    @Override
    public void onAfterLoad() {
        super.onAfterLoad();
        setEnabled(enabled);
        if (overlayDrawingMode) {
            overlayView.setTexture(TexturesManager.getInstance().getTextureByResourceId(overlayView.getResourceId()));
        }
        if (numberDrawingMode) {
            numberView.setTexture(TexturesManager.getInstance().getTextureByResourceId(numberView.getResourceId()));
            numberDisabledView.setTexture(TexturesManager.getInstance().getTextureByResourceId(numberDisabledView.getResourceId()));
        }
    }

    @Override
    public void touchDownAction(Point touchDownPoint) {
        setRectOnTexture(rectOnTexturePressed);
        setOverlayVisible(true);
    }

    @Override
    public void touchUpAction(Point touchDownPoint, Point touchUpPoint) {
    	if(modeDisabledTextureAsMainTexture){
    		setRectOnTexture(rectOnTextureDisabled);
    	} else {
    		 setRectOnTexture(rectOnTexture);
    	}
    }

    public void setOverlayVisible(boolean visible) {
        if (overlayDrawingMode) {
            overlayView.visible = visible;
        }
    }


    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        userInteractionEnabled = enabled;
        if (enabled && rectOnTexture != null) {
            setRectOnTexture(rectOnTexture);
            currentNumberView = numberView;
        } else if (!enabled && rectOnTextureDisabled != null) {
            setRectOnTexture(rectOnTextureDisabled);
            currentNumberView = numberDisabledView;
        }
    }

    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void draw(GL10 gl) {
        super.draw(gl);
        if (overlayDrawingMode && overlayView.visible) {
            overlayView.draw(gl);
        }
        if (numberDrawingMode && currentNumberView.visible) {
            currentNumberView.draw(gl);
        }
    }


    ///***********NUMBER VIEW************///

    //TODO add various alignments (like GRAVITY in Android)

    /**
     * assume that rectOnScreenRip and  rectOnTexture are set
     */
    public void initializeNumberSubview(String digitsFramesNames[], String disabledDigitsFramesNames[], int sizeRip, Point numberPosition, int align) {
        numberView = new NumberView2D(digitsResourceId);
        numberDisabledView = new NumberView2D(digitsResourceId);
        fixedPointRip = numberPosition;
        if (fixedPointRip == null) {
            fixedPointRip = new Point(rectOnScreenRip.right - 5, rectOnScreenRip.bottom - sizeRip - 5);
        }
        fixedPointOffset = new Point(fixedPointRip.x - rectOnScreenRip.left, fixedPointRip.y - rectOnScreenRip.top);
        numberView.initNumberView(fixedPointRip, digitsFramesNames, align, sizeRip);
        numberView.initialize();
        numberDisabledView.initNumberView(fixedPointRip, disabledDigitsFramesNames, align, sizeRip);
        numberDisabledView.initialize();
        currentNumberView = numberView;
        numberDrawingMode = true;
    }

    /**
     * @param numberOnButton - to hide set the negative value
     */
    public void setNumberOnButton(int numberOnButton) {
        boolean visible = numberOnButton >= 0;
        if (numberView == null || numberDisabledView == null) {
            return;
        }
        numberView.visible = visible;
        numberDisabledView.visible = visible;
        if (visible) {
            numberView.setNumberToDraw(numberOnButton);
            numberDisabledView.setNumberToDraw(numberOnButton);
        }
    }

    @Override
    public void setPositionInRip(Point posInRip) {
        super.setPositionInRip(posInRip);
        if (numberDrawingMode) {
            fixedPointRip.set(posInRip.x + fixedPointOffset.x, posInRip.y + fixedPointOffset.y);
            numberView.setFixedPointRip(fixedPointRip);
            numberDisabledView.setFixedPointRip(fixedPointRip);

        }
    }

    @Override
    public void setPositionInRip(int x, int y) {
        super.setPositionInRip(x, y);
        if (numberDrawingMode) {
            fixedPointRip.set(x + fixedPointOffset.x, y + fixedPointOffset.y);
            numberDisabledView.setFixedPointRip(fixedPointRip);
            numberView.setFixedPointRip(fixedPointRip);
        }
    }

    @Override
    public void setZGL(float zGL) {
        super.setZGL(zGL);
        if (numberDrawingMode) {
            numberView.setZGL(zGL);
            numberDisabledView.setZGL(zGL);
        }
    }
}
