/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.elements;

import android.graphics.Point;
import android.graphics.Rect;
import com.boolbalabs.lib.game.ZDrawable;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.managers.TexturesManager;

import javax.microedition.khronos.opengles.GL10;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 *         Date: 20/03/2011
 *         Time: 17:49
 */
public class BLCheckButton extends ZNode {
    private Rect rectOnScreenRip;
    private Rect rectOnTexture;
    private Rect rectOnTexturePressed;
    private boolean selected;

    private Rect rectOnScreenOverlayRip;
    private Rect rectOnTextureOverlay;

    public boolean overlayDrawingMode = false;
    private ZNode overlayView;


    public BLCheckButton(int resourceId, int overlayResourceId) {
        super(resourceId, ZDrawable.MODE_OPENGL_OES_EXT);
        overlayView = new ZNode(overlayResourceId, ZDrawable.MODE_OPENGL_OES_EXT);
        userInteractionEnabled = true;
        selected = false;
    }

    public BLCheckButton(int resourceId) {
        this(resourceId, resourceId);
    }

    public void setRects(Rect rectOnScreen, Rect rectOnTexture, Rect rectOnTexture_pressed) {
        this.rectOnScreenRip = rectOnScreen;
        this.rectOnTexture = rectOnTexture;
        this.rectOnTexturePressed = rectOnTexture_pressed;
    }


    @Override
    public void initialize() {
        initWithFrame(rectOnScreenRip, rectOnTexture);
        overlayView.visible = false;
        if (overlayDrawingMode) {
            overlayView.initWithFrame(rectOnScreenOverlayRip, rectOnTextureOverlay);
        }
    }

    @Override
    public void onAfterLoad() {
        super.onAfterLoad();
        if (overlayDrawingMode) {
            overlayView.setTexture(TexturesManager.getInstance().getTextureByResourceId(overlayView.getResourceId()));
        }
    }


    @Override
    public void touchDownAction(Point touchDownPoint) {
        setSelected(!selected);
    }


    private void afterSelectionChange(boolean selected) {
        if (selected) {
            setRectOnTexture(rectOnTexturePressed);
        } else {
            setRectOnTexture(rectOnTexture);
        }
        onSelectionChange(selected);
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        if (overlayDrawingMode) {
            overlayView.visible = selected;
        }
        afterSelectionChange(selected);
    }

    public void onSelectionChange(boolean selected) {

    }

    public void setOverlayVisible(boolean visible) {
        overlayView.visible = visible;
    }


    @Override
    public void draw(GL10 gl) {
        super.draw(gl);
        if (overlayDrawingMode && overlayView.visible) {
            overlayView.draw(gl);
        }
    }

    public boolean isSelected() {
        return selected;
    }
}
