/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.geometry;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 *         Date: 11/02/2011
 *         Time: 15:34
 *         <p/>
 *         represents [min(rangeStart, rangeEnd), max(rangeStart, rangeEnd) ) range
 */
public class Range {

    private float rangeStart;
    private float rangeEnd;

    private float leftBoundary;
    private float rightBoundary;
    private float rangeLength;

    public Range() {
        rangeStart = 0;
        rangeEnd = 0;
        adjustBoundaries();
    }

    public Range(int start, int end) {
        rangeStart = (float) start;
        rangeEnd = (float) end;
        adjustBoundaries();
    }

    public Range(float start, float end) {
        rangeStart = start;
        rangeEnd = end;
        adjustBoundaries();
    }

    public void set(int start, int end) {
        rangeStart = (float) start;
        rangeEnd = (float) end;
        adjustBoundaries();
    }

    public void set(float start, float end) {
        rangeStart = start;
        rangeEnd = end;
        adjustBoundaries();
    }


    private void adjustBoundaries() {
        leftBoundary = Math.min(rangeStart, rangeEnd);
        rightBoundary = Math.max(rangeStart, rangeEnd);
        rangeLength = rightBoundary - leftBoundary;
    }


    public boolean isInRange(float value) {
        float a = rangeStart;
        float b = rangeEnd;
        if (rangeEnd < rangeStart) {
            a = rangeEnd;
            b = rangeStart;
        }
        return a <= value && value < b;
    }

    public boolean isInRange(int value) {
        return isInRange((float) value);
    }

    public float getRangeIntermediateValue(float alpha) {
        float a = alpha;
        if (a < 0) {
            a = 0;
        }
        if (a > 1) {
            a = 1;
        }
        return leftBoundary + a * rangeLength;
    }

    public float getLength() {
        return rangeLength;
    }

    public float getLeftBoundary() {
        return leftBoundary;
    }

    public float getRightBoundary() {
        return rightBoundary;
    }

}
