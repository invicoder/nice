/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.lib.geometry;

import java.io.Serializable;

/**
 * the physical object coordinates in 3D space
 */
public class Point3D implements Serializable {

    private static final long serialVersionUID = 172642776147L;

    public float x = 0;
    public float y = 0;
    public float z = 0;

    public Point3D() {
    }

    public Point3D(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point3D(Point3D arg) {
        this.x = arg.x;
        this.y = arg.y;
        this.z = arg.z;
    }

    public void set(Point3D arg) {
        this.x = arg.x;
        this.y = arg.y;
        this.z = arg.z;
    }

    public void set(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }


    public double norm() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    public void normalise() {
        double norm = norm();
        if (norm != 0) {
            x = (float) (x / norm);
            y = (float) (y / norm);
            z = (float) (z / norm);
        }
    }

    public float dot(Point3D p) {
        return x * p.x + y * p.y + z * p.z;
    }

    public float distanceTo(Point3D p) {
        return (float) Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y) + (z - p.z) * (z - p.z));
    }


    /**
     * sends the current point to its antipodal point:
     * x -> -x, y-> -y, z->-z
     */
    public void mapToAntipodal(){
        x*=-1;
        y*=-1;
        z*=-1;
    }

    /**
     * Helper method for debugging.
     */
    @Override
    public String toString() {
        return "Coordinates: (" + x + "/" + y + "/" + z + ")";
    }

}
