/*
 * Confidential property of Boolba Labs
 * http://boolbalabs.com
 */
package com.boolbalabs.lib.geometry;

import android.graphics.Point;
import android.graphics.PointF;
import android.util.Log;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class Line {


    /**
     * slop of the line
     */
    private float a;
    /**
     * y-intercept of the line
     */
    private float b;
    /**
     * when line is parallel to the Y axe
     */
    private float defaultValue;
    //parametric representation
    private float x0;
    private float y0;
    private float u;
    private float v;

    private PointF valuePoint;


    public Line(float a, float b) {
        commonInitialize();
        set(a, b);
    }

    public Line(float startX, float startY, float endX, float endY) {
        commonInitialize();
        set(startX, startY, endX, endY);
    }

    public Line(PointF start, PointF end) {
        commonInitialize();
        set(start.x, start.y, end.x, end.y);
    }

    private void commonInitialize() {
        valuePoint = new PointF(0, 0);
    }


    public float getValue(float xValue) {
        valuePoint.x = xValue;
        valuePoint.y = a * xValue + b;
        return valuePoint.y;
    }

    public float getInverseValue(float yValue) {
        valuePoint.y = yValue;
        if (a != 0) {
            valuePoint.x = (yValue - b) / a;
        } else {
            valuePoint.x = defaultValue;
        }
        return valuePoint.x;
    }

    public void set(float startX, float startY, float endX, float endY) {
        if (startX == endX) {
            a = 0;
            defaultValue = startX;
            b = defaultValue;
        } else {
            a = (endY - startY) / (endX - startX);
            b = startY - startX * a;
        }
        adjustParametricRepresentationNormed(startX, startY, endX, endY);
    }

    public void set(float a, float b) {
        this.a = a;
        this.b = b;
        adjustParametricRepresentation();
    }

    public PointF getParametricValue(float t) {
        valuePoint.x = x0 + u * t;
        valuePoint.y = y0 + v * t;
        return valuePoint;
    }

    private void adjustParametricRepresentation() {
        x0 = 0;
        u = 1;
        y0 = a;
        v = b;
    }

    private void adjustParametricRepresentationNormed(float startX, float startY, float endX, float endY) {
        x0 = startX;
        y0 = startY;
        u = endX - startX;
        v = endY - startY;
    }

    @Override
    public String toString() {
        return "Line parameters:" + Float.toString(a) + "x+"
                + Float.toString(b);
    }


}
