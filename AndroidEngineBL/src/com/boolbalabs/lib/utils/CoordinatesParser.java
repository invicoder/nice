package com.boolbalabs.lib.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Rect;

/**
 * Class for converting .plist XML files to {@link android.graphics.Rect Rect}.</br>
 * CoordinatesParser must be initialized
 * </br>
 * Detects .png and .jpg files
 *
 * @author Anton Rutkevich <anton.rutkevich@boolbalabs.com>
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class CoordinatesParser {
    private static CoordinatesParser coordinatesParser;
    private Resources gameResources;
    private static boolean isInitialised = false;
    private static final Object isInitialisedLock = new Object();

    public static CoordinatesParser getInstance() {
        if (coordinatesParser == null) {
            coordinatesParser = new CoordinatesParser();
        }
        return coordinatesParser;
    }


    public static void initialise(Resources gameResources) {
        synchronized (isInitialisedLock) {
            if (isInitialised) {
                return;
            }
            coordinatesParser = new CoordinatesParser();
            coordinatesParser.gameResources = gameResources;
            isInitialised = true;
        }
    }

    private CoordinatesParser() {

    }

    /**
     * Adds all the frames from the file to allRectangles
     *
     * @param fileName name of the source plist file, <b>without</b> ".plist"
     */
    public HashMap<String, Rect> parseEntireFile(String fileName) {
        HashMap<String, Rect> allRectangles = new HashMap();

        AssetManager assetManager = gameResources.getAssets();
        try {
            InputStream inStream = assetManager.open(fileName + ".plist");
            InputStreamReader isReader = new InputStreamReader(inStream);
            BufferedReader bufReader = new BufferedReader(isReader);

            String frameName = new String();
            String[] coordinates = new String[4];

            while ((frameName = bufReader.readLine()) != null) {
                coordinates = bufReader.readLine().split(" ");
                allRectangles.put(new String(frameName),
                        new Rect(Integer.parseInt(coordinates[0]),
                                Integer.parseInt(coordinates[1]),
                                Integer.parseInt(coordinates[2]) + Integer.parseInt(coordinates[0]),
                                Integer.parseInt(coordinates[3]) + Integer.parseInt(coordinates[1]))
                );
            }

            inStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allRectangles;
    }



    /**
     * @param fileName   name of the source plist file
     * @param frameNames requested frames
     * @return List of rectangles with coordinates of <b>frameNames</b>
     *         on the texture <b>fileName</b>
     */
    public ArrayList<Rect> getRectsByFrameNames(String fileName, String[] frameNames) {
        String sourceText = getSourceText(fileName);

        ArrayList<Rect> allRects = getRectangles(frameNames, sourceText);
        sourceText = null;
        return allRects;
    }


    /**
     * @param frameNames the same as <b>frameNames</b>
     *                   in {@link #getRectsByFrameNames(String, String[]) }
     * @param sourceText File returned
     *                   by {@link #getSourceText(String)}
     * @return List of rectangles with coordinates
     *         extracted from the <b>sourceText</b>
     */
    @Deprecated
    private ArrayList<Rect> getRectangles(String[] frameNames, String sourceText) {
        // this code is not extracted as separete method
        ArrayList<Rect> rectangles = new ArrayList<Rect>();
        for (int i = 0; i < frameNames.length; ++i) {

            int framenemeIndex = sourceText.indexOf(frameNames[i]);
            int openingBraceIndex = sourceText.indexOf("{{", framenemeIndex);
            int closingBraceIndex = sourceText.indexOf("}}", framenemeIndex);

            String coordinates
                    = sourceText.substring(openingBraceIndex + 2, closingBraceIndex);
            String[] coords = coordinates.split("\\D+");

            rectangles.add(
                    new Rect(Integer.parseInt(coords[0]),
                            Integer.parseInt(coords[1]),
                            Integer.parseInt(coords[2]) + Integer.parseInt(coords[0]),
                            Integer.parseInt(coords[3]) + Integer.parseInt(coords[1]))
            );
        }
        return rectangles;
    }

    /**
     * @param fileName name of the source plist file, <b>without</b> ".plist"
     * @return text containing <b>strings</b> of the following type:</br></br>
     *         &ltkey&gtframename.png&lt/key&gt  <br>
     *         &ltstring&gt{{2, 684}, {79, 79}}&lt/string&gt
     */
    @Deprecated
    private String getSourceText(String fileName) {
        AssetManager assetManager = gameResources.getAssets();
        try {
            InputStream inStream = assetManager.open(fileName + ".plist");
            InputStreamReader isReader = new InputStreamReader(inStream);
            BufferedReader bufReader = new BufferedReader(isReader);

            // We extract only strings with frame names
            // and their rectangle coordinates
            String sourceText = new String();

            String temp = null;
            boolean nextLineContainsCoordinates = false;

            while ((temp = bufReader.readLine()) != null) {
                if (nextLineContainsCoordinates) {
                    sourceText += temp + '\n';
                    nextLineContainsCoordinates = false;
                } else if (temp.contains(".png")
                        || temp.contains(".jpg")
                        || temp.contains(".jpeg")) {
                    sourceText += temp + '\n';
                } else if (temp.contains("textureRect")) {
                    nextLineContainsCoordinates = true;
                }
            }

            inStream.close();
            return sourceText;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
