/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Rect;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import com.boolbalabs.lib.geometry.Range;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com> Date: 09/02/2011 Time: 13:52
 *         <p/>
 *         To understand the basics of Android multiple screen support read
 *         here: {@link http
 *         ://developer.android.com/guide/practices/screens_support.html}
 *         <p/>
 *         The problem is that there exist both phones and tablets with the SAME
 *         screen resolution (i.e. 480x800), but with DIFFERENT screen density.
 *         In this case a phone with 480x800 res (device A) and a tablet with
 *         480x800 res (device B) will be using different resources (textures,
 *         images, coordinate files, etc.), while we want them to use the same
 *         resources.
 *         <p/>
 *         Using this basic idea as the main motivation we introduce various
 *         resolution groups: low, medium, high, extra high.
 *         <p/>
 *         Together with this we introduce the concept of RIP: resolution
 *         independent pixel.
 *         <p/>
 *         RIP is a virtual pixel unit that applications can use in defining
 *         their UI, to express layout dimensions or position in a
 *         resolution-independent way. Whatever the resolution of a device is,
 *         we consider the smaller side of the screen to have 320 RIP length and
 *         the larger side to have 320/SCREEN_ASPECT_RATIO RIP in length. At run
 *         time, the platform transparently handles any scaling of the RIP units
 *         needed, based on the actual resolution of the screen in use.
 *         <p/>
 *         For example, on 480x800 screen 1 RIP would equal 1.5 physical pixels.
 *         Such screen (480x800) is expressed in RIPs as 320x533 (smaller side
 *         is 320 RIP, SCREEN_ASPECT_RATIO = 400/800 = 0.6, therefore the large
 *         side is 320/0.6 = 533 RIP). Using RIP units to define your
 *         application's UI is highly recommended, as a way of ensuring proper
 *         display of your UI on different screens.
 */
public class ScreenMetrics {

	/**
	 * Standard quantized resolution for low-resolution screens.
	 */
	public static int RESOLUTION_LOW = 0;
	/**
	 * Standard quantized resolution for medium-resolution screens.
	 */
	public static int RESOLUTION_MEDIUM = 1;
	/**
	 * Standard quantized resolution for high-resolution screens.
	 */
	public static int RESOLUTION_HIGH = 2;
	/**
	 * Standard quantized resolution for extra high-resolution screens.
	 */
	public static int RESOLUTION_XHIGH = 3;
	/**
	 * Default quantized resolution.
	 */
	public static int RESOLUTION_DEFAULT = RESOLUTION_HIGH;

	/**
	 * Pixels range for low resolution devices (smaller screen side length)
	 */
	public static final Range RESOLUTION_RANGE_LOW = new Range(0, 250);

	/**
	 * Pixels range for medium resolution devices (smaller screen side length)
	 */
	public static final Range RESOLUTION_RANGE_MEDIUM = new Range(250, 330);

	/**
	 * Pixels range for high resolution devices (smaller screen side length)
	 */
	public static final Range RESOLUTION_RANGE_HIGH = new Range(330, 500);

	/**
	 * Pixels range for extra high resolution devices (smaller screen side
	 * length)
	 */
	public static final Range RESOLUTION_RANGE_XHIGH = new Range(500, 5000);

	/**
	 * Default quantized resolution.
	 */
	public static int resolution = RESOLUTION_DEFAULT;

	public static String resolution_postfix;

	public static int screenSmallSideRip = 320;
	public static int screenLargeSideRip;
	public static int screenSmallSidePix;
	public static int screenLargeSidePix;

	public static int screenWidthRip;
	public static int screenHeightRip;
	public static int screenWidthPix;
	public static int screenHeightPix;

	/**
	 * Equals to (smaller_side_pix)/(larger_side_pix)
	 */
	public static float aspectRatio;

	/**
	 * Equals to (screen_width_pix)/(screen_height_pix)
	 */
	public static float aspectRatioOriented;

	private static float ripToPixCoefficient;
	private static float pixToRipCoefficient;

	/**
	 * better be positive, or else you will see the images upside down or like
	 * in a mirror. Used in OpenGL.
	 */
	public static float defaultViewingDistance = 3.0f;

	/**
	 * screen orientation
	 */
	public static int SCREEN_ORIENTATION;

	/**
	 * game orientation
	 */
	public static int GAME_ORIENTATION;

	/**
	 * current viewport
	 */
	public static int[] viewport;

	@Deprecated
	public static int screenDensityX;

	public static void initScreenMetrics(Context context, int orientation) {
		DisplayMetrics metrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(metrics);
		
		screenDensityX = (int) metrics.density * 160;
		
		Configuration config = context.getResources().getConfiguration();
		
//		SCREEN_ORIENTATION = config.orientation;
//		if (SCREEN_ORIENTATION == Configuration.ORIENTATION_PORTRAIT) {
//			screenSmallSidePix = metrics.widthPixels;
//			screenLargeSidePix = metrics.heightPixels;
//		} else {
//			screenSmallSidePix = metrics.heightPixels;
//			screenLargeSidePix = metrics.widthPixels;
//		}
		
		screenSmallSidePix = Math.min(metrics.widthPixels, metrics.heightPixels);
		screenLargeSidePix = Math.max(metrics.widthPixels, metrics.heightPixels);

		aspectRatio = ((float) screenSmallSidePix) / screenLargeSidePix;
		screenLargeSideRip = (int) (screenSmallSideRip / aspectRatio);

		GAME_ORIENTATION = orientation;
		if (GAME_ORIENTATION == Configuration.ORIENTATION_PORTRAIT) {
			screenWidthRip = screenSmallSideRip;
			screenHeightRip = screenLargeSideRip;
			screenWidthPix = screenSmallSidePix;
			screenHeightPix = screenLargeSidePix;
		} else {
			screenWidthRip = screenLargeSideRip;
			screenHeightRip = screenSmallSideRip;
			screenWidthPix = screenLargeSidePix;
			screenHeightPix = screenSmallSidePix;
		}

		aspectRatioOriented = ((float) screenWidthRip) / screenHeightRip;

		pixToRipCoefficient = ((float) screenSmallSideRip) / screenSmallSidePix;
		ripToPixCoefficient = 1 / pixToRipCoefficient;

		createResolutionPostfix();

		// TODO think later about orientation and viewport
		viewport = new int[] { 0, 0, screenSmallSidePix, screenLargeSidePix };
	}

	private static void createResolutionPostfix() {
		if (RESOLUTION_RANGE_LOW.isInRange(screenSmallSidePix)) {
			resolution = RESOLUTION_LOW;
			resolution_postfix = "lres";
		} else if (RESOLUTION_RANGE_MEDIUM.isInRange(screenSmallSidePix)) {
			resolution = RESOLUTION_MEDIUM;
			resolution_postfix = "mres";
		} else if (RESOLUTION_RANGE_HIGH.isInRange(screenSmallSidePix)) {
			resolution = RESOLUTION_HIGH;
			resolution_postfix = "hres";
		} else if (RESOLUTION_RANGE_XHIGH.isInRange(screenSmallSidePix)) {
			resolution = RESOLUTION_XHIGH;
			resolution_postfix = "xhres";
		}
	}

	public static int convertRipToPixel(float ripValue) {
		return (int) (ripValue * ripToPixCoefficient);
	}

	public static int convertPixelToRip(int pixelValue) {
		return (int) (pixelValue * pixToRipCoefficient);
	}

	public static void convertRectRipToPix(Rect fromRip, Rect toPix) {
		if (fromRip != null && toPix != null) {
			toPix.set(convertRipToPixel(fromRip.left), convertRipToPixel(fromRip.top), convertRipToPixel(fromRip.right),
					convertRipToPixel(fromRip.bottom));
		}
	}

	public static void convertRectPixToRip(Rect fromPix, Rect toRip) {
		if (fromPix != null && toRip != null) {
			toRip.set(convertPixelToRip(fromPix.left), convertPixelToRip(fromPix.top), convertPixelToRip(fromPix.right),
					convertPixelToRip(fromPix.bottom));
		}
	}

	public static void convertPointRipToPix(Point fromRip, Point toPix) {
		if (toPix != null && fromRip != null) {
			toPix.set(convertRipToPixel(fromRip.x), convertRipToPixel(fromRip.y));
		}
	}

	public static void convertPointPixToRip(Point fromPix, Point toRip) {
		if (toRip != null && fromPix != null) {
			toRip.set(convertPixelToRip(fromPix.x), convertPixelToRip(fromPix.y));
		}
	}

	public static int getHeightProportionalToWidth(int width) {
		int result = (int) (width / aspectRatioOriented);
		return result;
	}

	public static int getWidthProportionalToHeight(int height) {
		int result = (int) (height * aspectRatioOriented);
		return result;
	}

	public static String description() {
		return "Screen pixels: " + screenSmallSidePix + "x" + screenLargeSidePix + "\n" + "Screen RIPs: " + screenSmallSideRip + "x"
				+ screenLargeSideRip + "\n" + "Aspect ratio: " + aspectRatio;
	}
}
