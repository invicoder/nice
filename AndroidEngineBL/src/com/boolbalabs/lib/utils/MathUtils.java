/*
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */
package com.boolbalabs.lib.utils;

import java.util.Random;

import android.graphics.Point;
import android.graphics.PointF;
import android.util.Log;

/**
 * Various math utils
 *
 * @author Igor Trubnikov <igor@boolbalabs.com>
 */
public class MathUtils {

    public static Random rand = new Random();

    /**
     * Let's introduce another point: thirdPoint = (start.x+1, start.y)
     * This method returns angle between the lines:
     * line 1 - goes through start and end
     * line 2 goes though start and thirdPoint
     *
     * @param start
     * @param end
     * @return
     */
    public static double flatAnglef(PointF start, PointF end) {
        int sign = 1;
        if (end.y > start.y) {
            sign = -1;
        }
        return sign * Math.acos((end.x - start.x) / vectorLength(start, end));
    }

    /**
     * Let's introduce another point: thirdPoint = (start.x+1, start.y)
     * This method returns angle between the lines:
     * line 1 - goes through start and end
     * line 2 goes though start and thirdPoint
     *
     * @param start
     * @param end
     * @return
     */
    public static double flatAnglei(Point start, Point end) {
        int sign = 1;
        if (end.y > start.y) {
            sign = -1;
        }
        return sign * Math.acos((end.x - start.x) / vectorLength(start, end));
    }


    /**
     * Returns the polar angle of a point. The origin is the screen center.
     *
     * @param pointOnScreen
     * @return
     */
    public static double centralFlatAnglePix(Point pointOnScreen) {
        int xCenter = ScreenMetrics.screenWidthPix / 2;
        int yCenter = ScreenMetrics.screenHeightPix / 2;
        int sign = 1;
        if (pointOnScreen.y > yCenter) {
            sign = -1;
        }
        double length = Math.sqrt(Math.pow((pointOnScreen.x - xCenter), 2)
                + Math.pow((pointOnScreen.y - yCenter), 2));
        return sign * Math.acos((pointOnScreen.x - xCenter) / length);
    }

    /**
     * Returns the polar angle of a point in RIP. The origin is the screen center in RIP.
     *
     * @param pointOnScreenRip
     * @return
     */
    public static double centralFlatAngleRip(Point pointOnScreenRip) {
        int xCenter = ScreenMetrics.screenWidthRip / 2;
        int yCenter = ScreenMetrics.screenHeightRip / 2;
        int sign = 1;
        if (pointOnScreenRip.y > yCenter) {
            sign = -1;
        }
        double length = Math.sqrt(Math.pow((pointOnScreenRip.x - xCenter), 2)
                + Math.pow((pointOnScreenRip.y - yCenter), 2));
        return sign * Math.acos((pointOnScreenRip.x - xCenter) / length);
    }

    public static double vectorLength(PointF point1, PointF point2) {
        return Math.sqrt(Math.pow((point1.x - point2.x), 2)
                + Math.pow((point1.y - point2.y), 2));
    }

    public static double vectorLength(Point point1, Point point2) {
        return Math.sqrt(Math.pow((point1.x - point2.x), 2)
                + Math.pow((point1.y - point2.y), 2));
    }

    public static int getMaxElement(int[] array) {
        if (array.length == 0) {
            return 0;
        }
        int result = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] >= result) {
                result = array[i];
            }
        }
        return result;
    }


    public static int getFractionalPartAsInt(float floatNumber, int digitsAfterPoint) {
        int result = 0;
        float cfPart = floatNumber;
        for (int i = digitsAfterPoint; i > 0; i--) {
            int currentDigit = getFirstFracDigit(cfPart);
            result += currentDigit * Math.pow(10, i - 1);
            cfPart *= 10;
        }
        return result;
    }

    private static int getFirstFracDigit(float floatNumber) {
        int iPart = (int) floatNumber;
        float fPart = floatNumber - iPart;
        return (int) (fPart * 10);
    }

}
