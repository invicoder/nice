/**
 * Confidential property of Boolba Labs LLC
 * http://boolbalabs.com
 */

package com.boolbalabs.lib.utils;

/**
 * @author Igor Trubnikov <igor@boolbalabs.com>
 *         Date: 11/04/2011
 *         Time: 17:26
 */
public class ZageCommonSettings {
    
	public static boolean soundEnabled = true;
    public static boolean musicEnabled = true;
    public static boolean vibroEnabled = true;
    
    public static float openGLVertexModeDefaultViewDistance = 3.0f;

}
