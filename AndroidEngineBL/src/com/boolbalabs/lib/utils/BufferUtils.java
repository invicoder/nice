package com.boolbalabs.lib.utils;

import java.nio.*;

import android.graphics.Bitmap;
import android.graphics.Rect;

public class BufferUtils {

    /**
     * Make a direct NIO FloatBuffer from an array of floats
     *
     * @param arr The array
     * @return The newly created FloatBuffer
     */
    public static FloatBuffer makeFloatBuffer(float[] arr) {
        ByteBuffer bb = ByteBuffer.allocateDirect(arr.length * 4);
        bb.order(ByteOrder.nativeOrder());
        FloatBuffer fb = bb.asFloatBuffer();
        fb.put(arr);
        fb.position(0);
        return fb;
    }

    public static ShortBuffer makeShortBuffer(short[] arr) {
        ByteBuffer bb = ByteBuffer.allocateDirect(arr.length * 4);
        bb.order(ByteOrder.nativeOrder());
        ShortBuffer ib = bb.asShortBuffer();
        ib.put(arr);
        ib.position(0);
        return ib;
    }

    public static ByteBuffer makeByteBuffer(Bitmap bmp) {
        ByteBuffer bb = ByteBuffer.allocateDirect(bmp.getHeight() * bmp.getWidth() * 4);
        bb.order(ByteOrder.BIG_ENDIAN);
        IntBuffer ib = bb.asIntBuffer();

        for (int y = 0; y < bmp.getHeight(); y++)
            for (int x = 0; x < bmp.getWidth(); x++) {
                int pix = bmp.getPixel(x, bmp.getHeight() - y - 1);
                // Convert ARGB -> RGBA
                byte alpha = (byte) ((pix >> 24) & 0xFF);
                byte red = (byte) ((pix >> 16) & 0xFF);
                byte green = (byte) ((pix >> 8) & 0xFF);
                byte blue = (byte) ((pix) & 0xFF);

                ib.put(((red & 0xFF) << 24) | ((green & 0xFF) << 16) | ((blue & 0xFF) << 8) | ((alpha & 0xFF)));
            }
        ib.position(0);
        bb.position(0);
        return bb;
    }

    public static FloatBuffer makeTextureBuffer(Rect rectOnTexture, int width, int height) {
        float textCoords[] = {(float) rectOnTexture.left / (float) width, (float) rectOnTexture.top / (float) height,//  TOP LEFT
                (float) rectOnTexture.left / (float) width, (float) rectOnTexture.bottom / (float) height,// BOTTOM LEFT
                (float) rectOnTexture.right / (float) width, (float) rectOnTexture.bottom / (float) height,// BOTTOM RIGHT
                (float) rectOnTexture.right / (float) width, (float) rectOnTexture.top / (float) height // TOP RIGHT
        };
        return makeFloatBuffer(textCoords);
    }

}
